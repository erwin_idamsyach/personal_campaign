/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : 127.0.0.1:3306
Source Database       : db_personal

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-08-28 14:10:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_assignment
-- ----------------------------
DROP TABLE IF EXISTS `t_assignment`;
CREATE TABLE `t_assignment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL,
  `asignment_start` datetime DEFAULT NULL,
  `asignment_stop` datetime DEFAULT NULL,
  `assignment` varchar(255) DEFAULT NULL,
  `assignment_description` varchar(255) DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_stop` time DEFAULT NULL,
  `shift_id` int(10) NOT NULL,
  `id_pegawai` bigint(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `provinsiId` bigint(50) DEFAULT NULL,
  `kabupatenId` bigint(50) DEFAULT NULL,
  `kecamatanId` bigint(50) DEFAULT NULL,
  `kelurahanId` bigint(50) DEFAULT NULL,
  `tpsId` bigint(50) DEFAULT NULL,
  `jenis_calon_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_assignment
-- ----------------------------
INSERT INTO `t_assignment` VALUES ('1', 'Gg. VII, Ngaglik, Kec. Batu, Kota Batu, Jawa Timur 65311, Indonesia', '-7.871427088386458', '112.52176880836487', '2017-04-30 07:00:00', '2017-05-01 07:00:00', '3', 'mohon bantuannya', null, null, '0', '7', '2017-04-30 12:01:26', '26141', '3579', '3579010', '3579010004', '1834012', '5');
INSERT INTO `t_assignment` VALUES ('2', 'Gg. VII, Ngaglik, Kec. Batu, Kota Batu, Jawa Timur 65311, Indonesia', '-7.871427088386458', '112.52176880836487', '2017-04-30 07:00:00', '2017-05-01 07:00:00', '3', 'mohon bantuannya', null, null, '0', '7', '2017-04-30 12:01:35', '26141', '3579', '3579010', '3579010004', '1834012', '5');
INSERT INTO `t_assignment` VALUES ('3', 'Gg. VII, Ngaglik, Kec. Batu, Kota Batu, Jawa Timur 65311, Indonesia', '-7.871427088386458', '112.52176880836487', '2017-04-30 07:00:00', '2017-05-01 07:00:00', '3', 'mohon bantuannya', null, null, '0', '7', '2017-04-30 12:01:29', '26141', '3579', '3579010', '3579010004', '1834012', '5');
INSERT INTO `t_assignment` VALUES ('4', 'Gg. VII, Ngaglik, Kec. Batu, Kota Batu, Jawa Timur 65311, Indonesia', '-7.871427088386458', '112.52176880836487', '2017-04-30 07:00:00', '2017-05-01 07:00:00', '3', 'mohon bantuannya', null, null, '0', '7', '2017-04-30 12:01:30', '26141', '3579', '3579010', '3579010004', '1834012', '5');
INSERT INTO `t_assignment` VALUES ('5', 'Gg. VII, Ngaglik, Kec. Batu, Kota Batu, Jawa Timur 65311, Indonesia', '-7.871427088386458', '112.52176880836487', '2017-04-30 07:00:00', '2017-05-01 07:00:00', '3', 'mohon bantuannya', null, null, '0', '7', '2017-04-30 12:01:32', '26141', '3579', '3579010', '3579010004', '1834012', '5');
INSERT INTO `t_assignment` VALUES ('6', 'Gg. VII, Ngaglik, Kec. Batu, Kota Batu, Jawa Timur 65311, Indonesia', '-7.871427088386458', '112.52176880836487', '2017-04-30 07:00:00', '2017-05-01 07:00:00', '3', 'mohon bantuannya', null, null, '0', '7', '2017-04-30 12:01:31', '26141', '3579', '3579010', '3579010004', '1834012', '5');

-- ----------------------------
-- Table structure for t_daerah_pelaksana
-- ----------------------------
DROP TABLE IF EXISTS `t_daerah_pelaksana`;
CREATE TABLE `t_daerah_pelaksana` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provinsiId` int(11) DEFAULT NULL,
  `kabupatenId` bigint(20) DEFAULT NULL,
  `jenis_pemilihan_id` int(5) DEFAULT NULL,
  `jenis_calon_id` int(11) DEFAULT NULL,
  `jumlah_dpt` int(20) DEFAULT NULL,
  `tahun_pelaksanaan` int(5) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_daerah_pelaksana
-- ----------------------------
INSERT INTO `t_daerah_pelaksana` VALUES ('1', '53241', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('2', '26141', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('3', '32676', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('4', '42385', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('5', '58285', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('6', '64111', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('7', '22328', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('8', '76096', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('9', '77085', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('10', '54020', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('11', '55065', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('12', '78203', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('13', '13086', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('14', '69268', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('15', '72551', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('16', '17404', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('17', '6728', null, '3', '5', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('172', '51578', '53106', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('173', '51578', '52936', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('174', '20802', '22252', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('175', '74716', '75365', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('176', '15885', '17333', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('177', '26141', '32403', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('178', '26141', '32375', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('179', '26141', '32152', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('180', '26141', '32193', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('181', '26141', '32646', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('182', '26141', '32077', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('183', '32676', '41831', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('184', '42385', '51170', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('185', '42385', '51306', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('186', '42385', '51233', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('187', '42385', '51095', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('188', '42385', '51327', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('189', '58285', '60183', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('190', '60371', '61929', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('191', '928068', '65631', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('192', '24993', '25363', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('193', '25405', '25406', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('194', '76096', '77051', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('195', '1', '6648', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('196', '54020', '54982', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('197', '69268', '72493', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('198', '69268', '72467', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('199', '69268', '72309', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('200', '72551', '74665', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('201', '65702', '67355', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('202', '12920', '13844', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('203', '12920', '13886', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('204', '12920', '14010', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('205', '12920', '13712', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('206', '17404', '20677', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('207', '17404', '20636', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('208', '17404', '20758', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('209', '17404', '20512', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('210', '6728', '12122', '3', '7', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('211', '1', '2', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('212', '1', '6166', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('213', '6728', '12208', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('214', '6728', '11635', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('215', '6728', '12606', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('216', '6728', '8094', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('217', '6728', '8688', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('218', '6728', '7438', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('219', '6728', '9835', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('220', '14086', '14741', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('221', '15885', '16183', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('222', '15885', '15961', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('223', '17404', '17895', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('224', '17404', '20345', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('225', '17404', '19172', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('226', '17404', '18244', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('227', '17404', '17570', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('228', '22328', '23903', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('229', '22328', '23189', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('230', '24993', '24994', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('231', '24993', '25072', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('232', '26141', '31135', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('233', '26141', '31896', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('234', '26141', '30197', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('235', '26141', '28960', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('236', '26141', '29834', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('237', '26141', '30851', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('238', '26141', '26142', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('239', '26141', '27714', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('240', '26141', '29369', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('241', '26141', '28573', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('242', '32676', '32986', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('243', '32676', '39436', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('244', '32676', '38564', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('245', '32676', '36790', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('246', '32676', '40859', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('247', '32676', '35181', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('248', '42385', '45561', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('249', '42385', '50331', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('250', '42385', '50031', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('251', '42385', '48347', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('252', '42385', '47329', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('253', '42385', '50532', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('254', '42385', '43069', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('255', '42385', '45916', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('256', '42385', '47856', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('257', '42385', '47634', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('258', '42385', '44417', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('259', '42385', '45165', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('260', '42385', '47001', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('261', '51578', '52287', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('262', '51578', '51913', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('263', '53241', '53508', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('264', '53241', '53586', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('265', '54020', '54260', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('266', '54020', '54021', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('267', '55065', '56489', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('268', '55065', '57886', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('269', '55065', '57778', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('270', '55065', '57560', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('271', '55065', '58098', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('272', '55065', '55347', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('273', '55065', '56051', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('274', '55065', '55066', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('275', '55065', '56671', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('276', '55065', '57935', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('277', '58285', '60134', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('278', '58285', '58567', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('279', '58285', '60255', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('280', '58285', '58490', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('281', '60371', '60653', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('282', '60371', '61362', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('283', '60371', '61400', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('284', '60371', '61251', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('285', '60371', '61077', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('286', '60371', '61614', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('287', '60371', '61711', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('288', '60371', '61846', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('289', '60371', '60967', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('290', '60371', '61492', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('291', '61965', '62643', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('292', '61965', '62861', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('293', '61965', '63007', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('294', '61965', '61966', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('295', '61965', '63578', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('296', null, null, '3', '6', null, '2018', 'Kab Panajam Pasut');
INSERT INTO `t_daerah_pelaksana` VALUES ('297', '65702', '65974', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('298', null, null, '3', '6', null, '2018', 'Kab Bolmong Utara');
INSERT INTO `t_daerah_pelaksana` VALUES ('299', null, null, '3', '6', null, '2018', 'Kab Sitaro');
INSERT INTO `t_daerah_pelaksana` VALUES ('300', '65702', '66853', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('301', '65702', '66371', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('302', '67393', '68428', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('303', '67393', '68887', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('304', '67393', '68041', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('305', '69268', '70239', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('306', '69268', '70149', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('307', '69268', '69668', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('308', '69268', '71437', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('309', '69268', '71209', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('310', '69268', '69744', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('311', '69268', '71014', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('312', '69268', '71579', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('313', '69268', '71327', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('314', '72551', '72552', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('315', '74716', '75301', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('316', '75425', '75670', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('317', '75425', '75863', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('318', '76096', '76470', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('319', '78203', '79466', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('320', '78203', '80851', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('321', '78203', '79663', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('322', '78203', '78289', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('323', '78203', '80015', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('324', '78203', '80526', '3', '6', null, '2018', null);
INSERT INTO `t_daerah_pelaksana` VALUES ('325', '78203', '81007', '3', '6', null, '2018', null);

-- ----------------------------
-- Table structure for t_paslon
-- ----------------------------
DROP TABLE IF EXISTS `t_paslon`;
CREATE TABLE `t_paslon` (
  `paslon_id` int(11) NOT NULL AUTO_INCREMENT,
  `calon_ketua_id` bigint(10) DEFAULT NULL,
  `calon_wakil_id` bigint(10) DEFAULT NULL,
  `jenis_calon_id` tinyint(1) DEFAULT NULL,
  `provinsiId` bigint(10) DEFAULT NULL,
  `kabupatenId` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`paslon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_paslon
-- ----------------------------
INSERT INTO `t_paslon` VALUES ('8', '1', '2', '5', '35', '3579');
INSERT INTO `t_paslon` VALUES ('9', '3', '4', '5', '35', '3579');
INSERT INTO `t_paslon` VALUES ('10', '5', '6', '5', '35', '3579');
INSERT INTO `t_paslon` VALUES ('11', '7', '8', '5', '32', null);
INSERT INTO `t_paslon` VALUES ('12', '9', '10', '5', '6728', null);
INSERT INTO `t_paslon` VALUES ('13', '11', '12', '5', '1', null);
INSERT INTO `t_paslon` VALUES ('14', '13', '14', '5', '1', null);
INSERT INTO `t_paslon` VALUES ('15', '15', '16', '5', '6728', null);
INSERT INTO `t_paslon` VALUES ('16', '17', '18', '5', '1', null);

-- ----------------------------
-- Table structure for t_vote
-- ----------------------------
DROP TABLE IF EXISTS `t_vote`;
CREATE TABLE `t_vote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_paslon` varchar(255) DEFAULT NULL,
  `id_tps` varchar(255) DEFAULT NULL,
  `id_assignment` varchar(255) DEFAULT NULL,
  `vote` decimal(10,0) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_vote
-- ----------------------------
INSERT INTO `t_vote` VALUES ('1', '8', '1798951', null, '20', null, null);
INSERT INTO `t_vote` VALUES ('2', '9', '1798952', null, '10', null, null);

-- ----------------------------
-- Table structure for tb_agenda
-- ----------------------------
DROP TABLE IF EXISTS `tb_agenda`;
CREATE TABLE `tb_agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_german1_ci NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `description` mediumtext NOT NULL,
  `provinsi` varchar(11) NOT NULL,
  `kabupaten` varchar(11) NOT NULL,
  `kecamatan` varchar(11) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `create_date` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `image_link` varchar(255) NOT NULL,
  `akses` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_agenda
-- ----------------------------
INSERT INTO `tb_agenda` VALUES ('1', 'kampanye bersama', '2017-07-26', '2017-07-26', '<p class=\"MsoNormal\" style=\"text-align: justify; line-height: 115%;\">Lorem ipsum dolor sit amet, vix inimicus expetenda ad, cu odio stet oporteat est. Mea euripidis prodesset adipiscing ne, per te simul sensibus complectitur. Vix ne iisque splendide, ne has alii scriptorem, ad hendrerit torquatos eum. Quem erant nam at, mel posse labore animal an. In quis nostrum evertitur eum, eos volutpat accusamus id, ut dico minimum eos. Ad mea graecis perpetua complectitur, has in tota periculis. Feugait commune postulant per ut, est duis fastidii efficiendi in.</p>\n<p class=\"MsoNormal\" style=\"text-align: justify; line-height: 115%;\">Te sit tantas eruditi torquatos, rebum graeci aliquip vis ut, eam et nulla quaeque. Te nulla docendi mel, mel doctus corpora cu. Mediocrem intellegat ex est, no vivendum adolescens definitiones pri. Eam facilisi consetetur eu. Sed quem dicta antiopam ei, sit cu consul vidisse dissentiunt.</p>\n<p class=\"MsoNormal\" style=\"text-align: justify; line-height: 115%;\">Diceret ocurreret in sea, vel electram persecuti ut. Exerci laoreet an vim. Cibo error qui an, pro ad dolore pertinacia eloquentiam, dicunt antiopam ne nam. Vim prima molestie quaerendum at, ex mel ullum temporibus. Dictas appareat mel te, consequat voluptatibus pri et, an vix fuisset detracto.</p>\n<p class=\"MsoNormal\" style=\"text-align: justify; line-height: 115%;\">Vel tota prompta prodesset an, sed aliquando reformidans cu, ea odio ridens hendrerit sit. Eum no summo primis insolens, cum malis animal oporteat id. Etiam dolore vim in, harum inimicus pertinacia nec id. Tota essent bonorum eum et. Eam liber quaeque id, sed liber dicam euismod ea. Ex pri docendi perfecto sententiae.</p>\n<p>&nbsp;</p>\n<p class=\"MsoNormal\" style=\"text-align: justify; line-height: 115%;\">Ei nam diam omnium aperiam, perfecto disputationi pri ex. Dicit inimicus pertinacia ut vix, falli deterruisset no sed. Duo persius probatus electram te. An eam choro gloriatur.</p>', '41863', '42221', '42247', 'Jl Paregrek', 'Drs.Hi. Moh. Yasin Payapo,M.Pd', '2017-07-26 00:00:00', 'Terjadwal', 'http://localhost/personal/assets/images/agenda/55978.jpg', '1');
INSERT INTO `tb_agenda` VALUES ('2', 'Silaturahim Nasional', '2017-08-23', '2017-08-24', '<p>&nbsp;Konferensi untuk konsolidasi bersama saksi</p>', '25823', '25833', '25867', 'Gedun Perpus Nas', 'kader1', '2017-08-24 00:00:00', 'Terjadwal', 'http://localhost/personal_campaign/assets/images/agenda/80938.jpg', '1');

-- ----------------------------
-- Table structure for tb_berita2
-- ----------------------------
DROP TABLE IF EXISTS `tb_berita2`;
CREATE TABLE `tb_berita2` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `image_link` varchar(255) NOT NULL,
  `url_link` varchar(255) NOT NULL,
  `create_by` varchar(20) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_by` varchar(20) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_by` varchar(20) NOT NULL,
  `delete_date` datetime NOT NULL,
  `view_count` int(5) DEFAULT '0',
  `akses` varchar(100) NOT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_berita2
-- ----------------------------
INSERT INTO `tb_berita2` VALUES ('2', 'Pembekalan Relawan Guna Persiapan Pemenangan', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque lau</p>', 'assets/images/berita/77012.jpg', '', 'kader1', '2017-08-27 00:00:00', 'kader1', '2017-08-27 18:12:02', 'kader1', '2017-08-27 18:12:02', '0', '1');
INSERT INTO `tb_berita2` VALUES ('3', 'Kegiatan Awal Kampanye', '<p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es. Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es. Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues dif</p>', 'assets/images/berita/12339.png', '', 'kader1', '2017-08-27 21:40:00', 'kader1', '2017-08-27 21:41:39', 'kader1', '2017-08-27 21:41:39', '0', '1');

-- ----------------------------
-- Table structure for tb_kinerja_rel
-- ----------------------------
DROP TABLE IF EXISTS `tb_kinerja_rel`;
CREATE TABLE `tb_kinerja_rel` (
  `id_kinerja` int(11) NOT NULL AUTO_INCREMENT,
  `id_agenda` int(11) NOT NULL,
  `id_rel` int(11) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `akses` varchar(200) NOT NULL,
  PRIMARY KEY (`id_kinerja`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_kinerja_rel
-- ----------------------------
INSERT INTO `tb_kinerja_rel` VALUES ('1', '1', '1', '', '1');

-- ----------------------------
-- Table structure for tb_login
-- ----------------------------
DROP TABLE IF EXISTS `tb_login`;
CREATE TABLE `tb_login` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `akses` varchar(255) DEFAULT NULL,
  `id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_login
-- ----------------------------
INSERT INTO `tb_login` VALUES ('1', 'admin', 'password', '1', null, null);
INSERT INTO `tb_login` VALUES ('2', 'kader1', 'password', '2', '1', '1');
INSERT INTO `tb_login` VALUES ('3', 'kader2', 'password', '2', '2', '2');

-- ----------------------------
-- Table structure for tb_moto
-- ----------------------------
DROP TABLE IF EXISTS `tb_moto`;
CREATE TABLE `tb_moto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `create_by` varchar(20) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_by` varchar(20) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_by` varchar(20) NOT NULL,
  `delete_date` datetime NOT NULL,
  `akses` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_moto
-- ----------------------------
INSERT INTO `tb_moto` VALUES ('1', 'Indonesia kuat dari masyarakat', 'Aktif', '1', '2017-07-26 10:24:14', 'kader1', '2017-08-24 11:51:09', '1', '2017-07-26 10:24:14', '1', 'Bersama membangun Madura,');
INSERT INTO `tb_moto` VALUES ('5', 'Indonesia kuat dari desa!', 'Aktif', '2', '2017-08-21 20:30:42', 'kader2', '2017-08-21 20:30:42', '2', '2017-08-21 20:30:42', '2', 'Bekerja Bersama Rakyat');

-- ----------------------------
-- Table structure for tb_oleh
-- ----------------------------
DROP TABLE IF EXISTS `tb_oleh`;
CREATE TABLE `tb_oleh` (
  `id_oleh` int(11) NOT NULL AUTO_INCREMENT,
  `nama_oleh` varchar(100) DEFAULT NULL,
  `stok` int(20) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `image_link` varchar(255) DEFAULT NULL,
  `akes` varchar(255) NOT NULL,
  PRIMARY KEY (`id_oleh`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_oleh
-- ----------------------------
INSERT INTO `tb_oleh` VALUES ('1', 'Gelas Hanura', '20', '<p>Ini Gelas Hanura</p>', 'http://localhost/PRAKERIN/Sisaksi%20Personal/assets/images/agenda/68006.jpg', 'Fatchur');
INSERT INTO `tb_oleh` VALUES ('2', 'Mug', '1040', '<p>Ini Merchandise mekmkmmimimmimmmimmii</p>', 'http://localhost/PRAKERIN/Sisaksi%20Personal/assets/images/agenda/75770.jpg', '1');
INSERT INTO `tb_oleh` VALUES ('4', 'Kaos Teman Ahok', '1100', '<p>as daks dksad&nbsp;</p>', 'http://localhost/PRAKERIN/SisaksiPersonal/assets/images/agenda/18313.jpg', 'Yusron');
INSERT INTO `tb_oleh` VALUES ('5', 'Jaket', '20', '<p>Ini Jaket Ahok</p>', 'http://localhost/PRAKERIN/SisaksiPersonal/assets/images/agenda/75322.jpg', 'Drs.Hi. Moh. Yasin Payapo,M.Pd');
INSERT INTO `tb_oleh` VALUES ('6', 'Kaos', '24', '<p>Ini Kaos Ahok</p>', 'http://localhost/PRAKERIN/SisaksiPersonal/assets/images/agenda/67285.jpg', 'Drs.Hi. Moh. Yasin Payapo,M.Pd');
INSERT INTO `tb_oleh` VALUES ('7', 'Syal', '3', '<p>ini syal</p>', 'http://localhost/PRAKERIN/SisaksiPersonal/assets/images/agenda/23730.jpg', 'Drs.Hi. Moh. Yasin Payapo,M.Pd');
INSERT INTO `tb_oleh` VALUES ('8', 'Marjan', '0', '<p>stok habis</p>', 'http://localhost/PRAKERIN/SisaksiPersonal/assets/images/agenda/18231.png', 'Drs.Hi. Moh. Yasin Payapo,M.Pd');
INSERT INTO `tb_oleh` VALUES ('9', 'Kaos', '20', '<p>Kaos Calon&nbsp;</p>', 'http://localhost/personal/assets/images/agenda/35392.jpg', '1');
INSERT INTO `tb_oleh` VALUES ('10', 'Jaket Bomber', '1000', '<p>Jaket Relawan</p>', 'http://localhost/personal_campaign/assets/images/agenda/90502.jpg', '1');
INSERT INTO `tb_oleh` VALUES ('11', 'Jaket', '100', '<p>Jaket bomber untuk relawan</p>', 'http://localhost/personal_campaign/assets/images/agenda/69723.jpg', 'as');
INSERT INTO `tb_oleh` VALUES ('12', 'Jaket', '100', '<p>Jaket Bomber untuk relawan</p>', 'http://localhost/personal_campaign/assets/images/agenda/12554.jpg', 'kader1');
INSERT INTO `tb_oleh` VALUES ('13', 'Sticker', '10000', '<p>Sticker branding personal</p>', 'http://localhost/personal_campaign/assets/images/agenda/38199.png', '1');

-- ----------------------------
-- Table structure for tb_personal_act_for
-- ----------------------------
DROP TABLE IF EXISTS `tb_personal_act_for`;
CREATE TABLE `tb_personal_act_for` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `activated_for` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_personal_act_for
-- ----------------------------
INSERT INTO `tb_personal_act_for` VALUES ('1', '1');
SET FOREIGN_KEY_CHECKS=1;
