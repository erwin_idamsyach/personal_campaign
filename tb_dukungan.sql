/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : 127.0.0.1:3306
Source Database       : db_personal

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-08-29 10:02:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_dukungan
-- ----------------------------
DROP TABLE IF EXISTS `tb_dukungan`;
CREATE TABLE `tb_dukungan` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama_pendukung` varchar(100) DEFAULT NULL,
  `no_telpon` varchar(14) DEFAULT NULL,
  `caleg_didukung` int(5) DEFAULT NULL,
  `follow_up` enum('YES','NO') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_dukungan
-- ----------------------------
INSERT INTO `tb_dukungan` VALUES ('1', 'Erwin Idamsyach Putra', '+6285645667199', '1', 'NO');
SET FOREIGN_KEY_CHECKS=1;
