/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : db_personal

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-08-31 15:30:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for m_bulan
-- ----------------------------
DROP TABLE IF EXISTS `m_bulan`;
CREATE TABLE `m_bulan` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `bulan` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of m_bulan
-- ----------------------------
INSERT INTO `m_bulan` VALUES ('1', 'Januari');
INSERT INTO `m_bulan` VALUES ('2', 'Februari');
INSERT INTO `m_bulan` VALUES ('3', 'Maret');
INSERT INTO `m_bulan` VALUES ('4', 'April');
INSERT INTO `m_bulan` VALUES ('5', 'Mei');
INSERT INTO `m_bulan` VALUES ('6', 'Juni');
INSERT INTO `m_bulan` VALUES ('7', 'Juli');
INSERT INTO `m_bulan` VALUES ('8', 'Agustus');
INSERT INTO `m_bulan` VALUES ('9', 'September');
INSERT INTO `m_bulan` VALUES ('10', 'Oktober');
INSERT INTO `m_bulan` VALUES ('11', 'November');
INSERT INTO `m_bulan` VALUES ('12', 'Desember');

-- ----------------------------
-- Table structure for m_tahun
-- ----------------------------
DROP TABLE IF EXISTS `m_tahun`;
CREATE TABLE `m_tahun` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `tahun` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of m_tahun
-- ----------------------------
INSERT INTO `m_tahun` VALUES ('1', '1900');
INSERT INTO `m_tahun` VALUES ('2', '1901');
INSERT INTO `m_tahun` VALUES ('3', '1902');
INSERT INTO `m_tahun` VALUES ('4', '1903');
INSERT INTO `m_tahun` VALUES ('5', '1904');
INSERT INTO `m_tahun` VALUES ('6', '1905');
INSERT INTO `m_tahun` VALUES ('7', '1906');
INSERT INTO `m_tahun` VALUES ('8', '1907');
INSERT INTO `m_tahun` VALUES ('9', '1908');
INSERT INTO `m_tahun` VALUES ('10', '1909');
INSERT INTO `m_tahun` VALUES ('11', '1910');
INSERT INTO `m_tahun` VALUES ('12', '1911');
INSERT INTO `m_tahun` VALUES ('13', '1912');
INSERT INTO `m_tahun` VALUES ('14', '1913');
INSERT INTO `m_tahun` VALUES ('15', '1914');
INSERT INTO `m_tahun` VALUES ('16', '1915');
INSERT INTO `m_tahun` VALUES ('17', '1916');
INSERT INTO `m_tahun` VALUES ('18', '1917');
INSERT INTO `m_tahun` VALUES ('19', '1918');
INSERT INTO `m_tahun` VALUES ('20', '1919');
INSERT INTO `m_tahun` VALUES ('21', '1920');
INSERT INTO `m_tahun` VALUES ('22', '1921');
INSERT INTO `m_tahun` VALUES ('23', '1922');
INSERT INTO `m_tahun` VALUES ('24', '1923');
INSERT INTO `m_tahun` VALUES ('25', '1924');
INSERT INTO `m_tahun` VALUES ('26', '1925');
INSERT INTO `m_tahun` VALUES ('27', '1926');
INSERT INTO `m_tahun` VALUES ('28', '1927');
INSERT INTO `m_tahun` VALUES ('29', '1928');
INSERT INTO `m_tahun` VALUES ('30', '1929');
INSERT INTO `m_tahun` VALUES ('31', '1930');
INSERT INTO `m_tahun` VALUES ('32', '1931');
INSERT INTO `m_tahun` VALUES ('33', '1932');
INSERT INTO `m_tahun` VALUES ('34', '1933');
INSERT INTO `m_tahun` VALUES ('35', '1934');
INSERT INTO `m_tahun` VALUES ('36', '1935');
INSERT INTO `m_tahun` VALUES ('37', '1936');
INSERT INTO `m_tahun` VALUES ('38', '1937');
INSERT INTO `m_tahun` VALUES ('39', '1938');
INSERT INTO `m_tahun` VALUES ('40', '1939');
INSERT INTO `m_tahun` VALUES ('41', '1940');
INSERT INTO `m_tahun` VALUES ('42', '1941');
INSERT INTO `m_tahun` VALUES ('43', '1942');
INSERT INTO `m_tahun` VALUES ('44', '1943');
INSERT INTO `m_tahun` VALUES ('45', '1944');
INSERT INTO `m_tahun` VALUES ('46', '1945');
INSERT INTO `m_tahun` VALUES ('47', '1946');
INSERT INTO `m_tahun` VALUES ('48', '1947');
INSERT INTO `m_tahun` VALUES ('49', '1948');
INSERT INTO `m_tahun` VALUES ('50', '1949');
INSERT INTO `m_tahun` VALUES ('51', '1950');
INSERT INTO `m_tahun` VALUES ('52', '1951');
INSERT INTO `m_tahun` VALUES ('53', '1952');
INSERT INTO `m_tahun` VALUES ('54', '1953');
INSERT INTO `m_tahun` VALUES ('55', '1954');
INSERT INTO `m_tahun` VALUES ('56', '1955');
INSERT INTO `m_tahun` VALUES ('57', '1956');
INSERT INTO `m_tahun` VALUES ('58', '1957');
INSERT INTO `m_tahun` VALUES ('59', '1958');
INSERT INTO `m_tahun` VALUES ('60', '1959');
INSERT INTO `m_tahun` VALUES ('61', '1960');
INSERT INTO `m_tahun` VALUES ('62', '1961');
INSERT INTO `m_tahun` VALUES ('63', '1962');
INSERT INTO `m_tahun` VALUES ('64', '1963');
INSERT INTO `m_tahun` VALUES ('65', '1964');
INSERT INTO `m_tahun` VALUES ('66', '1965');
INSERT INTO `m_tahun` VALUES ('67', '1966');
INSERT INTO `m_tahun` VALUES ('68', '1967');
INSERT INTO `m_tahun` VALUES ('69', '1968');
INSERT INTO `m_tahun` VALUES ('70', '1969');
INSERT INTO `m_tahun` VALUES ('71', '1970');
INSERT INTO `m_tahun` VALUES ('72', '1971');
INSERT INTO `m_tahun` VALUES ('73', '1972');
INSERT INTO `m_tahun` VALUES ('74', '1973');
INSERT INTO `m_tahun` VALUES ('75', '1974');
INSERT INTO `m_tahun` VALUES ('76', '1975');
INSERT INTO `m_tahun` VALUES ('77', '1976');
INSERT INTO `m_tahun` VALUES ('78', '1977');
INSERT INTO `m_tahun` VALUES ('79', '1978');
INSERT INTO `m_tahun` VALUES ('80', '1979');
INSERT INTO `m_tahun` VALUES ('81', '1980');
INSERT INTO `m_tahun` VALUES ('82', '1981');
INSERT INTO `m_tahun` VALUES ('83', '1982');
INSERT INTO `m_tahun` VALUES ('84', '1983');
INSERT INTO `m_tahun` VALUES ('85', '1984');
INSERT INTO `m_tahun` VALUES ('86', '1985');
INSERT INTO `m_tahun` VALUES ('87', '1986');
INSERT INTO `m_tahun` VALUES ('88', '1987');
INSERT INTO `m_tahun` VALUES ('89', '1988');
INSERT INTO `m_tahun` VALUES ('90', '1989');
INSERT INTO `m_tahun` VALUES ('91', '1990');
INSERT INTO `m_tahun` VALUES ('92', '1991');
INSERT INTO `m_tahun` VALUES ('93', '1992');
INSERT INTO `m_tahun` VALUES ('94', '1993');
INSERT INTO `m_tahun` VALUES ('95', '1994');
INSERT INTO `m_tahun` VALUES ('96', '1995');
INSERT INTO `m_tahun` VALUES ('97', '1996');
INSERT INTO `m_tahun` VALUES ('98', '1997');
INSERT INTO `m_tahun` VALUES ('99', '1998');
INSERT INTO `m_tahun` VALUES ('100', '1999');
INSERT INTO `m_tahun` VALUES ('101', '2000');

-- ----------------------------
-- Table structure for m_tanggal
-- ----------------------------
DROP TABLE IF EXISTS `m_tanggal`;
CREATE TABLE `m_tanggal` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `tgl` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of m_tanggal
-- ----------------------------
INSERT INTO `m_tanggal` VALUES ('1', '1');
INSERT INTO `m_tanggal` VALUES ('2', '2');
INSERT INTO `m_tanggal` VALUES ('3', '3');
INSERT INTO `m_tanggal` VALUES ('4', '4');
INSERT INTO `m_tanggal` VALUES ('5', '5');
INSERT INTO `m_tanggal` VALUES ('6', '6');
INSERT INTO `m_tanggal` VALUES ('7', '7');
INSERT INTO `m_tanggal` VALUES ('8', '8');
INSERT INTO `m_tanggal` VALUES ('9', '9');
INSERT INTO `m_tanggal` VALUES ('10', '10');
INSERT INTO `m_tanggal` VALUES ('11', '11');
INSERT INTO `m_tanggal` VALUES ('12', '12');
INSERT INTO `m_tanggal` VALUES ('13', '13');
INSERT INTO `m_tanggal` VALUES ('14', '14');
INSERT INTO `m_tanggal` VALUES ('15', '15');
INSERT INTO `m_tanggal` VALUES ('16', '16');
INSERT INTO `m_tanggal` VALUES ('17', '17');
INSERT INTO `m_tanggal` VALUES ('18', '18');
INSERT INTO `m_tanggal` VALUES ('19', '19');
INSERT INTO `m_tanggal` VALUES ('20', '20');
INSERT INTO `m_tanggal` VALUES ('21', '21');
INSERT INTO `m_tanggal` VALUES ('22', '22');
INSERT INTO `m_tanggal` VALUES ('23', '23');
INSERT INTO `m_tanggal` VALUES ('24', '24');
INSERT INTO `m_tanggal` VALUES ('25', '25');
INSERT INTO `m_tanggal` VALUES ('26', '26');
INSERT INTO `m_tanggal` VALUES ('27', '27');
INSERT INTO `m_tanggal` VALUES ('28', '28');
INSERT INTO `m_tanggal` VALUES ('29', '29');
INSERT INTO `m_tanggal` VALUES ('30', '30');
INSERT INTO `m_tanggal` VALUES ('31', '31');

-- ----------------------------
-- Table structure for m_tingkat_pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `m_tingkat_pendidikan`;
CREATE TABLE `m_tingkat_pendidikan` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `tingkat` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of m_tingkat_pendidikan
-- ----------------------------
INSERT INTO `m_tingkat_pendidikan` VALUES ('1', 'SD / Sederajat');
INSERT INTO `m_tingkat_pendidikan` VALUES ('2', 'SMP / Sederajat');
INSERT INTO `m_tingkat_pendidikan` VALUES ('3', 'SMA / SMK / Sederajat');
INSERT INTO `m_tingkat_pendidikan` VALUES ('4', 'Sarjana (S1)');
INSERT INTO `m_tingkat_pendidikan` VALUES ('5', 'Magister (S2)');
INSERT INTO `m_tingkat_pendidikan` VALUES ('6', 'Doktoral (S3)');
INSERT INTO `m_tingkat_pendidikan` VALUES ('7', 'Lainnya');
