/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : db_personal

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-09-04 11:06:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_background
-- ----------------------------
DROP TABLE IF EXISTS `tb_background`;
CREATE TABLE `tb_background` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motto` varchar(255) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `background` varchar(255) DEFAULT NULL,
  `status` enum('ACTIVE','PASSIVE') DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_background
-- ----------------------------
INSERT INTO `tb_background` VALUES ('4', 'Indonesia Raya Bersama Saya', 'Kami kuat, kami hebat!', '1504495574_.png', 'ACTIVE', '2017-09-04 10:36:24');
