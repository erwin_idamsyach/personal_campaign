
$.ajaxSetup({ 
	headers: { 
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
	}  
});
$(document).ready(function() {
	var $btnSets = $('#responsive'),
	$btnLinks = $btnSets.find('a');
	
	$btnLinks.click(function(e) {
		e.preventDefault();
		$(this).siblings('a.active').removeClass("active");
		$(this).addClass("active");
		var index = $(this).index();
		$("div.user-menu>div.user-menu-content").removeClass("active");
		$("div.user-menu>div.user-menu-content").eq(index).addClass("active");
	});
});

$( document ).ready(function() {
	$("[rel='tooltip']").tooltip(); 
	$('[data-toggle="tooltip"]').tooltip();
	$('.view').hover(
		function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
        ); 
});
$(document).keydown(function(e){
	if(e.keyCode == 33){
		window.scrollTo(0, $(window).scrollTop()-52);
	}else if(e.keyCode == 34){
		window.scrollTo(0, $(window).scrollTop()+62);
	}else{

	}
});
function close_modal(nama) {
	$('#'+nama).modal('hide');
}

$(document).ready(function() {   
	var sideslider = $('[data-toggle=collapse-side]');
	var sel = sideslider.attr('data-target');
	var sel2 = sideslider.attr('data-target-2');
	sideslider.click(function(event){
		$(sel).toggleClass('in');
		$(sel2).toggleClass('out');
	});
});

function show_modal(id) {
	$.ajax({
		type : "GET",
		url  : "show_modal",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.modal-view').html(html);
		}
	});
}

function add_cart(id){
	$.ajax({
		type : "GET",
		url  : "add_cart/"+id,
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function login(){
	var user = $('#username').val();
	var pass = $('#password').val();
	$.ajax({
		type : "GET",
		url  : "login",
		data : {
			'username' : user,
			'password' : pass
		},
		success:function(data){
			if(data == "FACEK"){
				window.location=('./admin');
			}else if(data == ""){
				$('#pesan').html('<div class="alert alert-danger"><i class="fa fa-warning"></i> WRONG USERNAME & PASSWORD</div>');
			}
		}
	});
}

function tambah_object(){
	var name  = $('#name').val();
	var brand = $('#brand').val();
	var price = $('#price').val();
	var size  = $('#size').val();
	var cate  = $('#categories').val();
	var gend  = $('#gender').val();
	var spec  = tinyMCE.get('texta').getContent();
	var image = $('#gambarout').val();
	$.ajax({
		type : "GET",
		url	 : "./tambah_item",
		data : {
			'name' : name,
			'brand': brand,
			'price': price,
			'size' : size,
			'cate' : cate,
			'gende': gend,
			'spec' : spec,
			'image': image
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function save_object(id){
	var name  = $('#name').val();
	var brand = $('#brand').val();
	var price = $('#price').val();
	var size  = $('#size').val();
	var cate  = $('#categories').val();
	var spec  = tinyMCE.get('texta').getContent();
	var image = $('#gambarout').val();
	$.ajax({
		type : "GET",
		url	 : "./save_item",
		data : {
			'name' : name,
			'brand': brand,
			'price': price,
			'size' : size,
			'cate' : cate,
			'spec' : spec,
			'image': image,
			'id'   : id
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function view_item(id){
	$.ajax({
		type : "GET",
		url  : "./view_item",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}

function page_item(id){
	$('.table-item').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "./page_item",
		data : "id="+id,
		success:function(html){
			$('.table-item').show();
			$('.table-item').html(html);
			$('.loading').hide();
		}
	});
}

function edit_item(id){
	$.ajax({
		type : "GET",
		url  : "./edit_item",
		data : "id="+id,
		success:function(html){
			$('.hok').html(html);
			$('.hok').show();
		}
	});
}

function delete_item(id){
	$.ajax({
		type : "POST",
		url  : "./delete_item",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function add_brand(){
	var name  = $('#name').val();
	var image = $('#gambarout').val();
	$.ajax({
		type : "POST",
		url	 : "./tambah_brand",
		data : {
			'name' : name,
			'image': image
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function page_brand(id){
	$('.table-brand').hide();
	$('.loading').show();
	$.ajax({
		type : "GET",
		url  : "./page_brand",
		data : "id="+id,
		success:function(html){
			$('.table-brand').show();
			$('.table-brand').html(html);
			$('.loading').hide();
		}
	});
}


function save_brand(id){
	var name  = $('#name').val();
	var image = $('#gambarout').val();
	$.ajax({
		type : "POST",
		url	 : "./save_brand",
		data : {
			'name' : name,
			'image': image,
			'id'   : id
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function view_brand(id){
	$.ajax({
		type : "POST",
		url  : "./view_brand",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}

function edit_brand(id){
	$.ajax({
		type : "POST",
		url  : "./edit_brand",
		data : "id="+id,
		success:function(html){
			$('.hok').html(html);
			$('.hok').show();
		}
	});
}

function delete_brand(id){
	$.ajax({
		type : "POST",
		url  : "./delete_brand",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function add_supply(){
	var first  = $('#first_name').val();
	var last   = $('#last_name').val();
	var gender = $('#gender').val();
	var phone  = $('#phone').val();
	var email  = $('#email').val();
	var addres = $('#address').val();
	var postal = $('#postal').val();
	var provin = $('#province').val();
	var city   = $('#city').val();
	var distri = $('#district').val();
	var image  = $('#gambarout').val();
	$.ajax({
		type : "GET",
		url  : "add_supply",
		data : {
			'first'  	: first,
			'last'   	: last,
			'gender' 	: gender,
			'phone'  	: phone,
			'email'  	: email,
			'address'   : addres,
			'postal'  	: postal,
			'province'  : provin,
			'city'  	: city,
			'district'  : distri,
			'image'		: image
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function save_supply(id){
	var first  = $('#first_name').val();
	var last   = $('#last_name').val();
	var gender = $('#gender').val();
	var phone  = $('#phone').val();
	var email  = $('#email').val();
	var addres = $('#address').val();
	var postal = $('#postal').val();
	var provin = $('#province').val();
	var city   = $('#city').val();
	var distri = $('#district').val();
	var image  = $('#gambarout').val();
	$.ajax({
		type : "GET",
		url	 : "./save_supply",
		data : {
			'first'  	: first,
			'last'   	: last,
			'gender' 	: gender,
			'phone'  	: phone,
			'email'  	: email,
			'address'   : addres,
			'postal'  	: postal,
			'province'  : provin,
			'city'  	: city,
			'district'  : distri,
			'image'		: image,
			'id'		: id
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function edit_supply(id){
	$.ajax({
		type : "GET",
		url  : "./edit_supply",
		data : "id="+id,
		success:function(html){
			$('.hok').html(html);
			$('.hok').show();
		}
	});
}

function view_supply(id){
	$.ajax({
		type : "GET",
		url  : "view_supply",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}

function delete_supply(id){
	$.ajax({
		type : "GET",
		url  : "./delete_supply",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function page_supply(id){
	$('.table-supply').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "page_supply",
		data : "id="+id,
		success:function(html){
			$('.table-supply').show();
			$('.table-supply').html(html);
			$('.loading').hide();
		}
	});
}

function get_city(){
	var id = $('#province').val();
	$.ajax({
		type : "GET",
		url  : "get_city",
		data : "id="+id,
		success:function(html){
			$('#city').html(html);
		}
	});
}

function get_district(){
	var id = $('#city').val();
	$.ajax({
		type : "GET",
		url  : "get_district",
		data : "id="+id,
		success:function(html){
			$('#district').html(html);
		}
	});
}

function add_categories(){
	var code  = $('#code').val();
	var name  = $('#name').val();
	$.ajax({
		type : "GET",
		url  : "add_categories",
		data : {
			'code' : code,
			'name'	: name
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function save_categories(id){
	var code  = $('#code').val();
	var name  = $('#name').val();
	$.ajax({
		type : "GET",
		url  : "save_categories",
		data : {
			'code' 	: code,
			'name'	: name,
			'id'	: id
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function edit_categories(id){
	$.ajax({
		type : "GET",
		url  : "./edit_categories",
		data : "id="+id,
		success:function(html){
			$('.hok').html(html);
			$('.hok').show();
		}
	});
}

function delete_categories(id){
	$.ajax({
		type : "GET",
		url  : "./delete_categories",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function page_categories(id){
	$('.table-categories').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "page_categories",
		data : "id="+id,
		success:function(html){
			$('.table-categories').show();
			$('.table-categories').html(html);
			$('.loading').hide();
		}
	});
}

function view_member(id){
	$.ajax({
		type : "GET",
		url  : "view_member",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}

function delete_member(id){
	$.ajax({
		type : "GET",
		url  : "./delete_member",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function page_member(id){
	$('.table-member').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "page_member",
		data : "id="+id,
		success:function(html){
			$('.table-member').show();
			$('.table-member').html(html);
			$('.loading').hide();
		}
	});
}

function add_shipping(){
	var title   = $('#name').val();
	var content = tinyMCE.get('texta').getContent();
	$.ajax({
		type : "GET",
		url  : "add_shipping",
		data : {
			'title' 	: title,
			'content'	: content
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function save_shipping(id){
	var title   = $('#name').val();
	var content = tinyMCE.get('texta').getContent();
	$.ajax({
		type : "GET",
		url  : "save_shipping",
		data : {
			'title' 	: title,
			'content'	: content,
			'id'		: id
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function edit_shipping(id){
	$.ajax({
		type : "GET",
		url  : "./edit_shipping",
		data : "id="+id,
		success:function(html){
			$('.hok').html(html);
			$('.hok').show();
		}
	});
}

function delete_shipping(id){
	$.ajax({
		type : "GET",
		url  : "./delete_shipping",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function page_shipping(id){
	$('.table-shipping').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "page_shipping",
		data : "id="+id,
		success:function(html){
			$('.table-shipping').show();
			$('.table-shipping').html(html);
			$('.loading').hide();
		}
	});
}

function active_shipping(id,start){
	$('.table-shipping').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "active_shipping",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-shipping').show();
			$('.table-shipping').html(html);
			$('.loading').hide();
		}
	});
}

function deactive_shipping(id,start){
	$('.table-shipping').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "deactive_shipping",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-shipping').show();
			$('.table-shipping').html(html);
			$('.loading').hide();
		}
	});
}

function add_carreers(){
	var title   = $('#name').val();
	var content = tinyMCE.get('texta').getContent();
	$.ajax({
		type : "GET",
		url  : "add_carreers",
		data : {
			'title' 	: title,
			'content'	: content
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function save_carreers(id){
	var title   = $('#name').val();
	var content = tinyMCE.get('texta').getContent();
	$.ajax({
		type : "GET",
		url  : "save_carreers",
		data : {
			'title' 	: title,
			'content'	: content,
			'id'		: id
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function edit_carreers(id){
	$.ajax({
		type : "GET",
		url  : "./edit_carreers",
		data : "id="+id,
		success:function(html){
			$('.hok').html(html);
			$('.hok').show();
		}
	});
}

function delete_carreers(id){
	$.ajax({
		type : "GET",
		url  : "./delete_carreers",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function page_carreers(id){
	$('.table-carreers').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "page_carreers",
		data : "id="+id,
		success:function(html){
			$('.table-carreers').show();
			$('.table-carreers').html(html);
			$('.loading').hide();
		}
	});
}

function active_carreers(id,start){
	$('.table-carreers').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "active_carreers",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-carreers').show();
			$('.table-carreers').html(html);
			$('.loading').hide();
		}
	});
}

function deactive_carreers(id,start){
	$('.table-carreers').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "deactive_carreers",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-carreers').show();
			$('.table-carreers').html(html);
			$('.loading').hide();
		}
	});
}

function add_term(){
	var title   = $('#name').val();
	var content = tinyMCE.get('texta').getContent();
	$.ajax({
		type : "GET",
		url  : "add_term",
		data : {
			'title' 	: title,
			'content'	: content
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function save_term(id){
	var title   = $('#name').val();
	var content = tinyMCE.get('texta').getContent();
	$.ajax({
		type : "GET",
		url  : "save_term",
		data : {
			'title' 	: title,
			'content'	: content,
			'id'		: id
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function edit_term(id){
	$.ajax({
		type : "GET",
		url  : "./edit_term",
		data : "id="+id,
		success:function(html){
			$('.hok').html(html);
			$('.hok').show();
		}
	});
}

function delete_term(id){
	$.ajax({
		type : "GET",
		url  : "./delete_term",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function page_term(id){
	$('.table-term').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "page_term",
		data : "id="+id,
		success:function(html){
			$('.table-term').show();
			$('.table-term').html(html);
			$('.loading').hide();
		}
	});
}

function active_term(id,start){
	$('.table-term').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "active_term",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-term').show();
			$('.table-term').html(html);
			$('.loading').hide();
		}
	});
}

function deactive_term(id,start){
	$('.table-term').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "deactive_term",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-term').show();
			$('.table-term').html(html);
			$('.loading').hide();
		}
	});
}

function add_privacy(){
	var title   = $('#name').val();
	var content = tinyMCE.get('texta').getContent();
	$.ajax({
		type : "GET",
		url  : "add_privacy",
		data : {
			'title' 	: title,
			'content'	: content
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function save_privacy(id){
	var title   = $('#name').val();
	var content = tinyMCE.get('texta').getContent();
	$.ajax({
		type : "GET",
		url  : "save_privacy",
		data : {
			'title' 	: title,
			'content'	: content,
			'id'		: id
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function edit_privacy(id){
	$.ajax({
		type : "GET",
		url  : "./edit_privacy",
		data : "id="+id,
		success:function(html){
			$('.hok').html(html);
			$('.hok').show();
		}
	});
}

function delete_privacy(id){
	$.ajax({
		type : "GET",
		url  : "./delete_privacy",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function page_privacy(id){
	$('.table-privacy').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "page_privacy",
		data : "id="+id,
		success:function(html){
			$('.table-privacy').show();
			$('.table-privacy').html(html);
			$('.loading').hide();
		}
	});
}

function active_privacy(id,start){
	$('.table-privacy').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "active_privacy",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-privacy').show();
			$('.table-privacy').html(html);
			$('.loading').hide();
		}
	});
}

function deactive_privacy(id,start){
	$('.table-privacy').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "deactive_privacy",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-privacy').show();
			$('.table-privacy').html(html);
			$('.loading').hide();
		}
	});
}

function add_contact(){
	var address 	= $('#address').val();
	var coordinates = $('#coordinates').val();
	var phone 		= $('#phone').val();
	var facebook 	= $('#facebook').val();
	var twitter 	= $('#twitter').val();
	var google 		= $('#google').val();
	var youtube     = $('#youtube').val();
	var instagram 	= $('#instagram').val();
	$.ajax({
		type : "GET",
		url  : "add_contact",
		data : {
			'address' 	  : address,
			'coordinates' : coordinates,
			'phone'		  : phone,
			'facebook' 	  : facebook,
			'twitter' 	  : twitter,
			'google' 	  : google,
			'instagram'	  : instagram,
			'youtube'	  : youtube
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function save_contact(id){
	var address 	= $('#address').val();
	var coordinates = $('#coordinates').val();
	var phone 		= $('#phone').val();
	var facebook 	= $('#facebook').val();
	var twitter 	= $('#twitter').val();
	var google 		= $('#google').val();
	var youtube     = $('#youtube').val();
	var instagram 	= $('#instagram').val();
	$.ajax({
		type : "GET",
		url  : "save_contact",
		data : {
			'address' 	  : address,
			'coordinates' : coordinates,
			'phone'		  : phone,
			'facebook' 	  : facebook,
			'twitter' 	  : twitter,
			'google' 	  : google,
			'instagram'	  : instagram,
			'youtube'	  : youtube,
			'id'		  : id
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function edit_contact(id){
	$.ajax({
		type : "GET",
		url  : "./edit_contact",
		data : "id="+id,
		success:function(html){
			$('.hok').html(html);
			$('.hok').show();
		}
	});
}

function delete_contact(id){
	$.ajax({
		type : "GET",
		url  : "./delete_contact",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function page_contact(id){
	$('.table-contact').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "page_contact",
		data : "id="+id,
		success:function(html){
			$('.table-contact').show();
			$('.table-contact').html(html);
			$('.loading').hide();
		}
	});
}

function active_contact(id,start){
	$('.table-contact').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "active_contact",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-contact').show();
			$('.table-contact').html(html);
			$('.loading').hide();
		}
	});
}

function deactive_contact(id,start){
	$('.table-contact').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "deactive_contact",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-contact').show();
			$('.table-contact').html(html);
			$('.loading').hide();
		}
	});
}

function add_faq(){
	var title   = $('#name').val();
	var content = tinyMCE.get('texta').getContent();
	$.ajax({
		type : "GET",
		url  : "add_faq",
		data : {
			'title' 	: title,
			'content'	: content
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function save_faq(id){
	var title   = $('#name').val();
	var content = tinyMCE.get('texta').getContent();
	$.ajax({
		type : "GET",
		url  : "save_faq",
		data : {
			'title' 	: title,
			'content'	: content,
			'id'		: id
		},
		success:function(html){
			window.location.reload();
		}
	});
}

function edit_faq(id){
	$.ajax({
		type : "GET",
		url  : "./edit_faq",
		data : "id="+id,
		success:function(html){
			$('.hok').html(html);
			$('.hok').show();
		}
	});
}

function delete_faq(id){
	$.ajax({
		type : "GET",
		url  : "./delete_faq",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function page_faq(id){
	$('.table-faq').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "page_faq",
		data : "id="+id,
		success:function(html){
			$('.table-faq').show();
			$('.table-faq').html(html);
			$('.loading').hide();
		}
	});
}

function active_faq(id,start){
	$('.table-faq').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "active_faq",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-faq').show();
			$('.table-faq').html(html);
			$('.loading').hide();
		}
	});
}

function deactive_faq(id,start){
	$('.table-faq').hide();
	$('.loading').show();
	$.ajax({
		type : "POST",
		url  : "deactive_faq",
		data : {
			'id' 	: id,
			'start' : start
		},
		success:function(html){
			$('.table-faq').show();
			$('.table-faq').html(html);
			$('.loading').hide();
		}
	});
}

function delete_cart(id){
	$.ajax({
		type : "GET",
		url  : "./delete_cart",
		data : "id="+id,
		success:function(html){
			window.location.reload();
		}
	});
}

function create(){
	$('.loading2').show();
	var email = $('#email').val();
	var passw = $('#password').val();
	var rpass = $('#repassword').val();
	var fname = $('#first_name').val();
	var lname = $('#last_name').val();
	var gende = $('#gender').val();
	var phone = $('#phone').val();
	var day   = $('#day').val();
	var month = $('#month').val();
	var year  = $('#year').val();
	if(email == "" || passw == "" || rpass == "" || fname == "" || phone == "" || day == "" || month == "" || year == ""){
		$('#pesan').html('<div class="alert alert-danger" style="height:50px;padding-top:15px;"><h6><i class="fa fa-warning"></i> * CAN NOT BE EMPTY</h6></div>');
		$('.loading2').hide();
	}else{
		if(passw != rpass){
			$('#pesan').html('<div class="alert alert-danger" style="height:50px;padding-top:15px;"><h6> <i class="fa fa-warning"></i> PASSWORD AND RETYPE PASSWORD MUST BE SAMED</h6></div>');
			$('.loading2').hide();
		}else{
			$.ajax({
				type : "GET",
				url  : "addregister",
				data : {
					'email' : email,
					'passw' : passw,
					'rpass' : rpass,
					'fname' : fname,
					'lname' : lname,
					'gende'	: gende,
					'phone' : phone,
					'day'	: day,
					'month' : month,
					'year'  : year
				},
				success:function(html){
					$('#email').val("");
					$('#password').val("");
					$('#repassword').val("");
					$('#first_name').val("");
					$('#last_name').val("");
					$('#gender').val("");
					$('#phone').val("");
					$('#day').val("");
					$('#month').val("");
					$('#year').val("");
					$('#pesan').html('<div class="alert alert-success" style="height:50px;padding-top:15px;"><h6> <i class="fa fa-thumbs-o-up"></i> SUCCESSFULLY CREATE ACCOUNT</h6></div>');
					$('.loading2').hide();
				}
			});
		}
	}
}

function sign_in(){
	var email = $('.email').val();
	var passw = $('.password').val();
	if(email == "" || passw == ""){
		$('#pesansign').html('<div class="alert alert-danger" style="height:20px;padding-top:15px;"><h6 style="font-size:12px;"><i class="fa fa-warning"></i> NO FIELD EMPTY!</h6></div>')
	}else{
		$.ajax({
			type : "GET",
			url  : "login_user",
			data : {
				'email' 	: email,
				'password'  : passw
			},
			success:function(html){
				if(html == "gagal"){
					$('#pesansign').html('<div class="alert alert-danger" style="height:20px;padding-top:15px;"><h6 style="font-size:12px;"><i class="fa fa-warning"></i> ACCOUNT NOT EXIST!</h6></div>')
				}else if(html == "success"){
					window.location.reload();
				}
			}
		});
	}
}

function sign_user(){
	var email = $('#email').val();
	var passw = $('#password').val();
	if(email == "" || passw == ""){
		$('#pesansignf').html('<div class="alert alert-danger" style="height:20px;padding-top:15px;"><h6 style="font-size:12px;"><i class="fa fa-warning"></i> NO FIELD EMPTY!</h6></div>')
	}else{
		$.ajax({
			type : "GET",
			url  : "login_user",
			data : {
				'email' 	: email,
				'password'  : passw
			},
			success:function(html){
				if(html == "gagal"){
					$('#pesansignf').html('<div class="alert alert-danger" style="height:20px;padding-top:15px;"><h6 style="font-size:12px;"><i class="fa fa-warning"></i> ACCOUNT NOT EXIST!</h6></div>')
				}else if(html == "success"){
					window.location=('./checkout');
				}
			}
		});
	}
}

function save_address(){
	var first  = $('#fname').val();
	var last   = $('#lname').val();
	var phone  = $('#phone').val();
	var phone2 = $('#other').val();
	var addres = $('#address').val();
	var postal = $('#postal').val();
	var provin = $('#province').val();
	var city   = $('#city').val();
	var distri = $('#district').val();
	if(first == "" || addres == "" || postal == "" || provin == "" || city == ""){
		$('#pesansave').html("<div class='alert alert-danger' style='height:20px;padding-top:15px;'><h6 style='font-size:12px;'><i class='fa fa-warning'></i> (*) CAN'T BE EMPTY!</h6></div>");
	}else{
		$.ajax({
			type : "GET",
			url  : "save_address",
			data : {
				'first'  	: first,
				'last'   	: last,
				'phone'  	: phone,
				'phone2'	: phone2,
				'address'   : addres,
				'postal'  	: postal,
				'province'  : provin,
				'city'  	: city,
				'district'  : distri,
			},
			success:function(html){
				window.location.reload();
			}
		});
	}
}

function payment(pay){
	$.ajax({
		type : "GET",
		url  : "./payment",
		data : "pay="+pay,
		success:function(html){
			$('#Modalpay').modal('show');
			$('.modal-payment').html(html);
		}
	});
}

function agree(pay){
	var newWindow = window.open();
	$.ajax({
		type : "GET",
		url  : "./agree",
		data : "pay="+pay,
		success:function(html){
			newWindow.location = './invoice';
			window.location=('./');
		}
	});
}

function page_view_item(id){
	$('.page_jos').hide();
	$.ajax({
		type : "GET",
		url  : "./page_view_item",
		data : "id="+id,
		success:function(html){
			$('.page_jos').show();
			$('.page_jos').html(html);
		}
	});
}

function page_featured(id,brand){
	$('.page-feat').hide();
	var url = $('#url').val();
	$.ajax({
		type : "GET",
		url  : url,
		data : {
			'id'    : id,
			'brand' : brand,
		},
		success:function(html){
			$('.page-feat').show();
			$('.page-feat').html(html);
		}
	});
}

$(document).ready(function(){
	$('.hok').hide();
	$('.loading').hide();
	$('.loading2').hide();
});

function tambah_item(){
	$('.hok').show();
	$('.josbu').hide();
}

function tambah_brand(){
	$('.hok').show();
	$('.josbu').hide();
}

function tambah_supply(){
	$('.hok').show();
	$('.josbu').hide();
}

function tambah_categories(){
	$('.hok').show();
	$('.josbu').hide();
}

function tambah_shipping(){
	$('.hok').show();
	$('.josbu').hide();
}

function tambah_carreers(){
	$('.hok').show();
	$('.josbu').hide();
}

function tambah_term(){
	$('.hok').show();
	$('.josbu').hide();
}

function tambah_privacy(){
	$('.hok').show();
	$('.josbu').hide();
}

function tambah_contact(){
	$('.hok').show();
	$('.josbu').hide();
}

function tambah_faq(){
	$('.hok').show();
	$('.josbu').hide();
}

function hide_item(){
	$('.hok').hide();
	$('.josbu').show();
}

function hidex_item(){
	window.location.reload();
}


