

$( document ).ready(function() {
	$('.loading').hide();
});

function validAngka(a)
{
	if(!/^[0-9.]+$/.test(a.value))
	{
		a.value = a.value.substring(0,a.value.length-1000);
	}
}	

function getKota() {
	var prov = $('#provinsi2').val();
	$.ajax({
		type : "GET",
		data : "prov="+prov,
		url  : "./getKota",
		success:function(html){
			$('#kota2').html(html);
		}
	});
}

function getBarang(){
	var id_oleh = $('#id_oleh').val();
	$.ajax({
		type : "GET",
		data : "id_oleh="+id_oleh,
		url  : "./barange",
		success:function(html){
			$('#lol').html(html);
		}
	});		
}	

function getKota2() {
	var prov = $('#provinsi').val();
	$.ajax({
		type : "GET",
		data : "prov="+prov,
		url  : "./getKota2",
		success:function(html){
			$('#kota').html(html);
		}
	});
}	

function getEditKota2() {
	var prov = $('#provinsi').val();
	$.ajax({
		type : "GET",
		data : "prov="+prov,
		url  : "../getKota2",
		success:function(html){
			$('#kota').html(html);
		}
	});
}

function getEditKota() {
	var prov = $('.provinsi').val();
	$.ajax({
		type : "GET",
		data : "prov="+prov,
		url  : "../getKota",
		success:function(html){
			$('#kota').html(html);
		}
	});
}

function getCamat(){
	var city = $('#kota').val();
	$.ajax({
		type : "GET",
		data : "city="+city,
		url : "./getCamat",
		success:function(html){
			$('#kecamatan').html(html);
		}
	});
}

function getCamat2(){
	var city = $('#kota').val();
	$.ajax({
		type : "GET",
		data : "city="+city,
		url : "./getCamat",
		success:function(html){
			$('#kecamatan').html(html);
		}
	});
}	

function getLurah2(){
	var camat = $('#kecamatan').val();
	$.ajax({
		type : "GET",
		data : "camat="+camat,
		url : "./getLurah2",
		success:function(html){
			$('#kelurahan').html(html);
		}
	});
}	

function getEditCamat2(){
	var city = $('#kota').val();
	$.ajax({
		type : "GET",
		data : "city="+city,
		url : "../getCamat",
		success:function(html){
			$('#kecamatan').html(html);
		}
	});
}	


function getEditCamat(){
	var city = $('#kota').val();
	$.ajax({
		type : "GET",
		data : "city="+city,
		url : "../getCamat",
		success:function(html){
			$('#kecamatan').html(html);
		}
	});		
}

function getJurusan() {
	var fak = $('#fakultas').val();
	$.ajax({
		type : "GET",
		data : "fak="+fak,
		url  : "./getJurusan",
		success:function(html){
			$('#jurusan').html(html);
		}
	});
}

function getEditJurusan() {
	var fak = $('#fakultas').val();
	$.ajax({
		type : "GET",
		data : "fak="+fak,
		url  : "../getJurusan",
		success:function(html){
			$('#jurusan').html(html);
		}
	});
}

function close_modal(nama) {
	$('#'+nama).modal('hide');
}

function view_a(id){
	$.ajax({
		type : "post",
		url  : "./view_a",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}	


function view_oleh(id_oleh){
	$.ajax({
		type : "get",
		url  : "./view_oleh",
		data : "id_oleh="+id_oleh,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}

function view_detil(id_de){
	$.ajax({
		type : "get",
		url  : "./view_det",
		data : "id_de="+id_de,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}
function view_detil1(id_de_trim){
	$.ajax({
		type : "get",
		url  : "./view_det_trim",
		data : "id_de_trim="+id_de_trim,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}

function delete_a(id){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "./delete_a",
			data : "id="+id,
			success:function(html){
				window.location.reload();
			}
		});
	}else{

	}
}	

function tambah_oleh(){		
	var nama  		= $('#nama').val();
	var stok	    = $('#stok').val();
	var description = tinyMCE.get('description').getContent();
	var image_link	= $('#gambarout').val();

	if(nama==""){
		alert("nama Harus Diisi");
	}else if (stok==""){
		alert("Stok Harus Diisi");
	}else if (image_link==""){
		alert("Gambar Harus Dipilih");
	}else if (description==""){
		alert("Deskripsi Harus Diisi");
	}else{
		$.ajax({
			type : "get",
			url	 : "./tambah_oleh",
			data : {
				'nama'		 	: nama,
				'stok' 			: stok,
				'description'   : description,
				'image_link'	: image_link
			},
			success:function(html){
				window.location.href = "./merchandise";
			}
		});
	}
}


function tambah_stok(id_oleh){		
	var nama  		= $('#id_oleh').val();
	var stok	    = $('#stok').val();

	if(nama==""){
		alert("nama Barang Harus Di Pilih");
	}else if (stok==""){
		alert("Stok Harus Diisi");
	}else{
		$.ajax({
			type : "get",
			url	 : "tambah_stok",
			data : {
				'nama'		 	: nama,
				'stok' 			: stok
			},
			success:function(html){
				window.location.href = "./merchandise";
			}
		});
	}
}


function kirim_sovenir(){

	var nama  		= $('#id_oleh').val();
	var kirim	    = $('#kirim').val();
	var relawan 	= $('#id_relawan').val();
	var provinsi 	= $('#provinsi').val();
	var kota 		= $('#kota').val();
	var Kecamatan   = $('#kecamatan').val();
	var lokasi 		= $('#lokasi').val();
	var zero		= $('#lol').val();

	if(nama==""){
		alert("nama Barang Harus Di Pilih");
	}else if (kirim==""){
		alert("Stok Harus Diisi");
	}else if (relawan==""){
		alert("Relawan Harus Dipilih");
	}else if (provinsi==""){
		alert("Provinsi Harus Dipilih");
	}else if (kota==""){
		alert("Kota Harus DiPilih");
	}else if (lokasi==""){
		alert("Lokasi Harus Dipilih");
	}else if (zero == nama){
		alert("Stok Habis");
	}else{
		$.ajax({
			type : "get",
			url	 : "./kirim_barang",
			data : {
				'nama'		 	: nama,
				'kirim' 		: kirim,
				'relawan'		: relawan,
				'provinsi'		: provinsi,
				'kota'			: kota,
				'lokasi'		: lokasi
			},
			success:function(html){
					// window.location.href = "./merchandise";
				}
			});
	}
}		


function edit_a(id){		
	var judul  		= $('#judul').val();
	var date_start	= $('#date_start').val();
	var date_end	= $('#date_end').val();
	var description = tinyMCE.get('description').getContent();
	var status  	= $('#status').val();
	var kecamatan  	= $('#kecamatan').val();
	var kota   		= $('#kota').val();
	var provinsi 	= $('#provinsi').val();
	var lokasi 		= $('#lokasi').val();
	var image_link	= $('#gambarout').val();

	if(judul==""){
		alert("Judul Harus Diisi");
	}else if (date_start==""){
		alert("Tanggal Harus Diisi");
	}else if (provinsi==""){
		alert("provinsi Harus Diisi");
	}else if (kota==""){
		alert("Kota Harus Diisi");
	}else if (kecamatan==""){
		alert("Kecamatan Harus Diisi");
	}else if (lokasi==""){
		alert("Lokasi Harus Diisi");
	}else if (description==""){
		alert("Deskripsi Harus Diisi");
	}else{
		$.ajax({
			type : "post",
			url	 : "../update_a",
			data : {
				'judul'		 	: judul,
				'date_start'	: date_start,
				'description' 	: description,
				'status'		: status,
				'kota'			: kota,
				'provinsi'		: provinsi,
				'kecamatan'		: kecamatan,
				'lokasi'		: lokasi,
				'image_link'	: image_link,
				'id'	: id
			},
			success:function(html){
				window.location.href = "../agenda_data";
			}
		});
	}
}		

function edit_oleh(id_oleh){		
	var nama  		= $('#nama').val();
	var stok  		= $('#stok').val();
	var harga  		= $('#harga').val();
	var description = tinyMCE.get('description').getContent();
	var image_link	= $('#gambarout').val();

	if(nama==""){
		alert("Judul Harus Diisi");
	}else if (stok==""){
		alert("Stok Harus Diisi");
	}else if (harga==""){
		alert("Harga Harus Diisi");
	}else if (description==""){
		alert("Deskripsi Harus Diisi");
	}else{
		$.ajax({
			type : "get",
			url	 : "../update_oleh",
			data : {
				'nama'		 	: nama,
				'stok'		 	: stok,
				'harga'		 	: harga,
				'image_link'	: image_link,
				'description'	: description,
				'id_oleh'	: id_oleh
			},
			success:function(html){
				window.location.href = "../merchandise";
			}
		});
	}
}		

function delete_a(id){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "./delete_a",
			data : "id="+id,
			success:function(html){
				window.location.reload();
			}
		});
	}else{

	}
}	

function tambah_a(id){		
	var judul  		= $('#judul').val();
	var date_start	= $('#date_start').val();
	var description = tinyMCE.get('description').getContent();
	var status  	= $('#status').val();
	var kecamatan  	= $('#kecamatan').val();
	var kota   		= $('#kota').val();
	var provinsi 	= $('#provinsi').val();
	var lokasi 		= $('#lokasi').val();
	var image_link	= $('#gambarout').val();

	if(judul==""){
		alert("Judul Harus Diisi");
	}else if (date_start==""){
		alert("Tanggal Harus Diisi");
	}else if (provinsi==""){
		alert("provinsi Harus Diisi");
	}else if (kota==""){
		alert("Kota Harus Diisi");
	}else if (kecamatan==""){
		alert("Kecamatan Harus Diisi");
	}else if (lokasi==""){
		alert("Lokasi Harus Diisi");
	}else if (image_link==""){
		alert("Gambar Harus Dipilih");
	}else if (description==""){
		alert("Deskripsi Harus Diisi");
	}else{
		$.ajax({
			type : "post",
			url	 : "./tambah_a",
			data : {
				'judul'		 	: judul,
				'date_start'	: date_start,
				'description' 	: description,
				'status'		: status,
				'kota'			: kota,
				'provinsi'		: provinsi,
				'kecamatan'		: kecamatan,
				'lokasi'		: lokasi,
				'image_link'	: image_link
			},
			success:function(html){
				window.location.href = "./agenda_data";
			}
		});
	}
}		

function edit_a(id){		
	var judul  		= $('#judul').val();
	var date_start	= $('#date_start').val();
	var date_end	= $('#date_end').val();
	var description = tinyMCE.get('description').getContent();
	var status  	= $('#status').val();
	var kecamatan  	= $('#kecamatan').val();
	var kota   		= $('#kota').val();
	var provinsi 	= $('#provinsi').val();
	var lokasi 		= $('#lokasi').val();
	var image_link	= $('#gambarout').val();

	if(judul==""){
		alert("Judul Harus Diisi");
	}else if (date_start==""){
		alert("Tanggal Harus Diisi");
	}else if (provinsi==""){
		alert("provinsi Harus Diisi");
	}else if (kota==""){
		alert("Kota Harus Diisi");
	}else if (kecamatan==""){
		alert("Kecamatan Harus Diisi");
	}else if (lokasi==""){
		alert("Lokasi Harus Diisi");
	}else if (description==""){
		alert("Deskripsi Harus Diisi");
	}else{
		$.ajax({
			type : "post",
			url	 : "../update_a",
			data : {
				'judul'		 	: judul,
				'date_start'	: date_start,
				'description' 	: description,
				'status'		: status,
				'kota'			: kota,
				'provinsi'		: provinsi,
				'kecamatan'		: kecamatan,
				'lokasi'		: lokasi,
				'image_link'	: image_link,
				'id'	: id
			},
			success:function(html){
				window.location.href = "../agenda_data";
			}
		});
	}
}		


function delete_b(id_berita){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "../delete_b",
			data : "id_berita="+id_berita,
			success:function(html){
				window.location.reload();
			}
		});        
	} else {

	}	
}

function view_b(id_berita){
	$.ajax({
		type : "post",
		url  : "../../admin/view_b",
		data : "id_berita="+id_berita,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}		


function submitImage(){
	$("#frmuploadImg").submit();
}

function tambah_b(id_berita){		
	var judul  		= $('#judul').val();
	var isi 		= tinyMCE.get('isi').getContent();
	var image_link	= $('#gambarout').val();
	var url_link	= $('#url_link').val();
	var tanggal		= $('#tanggal').val();

	if (judul=="") {
		alert("Judul Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else if(image_link=="") {
		alert("Foto Harus Di Pilih");
	} else{
		$.ajax({
			type : "post",
			url	 : "../../tambah_b",
			data : {
				'judul'		 	: judul,
				'isi'			: isi,
				'image_link'	: image_link,
				'url_link'		: url_link,
				'create_date'	: tanggal
			},
			success:function(html){
				window.location.href = "../../profil/news";
			}
		});
	}
}

function unto(){		
	var judul  		= $('#judul').val();
	var isi 		= tinyMCE.get('isi').getContent();
	var tanggal		= $('#tanggal').val();

	if (judul=="") {
		alert("Judul Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else{
		$.ajax({
			type : "get",
			url	 : "./moto_tok",
			data : {		
				'judul'		 	: judul,
				'deskripsi'			: isi,
				'create_date'	: tanggal
			},
			success:function(html){
				window.location.href = "./moto_tampil";
			}
		});
	}
}        



function kadal(id){		
	var judul  		= $('#judul').val();
	var isi 		= tinyMCE.get('isi').getContent();
	var tanggal		= $('#tanggal').val();

	if (judul=="") {
		alert("Judul Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else{
		$.ajax({
			type : "get",
			url	 : "../moto_update",
			data : {
				'id'			: id,		
				'judul'		 	: judul,
				'deskripsi'			: isi,
				'create_date'	: tanggal
			},
			success:function(html){
				window.location.href = "../moto_tampil";
			}
		});
	}
}    

function edit_b(id_berita){		
	var judul  		= $('#judul').val();
	var isi 		= tinyMCE.get('isi').getContent();
	var image_link	= $('#gambarout').val();
	var url_link	= $('#url_link').val();
	var tanggal		= $('#tanggal').val();

	if (judul=="") {
		alert("Judul Harus Diisi");
	} else if(tanggal=="") {
		alert("Tanggal Posting Harus Di Isi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else{

		$.ajax({
			type : "post",
			url	 : "../update_b",
			data : {
				'judul'		 	: judul,
				'isi'			: isi,
				'image_link'	: image_link,
				'url_link'		: url_link,
				'tanggal'		: tanggal,
				'id_berita'		: id_berita
			},
			success:function(html){
				window.location.href = "../berita_data";
			}
		});
	}
}    

function delete_bs(id){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "./delete_bs",
			data : "id="+id,
			success:function(html){
				window.location.reload();
			}
		});
	}else{

	}
}		

function view_bs(id){
	$.ajax({
		type : "post",
		url  : "./view_bs",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}

function view_moto(id){
	$.ajax({
		type : "get",
		url  : "./view_moto",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}

function edit_bs(id){		
	var name  			= $('#name').val();
	var kategori 		= $('#kategori').val();
	var alumni 			= $('#alumni').val();
	var stok 			= $('#stok').val();
	var harga 			= $('#harga').val();
	var deskripsi 		= tinyMCE.get('deskripsi').getContent();
	var image_link		= $('#gambarout').val();

	if (name=="") {
		alert("Nama Barang Harus Diisi");
	} else if(kategori=="") {
		alert("Kategori Harus Diisi");
	} else if(stok=="") {
		alert("Stok Harus Diisi");
	} else if(harga=="") {
		alert("Harga Penawaran Harus Diisi");
	} else if(deskripsi=="") {
		alert("Deskripsi Harus Diisi");
	} else {
		$.ajax({
			type : "post",
			url	 : "../update_bs",
			data : {
				'name'		 	: name,
				'kategori'		: kategori,
				'alumni'		: alumni,
				'stok'			: stok,
				'harga'			: harga,
				'image_link'	: image_link,
				'deskripsi'		: deskripsi,
				'id'			: id
			},
			success:function(html){
				window.location.href = "../bisnis_data";
			}
		});
	}
}    

function tambah_bs(id){		
	var name  			= $('#name').val();
	var kategori 		= $('#kategori').val();
	var alumni 			= $('#alumni').val();
	var stok 			= $('#stok').val();
	var harga 			= $('#harga').val();
	var deskripsi 		= tinyMCE.get('deskripsi').getContent();
	var image_link		= $('#gambarout').val();

	if (name=="") {
		alert("Nama Barang Harus Diisi");
	} else if(kategori=="") {
		alert("Kategori Harus Diisi");
	} else if(stok=="") {
		alert("Stok Harus Diisi");
	} else if(harga=="") {
		alert("Harga Penawaran Harus Diisi");
	} else if(deskripsi=="") {
		alert("Deskripsi Harus Diisi");
	} else if(image_link=="") {
		alert("Gambar Harus Di Pilih");
	} else {
		$.ajax({
			type : "post",
			url	 : "./tambah_bs",
			data : {
				'name'		 	: name,
				'kategori'		: kategori,
				'alumni'		: alumni,
				'stok'			: stok,
				'harga'			: harga,
				'image_link'	: image_link,
				'deskripsi'		: deskripsi
			},
			success:function(html){
				window.location.href = "./bisnis_data";
			}
		});
	}
}  	

function view_p(id){
	$.ajax({
		type : "post",
		url  : "./view_p",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}	

function tambah_p(id){		
	var judul  			= $('#judul').val();
	var isi 			= tinyMCE.get('isi').getContent();
	var image_link		= $('#gambarout').val();

	if(judul=="") {
		alert("Judu, Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else if(image_link=="") {
		alert("Gambar Harus Di Pilih");
	} else {
		$.ajax({
			type : "post",
			url	 : "./tambah_p",
			data : {
				'judul'		 	: judul,
				'image_link'	: image_link,
				'isi'			: isi
			},
			success:function(html){
				window.location.href = "./pandang_data";
			}
		});
	}
} 	

function edit_p(id){		
	var judul  			= $('#judul').val();
	var id  			= $('#id').val();
	var isi 			= tinyMCE.get('isi').getContent();
	var image_link		= $('#gambarout').val();

	if(judul=="") {
		alert("Judu, Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else {
		$.ajax({
			type : "post",
			url	 : "../edit_p",
			data : {
				'judul'		 	: judul,
				'image_link'	: image_link,
				'isi'			: isi,
				'id'			: id
			},
			success:function(html){
				window.location.href = "../pandang_data";
			}
		});
	}
} 	

function delete_p(id){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "./delete_p",
			data : "id="+id,
			success:function(html){
				window.location.reload();
			}
		});
	}else{

	}
}

function view_s(id){
	$.ajax({
		type : "post",
		url  : "./view_s",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}	

function tambah_s(id){		
	var judul  			= $('#judul').val();
	var isi 			= tinyMCE.get('isi').getContent();
	var image_link		= $('#gambarout').val();

	if(judul=="") {
		alert("Judu, Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else if(image_link=="") {
		alert("Gambar Harus Di Pilih");
	} else {
		$.ajax({
			type : "post",
			url	 : "./tambah_s",
			data : {
				'judul'		 	: judul,
				'image_link'	: image_link,
				'isi'			: isi
			},
			success:function(html){
				window.location.href = "./sejarah_data";
			}
		});
	}
} 	

function edit_s(id){		
	var judul  			= $('#judul').val();
	var id  			= $('#id').val();
	var isi 			= tinyMCE.get('isi').getContent();
	var image_link		= $('#gambarout').val();

	if(judul=="") {
		alert("Judu, Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else {
		$.ajax({
			type : "post",
			url	 : "../edit_s",
			data : {
				'judul'		 	: judul,
				'image_link'	: image_link,
				'isi'			: isi,
				'id'			: id
			},
			success:function(html){
				window.location.href = "../sejarah_data";
			}
		});
	}
} 

function delete_s(id){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "./delete_s",
			data : "id="+id,
			success:function(html){
				window.location.reload();
			}
		});
	}else{

	}
}

function view_st(id){
	$.ajax({
		type : "post",
		url  : "./view_st",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}		

function tambah_st(id){		
	var image_link		= $('#gambarout').val();

	if(image_link=="") {
		alert("Gambar Harus Di Pilih");
	} else {
		$.ajax({
			type : "post",
			url	 : "./tambah_st",
			data : {
				'image_link'	: image_link
			},
			success:function(html){
				window.location.href = "./struktur_data";
			}
		});
	}
} 	

function edit_st(id){		
	var id  			= $('#id').val();
	var image_link		= $('#gambarout').val();


	if(image_link=="") {
		alert("Gambar Harus Diisi");
	} else {
		$.ajax({
			type : "post",
			url	 : "../edit_st",
			data : {
				'image_link'	: image_link,
				'id'			: id
			},
			success:function(html){
				window.location.href = "../struktur_data";
			}
		});
	}
} 

function delete_st(id){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "./delete_st",
			data : "id="+id,
			success:function(html){
				window.location.reload();
			}
		});
	}else{

	}
}	

function view_be(id){
	$.ajax({
		type : "post",
		url  : "./view_be",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}	

function tambah_be(id){		
	var judul  		= $('#judul').val();
	var isi 		= tinyMCE.get('isi').getContent();
	var image_link	= $('#gambarout').val();
	var url_link	= $('#url_link').val();

	if (judul=="") {
		alert("Judul Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else if(image_link=="") {
		alert("Foto Harus Di Pilih");
	} else{
		$.ajax({
			type : "post",
			url	 : "./tambah_be",
			data : {
				'judul'		 	: judul,
				'isi'			: isi,
				'image_link'	: image_link,
				'url_link'		: url_link
			},
			success:function(html){
				window.location.href = "./beasiswa_data";
			}
		});
	}
}    

function edit_be(id){		
	var judul  		= $('#judul').val();
	var isi 		= tinyMCE.get('isi').getContent();
	var image_link	= $('#gambarout').val();

	if (judul=="") {
		alert("Judul Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else{

		$.ajax({
			type : "post",
			url	 : "../edit_be",
			data : {
				'judul'		 	: judul,
				'isi'			: isi,
				'image_link'	: image_link,
				'id'			: id
			},
			success:function(html){
				window.location.href = "../beasiswa_data";
			}
		});
	}
}    

function delete_be(id){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "./delete_be",
			data : "id="+id,
			success:function(html){
				window.location.reload();
			}
		});        
	} else {

	}	
}

function view_kinerja(id_kinerja){
	$.ajax({
		type : "get",
		url  : "./view_kinerja",
		data : "id_kinerja="+id_kinerja,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}		

function view_tk(id){
	$.ajax({
		type : "post",
		url  : "./view_tk",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}	

function tambah_tk(id){		
	var judul  		= $('#judul').val();
	var isi 		= tinyMCE.get('isi').getContent();
	var image_link	= $('#gambarout').val();

	if (judul=="") {
		alert("Judul Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else if(image_link=="") {
		alert("Foto Harus Di Pilih");
	} else{
		$.ajax({
			type : "post",
			url	 : "./tambah_tk",
			data : {
				'judul'		 	: judul,
				'isi'			: isi,
				'image_link'	: image_link
			},
			success:function(html){
				window.location.href = "./tips_karir_data";
			}
		});
	}
}  	

function edit_tk(id){		
	var judul  		= $('#judul').val();
	var isi 		= tinyMCE.get('isi').getContent();
	var image_link	= $('#gambarout').val();

	if (judul=="") {
		alert("Judul Harus Diisi");
	} else if(isi=="") {
		alert("Isi Harus Diisi");
	} else{

		$.ajax({
			type : "post",
			url	 : "../edit_tk",
			data : {
				'judul'		 	: judul,
				'isi'			: isi,
				'image_link'	: image_link,
				'id'			: id
			},
			success:function(html){
				window.location.href = "../tips_karir_data";
			}
		});
	}
}

function edit_relawan(id_relawan){		
	var nama  		= $('#nama').val();
	var agama 		= $('#agama').val();
	var alamat 		= $('#alamat').val();
	var no_hp		= $('#no_hp').val();

	if (nama=="") {
		alert("Nama Harus Diisi");
	}else if(agama=="") {
		alert("Agama Harus Di Pilih");
	}else if(alamat=="") {
		alert("Alamat Harus Diisi");
	}else if(no_hp=="") {
		alert("No HP Harus Diisi");
	}else{

		$.ajax({
			type : "get",
			url	 : "../update_relawan",
			data : {
				'nama'		 	: nama,
				'agama'			: agama,
				'alamat'		: alamat,
				'no_hp'			: no_hp,
				'id_relawan'    : id_relawan
			},
			success:function(html){
				window.location.href = 
				"../relawan_data";
			}
		});
	}
}    

function delete_tk(id){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "./delete_tk",
			data : "id="+id,
			success:function(html){
				window.location.reload();
			}
		});        
	} else {

	}	
}



function view_h(id){
	$.ajax({
		type : "post",
		url  : "./view_h",
		data : "id="+id,
		success:function(html){
			$('#myModal').modal('show');
			$('.viewitem').html(html);
		}
	});
}	

function tambah_h(id){		
	var deskripsi  		= $('#deskripsi').val();
	var image_link		= $('#gambarout').val();

	if (deskripsi=="") {
		alert("Deskripsi Harus Diisi");
	} else if(image_link=="") {
		alert("Foto Harus Di Pilih");
	} else{
		$.ajax({
			type : "post",
			url	 : "./tambah_h",
			data : {
				'deskripsi'		 	: deskripsi,
				'image_link'		: image_link
			},
			success:function(html){
				window.location.href = "./moto_tampil";
			}
		});
	}
}  	

function edit_h(id){		
	var deskripsi  		= $('#deskripsi').val();
	var image_link	= $('#gambarout').val();

	if (deskripsi=="") {
		alert("Deskripsi Harus Diisi");
	} else{

		$.ajax({
			type : "post",
			url	 : "../edit_h",
			data : {
				'deskripsi'		 	: deskripsi,
				'image_link'		: image_link,
				'id'				: id
			},
			success:function(html){
				window.location.href = "../moto_tampil";
			}
		});
	}
}    

function delete_h(id){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "./delete_h",
			data : "id="+id,
			success:function(html){
				window.location.reload();
			}
		});        
	} else {

	}	
}

function delete_moto(id){
	if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
		$.ajax({
			type : "GET",
			url  : "./delete_moto",
			data : "id="+id,
			success:function(html){
				window.location.reload();
			}
		});        
	} else {

	}	
}		

function getLulus(){
	var s = document.getElementById('status');
	var item1 = s.options[s.selectedIndex].value;

	if (item1 == "Mahasiswa") {
		$('.hok').hide();
	}
	else if (item1 == "Alumni") {
		$('.hok').show();
	}else{
		$('.hok').hide();	
	}
}		

function getStatus(){
	var item1 = $('#status').val();	
	$.ajax({
		type : "GET",
		url	 : "./status_1",
		data : {
			'status'		 	: item1
		},
		success:function(html){
			$('#example2').html(html);

		}
	});		    
}	

function getStatusUsr(){
	var item1 = $('#status').val();	
	$.ajax({
		type : "GET",
		url	 : "./status_usr",
		data : {
			'status'		 	: item1
		},
		success:function(html){
			$('#example2').html(html);

		}
	});		    
}				

function getTanggal(){
	var  tanggal= $('#tanggal').val();	
	$.ajax({
		type : "GET",
		url	 : "./tanggal",
		data : {
			'tanggal'		 	: tanggal
		},
		success:function(html){
			$('#example2').html(html);

		}
	});		    
}	

