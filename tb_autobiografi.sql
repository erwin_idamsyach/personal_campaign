/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : 127.0.0.1:3306
Source Database       : db_personal

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-08-25 21:56:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_autobiografi
-- ----------------------------
DROP TABLE IF EXISTS `tb_autobiografi`;
CREATE TABLE `tb_autobiografi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `auto_for` int(5) DEFAULT NULL,
  `autobiografi` text,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_autobiografi
-- ----------------------------
INSERT INTO `tb_autobiografi` VALUES ('1', '1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit aliquam ex culpa dolorem enim iste ab itaque odio, accusamus laboriosam, illo. Necessitatibus autem quis iure magnam consectetur consequuntur? Numquam, eos.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.velit esse cillum dolore eu fugiat', '');
SET FOREIGN_KEY_CHECKS=1;
