/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : db_personal

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-08-31 15:26:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for caleg_drh
-- ----------------------------
DROP TABLE IF EXISTS `caleg_drh`;
CREATE TABLE `caleg_drh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `tingkat_provinsi` bigint(255) DEFAULT NULL,
  `tingkat_kabupaten` bigint(255) DEFAULT '0',
  `parent` int(11) DEFAULT NULL,
  `nama_depan` varchar(30) DEFAULT NULL,
  `nama_tengah` varchar(30) DEFAULT NULL,
  `nama_belakang` varchar(30) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jenis_identitas` varchar(225) DEFAULT NULL,
  `nomer_identitas` varchar(20) DEFAULT '11389837983',
  `tempat_lahir` varchar(200) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(225) DEFAULT NULL,
  `agama` varchar(225) DEFAULT 'Islam',
  `alamat` text,
  `provinsi` int(11) DEFAULT NULL,
  `kabupaten` int(11) DEFAULT NULL,
  `kecamatan` int(11) DEFAULT NULL,
  `kelurahan` bigint(20) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `handphone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `status_kawin` varchar(255) DEFAULT 'Belum',
  `pasangan` varchar(255) DEFAULT NULL,
  `anak` int(11) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `no_urut` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `note` tinytext,
  `file_riwayat` varchar(255) DEFAULT NULL,
  `file_visi` varchar(255) DEFAULT NULL,
  `file_ktp` varchar(255) DEFAULT NULL,
  `file_kk` varchar(255) DEFAULT NULL,
  `file_npwp` varchar(255) DEFAULT NULL,
  `file_ijazah` varchar(255) DEFAULT NULL,
  `file_skck` varchar(255) DEFAULT NULL,
  `file_kta` varchar(255) DEFAULT NULL,
  `file_pendaftaran` varchar(255) DEFAULT NULL,
  `file_komitmen` varchar(255) DEFAULT NULL,
  `file_peryataan` varchar(255) DEFAULT NULL,
  `file_tidak_pailit` varchar(255) DEFAULT NULL,
  `file_nkri` varchar(255) DEFAULT NULL,
  `flag` enum('Balon','Calon') DEFAULT 'Balon',
  `no_sk` varchar(255) DEFAULT NULL,
  `foto_sk` varchar(255) DEFAULT NULL,
  `status_kader` varchar(20) DEFAULT NULL,
  `foto_komitmen` varchar(255) DEFAULT NULL,
  `tahun_aktif` varchar(10) DEFAULT NULL,
  `kategori` char(1) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `tingkat_id` int(11) DEFAULT NULL,
  `position_group_id` int(11) DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=302 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for caleg_organisasi
-- ----------------------------
DROP TABLE IF EXISTS `caleg_organisasi`;
CREATE TABLE `caleg_organisasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caleg_drh_id` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1649 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for caleg_pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `caleg_pendidikan`;
CREATE TABLE `caleg_pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caleg_drh_id` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `keterangan` text,
  `tingkat` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1605 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for detil_mercendise
-- ----------------------------
DROP TABLE IF EXISTS `detil_mercendise`;
CREATE TABLE `detil_mercendise` (
  `id_de` int(11) NOT NULL AUTO_INCREMENT,
  `id_oleh` int(11) NOT NULL,
  `jml_kirim` int(11) NOT NULL,
  `id_relawan` int(11) NOT NULL,
  `akses` varchar(100) NOT NULL,
  `tgl_trans` date NOT NULL,
  `jml_terima` int(20) NOT NULL,
  `provinsiId` int(11) NOT NULL,
  `kabupatenId` int(11) NOT NULL,
  `kecamatanId` int(11) DEFAULT NULL,
  `lokasi` varchar(255) NOT NULL,
  PRIMARY KEY (`id_de`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for detil_mercendise_copy
-- ----------------------------
DROP TABLE IF EXISTS `detil_mercendise_copy`;
CREATE TABLE `detil_mercendise_copy` (
  `id_de_trim` int(11) NOT NULL AUTO_INCREMENT,
  `id_oleh` int(11) NOT NULL,
  `akses` varchar(100) NOT NULL,
  `tgl_trans` date NOT NULL,
  `jml_terima` int(20) NOT NULL,
  PRIMARY KEY (`id_de_trim`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_bulan
-- ----------------------------
DROP TABLE IF EXISTS `m_bulan`;
CREATE TABLE `m_bulan` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `bulan` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_calon
-- ----------------------------
DROP TABLE IF EXISTS `m_calon`;
CREATE TABLE `m_calon` (
  `calon_id` int(11) NOT NULL AUTO_INCREMENT,
  `calon_nama_depan` varchar(50) DEFAULT NULL,
  `calon_nama_tengah` varchar(50) DEFAULT NULL,
  `calon_nama_belakang` varchar(50) DEFAULT NULL,
  `calon_nama_gelar` varchar(2) DEFAULT NULL,
  `calon_jenis_identitas` bigint(2) DEFAULT NULL,
  `calon_nomer_identitas` bigint(20) DEFAULT NULL,
  `calon_tempat_lahir` varchar(50) DEFAULT NULL,
  `calon_tanggal_lahir` date DEFAULT '1980-01-01',
  `calon_jenis_kelamin` bigint(2) DEFAULT NULL,
  `calon_agama` bigint(2) DEFAULT NULL,
  `calon_alamat` varchar(255) DEFAULT NULL,
  `calon_provinsi` bigint(20) DEFAULT NULL,
  `calon_kabupaten` bigint(20) DEFAULT NULL,
  `calon_kecamatan` bigint(20) DEFAULT NULL,
  `calon_kelurahan` bigint(20) DEFAULT NULL,
  `calon_telephone` varchar(20) DEFAULT NULL,
  `calon_handphone` varchar(20) DEFAULT NULL,
  `calon_email` varchar(255) DEFAULT NULL,
  `calon_facebook` varchar(255) DEFAULT NULL,
  `calon_twitter` varchar(255) DEFAULT NULL,
  `calon_status_kawin` int(2) DEFAULT NULL,
  `calon_nama_pasangan` varchar(50) DEFAULT NULL,
  `calon_anak` int(2) DEFAULT NULL,
  `calon_foto` varchar(50) DEFAULT NULL,
  `calon_created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `calon_created_by` varchar(10) DEFAULT NULL,
  `menjabat` tinyint(1) DEFAULT NULL,
  `calon_facebook_id` varchar(255) DEFAULT NULL,
  `calon_google_id` varchar(255) DEFAULT NULL,
  `calon_status` varchar(10) DEFAULT NULL,
  `calon_flag` int(5) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`calon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_dapil
-- ----------------------------
DROP TABLE IF EXISTS `m_dapil`;
CREATE TABLE `m_dapil` (
  `dapil_id` int(11) NOT NULL AUTO_INCREMENT,
  `tingkat_dapil` smallint(6) DEFAULT NULL,
  `pro_id` int(11) DEFAULT NULL,
  `kab_id` int(11) DEFAULT NULL,
  `kode_dapil` bigint(20) DEFAULT NULL,
  `nama_dapil` varchar(25) DEFAULT NULL,
  `ppk` int(11) DEFAULT NULL,
  `pps` int(11) DEFAULT NULL,
  `tps` int(11) DEFAULT NULL,
  `jumlah_laki` int(11) DEFAULT NULL,
  `jumlah_perempuan` int(11) DEFAULT NULL,
  `jumlah_pemilih` int(11) DEFAULT NULL,
  `dapil_kode` varchar(20) DEFAULT NULL,
  `dapil_nama` varchar(50) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` mediumint(9) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`dapil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2472 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_geo_deskel_kpu
-- ----------------------------
DROP TABLE IF EXISTS `m_geo_deskel_kpu`;
CREATE TABLE `m_geo_deskel_kpu` (
  `geo_deskel_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `geo_prov_id` int(10) DEFAULT NULL,
  `geo_kab_id` int(10) DEFAULT NULL,
  `geo_kec_id` int(10) DEFAULT NULL,
  `geo_deskel_nama` varchar(40) DEFAULT NULL,
  `geo_deskel_created_date` datetime DEFAULT NULL,
  `geo_deskel_created_by` int(10) DEFAULT NULL,
  `geo_deskel_status` tinyint(2) DEFAULT NULL,
  `geo_deskel_lat` varchar(255) DEFAULT NULL,
  `geo_deskel_lng` varchar(255) DEFAULT NULL,
  `geo_deskel_status_baru` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`geo_deskel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9471040013 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_geo_kab_kpu
-- ----------------------------
DROP TABLE IF EXISTS `m_geo_kab_kpu`;
CREATE TABLE `m_geo_kab_kpu` (
  `geo_kab_id` int(10) NOT NULL AUTO_INCREMENT,
  `geo_prov_id` int(10) DEFAULT NULL,
  `geo_kab_nama` varchar(50) DEFAULT NULL,
  `geo_kab_created_date` datetime DEFAULT NULL,
  `geo_kab_created_by` int(10) DEFAULT NULL,
  `geo_kab_status` tinyint(2) DEFAULT NULL,
  `geo_kab_lat` varchar(255) DEFAULT NULL,
  `geo_kab_lng` varchar(255) DEFAULT NULL,
  `geo_kab_status_baru` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`geo_kab_id`)
) ENGINE=InnoDB AUTO_INCREMENT=928092 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_geo_kec_kpu
-- ----------------------------
DROP TABLE IF EXISTS `m_geo_kec_kpu`;
CREATE TABLE `m_geo_kec_kpu` (
  `geo_kec_id` int(20) NOT NULL AUTO_INCREMENT,
  `geo_prov_id` int(10) DEFAULT NULL,
  `geo_kab_id` int(10) DEFAULT NULL,
  `geo_kec_nama` varchar(50) DEFAULT NULL,
  `geo_kec_created_date` datetime DEFAULT NULL,
  `geo_kec_created_by` int(10) DEFAULT NULL,
  `geo_kec_status` tinyint(2) DEFAULT NULL,
  `geo_kec_lat` varchar(255) DEFAULT NULL,
  `geo_kec_lng` varchar(255) DEFAULT NULL,
  `geo_kec_status_baru` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`geo_kec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=929217 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_geo_prov_kpu
-- ----------------------------
DROP TABLE IF EXISTS `m_geo_prov_kpu`;
CREATE TABLE `m_geo_prov_kpu` (
  `geo_prov_id` int(10) NOT NULL AUTO_INCREMENT,
  `geo_prov_nama` varchar(50) DEFAULT NULL,
  `geo_prov_created_date` datetime DEFAULT NULL,
  `geo_prov_created_by` int(10) DEFAULT NULL,
  `geo_prov_status` tinyint(2) DEFAULT NULL,
  `geo_prov_lat` varchar(255) DEFAULT NULL,
  `geo_prov_lng` varchar(255) DEFAULT NULL,
  `geo_prov_status_baru` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`geo_prov_id`)
) ENGINE=InnoDB AUTO_INCREMENT=928069 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_geo_tps
-- ----------------------------
DROP TABLE IF EXISTS `m_geo_tps`;
CREATE TABLE `m_geo_tps` (
  `geo_tps_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_prov_id` int(3) DEFAULT NULL,
  `geo_kab_id` int(10) DEFAULT NULL,
  `geo_kec_id` int(20) DEFAULT NULL,
  `geo_deskel_id` int(25) DEFAULT NULL,
  `geo_tps_nama` varchar(50) DEFAULT NULL,
  `geo_tps_created_date` varchar(255) DEFAULT NULL,
  `geo_tps_created_by` tinyint(2) DEFAULT NULL,
  `geo_tps_status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`geo_tps_id`)
) ENGINE=InnoDB AUTO_INCREMENT=900 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_penduduk
-- ----------------------------
DROP TABLE IF EXISTS `m_penduduk`;
CREATE TABLE `m_penduduk` (
  `penduduk_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_prov_id` int(11) DEFAULT NULL,
  `geo_kab_id` int(11) DEFAULT NULL,
  `penduduk_pria` int(11) DEFAULT NULL,
  `penduduk_wanita` int(11) DEFAULT NULL,
  `penduduk_jumlah` int(11) DEFAULT NULL,
  `penduduk_tahun` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`penduduk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=505 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for m_relawan
-- ----------------------------
DROP TABLE IF EXISTS `m_relawan`;
CREATE TABLE `m_relawan` (
  `id_relawan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `no_hp` varchar(30) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `alamat` varchar(255) NOT NULL,
  `provinsi` int(15) DEFAULT NULL,
  `kota` int(15) DEFAULT NULL,
  `kecamatan` int(15) DEFAULT NULL,
  `kelurahan` int(15) DEFAULT NULL,
  `location_lat` varchar(255) DEFAULT NULL,
  `location_long` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `akses` varchar(100) NOT NULL,
  `diterima` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_relawan`),
  UNIQUE KEY `no_hp` (`no_hp`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_tahun
-- ----------------------------
DROP TABLE IF EXISTS `m_tahun`;
CREATE TABLE `m_tahun` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `tahun` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_tanggal
-- ----------------------------
DROP TABLE IF EXISTS `m_tanggal`;
CREATE TABLE `m_tanggal` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `tgl` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for m_tingkat_pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `m_tingkat_pendidikan`;
CREATE TABLE `m_tingkat_pendidikan` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `tingkat` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ref_agama
-- ----------------------------
DROP TABLE IF EXISTS `ref_agama`;
CREATE TABLE `ref_agama` (
  `agama_id` int(11) NOT NULL AUTO_INCREMENT,
  `agama_label` varchar(255) DEFAULT NULL,
  `agama_deskripsi` varchar(255) NOT NULL,
  `agama_stat_input` int(11) DEFAULT NULL,
  `agama_stat_input_time` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`agama_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ref_desa
-- ----------------------------
DROP TABLE IF EXISTS `ref_desa`;
CREATE TABLE `ref_desa` (
  `desaId` bigint(20) NOT NULL AUTO_INCREMENT,
  `geo_deskel_id2` int(11) DEFAULT NULL,
  `provinsiId` tinyint(3) DEFAULT NULL,
  `kabupatenId` tinyint(10) DEFAULT NULL,
  `kecamatanId` int(11) DEFAULT NULL,
  `desaNama` varchar(40) DEFAULT NULL,
  `geo_deskel_created_date` datetime DEFAULT NULL,
  `geo_deskel_created_by` tinyint(2) DEFAULT NULL,
  `geo_deskel_status` tinyint(2) DEFAULT NULL,
  `geo_deskel_lat` varchar(255) DEFAULT NULL,
  `geo_deskel_lng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`desaId`)
) ENGINE=MyISAM AUTO_INCREMENT=9471040012 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ref_identitas
-- ----------------------------
DROP TABLE IF EXISTS `ref_identitas`;
CREATE TABLE `ref_identitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identitas` varchar(255) DEFAULT NULL,
  `deskripsi_identitas` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ref_jenis_calon
-- ----------------------------
DROP TABLE IF EXISTS `ref_jenis_calon`;
CREATE TABLE `ref_jenis_calon` (
  `jenis_calon_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pemilihan_id` int(11) DEFAULT NULL,
  `jenis_calon_nama` varchar(50) DEFAULT NULL,
  `jenis_calon_created_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `jenis_calon_created_by` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`jenis_calon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ref_jenis_kursi
-- ----------------------------
DROP TABLE IF EXISTS `ref_jenis_kursi`;
CREATE TABLE `ref_jenis_kursi` (
  `jenis_kursi_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_kursi_value` varchar(50) DEFAULT NULL,
  `jenis_kursi_created_by` int(11) DEFAULT NULL,
  `jenis_kursi_created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`jenis_kursi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ref_jenis_pemilihan
-- ----------------------------
DROP TABLE IF EXISTS `ref_jenis_pemilihan`;
CREATE TABLE `ref_jenis_pemilihan` (
  `jenis_pemilihan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pemilihan_nama` varchar(50) DEFAULT NULL,
  `jenis_pemilihan_created_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `jenis_pemilihan_created_by` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`jenis_pemilihan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ref_kabupaten
-- ----------------------------
DROP TABLE IF EXISTS `ref_kabupaten`;
CREATE TABLE `ref_kabupaten` (
  `kabupatenId` int(10) NOT NULL,
  `geo_kab_id2` varchar(255) DEFAULT NULL,
  `provinsiId` int(20) DEFAULT NULL,
  `kabupatenNama` varchar(50) DEFAULT NULL,
  `geo_kab_created_date` varchar(50) DEFAULT NULL,
  `geo_kab_created_by` int(2) DEFAULT NULL,
  `geo_kab_status` tinyint(2) DEFAULT NULL,
  `geo_kab_lat` varchar(255) DEFAULT NULL,
  `geo_kab_lng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kabupatenId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ref_kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `ref_kecamatan`;
CREATE TABLE `ref_kecamatan` (
  `kecamatanId` int(20) NOT NULL AUTO_INCREMENT,
  `geo_kec_id2` varchar(255) DEFAULT NULL,
  `provinsiId` int(3) DEFAULT NULL,
  `kabupatenId` int(10) DEFAULT NULL,
  `kecamatanNama` varchar(50) DEFAULT NULL,
  `geo_kec_created_date` varchar(50) DEFAULT NULL,
  `geo_kec_created_by` tinyint(2) DEFAULT NULL,
  `geo_kec_status` tinyint(2) DEFAULT NULL,
  `geo_kec_lat` varchar(255) DEFAULT NULL,
  `geo_kec_lng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kecamatanId`)
) ENGINE=InnoDB AUTO_INCREMENT=47734 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ref_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `ref_provinsi`;
CREATE TABLE `ref_provinsi` (
  `provinsiId` int(11) NOT NULL AUTO_INCREMENT,
  `geo_prov_id2` varchar(255) DEFAULT NULL,
  `provinsiNama` varchar(50) DEFAULT NULL,
  `geo_prov_created_date` datetime DEFAULT NULL,
  `geo_prov_created_by` tinyint(6) DEFAULT NULL,
  `geo_prov_status` tinyint(2) DEFAULT NULL,
  `geo_prov_lat` varchar(255) DEFAULT NULL,
  `geo_prov_lng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`provinsiId`)
) ENGINE=InnoDB AUTO_INCREMENT=928069 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ref_tps
-- ----------------------------
DROP TABLE IF EXISTS `ref_tps`;
CREATE TABLE `ref_tps` (
  `tpsId` int(11) NOT NULL,
  `desaId` int(11) DEFAULT NULL,
  `desaIdkw2` bigint(30) DEFAULT NULL,
  `desaIdkw1` varchar(255) DEFAULT NULL,
  `tpsNama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tpsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_agenda
-- ----------------------------
DROP TABLE IF EXISTS `tb_agenda`;
CREATE TABLE `tb_agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_german1_ci NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `description` mediumtext NOT NULL,
  `provinsi` varchar(11) NOT NULL,
  `kabupaten` varchar(11) NOT NULL,
  `kecamatan` varchar(11) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `create_date` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `image_link` varchar(255) NOT NULL,
  `akses` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_autobiografi
-- ----------------------------
DROP TABLE IF EXISTS `tb_autobiografi`;
CREATE TABLE `tb_autobiografi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `auto_for` int(5) DEFAULT NULL,
  `autobiografi` text,
  `foto` varchar(255) DEFAULT NULL,
  `status` enum('ACTIVE','NOT_ACTIVE') DEFAULT 'NOT_ACTIVE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_background
-- ----------------------------
DROP TABLE IF EXISTS `tb_background`;
CREATE TABLE `tb_background` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `background` varchar(255) DEFAULT NULL,
  `status` enum('ACTIVE','PASSIVE') DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_berita2
-- ----------------------------
DROP TABLE IF EXISTS `tb_berita2`;
CREATE TABLE `tb_berita2` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `image_link` varchar(255) NOT NULL,
  `url_link` varchar(255) NOT NULL,
  `create_by` varchar(20) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_by` varchar(20) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_by` varchar(20) NOT NULL,
  `delete_date` datetime NOT NULL,
  `view_count` int(5) DEFAULT '0',
  `akses` varchar(100) NOT NULL,
  `status` enum('ACTIVE','NOT_ACTIVE') DEFAULT 'NOT_ACTIVE',
  PRIMARY KEY (`id_berita`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_dukungan
-- ----------------------------
DROP TABLE IF EXISTS `tb_dukungan`;
CREATE TABLE `tb_dukungan` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama_pendukung` varchar(100) DEFAULT NULL,
  `no_telpon` varchar(14) DEFAULT NULL,
  `caleg_didukung` int(5) DEFAULT NULL,
  `follow_up` enum('YES','NO') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_kinerja_rel
-- ----------------------------
DROP TABLE IF EXISTS `tb_kinerja_rel`;
CREATE TABLE `tb_kinerja_rel` (
  `id_kinerja` int(11) NOT NULL AUTO_INCREMENT,
  `agenda` int(11) NOT NULL,
  `foto` int(11) NOT NULL,
  `tujuan` varchar(100) DEFAULT NULL,
  `akses` varchar(200) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `id_relawan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_kinerja`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_login
-- ----------------------------
DROP TABLE IF EXISTS `tb_login`;
CREATE TABLE `tb_login` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `akses` varchar(255) DEFAULT NULL,
  `id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_moto
-- ----------------------------
DROP TABLE IF EXISTS `tb_moto`;
CREATE TABLE `tb_moto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `create_by` varchar(20) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_by` varchar(20) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_by` varchar(20) NOT NULL,
  `delete_date` datetime NOT NULL,
  `akses` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_oleh
-- ----------------------------
DROP TABLE IF EXISTS `tb_oleh`;
CREATE TABLE `tb_oleh` (
  `id_oleh` int(11) NOT NULL AUTO_INCREMENT,
  `nama_oleh` varchar(100) DEFAULT NULL,
  `stok` int(20) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `image_link` varchar(255) DEFAULT NULL,
  `akes` varchar(255) NOT NULL,
  PRIMARY KEY (`id_oleh`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_personal_act_for
-- ----------------------------
DROP TABLE IF EXISTS `tb_personal_act_for`;
CREATE TABLE `tb_personal_act_for` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `activated_for` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for t_assignment
-- ----------------------------
DROP TABLE IF EXISTS `t_assignment`;
CREATE TABLE `t_assignment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL,
  `asignment_start` datetime DEFAULT NULL,
  `asignment_stop` datetime DEFAULT NULL,
  `assignment` varchar(255) DEFAULT NULL,
  `assignment_description` varchar(255) DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_stop` time DEFAULT NULL,
  `shift_id` int(10) NOT NULL,
  `id_pegawai` bigint(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `provinsiId` bigint(50) DEFAULT NULL,
  `kabupatenId` bigint(50) DEFAULT NULL,
  `kecamatanId` bigint(50) DEFAULT NULL,
  `kelurahanId` bigint(50) DEFAULT NULL,
  `tpsId` bigint(50) DEFAULT NULL,
  `jenis_calon_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for t_daerah_pelaksana
-- ----------------------------
DROP TABLE IF EXISTS `t_daerah_pelaksana`;
CREATE TABLE `t_daerah_pelaksana` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provinsiId` int(11) DEFAULT NULL,
  `kabupatenId` bigint(20) DEFAULT NULL,
  `jenis_pemilihan_id` int(5) DEFAULT NULL,
  `jenis_calon_id` int(11) DEFAULT NULL,
  `jumlah_dpt` int(20) DEFAULT NULL,
  `tahun_pelaksanaan` int(5) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for t_paslon
-- ----------------------------
DROP TABLE IF EXISTS `t_paslon`;
CREATE TABLE `t_paslon` (
  `paslon_id` int(11) NOT NULL AUTO_INCREMENT,
  `calon_ketua_id` bigint(10) DEFAULT NULL,
  `calon_wakil_id` bigint(10) DEFAULT NULL,
  `jenis_calon_id` tinyint(1) DEFAULT NULL,
  `provinsiId` bigint(10) DEFAULT NULL,
  `kabupatenId` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`paslon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for t_vote
-- ----------------------------
DROP TABLE IF EXISTS `t_vote`;
CREATE TABLE `t_vote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_paslon` varchar(255) DEFAULT NULL,
  `id_tps` varchar(255) DEFAULT NULL,
  `id_assignment` varchar(255) DEFAULT NULL,
  `vote` decimal(10,0) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
