<section id="header">
	@include('layout.menu')
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5">
				<img src="{{ asset('asset/img/foto/gambar-header.png') }}" class="img-responsive prof-pic">
			</div>
			<div class="col-md-7 col-sm-7 col-xs-7">
				<table>
					<tr>
						<td width="100%" height="550px;" style="vertical-align: middle">
							@foreach($datamoto as $get)
							<?php 
							str_replace("<p>", "", $get->deskripsi);
							str_replace("</p>", "", $get->deskripsi);
							 ?>
							<h1 class="main-title">"{{ $get->judul }}</h1>
							<p class="subtitle justify">{{ $get->deskripsi.'"' }}</p>
							@endforeach
							<h3 style="color: #fff">
								@foreach($databio as $dc)
								<cite>{{ "- ".$dc->nama }}</cite>
								@endforeach
							</h3>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</section>