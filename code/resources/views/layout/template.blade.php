<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>@yield('title-page')</title>

	<!-- Bootstrap Core CSS -->
	<link href="{{ asset('asset/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('asset/plugins/jQuery/jquery-2.2.3.min.js')}}">

	<!-- Custom Fonts -->
	<link href="{{ asset('asset/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
	<!-- Theme CSS -->
	<link href="{{ asset('asset/css/agency.min.css')}}" rel="stylesheet">
	<link href="{{ asset('asset/css/animate.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('asset/css/css.css')}}">
	<link rel="stylesheet" href="{{ asset('assets/asset/dropify-master/dist/css/dropify.css') }}">
	<link rel="shortcut icon" href="{{ asset('asset/img/foto/ipc-new.jpeg') }}">
</head>
<body id="page-top" class="index">
	<!-- Navigation -->
	@if(isset($tanpa_menu))
		
	@else
		@include('layout.navbar')
	@endif

	<!-- Header -->
	@yield('content')
	</body>
	<script src="{{ asset('asset/js/jquery.min.js')}}"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script src="{{ asset('assets/asset/dropify-master/dist/js/dropify.min.js') }}"></script>
	<script src="{{ asset('asset/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyCEVojWiqvNgSpxAtagsYYIvu1JSm00lA0"></script>
	<script src="{{ asset('asset/js/agency.min.js')}}"></script>
	<script>
		$(document).ready(function(){
	      $(".drop").dropify({
	        "height" : "120px"
	      });
	    });
	</script>
	@yield('function')
</html>