<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>@yield('title-page')</title>
	<!-- Bootstrap Core CSS -->
	<link href="{{ asset('asset/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('asset/css/css.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('asset/plugins/jQuery/jquery-2.2.3.min.js')}}">
	<!-- Custom Fonts -->
	<link href="{{ asset('asset/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
	<!-- Theme CSS -->
	<!-- <link href="{{ asset('asset/css/agency.min.css')}}" rel="stylesheet"> -->
</head>
<body>
	@include('layout.header')
	@yield('content')
</body>
<script src="{{ asset('asset/js/jquery.min.js')}}"></script>
<script src="{{ asset('asset/bootstrap/js/bootstrap.min.js')}}"></script>
@yield('function')
</html>