<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top" width="100%">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header page-scroll">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
				</button>	
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="hidden">
						<a href="#page-top"></a>
					</li>
					<li>
						<a class="page-scroll" href="#home" style="color: white;">BERANDA</a>
					</li>
					<li>
						<a class="page-scroll" href="#about" style="color: white;">TENTANG</a>
					</li>
					<li>
						<a class="page-scroll" href="#berita" style="color: white;">INFO TERBARU</a>
					</li>
					<li>
						<a class="page-scroll" href="#team" style="color: white;">SOUVENIR</a>
					</li>
					<li>
						<a class="page-scroll" href="#foot" style="color: white;">KONTAK</a>
					</li>
					<li>
						<a href="{{ asset('login') }}" style="color: white;">LOGIN</a>
					</li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>