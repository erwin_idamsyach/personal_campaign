  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Moto  
        <small>Daftar Moto</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Moto</li>
      <li class="active"><a href="{{ asset('admin/header') }}">Daftar Moto</a></li>
      </ol>  
    </section>


<?php

    $cek = Session::get('idcaleg');

    $users = DB::table('tb_moto')
            ->where('akses', $cek)
            ->get();


 ?>

    <!-- Main content -->
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <a href="{{ asset('admin/moto_tambah') }}">
              <button class="josbu btn btn-success pull-right" style="width: 15%;"><i class="fa fa-plus-circle"></i> Tambah</button>
              </a>
              <i class="fa fa-quote-right"></i>
              <h3 class="box-title">Daftar Moto</h3>
            </div>
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center" style="width: 5px;">No</th>
                  <th class="text-center" style="width: 20px;">judul</th>
                  <th class="text-center" style="width: 200px;">moto</th>
                  <th class="text-center" style="width: 100px;">Tanggal Posting</th>
                  <th class="text-center" style="width: 150px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  if(count($users) == 0){
                ?>    
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <?php
                  }else{
                    $start = 0;
                    foreach ($users as $row) {
                    $start++;
                ?>
                <tr>
                  <td class="numeric text-center"><?php echo $start ?></td>
                  <td><?php echo $row->judul; ?></td>
                  <td><?php echo $row->deskripsi ?></td>
                  <td><?php echo $row->create_by?> / <?php echo date("D, d M Y, H:i", strtotime($row->create_date));?></td>
                  <td align="right">
                      <?php
                        if($row->status == "Aktif"){
                      ?>
                      <a href="./moto_ref/<?php echo $row->id ?>">
                      <button class="btn btn-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Aktif"><i class="fa fa-check"></i></button>
                      </a>
                      <?php
                        }else{
                      ?>
                      <a href="./moto_refw/<?php echo $row->id ?>">
                      <button class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" data-original-title="Pasif"><i class="fa fa-minus-circle"></i></button>
                      </a>
                      <?php
                        }
                      ?>  
                    <button class="btn btn-default" onclick="view_moto('<?php echo $row->id ?>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Detail"><i class="fa fa-eye"></i></button>
                    <a href="./moto_edit/<?php echo $row->id ?>">
                    <button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit"><i class="fa fa-edit"></i></button> 
                    </a>
                    <button class="btn btn-danger" onclick="delete_moto('<?php echo $row->id ?>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Hapus"><i class="fa fa-close"></i></button>
                  </td>
                </tr>
                <?php
                    }
                  }                            
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
    </section>
    <!-- /.content -->
  </div>
  @include('include.footer')