  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Pendidikan</li>
      <li class="active"><a href="{{ asset('admin/Pendidikan_data') }}">Daftar Pendidikan</a></li>
      </ol>     
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="row">
        <div class="col-xs-12">
          <a href="{{ asset('admin/edit/pendidikan').'/'.session('idcaleg') }}" class="btn btn-warning">EDIT RIWAYAT PENDIDIKAN</a>
          <div class="box">
            <div class="box-header with-border">              
              <i class="fa fa-calendar"></i>
              <h3 class="box-title">Daftar Pendidikan</h3>
            </div>
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center">Tahun</th>
                  <th class="text-center">Sekolah</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  if(count($pendidikan) == 0){
                ?>    
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <?php
                  }else{
                    foreach ($pendidikan as $row) {
                ?>
                <tr>
                  <td align="center"><?php echo $row->tahun ?></td>
                  <td><?php echo $row->keterangan ?></td>
                </tr>
                <?php
                    }
                  }                            
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  @include('include.footer')