@include('include.static-top')
@include('include.menu')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb">
	      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	      <li><a href="#"><i></i> Personalisasi</a></li>
	      <li><a href="#"><i></i> Background</a></li>
      </ol> 
    </section>
    <section class="content">
      	<div class="row">
      		<div class="col-md-4 col-sm-6 col-xs-12">
      			<div class="box box-primary">
	      			<div class="box-body">
	      				<h5>Autobiografi saat ini</h5>
	      				<p class="justify">
	      					@foreach($dataAuto as $get)
	      					@if($get->foto == "")
	      					<img src="{{ asset('asset/img/foto/nopic.png') }}" alt="" class="floated">
	      					@else
	      					<img src="{{ asset('assets/images/autobiografi').'/'.$get->foto }}" alt="" class="floated">
	      					@endif
	      					<?php
	      					function custom_echo($x, $length){
								  if(strlen($x)<=$length)
								  {
								    echo $x;
								  }
								  else
								  {
								    $y=substr($x,0,$length) . '...';
								    echo $y;
								  }
								}
	      					?>
	      					<?php echo custom_echo($get->autobiografi, 720) ?>
	      					@endforeach
	      				</p>
	      			</div>
    			</div>
    		</div>
    		<div class="col-md-8 col-sm-6 col-xs-12">
      			<div class="box box-primary">
	      			<div class="box-body">
	      				<h4>
	      					Ganti Autobiografi<br>
	      					<form action="{{ asset('admin/autobiografi/update/').'/'.$id_user}}" method="post" enctype="multipart/form-data">
	      						<input type="hidden" name="_token" value="{{ csrf_token() }}">
	      						<div class="form-group" style="margin-top: 10px;">
	      							<textarea name="autobio" id="autobiogra" class="form-control" style="height: 170px; max-height: 170px;"></textarea>
	      							<div class="pull-right">
	      							</div>
	      						</div>
	      						<div class="form-group">
	      							<h5><b>Thumbnail</b></h5>
	      							<div class="row">
	      								<div class="col-md-4">
	      									<input type="file" class="form-control drop" name="thumb">	
	      								</div>
	      							</div>
	      						</div>
	      						<div class="form-group">
	      							<button class="btn btn-green">SUBMIT</button>
	      						</div>
	      					</form>
	      				</h4>
	      			</div>
    			</div>
    		</div>
    	</div>
    </section>
	<!-- /.content -->
</div>
 <script>
 	var limit = 1500;
 	$("#lim-2").text(limit);
 	// get tinymce instance of wp default editor
 	$("#autobiogra").on('keyup', function(){
 		var content = $(tinyMCE.get('autobio').getBody()).text();
		alert(content)
 	})
 </script>
 @include('include.footer')
