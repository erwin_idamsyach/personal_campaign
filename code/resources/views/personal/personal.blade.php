  @include('include.static-top')
  @include('include.menu')

<?php

  $cek = Session::get('username');

  $dataUsers2 = DB::table('tb_login')
            ->join('caleg_drh', 'tb_login.id_login', '=', 'caleg_drh.id')
            ->select('caleg_drh.*', 'tb_login.username', 'tb_login.password',DB::raw('DAY(create_date) as d_start,
                          MONTH(create_date) as m_start,
                          YEAR(create_date) as y_start'))
            ->where('nama', $cek)
            ->get();

  $dataUsers = DB::table('caleg_drh')
                ->where('id', Session::get('idcaleg'))
                ->get();

  foreach($dataUsers as $tmp){}
// ->join('caleg_drh', 'caleg_drh.tingkat_kabupaten', 'm_geo_kab_kpu.geo_kab_id')
  $kabupaten = DB::table('m_geo_kab_kpu')
            ->where('geo_kab_id', $tmp->tingkat_kabupaten)
            ->get();
  foreach ($kabupaten as $key) {
  }

  $provinsi = DB::table('m_geo_prov_kpu')
            ->where('geo_prov_id', $tmp->tingkat_provinsi)
            ->get();

  foreach ($provinsi as $ea) {
  }

  // $array_bulan = array(1=>"Januari","Febuari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");

  // $pick = $tmp->m_start; 

  // $bulan = $array_bulan[$pick];

 ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Data Personal</li>
      <li class="active">Data Personal</li>
      </ol> 
    </section>
    <section class="content">
      <div class="se-pre-con"></div>
      <a href="{{ asset('admin/edit/biodata/'.session('idcaleg')) }}" class="btn btn-warning">Edit Data Caleg</a>
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Data Caleg</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    

<div class="col-md-6 pull-right">
        
              <img src="<?php echo $tmp->foto; ?>" >
              
      </div>

    <div class="form-group">
      <div class="row">
        <label for="inputEmail3" class="col-md-2 col-sm-2 col-xs-12">Nama</label>
        <div class="col-md-3 col-sm-3 col-xs-12">
          {{ $tmp->nama }}
        </div>
        
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <label class="col-md-2">Calon</label>
        <div class="col-md-4">{{ $tmp->type }} / {{ $ea->geo_prov_nama }} - {{ $key->geo_kab_nama }}</div>
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-12"><label for="inputPassword3">Tempat/Tanggal Lahir</label></div>
        
        <div class="col-md-3 col-sm-3 col-xs-12 multi-row">
         {{ $tmp->tempat_lahir }} , <?php echo date("d M Y", strtotime($tmp->tanggal_lahir));?>
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <label for="inputPassword3" class="col-md-2 col-sm-4 col-xs-12">Agama</label>
        <div class="col-md-3 col-sm-3 col-xs-12">
          {{ $tmp->agama }}
        </div>
        
        </div>

        <br>
        <div class="form-group">
          
        
        <div class="row">
          <label for="inputPassword3" class="col-md-2 col-sm-3 col-xs-12">Jenis Kelamin</label>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <?php 
          $ini = $tmp->jenis_kelamin; 
          if ($ini = "lakilaki") {
            echo "Laki - Laki";
          }else{
            echo "Perempuan";
          }

          ?>
        </div>
      </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <label for="inputPassword3" class="col-md-2 col-sm-2 col-xs-12">Alamat</label>
        <div class="col-md-8 col-sm-8 col-xs-9">
          {{ $tmp->alamat }}
        </div>
      
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <label for="inputPassword3" class="col-md-2 col-sm-2 col-xs-12">No Telp</label>
        <div class="col-md-8 col-sm-8 col-xs-9">
          <?php 
        if ($tmp->telephone == "") {
          echo "---";
        }else{
          echo $tmp->telephone;
        }
  ?>
        </div>
      
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <label for="inputPassword3" class="col-md-2 col-sm-2 col-xs-12">Handphone</label>
        <div class="col-md-8 col-sm-8 col-xs-9">

  <?php 
        if ($tmp->handphone == "") {
          echo "---";
        }else{
          echo $tmp->handphone;
        }
  ?>

          
        </div>
      
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <label for="inputPassword3" class="col-md-2 col-sm-2 col-xs-12">Email</label>
        <div class="col-md-8 col-sm-8 col-xs-9">
  <?php 
        if ($tmp->email == "") {
          echo "---";
        }else{
          echo $tmp->email;
        }
  ?>
        </div>
      
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <label for="inputPassword3" class="col-md-2 col-sm-2 col-xs-12">FaceBook</label>
        <div class="col-md-8 col-sm-8 col-xs-9">
  <?php 
        if ($tmp->facebook == "") {
          echo "---";
        }else{
          echo $tmp->facebook;
        }
  ?>
        </div>
      
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <label for="inputPassword3" class="col-md-2 col-sm-2 col-xs-12">Alamat</label>
        <div class="col-md-8 col-sm-8 col-xs-9">
          <?php 
        if ($tmp->handphone == "") {
          echo "---";
        }else{
          echo $tmp->twitter;
        }
  ?>
        </div>
      
      </div>
    </div>

    
  
    
    
    
        
        
      </div>
    </section>
    <!-- /.content -->
  </div>
 @include('include.footer')