  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Berita
        <small>Tambah Data Berita</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Berita</li>
      <li class="active">Tambah Data Berita</li>
      </ol>  
    </section>
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-newspaper-o"></i>
          <h3 class="box-title">Tambah Data Berita</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label>Judul <font color="red">(*)</font></label>
                      <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" required="">
                    </div>
                  </div>
                  <div class='col-sm-4'>
                      <div class="form-group">
                          <label>Tanggal Posting <font color="red">(*)</font></label>
                          <div class='input-group date' id='datetimepicker1'>
                              <input type='text' class="form-control" name="tanggal" id="tanggal" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                      </div>
                  </div>                                                      
                 <div class="col-md-12">
                    <div class="form-group">
                      <label>Isi <font color="red">(*)</font></label>
                      <textarea class="form-control" name="isi" id="isi" placeholder="Isi" required=""></textarea>
                    </div>
                  </div>                                 
                </div>
                <div class="row">
                  
                <div class="box-footer clearfix pull-right"  align="left">
          <button type="submit" class="btn btn-success"  onclick="unto()">Simpan</button>
          <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
        </div>
      </div>

                </div>
              </div>
    

            </div>
          </div>
        </div>
              
  </div>
    </section>
    <!-- /.content -->
  </div>
 @include('include.footer')
 