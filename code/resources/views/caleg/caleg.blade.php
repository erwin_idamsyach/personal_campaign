  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Caleg
        <small>Daftar Caleg</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Caleg</li>
      <li class="active"><a href="{{ asset('admin/caleg_data') }}">Daftar Caleg</a></li>
      </ol>     
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <a href="{{ asset('admin/caleg_tambah') }}">
              <button class="josbu btn btn-success pull-right" style="width: 15%;"><i class="fa fa-plus-circle"></i> Tambah</button>
              </a>
              
              <i class="fa fa-calendar"></i>
              <h3 class="box-title">Daftar Caleg</h3>
            </div>
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center" style="width: 5px;">No</th>
                  <th class="text-center" style="width: 350px;">Nama</th>
                  <th class="text-center" style="width: 100px;">Provonsi</th>
                  <th class="text-center" style="width: 150px;">Kota/Kabupaten</th>
                  <th class="text-center" style="width: 150px;">Bakal Calon</th>
                  <th class="text-center" style="width: 150px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  if(count($users) == 0){
                ?>    
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <?php
                  }else{
                    $start = 0;
                    foreach ($users as $row) {
                    $start++;
                ?>
                <tr>
                  <td class="numeric text-center"><?php echo $start ?></td>
                  <td><?php echo $row->nama ?></td>
                  <td><?php echo $provinsi ?></td>
                  <td><?php echo $kabupaten ?></td>
                  <td><?php echo $row->type ?></td>
                  <td align="right">          
                    <a href="./caleg_edit/<?php echo $row->id ?>">
                    <button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit"><i class="fa fa-edit"></i></button> 
                    </a>
                    <button class="btn btn-danger" onclick="delete('<?php echo $row->id ?>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Hapus"><i class="fa fa-close"></i></button>
                  </td>
                </tr>
                <?php
                    }
                  }                            
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  @include('include.footer')