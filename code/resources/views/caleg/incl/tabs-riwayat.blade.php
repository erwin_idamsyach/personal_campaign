@if($goto == "Riwayat Pendidikan")
<div class="box box-primary">
	<div class="box-header with-border-bottom">
		Pendidikan
		<button type="button" class="btn btn-primary pull-right" id="add_pendidikan">+</button>
	</div>
    <?php $a = 1; ?>
    @foreach($dataCaleg as $get)
	<div class="box-body" id="pendidikan">
		<div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_pendidikan">
    		<div class="row">
    			<div class="col-md-2 col-sm-4 col-xs-12">
    				<div class="form-group">
              			<input type="text" class="form-control" id="pendidikan_tahun1" name="pendidikan_tahun1" value="{{ $get->tahun }}" placeholder="Tahun">
            		</div>
    			</div>	
        		<div class="col-md-6 col-sm-8 col-xs-12">
        			<div class="form-group">
                      	<input type="text" class="form-control" id="pendidikan_keterangan1" name="pendidikan_keterangan1" value="{{ $get->keterangan }}" placeholder="Keterangan">
                    </div>
        		</div>	
    		</div>	
		</div>
	</div>
    <?php $a++; ?>
    @endforeach
	<input type="hidden" value="<?php echo $a ?>" id="jml_pendidikan" name="jml_pendidikan">
</div>
@else
<div class="box box-primary">
	<div class="box-header with-border-bottom">
		Organisasi
		<button type="button" class="btn btn-primary pull-right" id="add_organisasi">+</button>
	</div>
    <?php $b = 1; ?>
    @foreach($dataCaleg as $get)
	<div class="box-body" id="organisasi">
		<div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_pendidikan">
    		<div class="row">
    			<div class="col-md-2 col-sm-4 col-xs-12">
    				<div class="form-group">
              			<input type="text" class="form-control" id="organisasi_tahun1" name="organisasi_tahun1" value="{{ $get->tahun }}" placeholder="Tahun">
            		</div>
    			</div>
        		<div class="col-md-2 col-sm-8 col-xs-12">
        			<div class="form-group">
                      	<input type="text" class="form-control" id="organisasi_keterangan1" name="organisasi_keterangan1" value="{{ $get->keterangan }}" placeholder="Keterangan">
                    </div>
        		</div>
    		</div>
		</div>
	</div>
    <?php $b++; ?>
    @endforeach
	<input type="hidden" value="<?php echo $b ?>" id="jml_organisasi" name="jml_organisasi">
</div>

@endif
<!-- <div class="box box-primary">
    <div class="box-header with-border-bottom">
        Pekerjaan
        <button type="button" class="btn btn-primary pull-right" id="add_pekerjaan">+</button>
    </div>
    <div class="box-body" id="pekerjaan">
        <div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_pekerjaan">
            <div class="row">
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <div class="form-group">
                          <input type="text" class="form-control" id="pekerjaan_tahun1" name="pekerjaan_tahun1" placeholder="Tahun">
                    </div>
                </div>
                <div class="col-md-2 col-sm-8 col-xs-12">
                    <div class="form-group">
                          <input type="text" class="form-control" id="pekerjaan_keterangan1" name="pekerjaan_keterangan1" placeholder="Keterangan">
                    </div>
                </div>
            </div>    
        </div>
    </div>
    <input type="hidden" value="1" id="jml_pekerjaan" name="jml_pekerjaan">
</div>
<div class="box box-primary">
    <div class="box-header with-border-bottom">
        Diklat
        <button type="button" class="btn btn-primary pull-right" id="add_diklat">+</button>
    </div>
    <div class="box-body " id="diklat">
        <div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_diklat">
            <div class="row">
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <div class="form-group">
                          <input type="text" class="form-control" id="diklat_tahun1" name="diklat_tahun1" placeholder="Tahun">
                    </div>
                </div>
                <div class="col-md-2 col-sm-8 col-xs-12">
                    <div class="form-group">
                          <input type="text" class="form-control" id="diklat_keterangan1" name="diklat_keterangan1" placeholder="Keterangan">
                    </div>
                </div>
            </div>    
        </div>
    </div>
    <input type="hidden" value="1" id="jml_diklat" name="jml_diklat">
</div>
<div class="box box-primary">
    <div class="box-header with-border-bottom">
        Perjuangan
        <button type="button" class="btn btn-primary pull-right" id="add_perjuangan">+</button>
    </div>
    <div class="box-body" id="perjuangan">
        <div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_pendidikan">
            <div class="row">
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <div class="form-group">
                          <input type="text" class="form-control" id="perjuangan_tahun1" name="perjuangan_tahun1" placeholder="Tahun">
                    </div>
                </div>
                <div class="col-md-2 col-sm-8 col-xs-12">
                    <div class="form-group">
                          <input type="text" class="form-control" id="perjuangan_keterangan1" name="perjuangan_keterangan1" placeholder="Keterangan">
                    </div>
                </div>
            </div>    
        </div>
    </div>
    <input type="hidden" value="1" id="jml_perjuangan" name="jml_perjuangan">
</div>
<div class="box box-primary">
    <div class="box-header with-border-bottom">
        Penghargaan
        <button type="button" class="btn btn-primary pull-right" id="add_penghargaan">+</button>
    </div>
    <div class="box-body" id="penghargaan">
        <div class="form-inline" role="form" style="width: 100%;padding: 5px;" id="form_penghargaan">
            <div class="row">
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <div class="form-group">
                          <input type="text" class="form-control" id="penghargaan_tahun1" name="penghargaan_tahun1" placeholder="Tahun">
                    </div>
                </div>
                <div class="col-md-2 col-sm-8 col-xs-12">
                    <div class="form-group">
                          <input type="text" class="form-control" id="penghargaan_keterangan1" name="penghargaan_keterangan1" placeholder="Keterangan">
                    </div>
                </div>    
            </div>    
        </div>
    </div>
    <input type="hidden" value="1" id="jml_penghargaan" name="jml_penghargaan">
</div> -->