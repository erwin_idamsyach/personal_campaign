@foreach($dataCaleg as $data)
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<hr>
		<div class="form-group">
			<div class="row">
				<label for="inputPassword3" class="col-md-2 col-sm-6 col-xs-12">Identitas</label>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<select class="form-control" id="identitas" name="identitas" required onchange="cekIdentitas()">
						<option value="">--- Pilih Identitas ---</option>
						<option value="KTP" {{ ($data->jenis_identitas == "KTP")?'selected=""':"" }}>KTP</option>
						<option value="SIM" {{ ($data->jenis_identitas == "SIM")?'selected=""':"" }}>SIM</option>
						<option value="Paspor" {{ ($data->jenis_identitas == "Paspor")?'selected=""':"" }}>PASPOR</option>
						<option value="NPWP" {{ ($data->jenis_identitas == "NPWP")?'selected=""':"" }}>NPWP</option>
						<option value="KK" {{ ($data->jenis_identitas == "KK")?'selected=""':"" }}>KK</option>
					</select>
				</div>	
				<div class="col-md-3 col-sm-6 col-xs-12 multi-row">
					<input type="text" class="form-control" id="noIdentitas" name="noIdentitas" placeholder="Nomer Identitas" disabled onkeyup="cekIdentitas()" value="{{ $data->nomer_identitas }}" />
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12 multi-row" id="responseCheck"></div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<label for="inputEmail3" class="col-md-2 col-sm-2 col-xs-12">Nama</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="namaDepan" class="form-control" id="namaDepan" placeholder="Nama Depan" value="{{ $data->nama }}" required>
				</div>
			</div>
		</div>
	</div> 
	<div class="form-group">
		<div class="row">
			<div class="col-md-2 col-sm-2 col-xs-12"><label for="inputPassword3">Tempat/Tanggal Lahir</label></div>
			<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
				<select class="form-control" id="tlProv" name="tlProv" required onchange="getKokabTTL()">
					<option value="0">--- Pilih Provinsi ---</option>
					<?php foreach($dataProvinsi as $tmpprop){?>
					<option value="{{ $tmpprop->geo_prov_id }}">{{ $tmpprop->geo_prov_nama }}</option>
					<?php }?>
				</select>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
				<select class="form-control" id="tlKab" name="tempatLahir" required>
					<option value="0" >--- Pilih Kota/Kabupaten ---</option>
				</select>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 multi-row">
				<input type="date" name="tanggalLahir" class="form-control" id="datepicker" placeholder="dd-mm-yyyy" value="{{ $data->tanggal_lahir }}" required>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<label for="inputPassword3" class="col-md-2 col-sm-4 col-xs-12">Agama</label>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<select class="form-control" id="agama" name="agama" required>
					<option value="">--- Pilih ---</option>
					<option value="Islam" {{ ($data->agama == "Islam")?'selected=""':"" }}>Islam</option>
					<option value="Kristen" {{ ($data->agama == "Kristen")?'selected=""':"" }}>Kristen</option>
					<option value="Katolik" {{ ($data->agama == "Katolik")?'selected=""':"" }}>Katolik</option>
					<option value="Hindu" {{ ($data->agama == "Hindu")?'selected=""':"" }}>Hindu</option>
					<option value="Budha" {{ ($data->agama == "Budha")?'selected=""':"" }}>Budha</option>
				</select>
			</div>
			<label for="inputPassword3" class="col-md-3 col-sm-3 col-xs-12">Jenis Kelamin</label>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<input type="radio" name="jenisKelamin" value="Laki-Laki" {{ ($data->jenis_kelamin == "lakilaki")?'checked=""':"" }}>Laki-Laki</input>
				<input type="radio" name="jenisKelamin" value="Perempuan" {{ ($data->jenis_kelamin == "perempuan")?'checked=""':"" }}>perempuan</input>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<label for="inputPassword3" class="col-md-2 col-sm-2 col-xs-12">Alamat</label>
			<div class="col-md-8 col-sm-8 col-xs-9">
				<input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat" value="{{ $data->alamat }}" required>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-3 col-sm-3 col-xs-12 multi-row col-md-offset-2 col-sm-offset-2 col-xs-offset-12">
				<select class="form-control" id="abProv" name="abProv" required>
					<option value="">--- Pilih Provinsi ---</option>
					<?php foreach($dataProvinsi as $tmpprop){?>
					<option value="{{ $tmpprop->geo_prov_id }}" {{ ($data->provinsi == $tmpprop->geo_prov_id)?"selected=''":"" }}>{{ $tmpprop->geo_prov_nama }}</option>
					<?php }?>
				</select>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
				<select class="form-control" id="abKab" name="abKab" required>
					<option value="" >--- Pilih Kota/Kabupaten ---</option>
				</select>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
				<select class="form-control" id="abKec" name="abKec" required>
					<option value="">--- Pilih Kecamatan ---</option>
				</select>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12 multi-row">
				<select class="form-control" id="abKel" name="abKel" required>
					<option value="">--- Pilih Kelurahan ---</option>
				</select>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12" style="padding:0;">
				<label for="inputEmail3" class="col-md-2 col-sm-6 col-xs-12">Status Pernikahan</label>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<select name="statusPernikahan" id="statusPernikahan" class="form-control" required>
						<option value="">--- Pilih ---</option>
						<option value="belum" {{ ($data->status_kawin == "belummenikah")?"selected=''":"" }}>Belum Menikah</option>
						<option value="menikah" {{ ($data->status_kawin == "menikah")?"selected=''":"" }}>Menikah</option>
						<option value="Janda/Duda" {{ ($data->status_kawin == "Janda/Duda")?"selected=''":"" }}>Janda/Duda</option>
					</select>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<input type="text" class="form-control" name="namaPasangan" id="namaPasangan" placeholder="Nama Pasangan" value="{{ $data->pasangan }}" disabled />
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<input type="text" class="form-control" name="jumlahAnak" id="jumlahAnak" placeholder="Jumlah Anak" value="{{ $data->anak }}" disabled />
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-2 col-sm-2 col-xs-12">
				<label for="inputPassword3">Kontak</label>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<input type="text" name="telp" id="telp" class="form-control" placeholder="No. Telp" value="{{ $data->telephone }}">
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<input type="text" name="hp" id="hp" class="form-control" placeholder="Handphone" value="{{ $data->handphone }}">
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<input type="email" name="emailBalon" id="emailBalon" class="form-control" placeholder="Email" value="{{ $data->email }}">
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-2 col-sm-2"></div>
			<div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="twitter" id="twitter" class="form-control" placeholder="Twitter" value="{{ $data->twitter }}"></div>
			<div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="facebook" id="facebook" class="form-control" placeholder="Facebook" value="{{ $data->facebook }}"></div>
			<!-- <div class="col-md-3 col-sm-3 col-xs-12"><input type="file" name="foto" id="foto" class="form-control"></div> -->
		</div>
	</div>
		<!-- <div class="form-group">
			<div class="row">
				<div class="col-md-12 pull-right">
					<button class="btn btn-primary" type="submit">SUBMIT</button>
				</div>
			</div>
		</div> -->
	</div>
</div>
@endforeach