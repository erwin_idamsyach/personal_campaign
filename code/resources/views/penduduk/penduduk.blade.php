  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penduduk
        <small>Daftar Penduduk</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Penduduk</li>
      <li class="active"><a href="{{ asset('admin/Penduduk_data') }}">Daftar Penduduk</a></li>
      </ol>  
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <i class="fa fa-newspaper-o"></i>
              <h3 class="box-title">Daftar Penduduk</h3>
            </div>
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center" style="width: 5px;">No</th>
                  <th class="text-center" style="width: 100px;">Provinsi</th>
                  <th class="text-center" style="width: 100px;">Jumlah Penduduk</th>
                  <!-- <th class="text-center" style="width: 150px;">Aksi</th> -->
                </tr>
                </thead>
                <tbody>
                <?php
                  if(count($users) == 0){
                ?>    
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <?php
                  }else{
                    $start = 0;
                    foreach ($users as $row) {
                    $start++;
                ?>
                <tr>
                  <td class="numeric text-center"><?php echo $start ?></td>
                  <td><?php
                     $prov = DB::table('m_geo_prov_kpu')
                    ->where('geo_prov_id', $row->geo_prov_id)
                    ->get();
                    foreach ($prov as $tmp ) {
                    echo $tmp->geo_prov_nama;
                    }
                   ?></td>
                   <td align="center"><?php $jmlpenduduk = DB::table('m_penduduk')
                      ->select('geo_prov_id', DB::raw('SUM(penduduk_jumlah) as pendudukjml'))
                      ->where('geo_prov_id', $row->geo_prov_id)
                      ->groupBy('geo_prov_id')
                      ->get();
                    foreach ($jmlpenduduk as $key) {
                      echo number_format($key->pendudukjml,0,",",".");
                    }
                  ?></td>
                </tr>
                <?php
                    }
                  }                            
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  @include('include.footer')