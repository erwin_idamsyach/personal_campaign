  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Saksi
        <small>Daftar Saksi</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Saksi</li>
      <li class="active"><a href="{{ asset('admin/sejarah_data') }}">Daftar Saksi</a></li>
      </ol>  
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <i class="fa fa-history"></i>
              <h3 class="box-title">Daftar Saksi</h3>
            </div>
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center" style="width: 5px;">No</th>
                  <th class="text-center" style="width: 200px;">Paslon</th>
                  <th class="text-center" style="width: 100px;">TPS</th>
                  <th class="text-center" style="width: 100px;">Vote</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  if(count($votetps) == 0){
                ?>    
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <?php
                  }else{
                    $start = 0;
                    foreach ($votetps as $row) {
                    $start++;
                ?>
                <tr>
                  <td class="numeric text-center"><?php echo $start ?></td>
                  <td><?php echo $namacalon ?></td>
                  <td class="numeric text-center"><?php echo $namatps ?></td>
                  <td class="numeric text-center"><?php echo $row->vote ?></td>
                </tr>
                <?php
                    }
                  }                            
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  @include('include.footer')