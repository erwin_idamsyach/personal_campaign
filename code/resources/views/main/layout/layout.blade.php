<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title-page')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('asset/bootstrap/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{asset('asset/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('asset/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('asset/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('asset/css/skins/skin-yellow.min.css ')}}">
  <link rel="stylesheet" href="{{asset('asset/plugins/datatables/dataTables.bootstrap.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('asset/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/jquery-ui.css')}}">
  <link rel="stylesheet" href="{{asset('asset/css/wickedpicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/plugins/datetimepicker/jquery.datetimepicker.min.css')}}">
  <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.9.1/themes/black-tie/jquery-ui.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="{{asset('asset/css/jquery-ui.css')}}">
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<?php
	$dataUsers = HelperData::getData(session('nameRole'));
	$dataMenu = HelperData::getMenu(session('nameRole'));
?>
<div class="wrapper">
  <header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SAKSI</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SAKSI</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        @foreach($dataUsers as $users)
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            @if($users->foto == "")
            <img src="{{asset('asset/img/blank_profil.png')}}" class="user-image" alt="User Image">
            @else
            <img src="{{asset('asset/img/profile/'.$users->foto)}}" class="user-image" alt="User Image">
            @endif
            <span class="hidden-xs">{{$users->nama}}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              @if($users->foto == "")
              <img src="{{asset('asset/img/blank_profil.png')}}" class="img-circle" alt="User Image">
              @else
              <img src="{{asset('asset/img/profile/'.$users->foto)}}" class="img-circle" alt="User Image">
              @endif

              <p>
                {{$users->nama}} - 
                <small>Member since <?php echo date('M',strtotime($users->create_date)); ?>. <?php echo date('Y', strtotime($users->create_date)); ?></small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="{{asset('profile/'.$users->id)}}" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="{{asset('proses/logout')}}" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
        @endforeach
        <!-- Control Sidebar Toggle Button -->
      </ul>
      </div>
    </nav>

  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    @include('main.layout.sidebarMenu')
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <div class="popup">
    <div class="window" style="overflow:auto;"></div>
  </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @yield('content-header')
    </section>
    @yield('content-panel')
  </div>
  <!-- /.content-wrapper -->

 <!--  <footer class="main-footer">
    <strong>Copyright &copy; by DISHUB 2015-2016 
  </footer> -->

</div>
<!-- ./wrapper -->
<!-- jQuery 3.0.0 -->
<script src="{{asset('asset/js/jquery.min.js')}}"></script>
<!-- jQuery 2.2.3 -->
<script src="{{asset('asset/plugins/jQuery/jquery.min.js')}}"></script>
<script src="{{asset('asset/plugins/jQueryUI/jquery-ui.min.js')}}"></script>
<script src="{{asset('asset/plugins/datetimepicker/jquery.datetimepicker.full.min.js')}}"></script>

<script src="{{ asset('asset/js/highchart/highcharts2.js') }}"></script>
<script src="{{ asset('asset/js/highchart/highcharts-more.js') }}"></script>	
<script>
  var d = new Date();
  $('#date').focusin(function(){
    $(this).attr('autocomplete','off');
  });
  $('#assignment_start').focusin(function(){
    $(this).attr('autocomplete','off');
  });
  $('#assignment_stop').focusin(function(){
    $(this).attr('autocomplete','off');
  });
   $('.date').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  d,
    format: 'd-m-Y'
    });
  $('#date').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  d,
    format: 'd-m-Y'
    });
  $('#date1').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  d,
    format: 'd-m-Y'
    });
  $('#date2').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  d,
    format: 'd-m-Y'
    });
  $('#date3').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  d,
    format: 'd-m-Y'
    });
  $('#datepicker').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  d,
    format: 'd-m-Y'
    });
  $('#datepicker2').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  d,
    format: 'd-m-Y'
    });
  var date4 = new Date($('#date4').val());
  var date5 = new Date($('#date5').val());
  var date6 = new Date($('#date6').val());
  var date7 = new Date($('#date7').val());
  $('#date4').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  date4,
    format: 'd-m-Y'
    });
  $('#date5').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  date5,
    format: 'd-m-Y'
    });
  $('#date6').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  date6,
    format: 'd-m-Y'
    });
  $('#date7').datetimepicker({
    dayOfWeekStart : 1,
    timepicker:false,
    lang:'ind',
    startDate:  date7,
    format: 'd-m-Y'
    });
  
  $('#assignment_start').datetimepicker({
    dayOfWeekStart : 1,
    lang:'ind',
    startDate:  d,
    format: 'd-m-Y H:i'
    });
   $('.assignment_start').datetimepicker({
    dayOfWeekStart : 1,
    lang:'ind',
    startDate:  d,
    format: 'd-m-Y H:i'
    });
  $('#assignment_stop').datetimepicker({
    dayOfWeekStart : 1,
    lang:'ind',
    startDate:  d,
    format: 'd-m-Y H:i'
    });
</script>
<script src="{{ asset('asset/js/wickedpicker.min.js') }}"></script>
<script type="text/javascript">
  
   $('#time_start').datetimepicker({
      dayOfWeekStart : 1,
      datepicker: false,
      lang:'ind',
      startDate:  d,
      format: 'H:i:s'
    });
  $('#time_stop').datetimepicker({
      dayOfWeekStart : 1,
      datepicker: false,
      lang:'ind',
      startDate:  d,
      format: 'H:i:s'
    });

</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('asset/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('asset/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('asset/js/app.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('asset/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('asset/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('asset/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{asset('asset/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{asset('asset/plugins/chartjs/Chart.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="{{asset('asset/js/pages/dashboard2.js')}}"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="{{asset('asset/js/demo.js')}}"></script>
<!-- Custom Ajax CRUD -->
<script src="{{asset('asset/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('asset/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('asset/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('asset/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('asset/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('asset/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
<script src="{{ asset('asset/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{asset('asset/js/function.js')}}"></script>
<script src="{{asset('asset/js/ajax-cms.js')}}"></script>
<script src="{{ asset('asset/js/jquery.table2excel.js') }}"></script>
<script type="text/javascript">
$(function () {
  if ( $.fn.dataTable.isDataTable( "#report" ) ) {
      table = $("table").DataTable();
  }
  else {
      table = $("#report").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": true,
      "autoWidth": true,
      "iDisplayLength": 8,
      "columnDefs":[
          { "width": "1%","targets": 0 },
      ]
    });
      table = $(".report").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": true,
      "autoWidth": true,
      "iDisplayLength": 8,
      "columnDefs":[
          { "width": "1%","targets": 0 },
      ]
    });
  }
  $("#print_excel").click(function (e) {
      $("#report").table2excel({
      exclude: ".noExl",
      name: "Excel Document Name",
      filename: "myFileName",
      fileext: ".xls",
      exclude_img: true,
      exclude_links: true,
      exclude_inputs: true
    });
  });
});

	function changeLokasi(val,get,responseTo){
/* 		$.ajax({
			url: '',
			type: 'get',
			data: {val:val,get:get},
			dataType: 'html',
			success: function(data){
				$(responseTo).html(data);
			}
		}); */
	}
	
	function choisePemilihan(jenis){
		$('#box_ketua').removeClass('none');
		$('#jenis_calon').removeAttr('disabled');
		$('option.pemilih_1').addClass('none');
		$('option.pemilih_2').addClass('none');
		$('option.pemilih_3').addClass('none');
		$('#box_partai').addClass('none');
		if(jenis == '1') {
			$('option.pemilih_1').removeClass('none');
			$('#box_wakil').removeClass('none');
			$('#jenisCalon').removeAttr('disabled');
		} else if(jenis == '2') {
			$('option.pemilih_2').removeClass('none');
			$('#box_wakil').addClass('none');
			$('#jenisCalon').removeAttr('disabled');
		} else if(jenis == '3') {
			$('option.pemilih_3').removeClass('none');
			$('#box_wakil').removeClass('none');
			$('#jenisCalon').removeAttr('disabled');
		} else {
			$('#box_ketua').addClass('none');
			$('#box_wakil').addClass('none');
			$('#jenisCalon').attr('disabled',true);
			$('#jenis_calon').attr('disabled',true);
		}
		$('#jenisCalon').val('');
		$('#provinsi').val('');
		$('#kab_kota').val('');
		$('#provinsi').attr('disabled',true);
		$('#kab_kota').attr('disabled',true);
	}
	
	function choiseDukungan(dukungan){
		if(dukungan == 'perseorangan') {
			$('#box_partai').addClass('none');
		} else if(dukungan == 'parpol') {
			$('#box_partai').removeClass('none');
		}
	}
	
	function choiseCalon(calon){
		var jenis = $('#jenisPemilihan').val();
		$('#provinsi').val('');
		$('#kab_kota').val('');
		if(calon == ''){
			$('#provinsi').attr('disabled',true);
			$('#kab_kota').attr('disabled',true);
		} else {			
			if(jenis == 1) {
				
			} else if(jenis == 2) {
				if(calon == 2) {				
					$('#provinsi').attr('disabled',true);
					$('#kab_kota').attr('disabled',true);
				} else if(calon == 3) {
					$('#provinsi').removeAttr('disabled');
					$('#kab_kota').attr('disabled',true);
				} else if(calon == 4) {
					$('#provinsi').removeAttr('disabled');
					$('#kab_kota').removeAttr('disabled');
				}
			} else if(jenis == 3) {
				if(calon == 5) {
					$('#provinsi').attr('disabled',false);
					$('#kab_kota').attr('disabled',true);	
				} else if(calon == 6) {
					$('#provinsi').removeAttr('disabled');
					$('#kab_kota').attr('disabled',false);
				} else if(calon == 7) {
					$('#provinsi').removeAttr('disabled');
					$('#kab_kota').removeAttr('disabled');
				}			
			}
		}
	}
</script>
</body>
</html>
