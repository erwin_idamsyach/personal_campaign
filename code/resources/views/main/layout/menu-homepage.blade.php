<ul class="nav navbar-nav navbar-right">
	<li class="hidden">
		<a href="#page-top"></a>
	</li>
	<li>
		<a class="page-scroll" href="{{ asset('index')}}" style="color: #fed136">Tabulasi</a>
	</li>
	<li>
		<a class="page-scroll" href="{{ asset('register')}}" style="color: #fed136">Register</a>
	</li>
	<li>
		<a class="page-scroll" href="{{ asset('login')}}" style="color: #fed136">Login</a>
	</li>
</ul>