<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
  <!-- Sidebar user panel -->
  
  @foreach($dataUsers as $users)
  <div class="user-panel">
    @if($users->foto == "")
    <div class="pull-left image">
      <img src="{{asset('asset/img/blank_profil.png')}}" class="img-circle" alt="User Image">
    </div>
    @else
    <div class="pull-left image">
      <img src="{{asset('asset/img/profile/'.$users->foto)}}" class="img-circle" alt="User Image">
    </div>
    @endif
    <div class="pull-left info">
      <p>{{$users->nama}}</p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>
  @endforeach
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    @foreach($dataMenu as $menu)
	<?php
		if($menu->menu == 'Dashboard'){ $icon = 'fa fa-dashboard'; } else 
		if($menu->menu == 'Live Monitoring') { $icon = 'fa fa-eye'; } else
		if($menu->menu == 'Pengaturan Tugas') { $icon = 'fa fa-edit'; } else
		if($menu->menu == 'HR Management') { $icon = 'fa fa-users'; } else
		if($menu->menu == 'Management Pesan') { $icon = 'fa fa-envelope'; } else
		if($menu->menu == 'Users Management') { $icon = 'fa fa-users'; } else
		if($menu->menu == 'Report') { $icon = 'fa fa-print'; } else
		if($menu->menu == 'Setting') { $icon = 'fa fa-cog'; } else
    if($menu->menu == 'Tracking Petugas') { $icon = 'fa fa-paw'; } else
		if($menu->menu == 'Logout') { $icon = 'fa fa-sign-out'; } else { $icon = 'fa fa-dashboard'; }
	?>

        @if($menu->menu_parent == 0)
          @if(@$menuActive == $menu->menu)
          <li class="treeview active">
            <a href="{{ asset($menu->menu_file) }}">
              <i class="{{ $icon }}"></i> <span>{{$menu->menu}}</span>
            </a>
          </li>
          @else
          <li class="treeview">
            <a href="{{ asset($menu->menu_file) }}">
              <i class="{{ $icon }}"></i> <span>{{$menu->menu}}</span>
            </a>
          </li>
          @endif
        @elseif($menu->menu_parent == 1)
          @if(@$menuActive == $menu->menu)
          <li class="treeview active">
            <a href="#">
              <i class="{{ $icon }}"></i> <span>{{$menu->menu}}</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @if(session('admin')) 
                <?php $dataMenuParent = HelperData::getMenuParetnId($menu->id,'admin'); ?>
              @elseif(session('operator'))
                <?php $dataMenuParent = HelperData::getMenuParetnId($menu->id,'operator'); ?>
              @elseif(session('pengguna'))
                <?php $dataMenuParent = HelperData::getMenuParetnId($menu->id,'pengguna'); ?>
              @endif
              @foreach($dataMenuParent as $parentMenu)
                @if($parentMenu->pos_menu == $menu->id AND $treeview_lv_1 == $parentMenu->menu)
                <li class="active"><a href="{{asset($parentMenu->menu_file)}}"><i class="fa fa-circle-o"></i>{{$parentMenu->menu}}</a></li>
                @else
                <li><a href="{{asset($parentMenu->menu_file)}}"><i class="fa fa-circle-o"></i>{{$parentMenu->menu}}</a></li>
                @endif
              @endforeach
            </ul>
          </li>
          @else
          <li class="treeview">
            <a href="#">
              <i class="{{ $icon }}"></i> <span>{{$menu->menu}}</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @if(session('admin')) 
                <?php $dataMenuParent = HelperData::getMenuParetnId($menu->id,'admin'); ?>
              @elseif(session('operator'))
                <?php $dataMenuParent = HelperData::getMenuParetnId($menu->id,'operator'); ?>
              @elseif(session('pengguna'))
                <?php $dataMenuParent = HelperData::getMenuParetnId($menu->id,'pengguna'); ?>
              @endif
              @foreach($dataMenuParent as $parentMenu)
                @if($parentMenu->pos_menu == $menu->id AND $treeview_lv_1 == $parentMenu->menu)
                <li class="active"><a href="{{asset($parentMenu->menu_file)}}"><i class="fa fa-circle-o"></i>{{$parentMenu->menu}}</a></li>
                @else
                <li><a href="{{asset($parentMenu->menu_file)}}"><i class="fa fa-circle-o"></i>{{$parentMenu->menu}}
                @if($parentMenu->menu_parent == 2 AND $parentMenu->menu_file == '#' )
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                @endif
                </a>
                <?php 
                  if($parentMenu->menu_parent == 2 && $parentMenu->menu_file == '#' ){
                    echo '<ul class="treeview-menu">';
                      $submenu2 = DB::table('m_role_menus')
                              ->join('m_pegawai', 'm_pegawai.role','=','m_role_menus.role_id')
                              ->join('m_menus', 'm_menus.id','=','m_role_menus.menu_id')
                              ->where('m_pegawai.id', session('idLogin'))
                              ->where('m_menus.pos_menu','=',$parentMenu->id)
                              ->where('m_menus.menu_parent',3)
                              ->orderBy('m_menus.menu_order')
                              ->get();  
                    foreach($submenu2 as $d_parents2){ 
                      ?>
                      <li class="active"><a href="{{ asset($d_parents2->menu_file) }}"><i class="fa fa-circle-o"></i><span>{{ $d_parents2->menu }}</span></a></li>
                    <?php }
                    echo '</ul>';
                  }
                ?>
                </li>
                @endif
              @endforeach
            </ul>
          </li>
          @endif
        @endif
    @endforeach
  </ul>
</section>
<!-- /.sidebar -->