  @include('include.static-top')
  @include('include.menu')
  <?php
  $ro = DB::table('tb_berita2')->where('id_berita', $id_berita)->get();
  foreach ($ro as $row) {                   
  }
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb" style="position: relative; right: 39%; margin-top: -36px;">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Berita</li>
      <li class="active">Edit Data Berita</li>
      </ol>  
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px;">
      <a href="{{ asset('admin/profil/news') }}" style="color: #3A3A3A; margin-bottom: 10px;">
        <i class="fa fa-arrow-left"></i> Kembali
      </a>&nbsp;&nbsp;
      <button type="submit" class="btn btn-green" onclick="edit_b('<?php echo $row->id_berita ?>')"">Edit</button>
    </div>
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="box box-primary">
        <div class="box-header">
          <div class="pull-left">
            <h4 class="title">Edit Data Berita</h4>
          </div>
          <div class="pull-right">
            <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
              <i class="fa fa-minus"></i> 
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
              <input type="hidden" class="form-control" name="id_berita" id="id_berita" value="<?php echo $row->id_berita ?>" readonly>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label>Judul</label>
                      <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo  $row->judul ?>" required="">
                    </div>
                  </div>
                  <div class='col-sm-4'>
                      <div class="form-group">
                          <label>Tanggal Posting </label>
                          <div class='input-group date' id='datetimepicker1'>
                              <input type='text' class="form-control" name="tanggal" id="tanggal" value="<?php echo date("m/d/Y H:i A", strtotime($row->create_date)) ?>" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                      </div>
                  </div>                     
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Isi</label>
                      <textarea class="form-control textarea" name="isi" id="isi" placeholder="Isi" required=""><?php echo $row->isi ?></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Foto</label>
                      <form action="{{ asset('admin/upload_image_b') }}" id="frmuploadImg" method="post" target="iframeUploadImg" enctype="multipart/form-data">
                      <input type="file" name="image" id="image" class="form-control drop" data-height="200" onchange="submitImage()" data-default-file="{{ $row->image_link }}">
                      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                      </form>
                      <input class="hidden" type="text" name="gambarout" id="gambarout">
                      <iframe class="hidden" name="iframeUploadImg" id="iframeUploadImg"></iframe>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-12" id="sizepic"></div>
                  </div>                  
                </div>
              </div>
               
            </div>
          </div>
 
        </div>
          <div class="box-footer clearfix"  align="right">
            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
          </div>
        </div>
        
        
      </div>
    </section>
    <!-- /.content -->
  </div>
 @include('include.footer')
 
