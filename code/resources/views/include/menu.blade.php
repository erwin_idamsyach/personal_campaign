 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- <div class="user-panel" style="height: 60px;">
        <div class="pull-left image">
          <?php

          $cek = Session::get('namacaleg');

          // $cek = "Drs.Hi. Moh. Yasin Payapo,M.Pd";
          $value = str_limit($cek, 19);

          $users = DB::table('m_calon')
            ->where('calon_nama_depan', Session::get('username'))
            ->get();
          foreach ($users as $k) { ?>
          <?php if(Session::get('akses')=='user'){ ?>
            <img src="<?php echo $k->image_link; ?>" align="center" class="img-circle" alt="User Image">
          <?php }else{ ?>
            <img src="{{ asset('assets/images/privilege/admin/avatar5.png')}}" class="img-circle" alt="User Image">
          <?php }
          } ?>

        </div>
          <div class="pull-left info">
      <p><?php echo $value ?></p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
      </div> -->
      <?php if(Session::get('akses') == "user"){ ?>
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <li <?php echo (Session::get('menu') == "dashboard")?"class='active'":"" ?>>
          <a href="{{ asset('admin/dashboard') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <!-- <li <?php echo (Session::get('menu') == "personal_bg" || Session::get('menu') == "personal_motto")?"class='treeview active'":"class='treeview'" ?>>
          <a href="#">
            <i class="fa fa-dashboard"></i> Personalisasi
          </a>
          <ul class="treeview-menu">
            <li <?php echo (Session::get('menu') == "personal_bg")?"class='active'":"" ?>>
              <a href="{{ asset('admin/personalize/background') }}">
                <i class="fa fa-circle-o"></i> Background
              </a>
            </li>
            <li <?php echo (Session::get('menu') == "personal_motto")?"class='active'":"" ?>>
              <a href="{{ asset('admin/personalize/motto') }}">
                <i class="fa fa-circle-o"></i> Motto
              </a>
            </li>
          </ul>
        </li> -->
       <!--  <?php if(Session::get('menu') == 'dashboard'){ echo '<li class="active treeview">'; }else{ echo '<li class="treeview">'; }?>
          <a href="{{ asset('admin')}}">
            <i class="fa fa-tachometer"></i> <span>Dashboard</span>
            <span class="pull-right-container">

            </span>
          </a>
        </li> -->


        <?php if(Session::get('akses') == 'user') { ?>
          <?php if(Session::get('menu') == 'data-personal' || Session::get('menu') == 'front-page' || Session::get('menu') == 'news'){ echo '<li class="active treeview">'; }else{ echo '<li class="treeview">'; }?>

            <a href="#">
              <i class="fa fa-user"></i> <span>Profil Calon</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
             <ul class="treeview-menu <?php if(Session::get('menu') == 'data-personal' || Session::get('menu') == 'front-page' || Session::get('menu') == 'news'){ echo 'active menu-open'; }else{} ?>">
              <li class="{{ (Session::get('menu') == 'data-personal')?'active':'' }}">
                <a href="{{ asset('admin/profil/personal') }}">
                  <i class="fa fa-circle-o"></i> Data Personal
                </a>
              </li>
              <li class="{{ (Session::get('menu') == 'front-page')?'active':'' }}">
                <a href="{{ asset('admin/profil/front-page') }}">
                  <i class="fa fa-circle-o"></i> Front Page
                </a>
              </li>
              <li class="{{ (Session::get('menu') == 'news')?'active':'' }}">
                <a href="{{ asset('admin/profil/news') }}">
                  <i class="fa fa-circle-o"></i> News Update
                </a>
              </li>
            </ul>
        </li>
        <?php } ?>

        <!-- <?php if(Session::get('akses') == 'user') { ?>
            <?php if(Session::get('menu') == 'agenda'){ echo '<li class="active treeview">'; }else{ echo '<li class="treeview">'; }?>
              <a href="{{ asset('admin/agenda_data') }}">
                <i class="fa fa-calendar"></i> <span>Agenda</span>

              </a>
            </li>
          <?php } ?>    -->
      <!--   <?php if(Session::get('akses') == 'user'){ ?>
                      <?php if(Session::get('menu') == 'berita'){ echo '<li class="active treeview">'; }else{ echo '<li class="treeview">'; }?>
                        <a href="{{ asset('admin/berita_data') }}">
                          <i class="fa fa-newspaper-o"></i> <span>Berita</span>
                        </a>
                      </li>
                    <?php } ?> -->

      <?php if(Session::get('akses') == 'user'){ ?>
          <?php if(Session::get('menu') == 'relawan' || Session::get('menu') == "inputrel" || Session::get('menu') == 'kinrelawan'){ echo '<li class="active treeview">'; }else{ echo '<li class="treeview">'; }?>
            <a href="#">
              <i class="fa fa-users"></i> <span>Relawan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
             <ul class="treeview-menu <?php if(Session::get('menu') == 'relawan' || Session::get('menu') == 'kinrelawan'){ echo 'active menu-open'; }else{} ?>">
              <li class="<?php if(Session::get('menu') == 'relawan' ){ echo 'active'; }else{} ?>">
                <a href="{{ asset('admin/relawan_data') }}"><i class="fa fa-circle-o"></i>Data Relawan</a>
              </li>
              <li class="<?php if(Session::get('menu') == 'inputrel' ){ echo 'active'; }else{} ?>">
                <a href="{{ asset('admin/inputrel') }}"><i class="fa fa-circle-o"></i>Input Relawan</a>
              <li class="<?php if(Session::get('menu') == 'kinrelawan') { echo 'active'; }else{} ?>">
                <a href="{{ asset('admin/kinerja_data') }}"><i class="fa fa-circle-o"></i>Penugasan Relawan</a>
              </li>
            </ul>
          </li>
        <?php } ?>

        <?php if(Session::get('akses') == 'user'){ ?>
          <?php if(Session::get('menu') == 'master-log' || Session::get('menu') == 'log-masuk' || Session::get('menu') == 'req-list'){ echo '<li class="active treeview">'; }else{ echo '<li class="treeview">'; }?>
            <a href="#">
              <i class="fa fa-cubes"></i> <span>Logistik</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
             <ul class="treeview-menu <?php if(Session::get('menu') == 'master-log' || Session::get('menu') == 'req-list' || Session::get('menu') == 'log-kirim'){ echo 'active'; }else{} ?>">
              <li <?php echo (Session::get('menu') == "master-log")?"class='active'":"" ?>>
                <a href="{{ asset('admin/logistik/master') }}">
                  <i class="fa fa-circle-o"></i> <span>Stok</span>
                </a>
              </li>
              <li <?php echo (Session::get('menu') == "req-list")?"class='active'":"" ?>>
                <a href="{{ asset('admin/logistik/request-list') }}">
                  <i class="fa fa-circle-o"></i> <span>Request List</span>
                </a>
              </li>
              <!-- <li class="<?php if(Session::get('menu') == 'merchandise' ){ echo 'active'; }else{} ?>">
                <a href="{{ asset('admin/merchandise') }}"><i class="fa fa-circle-o"></i>list Mercchandise</a>
              </li>
              <li class="<?php if(Session::get('menu') == 'log') { echo 'active'; }else{} ?>">
                <a href="{{ asset('admin/log_logistik') }}"><i class="fa fa-circle-o"></i>Log Kirim</a>
              </li>
              <li class="<?php if(Session::get('menu') == 'log') { echo 'active'; }else{} ?>">
                <a href="{{ asset('admin/log_masuk') }}"><i class="fa fa-circle-o"></i>Log Masuk</a>
              </li>
              <li class="<?php if(Session::get('menu') == 'tambah') { echo 'active'; }else{} ?>">
                <a href="{{ asset('admin/barang_masuk') }}"><i class="fa fa-circle-o"></i>Barang Masuk</a>
              </li>

              <li class="<?php if(Session::get('menu') == 'lirim') { echo 'active'; }else{} ?>">
                <a href="{{ asset('admin/barang_kirim') }}"><i class="fa fa-circle-o"></i>Kirim Logistik</a>
              </li> -->


              <!-- <li class="<?php if(Session::get('menu') == 'sejarah') { echo 'active'; }else{} ?>">
                <a href="#"><i class="fa fa-circle-o"></i>List Lokasi Ruang public</a>
              </li>
              <li class="<?php if(Session::get('menu') == 'struktur'){ echo 'active'; }else{} ?>">
                <a href="#"><i class="fa fa-circle-o"></i>List Peta Target</a>
              </li> -->
            </ul>
          </li>
        <?php } ?>
        <li class="<?php if(Session::get('menu') == 'report-relawan' || Session::get('menu') == 'report-merchandise'){ echo 'active menu-open'; }else{} ?>">
          <a href="#">
            <i class="fa fa-book"></i> <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu <?php if(Session::get('menu') == 'report-relawan' || Session::get('menu') == 'report-merchandise'){ echo 'active menu-open'; }else{} ?>">
            <li class="<?php if(Session::get('menu') == 'report-relawan'){ echo 'active'; }else{} ?>">
              <a href="{{asset('report/relawan')}}"><i class="fa fa-circle-o"></i>Relawan</a>
            </li>
            <li class="<?php if(Session::get('menu') == 'report-merchandise'){ echo 'active'; }else{} ?>">
              <a href="{{asset('report/merchandise')}}"><i class="fa fa-circle-o"></i>Merchandise</a>
            </li>
          </ul>
        </li>
        <li <?php echo (Session::get('menu') == "master-kategori")?"class='active treeview'":"class='treeview'" ?>>
          <a href="#">
            <i class="fa fa-cog"></i> <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo (Session::get('menu') == "master-kategori")?"class='active'":"" ?>>
              <a href="{{ asset('admin/master/kategori') }}">
                <i class="fa fa-circle-o"></i> <span>Master Kategori</span>
              </a>
            </li>
          </ul>
        </li>
<!--
        <?php if(Session::get('akses') == 'admin' || Session::get('akses') == 'user'){ ?>
          <?php if(Session::get('menu') == 'geowilayah' || Session::get('menu') == 'penduduk' || Session::get('menu') == 'tps'){ echo '<li class="active treeview">'; }else{ echo '<li class="treeview">'; }?>
            <a href="#">
              <i class="fa fa-map"></i> <span>Statistik & Pemetaan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
             <ul class="treeview-menu <?php if(Session::get('menu') == 'geowilayah' || Session::get('menu') == 'penduduk' || Session::get('menu') == 'tps'){ echo 'active menu-open'; }else{} ?>">
              <li class="<?php if(Session::get('menu') == 'geowilayah' ){ echo 'active'; }else{} ?>">
                <a href="#"><i class="fa fa-circle-o"></i>GEO Wilayah</a>
              </li>
              <li class="<?php if(Session::get('menu') == 'penduduk') { echo 'active'; }else{} ?>">
                <a href="{{ asset('admin/penduduk_data') }}"><i class="fa fa-circle-o"></i>Data Kependudukan</a>
              </li>
              <li class="<?php if(Session::get('menu') == 'tps'){ echo 'active'; }else{} ?>">
                <a href="#"><i class="fa fa-circle-o"></i>Data TPS</a>
              </li>
            </ul>
          </li>
        <?php } ?>

        <?php if(Session::get('akses') == 'user'){ ?>
          <?php if(Session::get('menu') == 'saksi'){ echo '<li class="active treeview">'; }else{ echo '<li class="treeview">'; }?>
            <a href="{{ asset('admin/saksi_data') }}">
              <i class="fa fa-pie-chart"></i> <span>Tabulasi</span>
            </a>
          </li>
        <?php } ?>
 -->
        <?php if(Session::get('akses') == 'admin'){ ?>
          <?php if(Session::get('menu') == 'caleg'){ echo '<li class="active treeview">'; }else{ echo '<li class="treeview">'; }?>
            <a href="{{ asset('admin/caleg_data')}}">
              <i class="fa fa-users"></i> <span>Calon</span>
              <span class="pull-right-container">

              </span>
            </a>
          </li>
        <?php } ?>

        <li class="treeview">
          <a href="{{ asset('logout') }}">
            <i class="fa fa-sign-out"></i> <span>Keluar</span>
          </a>
        </li>


      </ul>
      <?php
    }else{
      ?>
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <li <?php echo (Session::get('menu') == "dashboard")?"class='active'":"" ?>>
          <a href="{{ asset('admin/dashboard') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li <?php if(Session::get('menu') == 'relawan' || Session::get('menu') == "inputrel" || Session::get('menu') == 'kinrelawan'){ echo 'class="active treeview">'; }else{ echo ' class="treeview"'; }?>>
          <a href="#">
            <i class="fa fa-users"></i> <span>Menu Relawan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="#">
                <i class="fa fa-circle-o"></i> <span>Laporan</span>
              </a>
            </li>
          </ul>
        </li>
        <li <?php echo (Session::get('menu') == "req-log" || Session::get('menu') == "form-req-log")?"class='active treeview'":"class='treeview'" ?>>
          <a href="">
            <i class="fa fa-cubes"></i> <span>Request Logistik</span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo (Session::get('menu') == "req-log")?"class='active'":"" ?>>
              <a href="{{ asset('relawan/request-logistik') }}">
                <i class="fa fa-circle-o"></i> <span>Request List</span>
              </a>
            </li>
            <li <?php echo (Session::get('menu') == "form-req-log")?"class='active'":"" ?>>
              <a href="{{asset('relawan/request-logistik/add')}}">
                <i class="fa fa-circle-o"></i> <span>Form Request</span>
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="{{ asset('logout') }}">
            <i class="fa fa-sign-out"></i> <span>Keluar</span>
          </a>
        </li>


      </ul>
      <?php
      }
      ?>
    </section>
    <!-- /.sidebar -->
  </aside>

    <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
