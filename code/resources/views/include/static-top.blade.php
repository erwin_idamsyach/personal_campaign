 <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Personal Campaign Management - Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets/asset/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/asset/font-awesome-4.7.0/css/font-awesome.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/asset/css/AdminLTE.min.css') }}">
  
  <link rel="stylesheet" href="{{ asset('assets/asset/css/skins/_all-skins.min.css') }}">
  <!-- mycss -->
  <!-- <link rel="stylesheet" href="{{ asset('assets/make/css/my_skins.css') }}"> -->
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('assets/asset/plugins/iCheck/flat/blue.css') }}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ asset('assets/asset/plugins/morris/morris.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('assets/asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset('assets/asset/plugins/datepicker/datepicker3.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('assets/asset/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('assets/asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/asset/dropify-master/dist/css/dropify.css') }}">
  <!-- <script src="{{ asset('assets/asset/plugins/jQuery/jquery-2.2.3.min.js') }}"></script> -->
  <link rel="stylesheet" href="{{asset('assets/asset/css/datatables/responsive.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/make/datetimepicker/bootstrap-datetimepicker.min.css')}}">
  <link rel="stylesheet" href="{{ asset('asset/css/css.css') }}">
  <link rel="shortcut icon" href="{{ asset('asset/img/foto/ipc-new.jpeg') }}">
  <script src="{{ asset('assets/asset/js/highchart/highcharts2.js') }}"></script>
  <script src="{{ asset('assets/asset/js/highchart/highcharts-more.js') }}"></script>   

  <style type="text/css" media="screen">
    body{
      font-family: 'Roboto Thin'
    }
  </style>



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <script src="{{ asset('assets/asset/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
  <script src="{{ asset('assets/asset/js/tinymce/tinymce.min.js') }}"></script>
  <script>
  var text_length;
  tinymce.init({ 
      selector:'.textarea'
  });
  </script>
</head>
<body class="hold-transition skin-green sidebar-mini">

<?php
$cek = Session::get('username');

if($cek != "Admin"){
  $value = str_limit($cek, 4);

   $array_bulan = array(1=>"Januari","Febuari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");

  $getdate = DB::table('caleg_drh')
              ->select(DB::raw('DAY(create_date) as d_start,
                            MONTH(create_date) as m_start,
                            YEAR(create_date) as y_start'))
              ->where('nama', $cek)
              ->get();



   /*foreach ($getdate as $key) {
        $pick = $key->m_start; 
   }*/


  // $bulan = $array_bulan[$pick];
}
 ?>

<!-- <div class="se-pre-con"></div> -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background: #870b04; color: #fff;">
        <button type="button" class="close" onclick="close_modal('myModal')"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title modal-maroon"><b>LIHAT</b></h4>
      </div>
      <div class="modal-body">
        <div class="viewitem"></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo" style="padding-bottom: 7px;">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
        <img src="{{ asset('asset/img/foto/ipc-new.jpeg') }}" class="img-responsive" alt="">
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
        <div class="row">
          <div class="col-md-3 col-sm-3 col-xs-3">
            <img src="{{ asset('asset/img/foto/ipc-new.jpeg') }}" height="48px;" style="margin-bottom: 5px;">
          </div>
          <div class="col-md-9 col-sm-9 col-xs-9" style="color: #000; font-size: 12px;">
            <div class="pull-left">
              <h5 style="margin: 0; margin-top: 10px;">
                <?php
              $cek = Session::get('namacaleg');

              // $cek = "Drs.Hi. Moh. Yasin Payapo,M.Pd";
              $value = str_limit($cek, 22);
              echo $value;
              ?>
              </h5>
              <div class="pull-left" style="margin-top: -14px">
                <i class="fa fa-circle text-success"></i> Online
              </div>
            </div>
          </div>
        </div>
      </span>
    </a>
     <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php 
              $users = DB::table('caleg_drh')
                ->where('nama', Session::get('username'))
                ->get();
              foreach ($users as $k) { ?>
              <?php if(Session::get('akses')=='users'){ ?>
                <img src="<?php echo $k->image_link; ?>" align="center" class="user-image" alt="User Image">
              <?php }else{ ?>
                <img src="{{ asset('assets/images/privilege/admin/avatar5.png')}}" class="user-image" alt="User Image">
              <?php } 
              }
              ?>
              <span class="hidden-xs"><?php echo Session::get('namacaleg') ?></span>
              <!-- <span class="hidden-xs">Drs.Hi. Moh. Yasin Payapo,M.Pd</span> -->
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php 
                foreach ($users as $k) {
                if(Session::get('akses')=='users'){ ?>
                  <img src="<?php echo $k->image_link; ?>" align="center" class="img-circle" alt="User Image">
                <?php }else{ ?>
                  <img src="{{ asset('assets/images/privilege/admin/avatar5.png')}}" class="img-circle" alt="User Image">
                <?php } ?>
                <p>
                  <!-- <?php echo Session::get('username') ?> -->
                  <?php if(Session::get('username') != "Admin"){ ?>
                    <span class="hidden-xs">Drs.Hi. Moh. Yasin Payapo,M.Pd</span>
                  <?php }else{ ?>
                  <small>Welcome!!!  
                  <?php }
                  } ?>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <?php if(Session::get('akses')=='users'){ ?>
                    <a href="{{ asset('admin/profile') }}" class="btn btn-default btn-flat">Profile</a>
                  <?php } ?>
                </div>
                <div class="pull-right">
                  <a href="{{ asset('logout') }}" class="btn btn-default btn-flat">Keluar</a>
                </div>
              </li>
            </ul>
          </li>          
        </ul>
      </div>
    </nav>
    <!-- Header Navbar: style can be found in header.less -->
  </header>