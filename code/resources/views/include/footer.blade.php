 <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2017 <a href="../admin/alumni_data">PT Internusa Cipta Solusi Perdana</a>.</strong>
  </footer>

  <!-- Control Sidebar -->

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="{{asset('assets/asset/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/asset/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<!-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- <script>
  $.widget.bridge('uibutton', $.ui.button);
</script> -->
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/asset/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ asset('assets/make/js/style.js') }}"></script>
<script src="{{ asset('assets/asset/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('assets/asset/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('assets/asset/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('assets/asset/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('assets/asset/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<!-- <script src="{{ asset('assets/asset/plugins/daterangepicker/daterangepicker.js') }}"></script> -->

<script src="{{ asset('assets/asset/dropify-master/dist/js/dropify.min.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('assets/asset/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('assets/asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('assets/asset/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/asset/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/asset/js/app.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="{{ asset('assets/asset/js/pages/dashboard.js') }}"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/asset/js/demo.js') }}"></script>
<script src="{{asset('assets/make/datetimepicker/moment.min.js')}}"></script>
<script src="{{asset('assets/make/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>



<script type="text/javascript"> 

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });   
    
   $(function () {
        $('#datetimepicker1').datetimepicker();
    });

    $(document).ready(function(){
      $(".drop").dropify({
        "height" : "120px"
      });
    });
    function submitImage(){
            $("#frmuploadImg").submit();
    }
    
    $(window).load(function() {
      $(".se-pre-con").fadeOut("slow");
    });      
  

  /*$(function () {
  if ( $.fn.dataTable.isDataTable( "table" ) ) {
    table = $('table').DataTable();
  } 
  else if(typeof(janganBuatDataTableLagiPlease) == "undefined"){
      table = $('table').DataTable({
        "dom": '<"pull-left top"l><"pull-right top form-group"f><"clear">t<"bottom"ip><"clear">',
         responsive: true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        columnDefs:[{
          targets:[-1],
          className:'table-aksi',
        }],
        "pageLength": 10,
      });
  }
  }); */   

</script>

</body>
</html>