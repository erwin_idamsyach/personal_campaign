  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb hidden-sm hidden-xs" style="position: relative; right: 33%; margin-top: -36px;">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Profil Calon</li>
      <li class="active"><a href="{{ asset('admin/berita_data') }}">News Update</a></li>
      </ol>  
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px">
      <a href="{{ asset('admin/profil/news/add') }}" class="btn btn-green">TAMBAH</a>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="row">
        <div class="col-md-12">
        </div>
        <div class="col-xs-12">
          <div class="box box-default">
            <div class="box-header">
              <div class="pull-left">
                <h4 class="title">BERITA</h4>
              </div>
              <div class="pull-right">
                <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                  <i class="fa fa-minus"></i> 
                </button>
              </div>
            </div>
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center" style="width: 5px;">ID</th>
                  <th class="text-center" style="width: 200px;">Title</th>
                  <th class="text-center" style="width: 100px;">Timestamp</th>
                  <th class="text-center" style="width: 430px;">Content</th>
                  <th class="text-center" style="width: 140px">File</th>
                  <th class="text-right" style="width: 220px;">Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    function custom_echo($x, $length){
                  if(strlen($x)<=$length)
                  {
                    echo $x;
                  }
                  else
                  {
                    $y=substr($x,0,$length) . '...';
                    echo $y;
                  }
                }
                ?>
                <?php
                  if(count($users) == 0){
                ?>    
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <?php
                  }else{
                    $start = 0;
                    foreach ($users as $row) {
                    $start++;
                ?>
                <tr>
                  <td class="numeric text-center"><?php echo $row->id_berita ?></td>
                  <td><?php echo $row->judul ?></td>
                  <td><?php echo date("D, d M Y, H:i", strtotime($row->create_date));?></td>
                  <td><?php echo custom_echo($row->isi, 50) ?></td>
                  <td><?php echo $row->image_link ?></td>
                  <td align="right">
                    @if($row->status == "NOT_ACTIVE")
                    <button class="btn btn-success" data-toggle="tooltip" data-placement="bottom" data-original-title="AKTIFKAN" onclick="toggleState({{$row->id_berita}}, 1)">
                      <i class="fa fa-check"></i>
                    </button>
                    @else
                    <button class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" data-original-title="NON AKTIFKAN" onclick="toggleState({{$row->id_berita}}, 0)">
                      <i class="fa fa-times"></i>
                    </button>
                    @endif
                    <button class="btn btn-green" onclick="view_b('<?php echo $row->id_berita ?>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Detail"><i class="fa fa-eye"></i></button>
                    <a href="{{ asset('admin/berita_edit/').'/'.$row->id_berita }}">
                    <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit"><i class="fa fa-edit"></i></button> 
                    </a>
                    <button class="btn btn-green" onclick="delete_b('<?php echo $row->id_berita ?>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Hapus"><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                <?php
                    }
                  }                            
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  @include('include.footer')
  <script>
    function toggleState(id, state){
      $.ajax({
        type : "GET",
        url : "{{ asset('ajax/toggle-state-berita') }}",
        data : {
          'id' : id,
          'state' : state
        },
        success:function(resp){
          location.reload();
        }
      })
    }
  </script>