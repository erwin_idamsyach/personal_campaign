< table id="example2" class="table table-bordered table-hover">
  <thead>
  <tr>
    <th class="text-center" style="width: 5px;">No</th>
    <th class="text-center" style="width: 350px;">Judul</th>
    <th class="text-center" style="width: 100px;">Tanggal</th>
    <th class="text-center" style="width: 150px;">Aksi</th>
  </tr>
  </thead>
  <tbody>
  <?php
    if(count($users) == 0){
  ?>    
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <?php
    }else{
      $start = 0;
      foreach ($users as $row) {
      $start++;
  ?>
  <tr>
    <td class="numeric text-center"><?php echo $start ?></td>
    <td><?php echo $row->judul ?></td>
    <td><?php echo date("D, d M Y", strtotime($row->date_start)); ?></td>
    <td align="right">          
      <button class="btn btn-default" onclick="view_a('<?php echo $row->id ?>')"><i class="fa fa-eye" data-toggle="tooltip" data-placement="bottom" data-original-title="Detail"></i></button>
      <a href="./agenda_edit/<?php echo $row->id ?>">
      <button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit"><i class="fa fa-edit"></i></button> 
      </a>
      <button class="btn btn-danger" onclick="delete_a('<?php echo $row->id ?>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Hapus"><i class="fa fa-close"></i></button>
    </td>
  </tr>
  <?php
      }
    }                            
  ?>
  </tbody>
</table>