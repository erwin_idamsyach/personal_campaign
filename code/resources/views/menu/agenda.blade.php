  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Agenda
        <small>Daftar Agenda</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Agenda</li>
      <li class="active"><a href="{{ asset('admin/agenda_data') }}">Daftar Agenda</a></li>
      </ol>     
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <a href="{{ asset('admin/agenda_tambah') }}">
              <button class="josbu btn btn-success pull-right" style="width: 15%;"><i class="fa fa-plus-circle"></i> Tambah</button>
              </a>
              
              <i class="fa fa-calendar"></i>
              <h3 class="box-title">Daftar Agenda</h3>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-3 col-sm-4 col-xs-6">
                    <div class="row" style="margin-bottom: 10px;">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="date" class="form-control pull-right" name="tanggal" id="tanggal" required="" onchange="getTanggal()" >
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center" style="width: 5px;">No</th>
                  <th class="text-center" style="width: 350px;">Judul</th>
                  <th class="text-center" style="width: 100px;">Tanggal</th>
                  <th class="text-center" style="width: 150px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  if(count($users) == 0){
                ?>    
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <?php
                  }else{
                    $start = 0;
                    foreach ($users as $row) {
                    $start++;
                ?>
                <tr>
                  <td class="numeric text-center"><?php echo $start ?></td>
                  <td><?php echo $row->judul ?></td>
                  <td><?php echo date("D, d M Y", strtotime($row->date_start)); ?></td>
                  <td align="right">          
                    <button class="btn btn-default" onclick="view_a('<?php echo $row->id ?>')"><i class="fa fa-eye" data-toggle="tooltip" data-placement="bottom" data-original-title="Detail"></i></button>
                    <a href="./agenda_edit/<?php echo $row->id ?>">
                    <button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit"><i class="fa fa-edit"></i></button> 
                    </a>
                    <button class="btn btn-danger" onclick="delete_a('<?php echo $row->id ?>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Hapus"><i class="fa fa-close"></i></button>
                  </td>
                </tr>
                <?php
                    }
                  }                            
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  @include('include.footer')