  @include('include.static-top')
  @include('include.menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Dashboard</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
      </ol>      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="se-pre-con"></div>
        <div class="row">
          <style>
            #map {
              width: 100%;
              height: 400px;
              background-color: grey;
            }
          </style>    
          <div id="map">      
          </div>
          <br>           
          <div class="col-md-4 col-sm-6 col-xs-12">
           <div class="info-box">
             <span class="info-box-icon bg-success" style="background-color: #007934"><i class="fa fa-user" style="color : #fff"></i></span>
             <div class="info-box-content">
              <span class="info-box-text">Kab/Kota</span>
                 <span class="info-box-number">{{ $kabupaten }}</span>
             </div>
           </div>
          </div>            
            
          <div class="col-md-4 col-sm-6 col-xs-12">
           <div class="info-box">
             <span class="info-box-icon bg-success" style="background-color: #007934"><i class="fa fa-user" style="color : #fff"></i></span>
             <div class="info-box-content">
              <span class="info-box-text">Kecamatan</span>
                 <span class="info-box-number">{{ $kecamatan }}</span>
             </div>
           </div>
          </div>            

          <div class="col-md-4 col-sm-6 col-xs-12">
           <div class="info-box">
             <span class="info-box-icon bg-success" style="background-color: #007934"><i class="fa fa-user" style="color : #fff"></i></span>
             <div class="info-box-content">
              <span class="info-box-text">Penduduk</span>
                 <span class="info-box-number"></span>
             </div>
           </div>
          </div>            

          <div class="col-md-4 col-sm-6 col-xs-12">
           <div class="info-box">
             <span class="info-box-icon bg-success" style="background-color: #007934"><i class="fa fa-newspaper-o" style="color : #fff"></i></span>
             <div class="info-box-content">
             <span class="info-box-text">Total Relawan Mendaftar</span>
                 <span class="info-box-number">{{ $relawan }}</span>
             </div>
           </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
           <div class="info-box">
             <span class="info-box-icon bg-success" style="background-color: #007934"><i class="fa fa-user" style="color : #fff"></i></span>
             <div class="info-box-content">
             <span class="info-box-text">Total Relawan Aktif</span>
                 <span class="info-box-number"></span>
             </div>
           </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
             <div class="info-box">
               <span class="info-box-icon bg-success" style="background-color: #007934"><i class="fa fa-map" style="color : #fff"></i></span>
               <div class="info-box-content">
               <span class="info-box-text">Total Logistik Tersebar</span>
                   <span class="info-box-number"></span>
               </div>
             </div>
          </div>
        </div>
      </div>        
    </section>
    <!-- /.content -->
  </div>
<script src="{{ asset('assets/asset/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
  <script>
      function initMap() {
        var uluru = {lat: -6.206518, lng: 106.843925};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }

    var url, response;    
    $('document').ready(function(){

      getGrafiks('user', '#canvasUser');
      getGrafiks('anggota', '#canvasAnggota');
      getGrafiks('transaksi', '#canvasTransaksi');
      getGrafiks('login', '#canvasLogin');


    });

    function getGrafiks(jenis,response,type,types){
      if(jenis == 'user'){
        getGrafikUser(response,type);
      }else if(jenis == 'anggota'){
        getGrafikAnggota(response,type);
      }else if(jenis == 'transaksi'){
        getGrafikTransaksi(response,type);
      }else if(jenis == 'login'){
        getGrafikLogin(response,type);
      }
    }

    function getGrafikUser(response,types){
      var url = '{{ asset("dashboard/get/data/grafik") }}';     

      $.ajax({
        type : "GET",
        url  : url,
        success:function(html){
          $('#canvasUser').html(html);
        }
      });       
    }

   function getGrafikAnggota(response,types){
      var url = '{{ asset("dashboard/get/data/grafik2") }}';     

      $.ajax({
        type : "GET",
        url  : url,
        success:function(html){
          $('#canvasAnggota').html(html);
        }
      });        
    }

    function getGrafikTransaksi(response,types){
      var url = '{{ asset("dashboard/get/data/grafik3") }}';     

      $.ajax({
        type : "GET",
        url  : url,
        success:function(html){
          $('#canvasTransaksi').html(html);
        }
      });        
    }

    function getGrafikLogin(response,types){
      var url = '{{ asset("dashboard/get/data/grafik4") }}';     

      $.ajax({
        type : "GET",
        url  : url,
        success:function(html){
          $('#canvasLogin').html(html);
        }
      });        
    }    

  </script>
     <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqXPlACQvh12Kab56fhR4zISkZ4A7WyO4&callback=initMap">
    </script>
  @include('include.footer')