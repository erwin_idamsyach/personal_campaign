@include('include.static-top')
@include('include.menu')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb hidden-sm hidden-xs" style="position: relative; right: 25%; margin-top: -36px;">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#"><i class="fa fa-user"></i> Profil Calon</a></li>
      <li><a href="#"><i></i> Front Page</a></li>
      </ol> 
    </section>
    <!-- <div class="pull-right" style="margin-top: -25px; margin-right: 15px;">
      <a href="{{ asset('admin/profil/front-page/add') }}" class="btn btn-green" id="button-action">TAMBAH ABOUT</a>
    </div> -->
    <section class="content">
      <div class="row">
      	<div class="col-md-12">
      		<div class="pull-right">
      		</div>
      	</div>
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header">
              <div class="pull-left">
                <h4 class="title">FRONT PAGE</h4>
              </div>
              <div class="pull-right">
                <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                  <i class="fa fa-minus"></i> 
                </button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Deskripsi</th>
                    <th class="text-center">File</th>
                    <th class="text-center">Side</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    function custom_echo($x, $length){
                      if(strlen($x)<=$length)
                      {
                        echo $x;
                      }
                      else
                      {
                        $y=substr($x,0,$length) . '...';
                        echo $y;
                      }
                    }
                    ?>
                  @foreach($dataFrontpage as $get)
                  <tr>
                    <td class="text-center">{{ $get->id }}</td>
                    <td style="width: 60%">
                      @if($get->position == "TOP")
                        <?php echo $get->title.", ".$get->subtitle   ?>
                      @elseif($get->position == "LEFT")
                        <?php echo custom_echo($get->deskripsi, 140) ?>
                      @elseif($get->position == "RIGHT")
                        <?php echo $get->title ?>
                      @else
                      @endif
                    </td>
                    <td>{{$get->foto  }}</td>
                    <td>{{ $get->position }}</td>
                    <td class="text-right">
                      @if($get->position == "TOP")
                        <a href="{{ asset('admin/profil/front-page/bg/edit').'/'.$get->id }}" class="btn btn-green">
                          <i class="fa fa-pencil"></i>
                        </a>
                      @elseif($get->position == "LEFT")
                        <a href="{{ asset('admin/profil/front-page/edit') }}" class="btn btn-green">
                          <i class="fa fa-pencil"></i>
                        </a>
                      @elseif($get->position == "RIGHT")
                        <a href="{{ asset('admin/profil/front-page/edit_campaign') }}" class="btn btn-green">
                          <i class="fa fa-pencil"></i>
                        </a>
                      @else
                        <a href="{{ asset('admin/profil/front-page/edit_disclaimer') }}" class="btn btn-green">
                          <i class="fa fa-pencil"></i>
                        </a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="pull-right">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@include('include.footer')
<script>
	function toggleStatus(id, state){
    $.ajax({
      type : "GET",
      url  : "{{ asset('ajax/toggle-status') }}",
      data : {
        'id' : id,
        'state' : state
      },
      success:function(resp){
        location.reload();
      }
    });
  }
  function toggleStatusBg(id, state){
    $.ajax({
      type : "GET",
      url  : "{{ asset('ajax/toggle-status/bg') }}",
      data : {
        'id' : id,
        'state' : state
      },
      success:function(resp){
        location.reload();
      }
    });
  }
  function deleteData(id){
    if(confirm("Apakah anda yakin akan menghapus data ini?")){
      $.ajax({
        type : "GET",
        url  : "{{ asset('ajax/delete-data') }}",
        data : {
          'id' : id
        },
        success:function(resp){
          location.reload();
        }
      });
    }
  }
  function deleteDataBg(id){
    if(confirm("Apakah anda yakin akan menghapus data ini?")){
      $.ajax({
        type : "GET",
        url  : "{{ asset('ajax/delete-data/bg') }}",
        data : {
          'id' : id
        },
        success:function(resp){
          location.reload();
        }
      });
    }
  }
</script>