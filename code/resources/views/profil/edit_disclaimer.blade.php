@include('include.static-top')
@include('include.menu')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb hidden-sm hidden-xs" style="position: relative; right: 21%; margin-top: -36px;">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#"><i class="fa fa-user"></i> Profil Calon</a></li>
      <li><a href="#"><i></i> Data Personal</a></li>
      </ol> 
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px;">
      <a href="{{ asset('admin/profil/front-page') }}" style="color: #3A3A3A; margin-bottom: 10px;">
        <i class="fa fa-arrow-left"></i> Kembali
      </a>&nbsp;&nbsp;
      <button class="btn btn-green" id="btn-sub" type="submit">SIMPAN</button>
    </div>
    <section class="content">
      <div class="row">
      	<div class="col-md-12">
      		<div class="pull-left">
      			
      		</div>
      	</div>
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <div class="pull-left">
                <h4 class="title">EDIT DISCLAIMER</h4>
              </div>
              <div class="pull-right">
                <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                  <i class="fa fa-minus"></i> 
                </button>
              </div>
            </div>
            <div class="box-body">
              <form action="{{ asset('admin/profil/front-page/edit_act') }}" id="form" method="post" enctype="multipart/form-data">
                @foreach($dataDisc as $get)
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group">
                    <label>Disclaimer</label>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <textarea class="textarea" name="disclaimer" id="" cols="30" rows="10"><?php echo $get->deskripsi ?></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                </div>
              </div>
              @endforeach
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@include('include.footer')
<script>
	$("#btn-sub").on('click', function(){
    $("#form").trigger('submit');
  });
</script>