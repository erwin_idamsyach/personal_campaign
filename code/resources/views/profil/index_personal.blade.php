@include('include.static-top')
@include('include.menu')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb hidden-sm hidden-xs" style="position: relative; right: 20%; margin-top: -36px;">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#"><i class="fa fa-user"></i> Profil Calon</a></li>
      <li><a href="#"><i></i> Data Personal</a></li>
      </ol>
    </section>
    <form id="form-drh" enctype="multipart/form-data">
    <div class="pull-right"  style="margin-top: -25px; margin-right: 15px;">
      <!-- <button class="btn btn-green" type="button">HEADER</button> -->
      <a href="{{asset('pdf/pernyataan').'/'.$id}}" class="btn btn-green btn-sm" type="button">CETAK PERNYATAAN</a>
      <a href="{{asset('pdf/drh').'/'.$id}}" class="btn btn-green btn-sm" type="button">CETAK DRH</a>
      <button class="btn btn-green btn-sm" id="button-action" type="button" onclick="toggleAct('OPEN')">EDIT DATA</button>
    </div>
    <section class="content">
      <div class="row">
      	<div class="col-md-12">
      		<div class="box box-default">
      			<div class="box-header">
      				<div class="pull-left">
      					<h4 class="title">DATA PERSONAL</h4>
      				</div>
              <div class="pull-right">
                <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
      			</div>
      			<div class="box-body">
      				@foreach($dataCalon as $get)
      				<?php
      				$exp = explode("-", $get->tanggal_lahir)
      				?>
  					<div class="row">
      					<div class="col-md-2 col-sm-3 col-xs-12">
      						@if($get->foto != "")
      						<input type="file" id="dropify" name="foto" class="form-control input-sm drop" data-default-file="{{ asset('asset/img/foto').'/'.$get->foto }}">
      						@else
      						<input type="file" id="dropify" name="foto" class="form-control input-sm drop">
      						@endif
      					</div>
      					<div class="col-md-4 col-sm-4 col-xs-12">
      						<div class="form-group row">
      							<div class="col-md-4">
      								<label>ID</label>
      							</div>
      							<div class="col-md-8">
      								<select name="jenis_identitas" id="jenis_identitas" class="form-control input-sm input-sm">
      									<option value="">--Pilih Identitas--</option>
      									<option value="KTP" {{ ($get->jenis_identitas == "KTP")?"selected":"" }}>KTP</option>
      									<option value="SIM" {{ ($get->jenis_identitas == "SIM")?"selected":"" }}>SIM</option>
      									<option value="PASPOR" {{ ($get->jenis_identitas == "PASPOR")?"selected":"" }}>PASPOR</option>
      									<option value="NPWP" {{ ($get->jenis_identitas == "NPWP")?"selected":"" }}>NPWP</option>
      									<option value="KK" {{ ($get->jenis_identitas == "KK")?"selected":"" }}>KK</option>
      								</select>
      							</div>
      						</div>
      						<div class="form-group row">
      							<div class="col-md-4">
      								<label>Nomor ID</label>
      							</div>
      							<div class="col-md-8">
      								<input type="text" name="nomor_identitas" class="form-control input-sm" value="{{ $get->nomer_identitas }}">
      							</div>
      						</div>
      						<div class="form-group row">
      							<div class="col-md-4 col-sm-3">
      								<label>Nama Depan</label>
      							</div>
      							<div class="col-md-8">
      								<input type="text" name="nama_depan" class="form-control input-sm" value="{{ $get->nama_depan }}">
      							</div>
      						</div>
      						<div class="form-group row">
      							<div class="col-md-4 col-sm-3">
      								<label>Nama Tengah</label>
      							</div>
      							<div class="col-md-8">
      								<input type="text" name="nama_tengah" class="form-control input-sm" value="{{ $get->nama_tengah }}">
      							</div>
      						</div>
      						<div class="form-group row">
      							<div class="col-md-4 col-sm-3">
      								<label>Nama Belakang</label>
      							</div>
      							<div class="col-md-8">
      								<input type="text" name="nama_belakang" class="form-control input-sm" value="{{ $get->nama_belakang }}">
      							</div>
      						</div>
      					</div>
      					<div class="col-md-6 col-sm-5 col-xs-12">
      						<div class="form-group row">
      							<div class="col-md-3 col-sm-3">
      								<label>Tanggal Lahir</label>
      							</div>
      							<div class="col-md-9">
      								<div class="row nopadding">
      									<div class="col-md-3">
      										<select name="tgl_lahir" class="form-control input-sm" disabled="">
      											<option value="">Tgl</option>
      											@foreach($tanggal as $tgl)
      											<option value="{{ $tgl->tgl }}" {{ ($exp[2] == $tgl->tgl)?"selected":"" }}>{{ $tgl->tgl }}</option>
      											@endforeach
      										</select>
      									</div>
      									<div class="col-md-5">
      										<select name="bln_lahir" id="bln_lahir" class="form-control input-sm">
      											<option value="">Bulan</option>
      											@foreach($bulan as $bln)
      											<option value="{{ $bln->id }}" {{ ($exp[1] == $bln->id)?"selected":"" }}>{{ $bln->bulan }}</option>
      											@endforeach
      										</select>
      									</div>
      									<div class="col-md-4">
      										<select name="tahun_lahir" id="tahun_lahir" class="form-control input-sm">
      											<option value="">Tahun</option>
      											@foreach($tahun as $thn)
      											<option value="{{ $thn->tahun }}" {{ ($exp[0] == $thn->tahun)?"selected":"" }}>{{ $thn->tahun }}</option>
      											@endforeach
      										</select>
      									</div>
      								</div>
      							</div>
      						</div>
      						<div class="form-group row">
      							<div class="col-md-3">
      								<label>Tempat Lahir</label>
      							</div>
      							<div class="col-md-6">
      								<input type="text" class="form-control input-sm" name="tempat_lahir" id="tempat_lahir" value="{{ $get->tempat_lahir }}">
      							</div>
      						</div>
                  <div class="form-group row">
                    <div class="col-md-3 col-sm-3">
                      <label>Jenis Kelamin</label>
                    </div>
                    <div class="col-md-6">
                      <select name="jenkel" class="form-control input-sm">
                        <option value="">--Jenis Kelamin--</option>
                        <option value="Laki-Laki" {{ ($get->jenis_kelamin == "Laki-Laki")?"selected":"" }}>Laki-Laki</option>
                        <option value="Perempuan" {{ ($get->jenis_kelamin == "Perempuan")?"selected":"" }}>Perempuan</option>
                      </select>
                    </div>
                  </div>
      						<div class="form-group row">
      							<div class="col-md-3">
      								<label>Status</label>
      							</div>
      							<div class="col-md-6">
      								<select name="status_pernikahan" id="status_pernikahans" class="form-control input-sm">
      									<option value="">--Pilih--</option>
      									<option value="Belum Menikah" {{ ($get->status_kawin == "Belum Menikah")?"selected":"" }}>Belum Menikah</option>
      									<option value="Menikah" {{ ($get->status_kawin == "Menikah")?"selected":"" }}>Menikah</option>
      									<option value="Janda/Duda" {{ ($get->status_kawin == "Janda/Duda")?"selected":"" }}>Janda / Duda</option>
      								</select>
      							</div>
      						</div>
      						<div class="form-group row">
      							<div class="col-md-3">
      								<label>Nama Pasangan</label>
      							</div>
      							<div class="col-md-6">
      								<input type="text" name="nama_pasangan" id="nama_pasangan" class="form-control input-sm" value="{{ $get->pasangan }}">
      							</div>
                    <div class="col-md-3">
                      <input type="number" name="jumlah_anak" id="jumlah_anak" class="form-control input-sm" value="{{ $get->anak }}">
                    </div>
      						</div>
      						<!-- <div class="form-group row">
                    <div class="col-md-3">
                      <label>Jumlah Anak</label>
                    </div>
                    <div class="col-md-2">
                    </div>
                  </div> -->
      					</div>
      				</div>
      				@endforeach
      			</div>
      		</div>
      	</div>
      	<div class="col-md-12">
      		<div class="box box-default">
      			<div class="box-header">
      				<div class="pull-left">
                <h4 class="title">ALAMAT & KONTAK</h4>
              </div>
      				<div class="pull-right">
                <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
      				</div>
      			</div>
      			<div class="box-body">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Provinsi</label>
                </div>
                <div class="col-md-9">
                  <select name="a_prov" id="a_prov" class="form-control input-sm">
                    <option value="">--Pilih Provinsi--</option>
                    @foreach($dataProvinsi as $data)
                    <option value="{{ $data->geo_prov_id }}" {{ ($get->provinsi == $data->geo_prov_id)?"selected":"" }}>{{ $data->geo_prov_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Kabupaten</label>
                </div>
                <div class="col-md-9">
                  <select name="a_kota" id="a_kota" class="form-control input-sm">
                    <option value="">--Pilih Kota / Kabupaten--</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Kecamatan</label>
                </div>
                <div class="col-md-9">
                  <select name="a_kec" id="a_kec" class="form-control input-sm">
                    <option value="">--Pilih Kecamatan--</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Kelurahan</label>
                </div>
                <div class="col-md-9">
                  <select name="a_desa" id="a_desa" class="form-control input-sm">
                    <option value="">--Pilih Kelurahan--</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="form-group row">
                <div class="col-md-3">
                  <label>RT/RW</label>
                </div>
                <div class="col-md-4">
                  <input type="text" name="RT" class="form-control input-sm">
                </div>
                <div class="col-md-4">
                  <input type="text" name="RW" class="form-control input-sm">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Alamat</label>
                </div>
                <div class="col-md-9">
                  <textarea name="alamat" class="form-control" style="height: 68px;"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Kodepos</label>
                </div>
                <div class="col-md-9">
                  <input type="text" class="form-control input-sm" name="kodepos">
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Handphone</label>
                </div>
                <div class="col-md-9">
                  <input type="phone" name="handphone" class="form-control input-sm">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Telepon</label>
                </div>
                <div class="col-md-9">
                  <input type="phone" name="telepon" class="form-control input-sm">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Fax</label>
                </div>
                <div class="col-md-9">
                  <input type="phone" name="fax" class="form-control input-sm">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Email</label>
                </div>
                <div class="col-md-9">
                  <input type="phone" name="email" class="form-control input-sm">
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Facebook</label>
                </div>
                <div class="col-md-9">
                  <input type="text" name="facebook" class="form-control input-sm">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Twitter</label>
                </div>
                <div class="col-md-9">
                  <input type="text" name="twitter" class="form-control input-sm">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-3">
                  <label>Instagram</label>
                </div>
                <div class="col-md-9">
                  <input type="text" name="instagram" class="form-control input-sm">
                </div>
              </div>
            </div>
      			</div>
      		</div>
      	</div>
      	<div class="col-md-12">
      		<div class="box box-default">
      			<div class="box-header">
              <div class="pull-left">
                <h4 class="title">RIWAYAT PENDIDIKAN</h4>
              </div>
      				<div class="pull-right">
                <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
      				</div>
      			</div>
      			<div class="box-body">
      				<?php $no = 1 ?>
      				@foreach($dataPendidikan as $pend)
      				<div class="form-group row">
      					<div class="col-md-1" style="margin-top: 8px;">
      						<center>
      							<label>{{ $no++."." }}</label>
      						</center>
      					</div>
      					<div class="col-md-3">
      						<select name="tingkat_sekolah" id="tingkat_sekolah" class="form-control input-sm">
      							<option value="">--Pilih Tingkat--</option>
      							@foreach($tingkat_pend as $pd)
      							<option value="{{ $pd->id }}" {{ ($pend->tingkat == $pd->id)?"selected":"" }}>{{ $pd->tingkat }}</option>
      							@endforeach
      						</select>
      					</div>
      					<div class="col-md-6">
      						<input type="text" name="pendidikan[]" class="form-control input-sm" value="{{ $pend->keterangan }}">
      					</div>
      				</div>
      				@endforeach
      			</div>
      		</div>
      	</div>
      	</form>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <input type="hidden" id="id_prov" value="{{ $get->provinsi }}">
  <input type="hidden" id="id_kota" value="{{ $get->kabupaten }}">
  <input type="hidden" id="id_kecamatan" value="{{ $get->kecamatan }}">
  <input type="hidden" id="id_desa" value="{{ $get->kelurahan }}">
@include('include.footer')
<script>
	var dropi;
	$(document).ready(function(){
    getStream();
		$("form#form-drh input, textarea").attr("disabled", true)
    $(".drop").removeAttr("disabled")
		$("form#form-drh select").attr("disabled", true)
	});
	function toggleAct(key){
		if(key == 'OPEN'){
			$("input.drop").removeAttr("disabled")
			$("form#form-drh input, textarea").removeAttr("disabled");
			$("form#form-drh select").removeAttr("disabled")
			$("#button-action")
        .text("SAVE DATA")
  			.removeAttr("onclick")
        .removeClass("btn-green")
        .addClass("btn-danger")
			setTimeout(function(){
				$("#button-action").attr("type", "submit")
			}, 700)
		}else{

		}
	}
  function getStream(){
    var i_prov = $("#a_prov").val();
    var id_kota = $("#id_kota").val();
    $.ajax({
      type : "GET",
      url  : "{{ asset('ajaxGetKabupaten') }}",
      data : {
        "key" : i_prov,
        "id_kota" : id_kota
      },
      success:function(resp){
        $("#a_kota").html(resp);

          var i_kota = $("#a_kota").val();
          var id_kec = $("#id_kecamatan").val();
          /*getNamaKota(i_kota);*/
          $.ajax({
            type : "GET",
            url  : "{{ asset('ajaxKecamatan') }}",
            data : { "key" : i_kota, 'id_kec' : id_kec},
            success:function(resp){
              $("#a_kec").html(resp);

                var i_kec = $("#a_kec").val();
                var id_desa = $("#id_desa").val();
                $.ajax({
                  type : "GET",
                  url  : "{{ asset('ajaxKelurahan') }}",
                  data : { "key" : i_kec, 'id_desa' : id_desa},
                  success:function(resp){
                    $("#a_desa").html(resp);

                      var id_desa = $("#a_desa").val();
                      /*getNamaDesa(id_desa);*/
                  }
                })
            }
          })
      }
    })
  }
  $("#a_prov").on("change", function(){
    var i_prov = $("#a_prov").val();
    $.ajax({
      type : "GET",
      url  : "{{ asset('ajaxGetKabupaten') }}",
      data : {
        "key" : i_prov
      },
      success:function(resp){
        $("#a_kota").html(resp);
      }
    })
  });
  $("#a_kota").on("change", function(){
      var i_kota = $("#a_kota").val();
      /*getNamaKota(i_kota);*/
      $.ajax({
        type : "GET",
        url  : "{{ asset('ajaxKecamatan') }}",
        data : "key="+i_kota,
        success:function(resp){
          $("#a_kec").html(resp);
        }
      })
    });
  $("#a_kec").on("change", function(){
      var i_kec = $("#a_kec").val();

      $.ajax({
        type : "GET",
        url  : "{{ asset('ajaxKelurahan') }}",
        data : "key="+i_kec,
        success:function(resp){
          $("#a_desa").html(resp);

        }
      })
    });
  $("#a_desa").on('change', function(){
    var id_desa = $("#a_desa").val();
    /*getNamaDesa(id_desa);*/
  });
	$("#form-drh").on('submit', function(){
				/*alert("SUBMITTED")*/
				var formData = new FormData($(this)[0]);
				$.ajax({
					type : "POST",
					url : "{{ asset('save-data-personal').'/'.$id }}",
					data : formData,
					processData : false,
					cache : false,
					async : false,
					contentType : false,
					success:function(resp){
						formReset();
					}
				})
				return false;
			})
	function formReset(){
		$("input.drop").attr("disabled", "disabled")
		$("form#form-drh input, textarea").attr("disabled", "disabled");
    $(".drop").removeAttr("disabled");
		$("form#form-drh select").attr("disabled", true)
		$("#button-action")
      .text("EDIT DATA")
    	.removeAttr('type')
    	.attr('type', 'button')
      .removeClass("btn-danger")
      .addClass("btn-green")
    	.attr("onclick", "toggleAct('OPEN')")
	}
</script>
