@include('include.static-top')
@include('include.menu')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb hidden-sm hidden-xs" style="position: relative; right: 21%; margin-top: -36px;">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#"><i class="fa fa-user"></i> Profil Calon</a></li>
      <li><a href="#"><i></i> Data Personal</a></li>
      </ol> 
    </section>
    <section class="content">
      <div class="row">
      	<div class="col-md-12">
      		<div class="pull-right">
      			
      		</div>
      	</div>
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header">
              <div class="pull-left">
                <h4 class="title">TAMBAH DATA</h4>
              </div>
              <div class="pull-right">
                <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                  <i class="fa fa-minus"></i> 
                </button>
              </div>
            </div>
            <div class="box-body">
              <form action="{{ asset('admin/profil/front-page/add_act') }}" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group">
                    <label>Autobiografi</label>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <textarea class="form-control textarea" name="autobio" id="" cols="30" rows="10"></textarea>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Foto Thumbnail</label>
                    <input type="file" name="thumb" class="form-control drop">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="pull-left">
                    <button class="btn btn-green" type="submit">SIMPAN</button>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@include('include.footer')
<script>
	
</script>