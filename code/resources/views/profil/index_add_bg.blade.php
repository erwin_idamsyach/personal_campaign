@include('include.static-top')
@include('include.menu')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#"><i class="fa fa-user"></i> Front Page</a></li>
      <li><a href="#"><i></i> Background & Motto</a></li>
      </ol> 
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header">
              <button class="btn btn-green btn-box-head" data-widget="collapse">
                <i class="fa fa-minus"></i> 
              </button>
              <div class="pull-right">
                <h4 class="title">TAMBAH DATA</h4>
              </div>
            </div>
            <div class="box-body">
              <form action="{{ asset('admin/profil/front-page/bg/add_bg') }}" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Motto</label>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="text" class="form-control" name="motto">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Deskripsi</label>
                        <input type="text" class="form-control" name="deskripsi">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Gambar Background</label>
                    <input type="file" name="thumb" class="form-control drop">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="pull-right">
                    <button class="btn btn-green" type="submit">SIMPAN</button>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@include('include.footer')
<script>
	
</script>