<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Personal Campaign Management</title>

	<!-- Bootstrap Core CSS -->
	<link href="{{ asset('asset/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('asset/plugins/jQuery/jquery-2.2.3.min.js')}}">

	<!-- Custom Fonts -->
	<link href="{{ asset('asset/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
	<!-- Theme CSS -->
	<link href="{{ asset('asset/css/agency.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('asset/css/css.css')}}">
</head>
<body id="page-top" class="index">
	<?php
	foreach($databio as $dc){
		$nama = $dc->nama;
	}
	foreach ($dataBg as $bg) {
		$bg = $bg->background;
	}
	?>
	<!-- Navigation -->
	<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top" width="100%">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header page-scroll">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
				</button>	
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="hidden">
						<a href="#page-top"></a>
					</li>
					<li>
						<a class="page-scroll" href="#home" style="color: white;">BERANDA</a>
					</li>
					<li>
						<a class="page-scroll" href="#about" style="color: white;">TENTANG</a>
					</li>
					<li>
						<a class="page-scroll" href="#berita" style="color: white;">INFO TERBARU</a>
					</li>
					<li>
						<a class="page-scroll" href="#team" style="color: white;">SOUVENIR</a>
					</li>
					<li>
						<a class="page-scroll" href="#foot" style="color: white;">KONTAK</a>
					</li>
					<li>
						<a href="{{ asset('login') }}" style="color: white;">LOGIN</a>
					</li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

	<!-- Header -->
	<header id="home" style="background-image:url({{ asset('assets/images/background/'.$bg)}});background-size: cover">
		<div class="container main-cont">
			<div class="intro-text">		
				@foreach($datamoto as $get)
				<?php
				$str = str_replace(" ", "-", $get->judul);
				$str = str_replace("/", "_", $str);
				?>
				<div class="intro-heading txt-judul">
					<div class="text-left">
						<div class="title-1">{{ $get->judul }}</div>
						<div class="title-2"><?php echo $get->deskripsi ?></div>
					</div>
				</div>
				<div>
					<div class="text-left txt-desc">
						@foreach($databio as $dc)
						{{ "- ".$dc->nama }}
						@endforeach
					</div>
				</div>
				@endforeach
				<div class="text-left" style="margin-top: 40px;">
					<a href="#daftar" class="page-scroll btn btn-trans">DAFTAR RELAWAN</a>
				</div>
			</div>
		</div>
	</header>
	<section id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="panel">
						<div class="panel-body">
							<div class="subtitle-head">About Us</div>
							<h3 class="title-content res-scr-1">Siapa {{ $nama }} ?</h3>
							<p class="justify">
								@foreach($dataAuto as $get)
								@if($get->foto == "")
								<img src="{{ asset('asset/img/foto/nopic.png') }}" alt="" class="floated">
								@else
								<img src="{{ asset('assets/images/autobiografi').'/'.$get->foto }}" alt="" class="floated">
								@endif
								{{ $get->autobiografi }}
								@endforeach
							</p>
							<button class="btn btn-block btn-green">LIHAT PROFIL LENGKAP</button>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel">
						<div class="panel-body">
							<div class="subtitle-head text-center">Relawan</div>
							<p class="text-center" style="text-transform: uppercase; line-height: 1">Total tim relawan kami<br>hingga saat ini mencapai	 </p>
							<h2 class="text-center green res-scr-2" style="margin-top: -5px;"><b>1.300</b></h2>
							<p class="text-center" style="text-transform: uppercase; line-height: 1">relawan terdaftar</p>
							<hr>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-6 text-center" style="border-right: 1px solid #E5E5E5">
									<div class="res-scr-4 green">500</div>
									<p class="text-center upper">Perempuan</p>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 text-center">
									<div class="res-scr-4 green">500</div>
									<p class="text-center upper">Laki-Laki</p>
								</div>
							</div><br>
							<center>
								<button class="btn btn-green btn-block">DAFTAR</button>
							</center>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel">
						<div class="panel-body">
							<div class="subtitle-head">Dukungan Anda</div>
							<h3 class="title-content res-scr-1">Dukungan anda sangat kami tunggu.</h3>	
							<p class="justify">Dengan mengisi nama dan nomor telpon dibawah berarti anda telah berkomitmen dan mendukung perubahan positif di daerah anda untuk menjadi lebih baik kedepannya, dan kami akan mengirimkan merchandise ke rumah anda.</p>
							<div class="row" style="margin-top: 6px;">
								<div class="col-md-12">
									<div class="form-group">
										<input type="text" name="nama" class="form-control form-khusus" placeholder="Masukkan Nama Anda">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8 col-sm-12 col-xs-12">
									<div class="form-group">
										<input type="text" name="nama" class="form-control form-khusus" placeholder="Masukkan Nomor Telpon anda" value="+62">
									</div>
								</div>
								<div class="col-md-4 col-sm-12 col-xs-12">
									<button class="btn btn-green">SUBMIT</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="berita" class="bg-light-gray">
		<div class="container">
			<h2 class=" text-center res-scr-3">INFO TERBARU</h2><br>
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel panel-default">
						<div class="panel-body box-menu">
							<div class="box-first">
								<div class="row">
									<div class="col-md-10 col-sm-10 col-xs-10">
										<?php echo strtoupper(date('l, d M Y')) ?><br>
										MY TEAM	
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2">
										<img src="{{ asset('asset/img/foto/nopic.png') }}" class="img-circle img-bordered" style="width: 30px">
									</div>
								</div>
							</div><hr>
							<div class="news-title">
								<h3 class="title-content res-scr-1">Kurangnya perhatian pemerintah kepada Desa</h3>
							</div>
							<div class="link-share">
								<i class="fa fa-arrow-right"></i> Read More
								<div class="pull-right">
									<i class="fa fa-facebook"></i> 100&nbsp;&nbsp;
									<i class="fa fa-twitter"></i> 42&nbsp;&nbsp;
									<i class="fa fa-share-alt"></i> 10&nbsp;&nbsp;
								</div>
								
							</div>
							<img src="{{ asset('asset/img/foto/ipc.jpg') }}" alt="" width="100%">
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel panel-default">
						<div class="panel-body box-menu ">
							<div class="box-first">
								<div class="row">
									<div class="col-md-10 col-sm-10 col-xs-10">
										<?php echo strtoupper(date('l, d M Y')) ?><br>
										MY TEAM	
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2">
										<img src="{{ asset('asset/img/foto/nopic.png') }}" class="img-circle img-bordered" style="width: 30px">
									</div>
								</div>
							</div><hr>
							<div class="news-title">
								<h3 class="title-content res-scr-1">Kurangnya perhatian pemerintah kepada Desa</h3>
							</div>
							<div class="link-share">
								<i class="fa fa-arrow-right"></i> Read More
								<div class="pull-right">
									<i class="fa fa-facebook"></i> 100&nbsp;&nbsp;
									<i class="fa fa-twitter"></i> 42&nbsp;&nbsp;
									<i class="fa fa-share-alt"></i> 10&nbsp;&nbsp;
								</div>
								
							</div>
							<img src="{{ asset('asset/img/foto/ipc.jpg') }}" alt="" width="100%">
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel panel-default">
						<div class="panel-body box-menu">
							<div class="box-first">
								<div class="row">
									<div class="col-md-10 col-sm-10 col-xs-10">
										<?php echo strtoupper(date('l, d M Y')) ?><br>
										MY TEAM	
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2">
										<img src="{{ asset('asset/img/foto/nopic.png') }}" class="img-circle img-bordered" style="width: 30px">
									</div>
								</div>
							</div><hr>
							<div class="news-title">
								<h3 class="title-content res-scr-1">Kurangnya perhatian pemerintah kepada Desa</h3>
							</div>
							<div class="link-share">
								<i class="fa fa-arrow-right"></i> Read More
								<div class="pull-right">
									<i class="fa fa-facebook"></i> 100&nbsp;&nbsp;
									<i class="fa fa-twitter"></i> 42&nbsp;&nbsp;
									<i class="fa fa-share-alt"></i> 10&nbsp;&nbsp;
								</div>
								
							</div>
							<img src="{{ asset('asset/img/foto/ipc.jpg') }}" alt="" width="100%">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Team Section -->
	<section id="team" style="min-height: 300">
		<div class="container">
			<h2 class=" text-center res-scr-3">SOUVENIR</h2><br>
			@foreach($datahadiah as $get)
			<?php
			$str = str_replace(" ", "-", $get->nama_oleh);
			$str = str_replace("/", "_", $str);
			?>
			<div class="col">
				<div class="col-lg-3 col-md-4 col-xs-6 thumb">
					<div class="team-member">
						<img class="img-responsive img-box" src="{{ asset('asset/img/foto/ipc.jpg')}}" alt="">
						<h4>{{ $get->nama_oleh }}</h4>
					</div>
				</div>

			</div>
			@endforeach
		</section>
		<footer id="foot" style="text-align: left">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-6 col-xs-12">
						<h3 class="title-content res-scr-1 white" style="margin-top: 35px;">MENU</h3>
						<ul class="straight-menu text-left">
							<li class="hidden">
								<a href="#page-top"></a>
							</li>
							<li>
								<a class="page-scroll" href="#home" style="color: white;">BERANDA</a>
							</li>
							<li>
								<a class="page-scroll" href="#about" style="color: white;">TENTANG</a>
							</li>
							<li>
								<a class="page-scroll" href="#team" style="color: white;">MERCHANDISE</a>
							</li>
							<li>
								<a class="page-scroll" href="#berita" style="color: white;">INFO TERBARU</a>
							</li>
							<li>
								<a class="page-scroll" href="#contact" style="color: white;">KONTAK</a>
							</li>
							
						</ul>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<h2 style="color: white;" align="center">Kontak</h2>
						<p style="color: white" align="justify">Kenali lebih jauh <b> @foreach($databio as $dc){{ $dc->nama }}@endforeach </b>
						melalui akun sosial media resmi. dapatkan informasi tentang aktifitas terbaru dan update mengenai jadwal kampanye berikutnya.
					</p>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-6" style="margin-top: 5px;">
							@foreach($databio as $dc)
							<center>
								<a href="https://twitter.com/{{ $dc->twitter }}" class="btn btn-default"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
							</center>
							@endforeach
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6" style="margin-top: 5px;">
							<center>
								<a href="https://instagram.com" class="btn btn-default"><i class="fa fa-instagram fa-fw"></i> <span class="network-name">Instagram</span></a>
							</center>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6" style="margin-top: 5px;">
							@foreach($databio as $dc)
							<center>
								<a href="https://facebook.com/{{ $dc->facebook }} " class="btn btn-default"><i class="fa fa-facebook"></i> <span class="network-name">Facebook</span></a>
							</center>
							@endforeach
						</div>
					</div>
				</div>
			</div><hr style="margin-top: 180px;">
			<div class="text-center white bottom">
				&copy; Intervey Political Consultant <?php echo date('Y') ?>
			</div>
		</div>
	</footer>
</body>
</html>
<script src="{{ asset('asset/js/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('asset/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>

<!-- Theme JavaScript -->
<script src="{{ asset('asset/js/agency.min.js')}}"></script>
<script type="text/javascript">
function terbaru($key) {
	location.href = "{{ asset('berita')}}"
}
function terpopuler($key) {
	location.href = "{{ asset('berita')}}"
}
function getLogin() {
	location.href = "{{ asset('login')}}"
}

function tambah_relawan(){		
	var nama  		= $('#nama').val();
	var nomer		= $('#no_hp').val();
	var agama		= $('#agama').val();	
	var alamat		= $('#alamat').val();
	var akses 		= $('#akses').val();

	if (nama=="") {
		alert("Nama Harus Diisi");
	} else if(nomer=="") {
		alert("Nomer Telefon Harus Di Pilih");
	} else if(agama=="") {
		alert("Agama Harus Di Pilih");
	}  else if(alamat=="") {
		alert("Alamat Harus Di Pilih");
	}  else{
		$.ajax({
			type : "get",
			url	 : "./tambah_relawan",
			data : {
				'nama'		: nama,
				'nomer'		: nomer,
				'agama'		: agama,
				'alamat'	: alamat,
				'akses'		: akses
			},
			success:function(html){
				window.location.reload();
			}
		});
	}
}  	

</script>
