@include('include.static-top')
@include('include.menu')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#"><i></i> Personalisasi</a></li>
      <li><a href="#"><i></i> Motto</a></li>
      </ol> 
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-md-4 col-sm-6.col-xs-12">
    			<div class="box box-primary">
      			<div class="box-body">
              <h5>Motto saat ini : </h5>
              @foreach($dataMotto as $mt)
              <h3>{{ $mt->judul }}</h3>
              <h4>{{ $mt->deskripsi }}</h4>
              @endforeach
            </div>
  			  </div>
  		  </div>
        <div class="col-md-8 col-sm-6.col-xs-12">
          <div class="box box-primary">
            <div class="box-body">
              <h4>
                Ganti Motto <br>
                <small>Buat motto yang akan mengajak masyarakat untuk memilih anda</small>
              </h4>
              <form action="{{ asset('admin/personalize/motto/update').'/'.$id_user }}" method="get">
                <div class="form-group">
                  <label>Motto Utama :</label>
                  <input type="text" name="judul" class="form-control">
                  <small id="emailHelp" class="form-text text-muted">Kami sarankan gunakan kalimat yang singkat namun padat</small>
                </div>
                <div class="form-group">
                  <label>Deskripsi :</label>
                  <input type="text" name="desk" class="form-control">
                  <small id="emailHelp" class="form-text text-muted">Sedikit jelaskan motto utama anda</small>
                </div>
                <div class="form-group">
                  <button class="btn green">SUBMIT</button>
                </div>
              </form>
            </div>
          </div>
        </div>
  	  </div>
    </section>
	<!-- /.content -->
</div>
 @include('include.footer')