@include('include.static-top')
@include('include.menu')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <!--  Personalisasi
       <small>Background</small> -->
       &nbsp;
      </h1>
      <ol class="breadcrumb">
	      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	      <li><a href="#"><i></i> Personalisasi</a></li>
	      <li><a href="#"><i></i> Background</a></li>
      </ol> 
    </section>
    <section class="content">
      	<div class="row">
      		<div class="col-md-6 col-sm-6 col-xs-12">
      			<div class="box box-primary">
	      			<div class="box-body">
	      				<h5>Gambar Latar belakang saat ini</h5>
	      				<center>
	      					@if($data_count == 0)
	      						<img src="{{ asset('assets/images/background/nopic.jpg') }}" alt="" class="img-person">
	      					@else 
		      					@foreach($data_background_active as $get)
			      				<img src="{{ asset('assets/images/background').'/'.$get->background }}" class="img-person">
			      				@endforeach
	      					@endif
	      				</center>
	      			</div>
    			</div>
    		</div>
    		<div class="col-md-6 col-sm-6 col-xs-12">
      			<div class="box box-primary">
	      			<div class="box-body">
	      				<h4>
	      					Tambah Gambar Latar Belakang<br>
	      					<small>Ukuran gambar yang disarankan : 1366 x 768 pixel</small>
	      				</h4>
	      				<form action="{{ asset('admin/personalize/background/update/').'/'.$id_user }}" method="post" enctype="multipart/form-data" novalidate>
	      					<div class="form-group">
	      						<input type="hidden" name="_token" value="{{ csrf_token() }}">
	      						<input type="file" name="gambar" class="form-control drop">
	      					</div>
							<div class="form-group">
								<button class="btn btn-green" type="submit">TAMBAH</button>
							</div>
	      				</form>
	      			</div>
    			</div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12 col-sm-12 col-xs-12">
    			<div class="box box-default">
    				<div class="box-body">
    					<table class="table table-bordered table-striped table-hover">
		    				<tr>
		    					<th class="text-center">No</th>
		    					<th class="text-center">Background</th>
		    					<th class="text-center">Status</th>
		    					<th class="text-right">Action</th>
		    				</tr>
		    				<?php $a = 1; ?>
		    				@foreach($dataBackground as $data)
		    				<tr>
		    					<td class="text-center" style="vertical-align: middle;">{{ $a++ }}</td>
		    					<td>
		    						<center>
		    							<img src="{{ asset('assets/images/background/').'/'.$data->background }}" alt="" style="width: 320px">
		    						</center>
		    					</td>
		    					<td class="text-center" style="vertical-align: middle;">
		    						<b>{{ $data->status }}</b>
		    					</td>
		    					<td class="text-right" style="vertical-align: middle;">
		    						@if($data->status == "ACTIVE")
		    							<button class="btn btn-green" onclick="toggleStatus({{ $data->id }}, 0)">NON-AKTIFKAN</button>
		    						@else
		    							<button class="btn btn-green" onclick="toggleStatus({{ $data->id }}, 1)">AKTIFKAN</button>
		    						@endif
		    						<button class="btn btn-green" onclick="delGambar({{ $data->id }})">HAPUS</button>
		    					</td>
		    				</tr>
		    				@endforeach
		    			</table>
		    			<div class="pull-right">
		    				<?php echo $dataBackground->render(); ?>
		    			</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
	<!-- /.content -->
</div>
 @include('include.footer')
 <script>
 	function toggleStatus(id, stat){
 		$.ajax({
 			type : "GET",
 			url  : "{{ asset('admin/personalize/background/toggle_activation/') }}",
 			data : {
 				"id" : id,
 				"stat" : stat
 			},
 			success:function(resp){
 				location.reload();
 			}
 		})
 	}
 	function delGambar(id){
 		var conf = confirm('Apakah anda yakin akan menghapus data ini?');
 		if(conf){
 			$.ajax({
	 			type : "GET",
	 			url  : "{{ asset('admin/personalize/background/delete/') }}",
	 			data : {
	 				"id" : id
	 			},
	 			success:function(resp){
	 				location.reload();
	 			}
	 		})
 		}
 	}
 </script>