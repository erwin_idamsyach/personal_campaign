  @include('include.static-top')
  @include('include.menu')
  <?php
  $ro = DB::table('tb_agenda')->where('id', $id)->get();
  foreach ($ro as $row) {                   
  }
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Agenda
        <small>Edit Data Agenda</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Agenda</li>
      <li class="active">Edit Data Agenda</li>
      </ol> 
    </section>
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Form Edit Agenda</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
            <!-- text -->
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Judul</label>
                      <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $row->judul ?>" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tanggal</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="date" class="form-control pull-right" name="date_start" id="date_start" value="<?php echo $row->date_start ?>" required="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Provinsi</label>
                      <select class="form-control" name="provinsi" id="provinsi" onchange="getEditKota2()" required="">
                            <?php
                              if($row->provinsi == ""){
                            ?>
                            <option value="">---select---</option>
                            <?php
                            $f = DB::table('m_geo_prov_kpu')->orderBy('geo_prov_nama', 'asc')->get();
                              foreach ($f as $o) {  
                            ?>
                              <option value="<?php echo $o->geo_prov_id ?>"><?php echo $o->geo_prov_nama ?></option>
                            <?php
                                }
                              }else{

                              $u = DB::table('m_geo_prov_kpu')
                              ->where('geo_prov_id', $row->provinsi)->get();
                              foreach ($u as $y) {
                              }
                              if(count($u) > 0){
                            ?>
                            <option value="<?php echo $y->geo_prov_id ?>"><?php echo $y->geo_prov_nama ?></option>
                            <?php
                              }else{
                            ?>
                            <option value="">---select---</option>
                            <?php
                              }
                              $f = DB::table('m_geo_prov_kpu')->where('geo_prov_id', '!=', $row->provinsi)->orderBy('geo_prov_nama', 'asc')->get();
                              foreach ($f as $o) {  
                            ?>
                              <option value="<?php echo $o->geo_prov_id ?>"><?php echo $o->geo_prov_nama ?></option>
                            <?php 
                              }
                            }
                            ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Kota</label>
                      <select class="form-control" name="kota" id="kota" onchange="getEditCamat2()" required="">
                        <?php
                          if($row->kabupaten == ""){
                        ?>
                        <option value="">---select---</option>
                        <?php
                          }else{
                          $u = DB::table('m_geo_kab_kpu')
                          ->where('geo_kab_id', $row->kabupaten)
                          ->where('geo_prov_id', $row->provinsi)->get();
                          foreach ($u as $y) {
                          }
                          if(count($u) > 0){
                        ?>
                        <option value="<?php echo $y->geo_kab_id ?>"><?php echo $y->geo_kab_nama ?></option>
                        <?php
                          }else{
                        ?>
                        <option value="">---select---</option>
                        <?php
                          }
                          $f = DB::table('m_geo_kab_kpu')
                          ->where('geo_kab_id', '!=', $row->kabupaten)
                          ->where('geo_prov_id', $row->provinsi)
                          ->orderBy('geo_kab_nama', 'asc')->get();
                          foreach ($f as $o) {  
                        ?>
                          <option value="<?php echo $o->geo_kab_id ?>"><?php echo $o->geo_kab_nama ?></option>
                        <?php 
                          }
                        }
                        ?>                    
                      </select>       
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Kecamatan</label>
                      <select class="form-control" name="kecamatan" id="kecamatan">
                        <?php
                          if($row->kecamatan == ""){
                        ?>
                        <option value="">---select---</option>
                        <?php
                          }else{

                          $u = DB::table('m_geo_kec_kpu')
                          ->where('geo_kec_id', $row->kecamatan)
                          ->where('geo_kab_id', $row->kabupaten)->get();
                          foreach ($u as $y) {
                          }
                          if(count($u) > 0){
                        ?>
                        <option value="<?php echo $y->geo_kec_id ?>"><?php echo $y->geo_kec_nama ?></option>
                        <?php
                          }else{
                        ?>
                        <option value="">---select---</option>
                        <?php
                          }
                          $f = DB::table('m_geo_kec_kpu')
                          ->where('geo_kab_id',  $row->kabupaten)
                          ->where('geo_kec_id', '!=', $row->kecamatan)
                          ->orderBy('geo_kec_nama', 'asc')->get();
                          foreach ($f as $o) {  
                        ?>
                          <option value="<?php echo $o->geo_kec_id ?>"><?php echo $o->geo_kec_nama ?></option>
                        <?php 
                          }
                        }
                        ?>                       
                      </select>       
                    </div>
                  </div>        
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Lokasi</label>
                      <input type="text" class="form-control" name="judul" id="lokasi" placeholder="lokasi"  value="<?php echo $row->lokasi ?>"  required="">
                    </div>
                  </div>               
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Deskripsi</label>
                      <textarea class="form-control" name="description" id="description" placeholder="Deskripsi" value="" required=""><?php echo $row->description ?></textarea>
                    </div>
                  </div>                   
                </div>
              </div>
              <!-- gambar -->
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Foto</label>
                      <form action="{{ asset('admin/upload_image_a') }}" id="frmuploadImg" method="post" target="iframeUploadImg" enctype="multipart/form-data">
                      <input type="file" name="image" id="image" class="form-control drop" data-height="200" onchange="submitImage()" data-default-file="{{ $row->image_link }}">
                      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                      </form>
                      <input class="hidden" type="text" name="gambarout" id="gambarout">
                      <iframe class="hidden" name="iframeUploadImg" id="iframeUploadImg"></iframe>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-12" id="sizepic"></div>
                  </div>
                </div>
              </div>
             
            </div>
          </div>
        </div>
          <div class="box-footer clearfix" align="right">
            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
            <button type="submit" class="btn btn-success" onclick="edit_a('<?php echo $row->id ?>')">Edit</button>
          </div>
        </div>
        
        
      </div>
    </section>
    <!-- /.content -->
  </div>
 @include('include.footer')
 
