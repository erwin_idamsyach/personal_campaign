<?php
	$qu = DB::table('tb_agenda')->where('id', $id)->get();
	foreach ($qu as $k) {
	}
?>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<!-- text -->
			<div class="col-md-8">
				<div class="col-md-2">
					<label>Judul</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->judul ?></label>
				</div>			
				<div class="col-md-2">
					<label>Tanggal</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->date_start ?></label>
				</div>		
				<div class="col-md-2">
					<label>Lokasi</label>
				</div>
				<div class="col-md-10">
					<?php 
					$u = DB::table('m_geo_prov_kpu')
	                  	->where('geo_prov_id', $k->provinsi)->get();
	                  	foreach ($u as $y) {
	                  	}
	                  	if(count($u) > 0){
	                  	$prov = $y->geo_prov_nama;
	                  	}else{$prov="";}
					$u2 = DB::table('m_geo_kab_kpu')
	                  	->where('geo_kab_id', $k->kabupaten)->get();
	                  	foreach ($u2 as $y2) {
	                  	}
	                  	if(count($u2) > 0){
	                  	$kab = $y2->geo_kab_nama;
	                  	}else{$kab="";}           
	                $u3 = DB::table('m_geo_kec_kpu')
	                  	->where('geo_kec_id', $k->kecamatan)->get();
	                  	foreach ($u3 as $y3) {
	                  	}
	                  	if(count($u3) > 0){
	                  	$kec = $y3->geo_kec_nama;
	                  	}else{$kec="";} 
					 ?>
					<label>: <?php echo "$prov - $kab - $k->lokasi $kec" ?></label>
				</div>	
				<div class="col-md-2">
					<label>Deskripsi</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->description ?></label>
				</div>										
			</div>
			<!-- gambar -->
			<div class="col-md-4">
				<div class="col-md-12">
		          <img src="<?php echo $k->image_link; ?>" style="height: 70%; width: 70%;">
		        </div>	
			</div>
		</div>		
	</div>
</div>