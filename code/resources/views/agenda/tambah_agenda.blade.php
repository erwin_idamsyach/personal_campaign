  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Agenda
        <small>Tambah Data Agenda</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Agenda</li>
      <li class="active">Tambah Data Agenda</li>
      </ol> 
    </section>
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Tambah Data Agenda</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Judul <font color="red">(*)</font></label>
                      <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tanggal <font color="red">(*)</font></label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="date" class="form-control pull-right" name="date_start" id="date_start" required="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Provinsi <font color="red">(*)</font></label>
                      <select class="form-control" name="provinsi" id="provinsi" onchange="getKota2()" required="">
                        <option value="">---select---</option>
                        <?php
                          $u = DB::table('m_geo_prov_kpu')->orderBy('geo_prov_nama', 'asc')->get();
                          foreach ($u as $y) {
                        ?>
                        <option value="<?php echo $y->geo_prov_id ?>"><?php echo $y->geo_prov_nama ?></option>
                        <?php
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Kota <font color="red">(*)</font></label>
                      <select class="form-control" name="kota" id="kota" onchange="getCamat2()" required="">
                        <option value="">---select---</option>
                      </select>       
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Kecamatan <font color="red">(*)</font></label>
                      <select class="form-control" name="kecamatan" id="kecamatan">
                        <option value="">---select---</option>
                      </select>       
                    </div>
                  </div>        
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Lokasi <font color="red">(*)</font></label>
                      <input type="text" class="form-control" name="judul" id="lokasi" placeholder="lokasi" required="">
                    </div>
                  </div>  
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Deskripsi <font color="red">(*)</font></label>
                      <textarea class="form-control" name="description" id="description" placeholder="Deskripsi" required=""></textarea>
                    </div>
                  </div>                  
                </div>
              </div>
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Foto <font color="red">(*)</font></label>
                      <form action="{{ asset('admin/upload_image_a') }}" id="frmuploadImg" method="post" target="iframeUploadImg" enctype="multipart/form-data">
                        <input type="file" name="image" id="image" class="form-control drop" data-height="200" onchange="submitImage()">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                      </form>
                      <input class="hidden" type="text" name="gambarout" id="gambarout">
                      <iframe class="hidden" name="iframeUploadImg" id="iframeUploadImg"></iframe>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-12" id="sizepic"></div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>  
        </div>
        <div class="box-footer clearfix" align="right">
          <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
          <button type="submit" class="btn btn-success" onclick="tambah_a()">Simpan</button>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
 @include('include.footer')
 