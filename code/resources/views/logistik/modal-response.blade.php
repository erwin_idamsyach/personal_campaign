<div class="row">
	@foreach($dataLogistik as $data)
	<div class="col-md-4">
		<center>
			<img src="{{ asset('assets/images/logistik/').'/'.$data->image_link }}" style="width: 120px; height: 140px">
		</center>
	</div>
	<div class="col-md-8">
		<table>
			<tr>
				<td class="pad-left">Nama barang</td>
				<td class="pad-mid">:</td>
				<td class="pad-right">{{ $data->nama_barang }}</td>
			</tr>
			<tr>
				<td class="pad-left">Kategori</td>
				<td class="pad-mid">:</td>
				<td class="pad-right">{{ $data->kategori_name }}</td>
			</tr>
			<tr>
				<td class="pad-left">Stok</td>
				<td class="pad-mid">:</td>
				<td class="pad-right">{{ $data->stok }}</td>
			</tr>
		</table>
		<table class="table table-bordered table-striped table-hover">
			<tr>
				<th class="text-center">No</th>
				<th class="text-center">Ukuran / Warna / Jenis</th>
				<th class="text-center">Stok</th>
			</tr>
			<?php $a = 1; ?>
			@foreach($dataDetail as $get)
			<tr>
				<td class="text-center">{{ $a++ }}</td>
				<td>{{ $get->detail }}</td>
				<td>{{ $get->stok }}</td>
			</tr>
			@endforeach
		</table>
	</div>
	@endforeach
</div>
