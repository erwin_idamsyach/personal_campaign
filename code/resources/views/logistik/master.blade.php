  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb" style="position: relative; right: 52%; margin-top: -36px;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Logistik</li>
        <li class="active"><a href="#">Stok</a></li>
      </ol>     
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px;">
      <a href="{{asset('admin/logistik/master/add')}}" class="btn btn-green btn-sm">TAMBAH</a>
    </div>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <div class="pull-left">
                <h4 class="title">Stok Logistik</h4>
              </div>
              <div class="pull-right">
                <button class="btn btn-green btn-xs" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Nama Barang</th>
                    <th class="text-center">Kategori</th>
                    <th class="text-center">Stok</th>
                    <th class="text-center">Foto</th>
                    <th class="text-center" width="200px">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataLogistik as $data)
                  <tr>
                    <td class="text-center">{{ $data->id }}</td>
                    <td>{{ $data->nama_barang }}</td>
                    <td>{{ $data->kategori_name }}</td>
                    <td class="text-center">{{ $data->stok }}</td>
                    <td class="text-center">
                      @if($data->image_link == "")
                        <img src="{{ asset('assets/images/logistik/nopic.jpg') }}" style="width: 50px; height: 40px;">
                      @else
                        <img src="{{ asset('assets/images/logistik/').'/'.$data->image_link }}" style="width: 60px; height: 58px;">
                      @endif
                    </td>
                    <td class="text-right">
                      <button class="btn btn-green btn-sm" data-toggle="tooltip" data-placement="bottom" title="Detail" onclick="viewDetailLogistik({{ $data->id }})">
                        <i class="fa fa-eye"></i>
                      </button>
                      <button class="btn btn-green btn-sm" data-toggle="tooltip" data-placement="bottom" title="Add Stok" onclick="addStock({{ $data->id }})">
                        <i class="fa fa-plus"></i>
                      </button>
                      <button class="btn btn-green btn-sm" data-toggle="tooltip" data-placement="bottom" title="Edit Detail">
                        <i class="fa fa-edit"></i>
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="pull-right">
                <?php echo $dataLogistik->render(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="modal fade" id="modal-detail">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="height: 46px; background-color: #870b04; color: #fff">
            <h4 class="title" style="margin-top: -5px; font-size: 14px;">Detail Logistik</h4>
            <div class="pull-right" style="margin-top: -30px">
              <button class="btn btn-green btn-sm" data-dismiss="modal">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <div class="modal-body" id="area-response">
            
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-stok">
      <div class="modal-dialog" id="area-form">
        <div class="modal-content">
          <form id="form-stok" action="{{ asset('admin/logistik/add_stock/action') }}" method="get">
          <div class="modal-header" style="height: 46px; background-color: #870b04; color: #fff">
            <h4 class="title" style="margin-top: -5px; font-size: 14px;">Add Stok</h4>
            <div class="pull-right" style="margin-top: -30px">
              <button class="btn btn-green btn-sm" data-dismiss="modal">CANCEL</button>
              <button class="btn btn-green btn-sm" type="submit">SAVE</button>
            </div>
          </div>
          <div class="modal-body">
            <table>
            <tr>
              <td class="pad-left">Nama barang</td>
              <td class="pad-mid">:</td>
              <td class="pad-right" id="area-nama"></td>
            </tr>
            <tr>
              <td class="pad-left">Kategori</td>
              <td class="pad-mid">:</td>
              <td class="pad-right" id="area-kategori"></td>
            </tr>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="id_log" id="id_log">
              <table class="table table-bordered table-striped table-hover">
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Ukuran / Warna / Jenis</th>
                  <th class="text-center">Stok Sekarang</th>
                  <th class="text-center" style="width: 16%">Tambahan Stok</th>
                </tr>
                <tbody id="area-append">
                  
                </tbody>
                <input type="hidden" id="jumlah_data" name="jumlah_data">
              </table>
          </table>
          </div>
          </form>
          </div>
      </div>
    </div>
  <!-- /.content -->
</div>
@include('include.footer')
<script type="text/javascript">
  function viewDetailLogistik(key){
    /*alert(key)*/
    $.ajax({
      type : "GET",
      url : "{{ asset('admin/logistik/viewDetail') }}",
      data : "key="+key,
      success:function(resp){
        $("#modal-detail").modal('show');
        $("#area-response").html(resp);
      }
    });
  }

  function addStock(key){
    $.ajax({
      type : "GET",
      url : "{{ asset('admin/logistik/add_stock') }}",
      data : "key="+key,
      dataType : "json",
      success:function(data){
        /*alert(data['info'][0]['nama_barang'])*/
        $("#modal-stok").modal('show');
        $("#area-nama").text(data['info'][0]['nama_barang']);
        $("#area-kategori").text(data['info'][0]['kategori']);
        $("#id_log").val(data['info'][0]['id_log']);
        $("#jumlah_data").val(data['dataLength']);
        for (var a = 0; a < data['detail'].length; a++) {
          $("#area-append").append('<tr>'+
                  '<td class="text-center" id="area-nomor"></td>'+
                  '<td id="area-detail">'+data['detail'][a]['detail']+'</td>'+
                  '<td class="text-center">'+data['detail'][a]['stok']+'</td>'+
                  '<td>'+
                    '<input type="hidden" name="id_detail[]" class="form-control input-sm" value="'+data['detail'][a]['id_detail']+'">'+
                    '<input type="number" name="stok[]" class="form-control input-sm">'+
                  '</td>'+
                '</tr>')
        }
        /*$("#area-form").html(resp)*/
      }
    })
  }
  $("#modal-stok").on('hide.bs.modal', function(e){
    /*alert("YES");*/
    $("#area-nama").text('');
    $("#area-kategori").text('');
    $("#id_log").val('');
    $("#jumlah_data").val('');
    $("#area-append").html("");
  });
</script>