<div class="modal-content">
  <div class="modal-header" style="height: 46px; background-color: #870b04; color: #fff">
    <h4 class="title" style="margin-top: -5px; font-size: 14px;">Add Stok</h4>
    <div class="pull-right" style="margin-top: -30px">
      <button class="btn btn-green btn-sm" data-dismiss="modal">CANCEL</button>
      <form id="form-stok" action="{{ asset('admin/logistik/add_stock/action') }}" method="get">
      <button class="btn btn-green btn-sm" type="submit">SAVE</button>
    </div>
  </div>
  <div class="modal-body">
    <table>
		@foreach($dataLogistik as $data)
		<tr>
			<td class="pad-left">Nama barang</td>
			<td class="pad-mid">:</td>
			<td class="pad-right">{{ $data->nama_barang }}</td>
		</tr>
		<tr>
			<td class="pad-left">Kategori</td>
			<td class="pad-mid">:</td>
			<td class="pad-right">{{ $data->kategori_name }}</td>
		</tr>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_log" value="{{ $data->id }}">
		@endforeach
			<table class="table table-bordered table-striped table-hover">
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Ukuran / Warna / Jenis</th>
					<th class="text-center">Stok Sekarang</th>
					<th class="text-center" style="width: 16%">Tambahan Stok</th>
				</tr>
				<?php $a = 1; ?>
				@foreach($dataStok as $get)
				<tr>
					<td class="text-center">{{ $a++ }}</td>
					<td>{{ $get->detail }}</td>
					<td class="text-center">{{ $get->stok }}</td>
					<td>
						<input type="hidden" name="id_detail[]" class="form-control input-sm" value="{{ $get->id }}">
						<input type="number" name="stok[]" class="form-control input-sm">
					</td>
				</tr>
				@endforeach
				<input type="hidden" name="jumlah_data" value="{{ $a-1 }}">
			</table>
		</form>
	</table>
  </div>
</div>
<script>
function saveStock(){
	$("#form-stok").trigger('submit');
}

</script>