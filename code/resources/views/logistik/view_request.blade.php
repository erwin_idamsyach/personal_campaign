  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb hidden-sm hidden-xs" style="position: relative; right: 40%; margin-top: -36px;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Logistik</li>
        <li class="active"><a href="#">Request List</a></li>
      </ol>     
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px;">
      <!-- <a href="{{ asset('admin/logistik/master') }}" style="color: #3A3A3A; margin-bottom: 10px;">
        <i class="fa fa-arrow-left"></i> Kembali
      </a>&nbsp;&nbsp;
      <button class="btn btn-green btn-sm" id="btn-submit">SIMPAN</button> -->
    </div>
    <section class="content">
      <div class="box box-primary">
        <div class="box-header">
          <div class="pull-left">
            <h4 class="title">Request List</h4>
          </div>
          <div class="pull-right">
            <button class="btn btn-green btn-xs" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">Request From</th>
                <th class="text-center">Stok Requested</th>
                <th class="text-center">Request Date</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($dataJoined as $data)
              <tr>
                <td class="text-center">{{ $data->id }}</td>
                <td>{{ $data->nama }}</td>
                <td>
                  <?php
                  $getD = DB::table('tb_request_detail')
                      ->select(DB::raw("SUM(stok_requested) as stok_req"))
                      ->where('id_request', $data->id)
                      ->get();
                  foreach ($getD as $d) {
                    echo $d->stok_req;
                  }
                  ?>
                </td>
                <td>{{ $data->create_date }}</td>
                <td class="text-right">
                  <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" title="Detail" onclick="viewDetail({{ $data->id }})"><i class="fa fa-eye"></i></button>
                  <!-- <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></button> -->
                  @if($data->status == "WAITING")
                  <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" title="Approve" onclick="toggleStat({{ $data->id }}, 1)"><i class="fa fa-check"></i></button>
                  <button class="btn btn-maroon" data-toggle="tooltip" data-placement="bottom" title="Decline" onclick="toggleStat({{ $data->id }}, 0)"><i class="fa fa-times"></i></button>
                  @else
                   @if($data->status == "APPROVE")
                    <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" title="Approved" disabled=""><i class="fa fa-check"></i></button>
                   @else
                    <button class="btn btn-maroon" data-toggle="tooltip" data-placement="bottom" title="Declined" disabled=""><i class="fa fa-times"></i></button>
                   @endif
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <div class="modal fade" id="modal-detail">
      <div class="modal-dialog" id="area-form">
        <div class="modal-content">
          <div class="modal-header" style="height: 46px; background-color: #870b04; color: #fff">
            <h4 class="title" style="margin-top: -5px; font-size: 14px;">Add Stok</h4>
            <div class="pull-right" style="margin-top: -30px">
              <button class="btn btn-green btn-sm" data-dismiss="modal">CLOSE</button>
            </div>
          </div>
          <div class="modal-body">
            <table>
            <tr>
              <td class="pad-left">Request From</td>
              <td class="pad-mid">:</td>
              <td class="pad-right" id="area-nama"></td>
            </tr>
            <tr>
              <td class="pad-left">Request Description</td>
              <td class="pad-mid">:</td>
              <td class="pad-right" id="area-desc"></td>
            </tr>
            <tr>
              <td class="pad-left">Request Date</td>
              <td class="pad-mid">:</td>
              <td class="pad-right" id="area-date"></td>
            </tr>
              <table class="table table-bordered table-striped table-hover">
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Nama Barang</th>
                  <th class="text-center">Stok Diminta</th>
                </tr>
                <tbody id="area-append">
                  
                </tbody>
                <input type="hidden" id="jumlah_data" name="jumlah_data">
              </table>
          </table>
          </div>
          </div>
      </div>
    </div>
  <!-- /.content -->
</div>
@include('include.footer')
<script type="text/javascript">
  function viewDetail(key){
    $.ajax({
      type : "GET",
      url : "{{ asset('admin/logistik/request-list/get_detail') }}",
      data : "key="+key,
      dataType : "json",
      success:function(data){
        $("#modal-detail").modal('show');
        $("#area-nama").text(data['info'][0]['req_by']);
        $("#area-desc").text(data['info'][0]['req_desc']);
        $("#area-date").text(data['info'][0]['req_date']);

        for (var a = 0; a < data['detail'].length; a++) {
          $("#area-append").append('<tr>'+
            '<td class="text-center">'+parseInt(a+1)+'</td>'+
            '<td>'+data['detail'][a]['nama_barang']+'</td>'+
            '<td class="text-center">'+data['detail'][a]['stok_requested']+'</td>'+
            '</tr>')
        }
      }
    });
  }
  function toggleStat(key, state){
    $.ajax({
      type : "GET",
      url : "{{ asset('admin/logistik/toggle-state') }}",
      data : {
        'key' : key, 'state' : state
      },
      success:function(resp){
        location.reload();
      }
    })
  }
</script>