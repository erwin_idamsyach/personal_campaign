  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb hidden-sm hidden-xs" style="position: relative; right: 22%; margin-top: -36px;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Logistik</li>
        <li class="active"><a href="#">Add Master Logistik</a></li>
      </ol>     
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px;">
      <a href="{{ asset('admin/logistik/master') }}" style="color: #3A3A3A; margin-bottom: 10px;">
        <i class="fa fa-arrow-left"></i> Kembali
      </a>&nbsp;&nbsp;
      <button class="btn btn-green btn-sm" id="btn-submit">SIMPAN</button>
    </div>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <div class="pull-left">
                <h4 class="title">Add Master Logistik</h4>
              </div>
              <div class="pull-right">
                <button class="btn btn-green btn-xs" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <form id="form-logistik" enctype="multipart/form-data" method="post" action="{{ asset('admin/logistik/master/act_add') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="row">
                <div class="col-md-4">
                  <div class="row">
                    <div class="col-md-12">
                      <b class="ro-bold px-12">Informasi Barang</b>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-4">Nama Barang</label>
                    <div class="col-md-8">
                      <input type="text" name="nama_barang" class="form-control input-sm">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-4">Kategori</label>
                    <div class="col-md-8">
                      <select name="kategori" id="" class="form-control input-sm">
                        <option value="">--Pilih--</option>
                        @foreach($dataKategori as $get)
                        <option value="{{ $get->id }}">{{ $get->kategori_name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-4">Stok</label>
                    <div class="col-md-8">
                      <input type="text" name="stok" class="form-control input-sm">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-12">
                      <b class="ro-bold px-12">Detail Barang</b>
                    </div>
                  </div>
                  <div id="area-append">
                    <div class="form-group row" id="detail-1">
                      <div class="col-md-5 col-sm-5">
                        <input type="text" name="detail[]" class="form-control input-sm" placeholder="ukuran / berat / warna">
                      </div>
                      <div class="col-md-5 col-sm-5">
                        <input type="text" name="stok_detail[]" class="form-control input-sm" placeholder="stok">
                      </div>
                      <div class="col-md-2 col-sm-2">
                        <button class="btn btn-green btn-xs" type="button" id="btn-append"><i class="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" id="detail_count" name="detail_count" value="1">
                </div>
                <div class="col-md-2">
                  <div class="row">
                    <div class="col-md-12">
                      <b class="ro-bold px-12">Foto</b>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <input type="file" name="foto" class="form-control drop">
                    </div>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  <!-- /.content -->
</div>
@include('include.footer')
<script type="text/javascript">
  $("#btn-submit").on('click', function(){
    $("#form-logistik").trigger('submit');
  });
  $("#form-logistik").on('submit', function(){
    var formData = new FormData($(this)[0]);
    $.ajax({
      type : "POST",
      url : "{{ asset('admin/logistik/master/act_add') }}",
      data : formData,
      async : false,
      success:function(resp){
        if(confirm('Apakah anda akan menambahkan item baru?')){
          location.reload();
        }else{
          window.location=("{{ asset('admin/logistik/master') }}");
        }
      },
      cache: false,
      contentType: false,
      processData: false,
      error:function(){
        if(confirm('Apakah anda akan menambahkan item baru?')){
          location.reload();
        }else{
          window.location=("{{ asset('admin/logistik/master') }}");
        }
      }
    })
    return false;
  });

  $("#btn-append").on('click', function(){
    var cnt = $("#detail_count").val();
    cnt = parseInt(cnt);
    cnt = cnt + 1;
    $("#detail_count").val(cnt);
    $("#area-append").append('<div class="form-group row" id="detail-'+cnt+'">'+
                      '<div class="col-md-5 col-sm-5">'+
                        '<input type="text" name="detail[]" class="form-control input-sm" placeholder="ukuran / berat / warna">'+
                      '</div>'+
                      '<div class="col-md-5 col-sm-5">'+
                        '<input type="text" name="stok_detail[]" class="form-control input-sm" placeholder="stok">'+
                      '</div>'+
                      '<div class="col-md-2 col-sm-2">'+
                        '<button class="btn btn-maroon btn-xs" type="button" onclick="removeDetail('+cnt+')"><i class="fa fa-times"></i></button>'+
                      '</div>'+
                    '</div>')
  });
  function removeDetail(cnt){
    $("#detail-"+cnt).remove();
  }
</script>