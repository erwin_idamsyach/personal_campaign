<div id="containerLine{{ $type }}" style="min-width: 100%; height: 205px; margin: 0 auto"></div>

<?php
	if($type == 'mahasiswa'){
		$struktur = 'Mahasiswa';
	} else if($type == 'user'){
		$struktur = 'User';
	} else if($type == 'bisnis'){
		$struktur = 'Bisnis';
	} else if($type == 'agenda'){
		$struktur = 'Agenda';
	} else {
		$struktur = '';		
	}
?>

<script type="text/javascript">
	Highcharts.chart('containerLine{{ $type }}', {
		chart: {
			type: 'column',
			backgroundColor: 'rgba(255, 255, 255,0)'
		},
		title: {
			text: 'DATA {{ strtoupper($struktur) }}'
		},
		credits: {
			enabled : false
		}, 
		xAxis: {     
			categories: [
				'DATA {{ strtoupper($struktur) }}'
			],
			crosshair: true
		},
		tooltip: {
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:f}</b></td></tr>'
		},
		yAxis: {
			min: 0,
			title: {
				text: false
			}
		},
		plotOptions: {
			series: {
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					format: '{point.y:f}'
				}
			}
		}<?php echo $dataSeries; ?>
	});
</script>