  @include('include.static-top')
  @include('include.menu')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="pull-left">
      <section class="content-header">
        <h1>
          &nbsp;
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
      </section>
    </div>
    <div class="pull-right">
      <div class="row">
        <div class="col-md-12">
          <div class="pull-right">
            <div class="form-group" style="margin-top: 13px; margin-right: 15px;">
              <select name="dapil" id="dapil" class="form-control" style="font-size: 11px;">
                <option value="">--Pilih Dapil--</option>
                @foreach($dataDapil as $dap)
                <option value="{{ $dap->kode_dapil }}">{{ $dap->nama_dapil }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">
      	<div class="col-md-12">
      		<div class="box box-primary">
      			<div class="box-body">
      				<div id="map" style="height: 560px; background-color: transparent;"></div>
      			</div>
      		</div>
      	</div>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="panel panel-default" style="min-height: 217px;">
            <div class="box-header">
              <h4 class="text-center title-box">DATA DAPIL JATIM XI</h4>
            </div>
            <div class="panel-body">              
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                  <h5 style="margin-top: 1px; margin-bottom: 2px;">Kab/Kota</h5>
                  <h2 style="margin-top: 3px;">4</h2>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                  <h5 style="margin-top: 1px; margin-bottom: 2px;">Kecamatan</h5>
                  <h2 style="margin-top: 3px;">72</h2>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                  <h5 style="margin-top: 1px; margin-bottom: 2px;">Kelurahan</h5>
                  <h2 style="margin-top: 3px;">990</h2>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                  <h5 style="margin-top: 4px; margin-bottom: 2px">TPS</h5>
                  <h2 style="margin-top: 3px;">8.490</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="panel panel-default" style="min-height: 217px;">
            <div class="box-header">
              <h4 class="text-center title-box">DAFTAR PEMILIH TETAP</h4>
            </div>
            <div class="panel-body">
              <h1 class="text-center" style="margin-top: 40px;">3.861.686</h1>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="panel panel-default" style="min-height: 217px;">
            <div class="box-header">
              <h4 class="text-center title-box">RELAWAN</h4>
            </div>
            <div class="panel-body">
              <h1 class="text-center font-bigger" style="margin-top: 24px;">002</h1>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="panel panel-default" style="min-height: 217px;">
            <div class="box-header">
              <h4 class="text-center title-box">TOTAL LOGISTIK TERSEBAR</h4>
            </div>
            <div class="panel-body">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEVojWiqvNgSpxAtagsYYIvu1JSm00lA0&callback=initMap"></script>
 <script>
  var iconKab = "{{ asset('asset/img/maps/user-black-orange.png') }}";
  var arrayKab = [
    @foreach($kabupaten as $get)
    ["{{ $get->geo_kab_nama }}", {{ $get->geo_kab_lat }}, {{ $get->geo_kab_lng }}],
    @endforeach
  ];
  var arrayKec = [
    @foreach($kecamatan as $data)
    ["{{ $data->geo_kec_nama }}", {{ $data->geo_kec_lat }}, {{ $data->geo_kec_lng }}]
    @endforeach
  ]
  function initMap() {
    var uluru = {lat: -7.050144, lng: 113.305491};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center : uluru,
      draggable : true
    });

    for (var i = 0; i < arrayKab.length; i++) {
      var markerKab = new google.maps.Marker({
          position: {lat : arrayKab[i][1], lng : arrayKab[i][2]},
          map: map,
          title : arrayKab[i][0]
        });
    }
  }
</script>
 @include('include.footer')