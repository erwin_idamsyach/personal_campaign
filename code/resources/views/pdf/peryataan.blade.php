<html>
<h2><center>SURAT PERYATAAN KESANGGUPAN<br/>BAKAL CALON LEGISLATIF (BACALEG) DPR-RI</center></h2>
<br/>
<br/>
<br/>
<table style="margin-left:40px;">
  <tr>
    <td width="10px">&nbsp;</td>
    <td width="100px">&nbsp;</td>
    <td width="300px">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">
      Yang bertanda tangan di bawah ini, saya :
    </td>
  <tr>
    <td colspan="2" valign="top">
      Nama
    </td>
    <td>
      {{$dataCaleg->nama}}
    </td>
  </tr>
  <tr>
    <td valign="top" colspan="2">
      Tempat/Tanggal Lahir
    </td>
    <td valign="top">
      {{$dataCaleg->tempat_lahir}} {{$dataCaleg->tanggal_lahir}}
    </td>
  </tr>
  <tr>
    <td valign="top" colspan="2">
      Jenis Kelamin
    </td>
    <td valign="top">
      {{$dataCaleg->jenis_kelamin}}
    </td>
  </tr>
  <tr>
    <td valign="top" colspan="2">
      Agama
    </td>
    <td valign="top">
      {{$dataCaleg->agama}}
    </td>
  </tr>
  <tr>
    <td  valign="top" colspan="2">
      Pekerjaan
    </td>
    <td>
      {{$dataCaleg->status_kawin}}
    </td>
  </tr>
  <tr>
    <td  valign="top" colspan="2">
      Alamat Tempat Tinggal
    </td>
    <td valign="top">
      JL. {{$dataCaleg->alamat}}<br/>
      Desa/Kelurahan {{$kelurahan_nama}}<br/>
      Kec. {{$kecamatan_nama}} Kab./ Kota {{$kabupaten_nama}}<br/>
      Provinsi {{$provinsi_nama}} Kode Pos :
    </td>
  </tr>
  <tr>
    <td colspan="3">
      Dengan ini menyatakan kesanggupan saya sebagai Bacaleg DPR-RI sebagai berikut :
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td colspan="2">Sanggup memenuhi persyaratan Bacaleg/Caleg DPR-RI yang ditentukan olh peraturan perundang - undangan dan kebijakan Partai HANURA</td>
  </tr>
  <tr>
    <td>2</td>
    <td colspan="2">Sanggu mengikuti kegiatan dan acara - acara partai HANURA baik di tingkat nasional maupun daerah.</td>
  </tr>
  <tr>
    <td>3</td>
    <td colspan="2">Sangup mengikuti kegiatan Pengaderan tingkat Nasional (KADER UTAMA) / Penyegaran KADER UTAMA </td>
  </tr>
  <tr>
    <td>4</td>
    <td colspan="2">Sangup mengukuti pembekalan Bacaleg / Caleg DPR-RI dan Juru Kampanye Nasional</td>
  </tr>
  <tr>
    <td>5</td>
    <td colspan="2">Sangup mengikuti <i>Fit and Proper Test</i> Seleksi Caleg DPR-RI yang dilaksanakan oleh DPP Partai HANURA</td>
  </tr>
  <tr>
    <td colspan="3">
      Demikian Surat Pernytaan Kesanggupan  ini saya buat dan di tandatangani dalam keadaan sadar rohani dan jasmani. Apabila dikemudian hari terdapat hal yang tidak benar, saya bersedia menerima keputusan Partai HANURA.
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
    </td>
    <td>
      <center>Jakarta, ............................ 2017<br/><br/>Yang membuat<br/><br/><br/><br/><br/>( {{$dataCaleg->nama}} )</center>
    </td>
  </tr>
</table>
</html>
