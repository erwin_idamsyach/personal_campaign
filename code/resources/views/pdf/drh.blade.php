<html>
<h2><center>DAFTAR RIWAYAT HIDUP<br/>BAKAL CALON LEGISLATIF (BACALEG) DPR-RI</center></h2>
<br/>
<br/>
<br/>
<br/>
<br/>
<table style="margin-left:40px;">
  <tr>
    <td valign="top" width="1px">
      1.
    </td>
    <td valign="top" width="100px">
      Nama Lengkap
    </td>
    <td width="200px">
      {{$dataCaleg->nama}}
    </td>
  </tr>
  <tr>
    <td valign="top">
      2.
    </td>
    <td valign="top">
      Tempat Tanggal Lahir
    </td>
    <td valign="top">
      {{$dataCaleg->tempat_lahir}} {{$dataCaleg->tanggal_lahir}}
    </td>
  </tr>
  <tr>
    <td valign="top">
      3.
    </td>
    <td valign="top">
      Jenis Kelamin
    </td>
    <td valign="top">
      {{$dataCaleg->jenis_kelamin}}
    </td>
  </tr>
  <tr>
    <td valign="top">
      4.
    </td>
    <td valign="top">
      Agama
    </td>
    <td valign="top">
      {{$dataCaleg->agama}}
    </td>
  </tr>
  <tr>
    <td valign="top">
      5.
    </td>
    <td  valign="top">
      Status Perkawinan
    </td>
    <td>
      {{$dataCaleg->status_kawin}}<br/>
      a. Nama Istri / Suami {{$dataCaleg->pasangan}}<br/>
      b. Jumlah Anak {{$dataCaleg->anak}}<br/>
    </td>
  </tr>
  <tr>
    <td valign="top">
      6.
    </td>
    <td  valign="top">
      Alamat Tempat Tinggal
    </td>
    <td valign="top">
      JL. {{$dataCaleg->alamat}}<br/>
      Desa/Kelurahan {{$kelurahan_nama}}<br/>
      Kec. {{$kecamatan_nama}} Kab./ Kota {{$kabupaten_nama}}<br/>
      Provinsi {{$provinsi_nama}} Kode Pos :
    </td>
  </tr>
  <tr>
    <td valign="top">
      7.
    </td>
    <td valign="top">
      Riwayat Pendidikan
    </td>
    <td>
      a. {{isset($pendidikan[0]->keterangan)?$pendidikan[0]->keterangan:''}} <br>
      b. {{isset($pendidikan[1]->keterangan)?$pendidikan[1]->keterangan:''}}<br>
      c. {{isset($pendidikan[2]->keterangan)?$pendidikan[2]->keterangan:''}}<br>
      d. {{isset($pendidikan[3]->keterangan)?$pendidikan[3]->keterangan:''}}<br>
      e. {{isset($pendidikan[4]->keterangan)?$pendidikan[4]->keterangan:''}}<br>
      f. {{isset($pendidikan[5]->keterangan)?$pendidikan[5]->keterangan:''}}<br>
      g. {{isset($pendidikan[6]->keterangan)?$pendidikan[6]->keterangan:''}}<br>
      h. {{isset($pendidikan[7]->keterangan)?$pendidikan[7]->keterangan:''}}
    </td>
  </tr>
  <tr>
    <td valign="top">
      8.
    </td>
    <td valign="top">
      Kusus/ Diklat yang Pernah diikuti
    </td>
    <td valign="top">
      a.<br>
      b.<br>
      c.<br>
      d.<br>
      e.<br>
      f.<br>
      g.<br>
      h.
    </td>
  </tr>
  <tr>
    <td valign="top">
      9.
    </td>
    <td valign="top">
      Riwayat Pekerjaan
    </td>
    <td valign="top">
      a.<br>
      b.<br>
      c.<br>
      d.<br>
      e.<br>
      f.<br>
      g.<br>
      h.
    </td>
  </tr>
  <tr>
    <td valign="top">
      10.
    </td>
    <td valign="top">
      Riwayat Organisasi
    </td>
    <td valign="top">
      a. {{isset($organisasi[0]->keterangan)?$organisasi[0]->keterangan:''}} <br>
      b. {{isset($organisasi[1]->keterangan)?$organisasi[1]->keterangan:''}}<br>
      c. {{isset($organisasi[2]->keterangan)?$organisasi[2]->keterangan:''}}<br>
      d. {{isset($organisasi[3]->keterangan)?$organisasi[3]->keterangan:''}}<br>
      e. {{isset($organisasi[4]->keterangan)?$organisasi[4]->keterangan:''}}<br>
      f. {{isset($organisasi[5]->keterangan)?$organisasi[5]->keterangan:''}}<br>
      g. {{isset($organisasi[6]->keterangan)?$organisasi[6]->keterangan:''}}<br>
      h. {{isset($organisasi[7]->keterangan)?$organisasi[7]->keterangan:''}}
    </td>
  </tr>
  <tr>
    <td valign="top">
      11.
    </td>
    <td valign="top">
      Tanda Penghargaan
    </td>
    <td valign="top">
      a.<br>
      b.<br>
      c.<br>
      d.<br>
      e.<br>
      f.<br>
      g.<br>
      h.
    </td>
  </tr>
  <tr>
    <td valign="top">
      12.
    </td>
    <td valign="top">
      Tanda Perjuangan
    </td>
    <td valign="top">
      a.<br>
      b.<br>
      c.<br>
      d.<br>
      e.<br>
      f.<br>
      g.<br>
      h.
    </td>
  </tr>
  <tr>
    <td colspan="3">
      Demikian Daftar Riwayat Hidup ini saya buat dengan sebenarnya untuk dapat digunakan sebaimana semestinya.
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
    </td>
    <td>
      <center>Jakarta, ............................ 2017<br/><br/>Yang membuat<br/><br/><br/><br/><br/>( {{$dataCaleg->nama}} )</center>
    </td>
  </tr>
</table>
</html>
