@extends('layout.template')
@section('title-page', 'Personal Campaign Management By Intervey Political Consultant')
@section('content')
<?php
foreach($databio as $dc){
	$nama = $dc->nama;
}
?>
@foreach($dataBg as $bg)
<header id="home" style="background-image:url({{ asset('assets/images/background/'.$bg->foto)}});background-size: cover">
		<div class="container main-cont">
			<div class="intro-text">		
				<div class="intro-heading txt-judul">
					<div class="text-left">
						<div class="title-1">{{ $bg->title }}</div>
						<div class="title-2"><?php echo $bg->subtitle ?></div>
					</div>
				</div>
				<div>
					<div class="text-left txt-desc">- {{ $bg->name_alias }}</div>
				</div>
				<div class="text-left" style="margin-top: 40px;">
					<button onclick="goDaftar()" class="page-scroll btn btn-trans">DAFTAR MENJADI RELAWAN</button>
				</div>
			</div>
		</div>
	</header>
@endforeach
	<section id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12" id="row-news">
					<div class="panel">
						<div class="panel-body">
							<div class="subtitle-head">About Us</div>
							<a href="#" class="row-toggle" onclick="changeRow()">
								<h3 class="title-content res-scr-1">Siapa {{ $nama }} ?</h3>
							</a>
							<p class="justify" id="news-first">
							@foreach($dataAuto as $get)
		      					@if($get->foto == "")
		      					<img src="{{ asset('asset/img/foto/nopic.png') }}" alt="" class="floated">
		      					@else
		      					<img src="{{ asset('assets/images/autobiografi').'/'.$get->foto }}" alt="" class="floated">
		      					@endif
		      					<?php
		      					function custom_echo($x, $length){
								  if(strlen($x)<=$length)
								  {
								    echo $x;
								  }
								  else
								  {
								    $y=substr($x,0,$length) . '...';
								    echo $y;
								  }
								}
								?><?php echo custom_echo($get->deskripsi, 720) ?>
		      					<p class="justify" id="news-show">
		      						<img src="{{ asset('assets/images/autobiografi').'/'.$get->foto }}" alt="" class="floated"><?php echo $get->deskripsi ?></p>
	      					@endforeach
	      				</p>
						<div class="pull-right">
							<a href="#" class="row-toggle">
								Baca selengkapnya <i class="fa fa-chevron-right"></i>
							</a>
						</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12" id="panel-first">
					<div class="panel">
						<div class="panel-body">
							<div class="subtitle-head text-center">Relawan</div>
							<p class="text-center" style="text-transform: uppercase; line-height: 1">Total tim relawan kami<br>hingga saat ini mencapai	 </p>
							<h2 class="text-center green res-scr-2" style="margin-top: -5px;"><b>1.300</b></h2>
							<p class="text-center" style="text-transform: uppercase; line-height: 1">relawan terdaftar</p>
							<hr>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-6 text-center" style="border-right: 1px solid #E5E5E5">
									<div class="res-scr-4 green">500</div>
									<p class="text-center upper">Perempuan</p>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 text-center">
									<div class="res-scr-4 green">500</div>
									<p class="text-center upper">Laki-Laki</p>
								</div>
							</div><br>
							<center>
								<button onclick="goDaftar()" class="btn btn-green btn-block">DAFTAR</button>
							</center>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel">
						<div class="panel-body">
							<div class="subtitle-head">Dukungan Anda</div>
							<div class="area-dukungan">
								@foreach($dataRight as $dt)
								<h3 class="title-content res-scr-1">{{ $dt->title }}</h3>	
								<p class="justify"><?php echo $dt->deskripsi ?></p>
								@endforeach
								<form id="form-dukungan" action="{{ asset('get_dukungan').'/'.$akses }}">
									<div class="row" style="margin-top: 6px;">
										<div class="col-md-12">
											<div class="form-group">
												<input type="text" name="nama" class="form-control form-khusus" placeholder="Masukkan Nama Anda" required="">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-8 col-sm-12 col-xs-12">
											<div class="form-group">
												<input type="text" name="telp" class="form-control form-khusus" placeholder="Masukkan Nomor Telpon anda" value="+62" required="">
											</div>
										</div>
										<div class="col-md-4 col-sm-12 col-xs-12">
											<button class="btn btn-green" type="submit" id="save-dukungan">SUBMIT</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="panel" id="panel-hide">
						<div class="panel-body">
							<div class="subtitle-head text-center">Relawan</div>
							<p class="text-center" style="text-transform: uppercase; line-height: 1">Total tim relawan kami<br>hingga saat ini mencapai	 </p>
							<h2 class="text-center green res-scr-2" style="margin-top: -5px;"><b>1.300</b></h2>
							<p class="text-center" style="text-transform: uppercase; line-height: 1">relawan terdaftar</p>
							<hr>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-6 text-center" style="border-right: 1px solid #E5E5E5">
									<div class="res-scr-4 green">500</div>
									<p class="text-center upper">Perempuan</p>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 text-center">
									<div class="res-scr-4 green">500</div>
									<p class="text-center upper">Laki-Laki</p>
								</div>
							</div><br>
							<center>
								<button onclick="goDaftar()" class="btn btn-green btn-block">DAFTAR</button>
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="berita" class="bg-light-gray">
		<div class="container">
			<div id="area-listing">
				<h2 class=" text-center res-scr-3">INFO TERBARU</h2><br>
				<div class="row">
				@foreach($dataNews as $data)
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel panel-default">
						<div class="panel-body box-menu">
							<div class="box-first">
								<div class="row">
									<div class="col-md-10 col-sm-10 col-xs-10">
										<?php
										$dc = date_create($data->create_date);
										$dc = date_format($dc, 'D, d M Y');
										echo $dc;
										?>
										<br>
										MY TEAM	
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2">
										<img src="{{ asset('asset/img/foto/nopic.png') }}" class="img-circle img-bordered" style="width: 30px">
									</div>
								</div>
							</div><hr>
							<div class="news-title">
								<a href="#" class="link-news" onclick="read_news({{ $data->id_berita }})">
									<h3 class="title-content res-scr-1"><?php echo $data->judul ?></h3>
								</a>
							</div>
							<div class="link-share">
								<a href="#" class="link-news" onclick="read_news({{ $data->id_berita }})">
									<i class="fa fa-arrow-right"></i> Read More
								</a>
								<div class="pull-right">
									<i class="fa fa-facebook"></i> 0&nbsp;&nbsp;
									<i class="fa fa-twitter"></i> 0&nbsp;&nbsp;
									<i class="fa fa-share-alt"></i> 0&nbsp;&nbsp;
								</div>
								
							</div>
							<a href="#" class="link-news" onclick="read_news({{ $data->id_berita }})">
								<img src="{{ asset('asset/img/foto/ipc.jpg') }}" alt="" width="100%">
							</a>
							<input type="hidden">
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
		<div id="area-news">
			<div class="row">
				<div class="col-md-8 col-sm-12 col-xs-12">
					<a href="#" type="button" class="link-news" onclick="goBack()">
						<i class="fa fa-chevron-left"></i> Kembali
					</a>
					<div class="subtitle-head" id="area-date"></div>
					<h3 class="title-content res-scr-1" id="area-title"></h3>
					<center>
						<img src="" id="area-image" class="img-responsive">
					</center>
					<p class="justify" id="area-content"></p>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12" style="margin-top: 83px;">
					<div class="row" id="area-data-other">
						
					</div>
				</div>
				<a href="#berita" class="trigger-click page-scroll"></a>
			</div>
		</div>
		</div>
	</section>

	<!-- Team Section -->
<section id="team" style="min-height: 300">
	<div class="container">
		<div id="area-souvenir-list">
			<h2 class=" text-center res-scr-3">SOUVENIR</h2><br>
			@foreach($datahadiah as $get)
			<?php
			$str = str_replace(" ", "-", $get->nama_barang);
			$str = str_replace("/", "_", $str);
			?>
			<div class="col">
				<div class="col-lg-3 col-md-4 col-xs-6 thumb">
					<div class="team-member">
						<img class="img-responsive img-box" src="{{ asset('asset/img/foto/ipc.jpg')}}" alt="" onclick="viewDetail()">
						<h4>{{ $get->nama_barang }}</h4>
					</div>
				</div>

			</div>
			@endforeach
		</div>
		<div id="area-souvenir-detail">
			
		</div>
	</div>
</section>

<section id="volunteer-registration" class="bg-light-gray">
	<div class="container" id="daftar">
		<div id="area-registrasi-volunteer">
			<form id="form-volunteer">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="col-md-12 well">
				<h1 class="res-scr-1" sty>Registrasi Relawan</h1><hr>
				<div class="row">
              <div class="col-md-2 col-sm-3 col-xs-12">
                <input type="file" name="foto" class="form-control input-sm drop">
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="form-group row">
                  <div class="col-md-4 col-sm-3">
                    <label>Nama Depan</label>
                  </div>
                  <div class="col-md-8">
                    <input type="text" name="nama_depan" class="form-control input-sm" placeholder="Nama Depan">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4 col-sm-3">
                    <label>Nama Tengah</label>
                  </div>
                  <div class="col-md-8">
                    <input type="text" name="nama_tengah" class="form-control input-sm" placeholder="Nama Tengah">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-4 col-sm-3">
                    <label>Nama Belakang</label>
                  </div>
                  <div class="col-md-8">
                    <input type="text" name="nama_belakang" class="form-control input-sm" placeholder="Nama Belakang">
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-5 col-xs-12">
                <div class="form-group row">
                    <div class="col-md-3 col-sm-3">
                      <label>Tanggal Lahir</label>
                    </div>
                    <div class="col-md-9">
                      <div class="row nopadding">
                        <div class="col-md-3">
                          <select name="tgl_lahir" class="form-control input-sm">
                            <option value="">Tgl</option>
                            @foreach($tanggal as $tgl)
                            <option value="{{ $tgl->tgl }}">{{ $tgl->tgl }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-md-5">
                          <select name="bln_lahir" id="bln_lahir" class="form-control input-sm">
                            <option value="">Bulan</option>
                            @foreach($bulan as $bln)
                            <option value="{{ $bln->id }}">{{ $bln->bulan }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-md-4">
                          <select name="thn_lahir" id="tahun_lahir" class="form-control input-sm">
                            <option value="">Tahun</option>
                            @foreach($tahun as $thn)
                            <option value="{{ $thn->tahun }}">{{ $thn->tahun }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="form-group row">
                  <div class="col-md-3">
                    <label>Tempat Lahir</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" class="form-control input-sm" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-3">
                    <label>Jenis Kelamin</label>
                  </div>
                  <div class="col-md-9">
                    <select name="jenkel" class="form-control input-sm">
                      <option value="">--Jenis Kelamin--</option>
                      <option value="Laki-Laki">Laki-Laki</option>
                      <option value="Perempuan">Perempuan</option>
                    </select>
                  </div>
                </div>
              </div>
              
            </div>
            <div class="row" style="margin-top: 10px;">
            	<div class="col-md-3 col-sm-6 col-xs-12">
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Provinsi</label>
	              </div>
	              <div class="col-md-9">
	                <select name="a_prov" id="a_prov" class="form-control input-sm">
	                  <option value="">--Pilih Provinsi--</option>
	                  @foreach($dataProvinsi as $data)
	                  <option value="{{ $data->geo_prov_id }}">{{ $data->geo_prov_nama }}</option>
	                  @endforeach
	                </select>
	              </div>
	            </div>
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Kabupaten</label>
	              </div>
	              <div class="col-md-9">
	                <select name="a_kota" id="a_kota" class="form-control input-sm">
	                  <option value="">--Pilih Kota / Kabupaten--</option>
	                </select>
	              </div>
	            </div>
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Kecamatan</label>
	              </div>
	              <div class="col-md-9">
	                <select name="a_kec" id="a_kec" class="form-control input-sm">
	                  <option value="">--Pilih Kecamatan--</option>
	                </select>
	              </div>
	            </div>
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Kelurahan</label>
	              </div>
	              <div class="col-md-9">
	                <select name="a_desa" id="a_desa" class="form-control input-sm">
	                  <option value="">--Pilih Kelurahan--</option>
	                </select>
	              </div>
	            </div>
	          </div>
	          <div class="col-md-3 col-sm-6 col-xs-12">
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>RT/RW</label>
	              </div>
	              <div class="col-md-4">
	                <input type="text" name="RT" class="form-control input-sm">
	              </div>
	              <div class="col-md-4">
	                <input type="text" name="RW" class="form-control input-sm">
	              </div>
	            </div>
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Alamat</label>
	              </div>
	              <div class="col-md-9">
	                <textarea name="alamat" class="form-control" style="height: 68px;"></textarea>
	              </div>
	            </div>
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Kodepos</label>
	              </div>
	              <div class="col-md-9">
	                <input type="text" class="form-control input-sm" name="kodepos">
	              </div>
	            </div>
	          </div>
	          <div class="col-md-3 col-sm-6 col-xs-12">
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Handphone</label>
	              </div>
	              <div class="col-md-9">
	                <input type="phone" name="handphone" class="form-control input-sm">
	              </div>
	            </div>
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Telepon</label>
	              </div>
	              <div class="col-md-9">
	                <input type="phone" name="telepon" class="form-control input-sm">
	              </div>
	            </div>
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Fax</label>
	              </div>
	              <div class="col-md-9">
	                <input type="phone" name="fax" class="form-control input-sm">
	              </div>
	            </div>
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Email</label>
	              </div>
	              <div class="col-md-9">
	                <input type="phone" name="email" class="form-control input-sm">
	              </div>
	            </div>
	          </div>
	          <div class="col-md-3 col-sm-6 col-xs-12">
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Facebook</label>
	              </div>
	              <div class="col-md-9">
	                <input type="text" name="facebook" class="form-control input-sm">
	              </div>
	            </div>
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Twitter</label>
	              </div>
	              <div class="col-md-9">
	                <input type="text" name="twitter" class="form-control input-sm">
	              </div>
	            </div>
	            <div class="form-group row">
	              <div class="col-md-3">
	                <label>Instagram</label>
	              </div>
	              <div class="col-md-9">
	                <input type="text" name="instagram" class="form-control input-sm">
	              </div>
	            </div>
	          </div>
	          <div class="row">
	          	<div class="col-md-12">
	          		<center>
	          			<div class="g-recaptcha" data-sitekey="6LejSS0UAAAAAGV2V9zDWXvQO1X6NjT-anSHpY6_"></div>
	          		</center>
	          	</div>
	          	<div class="col-md-12">
	          		<div class="row">
	          			<div class="col-md-12">
	          				<center>
	          					<button class="btn btn-green" type="submit" style="width: 250px;">DAFTAR</button>	
	          				</center>
	          			</div>
	          		</div>
	          	</div>
	          </div>
            </div>
			</div>
		</div>
		<div class="area-success">
			<h3 class="text-center">Terima kasih telah mendaftar</h3>
			<h5>Anda akan mendapat informasi lebih lanjut yang akan kami kirimkan melalui email atau nomor handphone yang telah anda daftarkan tadi</h5>
		</div>
		</form>
		</div>
		<div id="area-registrasi-success"></div>
	</div>
</section>

<footer id="foot" style="text-align: left">
	<div class="container">
		<div class="row">
		<div class="col-md-8 col-sm-6 col-xs-12">
			<h3 class="title-content res-scr-1 white" style="margin-top: 35px;">MENU</h3>
			<ul class="straight-menu text-left">
				<li class="hidden">
					<a href="#page-top"></a>
				</li>
				<li>
					<a class="page-scroll" href="#home" style="color: white;">BERANDA</a>
				</li>
				<li>
					<a class="page-scroll" href="#about" style="color: white;">TENTANG</a>
				</li>
				<li>
					<a class="page-scroll" href="#team" style="color: white;">MERCHANDISE</a>
				</li>
				<li>
					<a class="page-scroll" href="#berita" style="color: white;">INFO TERBARU</a>
				</li>
				<li>
					<a class="page-scroll" href="#contact" style="color: white;">KONTAK</a>
				</li>
				
			</ul>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<h2 style="color: white;" align="center">Kontak</h2>
				<p style="color: white" align="justify">Kenali lebih jauh <b> @foreach($databio as $dc){{ $dc->nama }}@endforeach </b>
					melalui akun sosial media resmi. dapatkan informasi tentang aktifitas terbaru dan update mengenai jadwal kampanye berikutnya.
				</p>
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-6" style="margin-top: 5px;">
						@foreach($databio as $dc)
						<center>
							<a href="https://twitter.com/{{ $dc->twitter }}" class="btn btn-default"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
						</center>
						@endforeach
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6" style="margin-top: 5px;">
						<center>
							<a href="https://instagram.com" class="btn btn-default"><i class="fa fa-instagram fa-fw"></i> <span class="network-name">Instagram</span></a>
						</center>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6" style="margin-top: 5px;">
						@foreach($databio as $dc)
						<center>
							<a href="https://facebook.com/{{ $dc->facebook }} " class="btn btn-default"><i class="fa fa-facebook"></i> <span class="network-name">Facebook</span></a>
						</center>
						@endforeach
					</div>
				</div>
		</div>
	</div><hr style="margin-top: 180px;">
	<div class="text-center white bottom">
		&copy; Intervey Political Consultant <?php echo date('Y') ?>
	</div>
	</div>
</footer>
<a href="#volunteer-registration" class="page-scroll regist"></a>
@endsection

@section('function')
<script>
	var nama_kota, nama_desa, desa_lat, desa_long;
	$(document).ready(function(){
		$("#news-show").hide();
		$("#panel-hide").hide();
		$("#area-news").hide();
		$("#volunteer-registration").hide();
		$(".area-success").hide();
	});
	$("#form-dukungan").on('submit', function(){
		var nama = $("[name='nama']").val();
		var telp = $("[name='telp']").val();
		/*alert(nama+" "+telp)*/
		$(".area-dukungan").empty();
		$(".area-dukungan").append('<center><i style="color: #383838; font-size: 32px; margin-top: 40px" class="fa fa-circle-o-notch fa-spin text-center"></i></center>');

		$.ajax({
			type : "GET",
			url  : "{{ asset('ajaxSaveDukungan').'/'.$akses }}",
			data : {
				'_token' : "{{ csrf_token() }}",
				'nama' : nama,
				'telp' : telp
			},
			success:function(resp){
				if(resp == "success"){
					$(".area-dukungan").html('<h1 class="text-center" style="margin-top: 80px;">Terima Kasih</h1><p class="justify">Terima kasih telah mendukung <b>{{ $nama }}</b>, Tim kami akan segera menghubungi anda dalam 3x24 Jam.</p>')
				}else{
					$(".area-dukungan").html('<h3 class="text-center" style="margin-top: 80px;">Maaf,</h3><p class="justify">Kami mendeteksi nomor telepon yang anda masukkan sudah mendukung <b>{{ $nama }}</b>, bila anda belum dihubungi oleh tim kami, mohon kesediaannya untuk menunggu.</p>')
				}
			}
		})
		return false;
	});
	function goDaftar(){
		window.location=("{{ asset('pendaftaran-relawan') }}");
	}
	$(".row-toggle").on('click', function(e){
		e.preventDefault();
		$("#row-news").removeClass('col-md-4')
		$("#row-news").addClass('col-md-8')
		$("#news-first").hide();
		$("#news-show").show();
		$("#panel-first").hide();
		$("#panel-hide").show().addClass('animated fadeIn');
	})
	$(".link-news, .linked").on('click', function(e){
		e.preventDefault();
	});
	function goDaftar(){
		$("#volunteer-registration").show();
		$(".regist").trigger('click');
	}
	function goBack(){
		$("#area-listing").removeClass('animated fadeOut').show();
		$("#area-date").text("");
		$("#area-title").text("");
		$("#area-content").html("");
		$("#area-image").attr('src', "");
		$("#area-news").hide();
		$(".trigger-click").trigger('click');
	}
	function read_news(key){
		$("#area-data-other").html("");
		$.ajax({
			type : "GET",
			url : "{{ asset('news/read/').'/' }}"+key,
			dataType : "json",
			success:function(resp){
				$("#area-listing")
					.addClass('animated fadeOut');
				setTimeout(function(){
					$("#area-listing")
						.hide()
				})
				$("#area-news")
					.show()
				$("#area-date").text(resp['date']);
				$("#area-title").text(resp['judul']);
				$("#area-content").html(resp['content']);
				$("#area-image").attr('src', "{{ asset('asset/img/foto/ipc.jpg') }}");
				$(".trigger-click").trigger('click');

				for (var a = 0; a < resp['data_other_count']; a++) {
					$("#area-data-other").append("<div class='col-md-12 col-sm-4'>"+
										"<a class='linked' onclick='read_news("+resp['data_other'][a]['id']+")'>"+
										"<img src='{{ asset('asset/img/foto/ipc.jpg') }}' class='img-responsive'>"+
										"</a>"+
										"<a class='linked' onclick='read_news("+resp['data_other'][a]['id']+")'>"+
										"<h4 class='text-center'>"+resp['data_other'][a]['judul']+"</h4>"+
										"</div></a>"+
										"<div class='pull-right'>"+
										"<a class='linked' onclick='read_news("+resp['data_other'][a]['id']+")'>"+
										"Baca Selengkapnya <i class='fa fa-chevron-right'></i>"+
										"</a>"+
										"</div>")
				}
			}
		});
	}

	$("#a_prov").on("change", function(){
		var i_prov = $("#a_prov").val();
		$.ajax({
			type : "GET",
			url  : "{{ asset('ajaxGetKabupaten') }}",
			data : "key="+i_prov,
			success:function(resp){
				$("#a_kota").html(resp);

				$("#a_kota").on("change", function(){
					var i_kota = $("#a_kota").val();
					getNamaKota(i_kota);
					$.ajax({
						type : "GET",
						url  : "{{ asset('ajaxKecamatan') }}",
						data : "key="+i_kota,
						success:function(resp){
							$("#a_kec").html(resp);

							$("#a_kec").on("change", function(){
								var i_kec = $("#a_kec").val();

								$.ajax({
									type : "GET",
									url  : "{{ asset('ajaxKelurahan') }}",
									data : "key="+i_kec,
									success:function(resp){
										$("#a_desa").html(resp);

										$("#a_desa").on('change', function(){
											var id_desa = $("#a_desa").val();
											getNamaDesa(id_desa);
										});
									}
								})
							});
						}
					})
				});
			}
		})
	});
	function getNamaKota(id){
		$.ajax({
			type : "GET",
			url : "{{ asset('ajaxGetNamaKota') }}",
			data : "key="+id,
			dataType : "json",
			success:function(resp){
				nama_kota = resp['nama_kota']
			}
		})
	}
	function getNamaDesa(id){
		$.ajax({
			type : "GET",
			url : "{{ asset('ajaxGetNamaDesa') }}",
			data : "key="+id,
			dataType : "json",
			success:function(resp){
				nama_desa = resp['nama_desa'];
				getLatLong(nama_kota, nama_desa);
			}
		})
	}
	function getLatLong(city, district){
		var geocoder =  new google.maps.Geocoder();
	    geocoder.geocode( { 'address': district+", "+city}, function(results, status) {
	          if (status == google.maps.GeocoderStatus.OK) {
	          	desa_lat = results[0].geometry.location.lat();
	          	desa_long = results[0].geometry.location.lng();
	          } else {
	            alert("Something got wrong " + status);
	          }
	        });
	}

	$("#form-volunteer").on("submit", function(){
		var nama = $("#nama").val();
		var no_hp = $("#no_hp").val();
		var email = $("#email").val();

		var alamat = $("#alamat").val();
		var prov = $("#a_prov").val();
		var kota = $("#a_kota").val();
		var kec = $("#a_kec").val();
		var kel = $("#a_desa").val();

		$.ajax({
			type : "GET",
			url  : "{{ asset('registrasi-relawan') }}",
			data : {
				'nama' : nama,
				'no_hp': no_hp,
				'email': email,
				'alamat':alamat,
				'prov':prov,
				'kota':kota,
				'kec':kec,
				'kel':kel,
				'lat':desa_lat,
				'long':desa_long
			},
			success:function(){

			}
		});
		return false;
	});
	$("#a_prov").on("change", function(){
  var i_prov = $("#a_prov").val();
  $.ajax({
   type : "GET",
   url  : "{{ asset('ajaxGetKabupaten') }}",
   data : "key="+i_prov,
   success:function(resp){
    $("#a_kota").html(resp);

    $("#a_kota").on("change", function(){
     var i_kota = $("#a_kota").val();
     /*getNamaKota(i_kota);*/
     $.ajax({
      type : "GET",
      url  : "{{ asset('ajaxKecamatan') }}",
      data : "key="+i_kota,
      success:function(resp){
       $("#a_kec").html(resp);

       $("#a_kec").on("change", function(){
        var i_kec = $("#a_kec").val();

        $.ajax({
         type : "GET",
         url  : "{{ asset('ajaxKelurahan') }}",
         data : "key="+i_kec,
         success:function(resp){
          $("#a_desa").html(resp);

          $("#a_desa").on('change', function(){
           var id_desa = $("#a_desa").val();
           /*getNamaDesa(id_desa);*/
         });
        }
      })
      });
     }
   })
   });
  }
})
});
$("#form-volunteer").on('submit', function(){
	var formData = new FormData($(this)[0]);
	  $.ajax({
	    type : "POST",
	    url : "{{ asset('admin/relawan/save-relawan/front') }}",
	    data : formData,
	    processData : false,
	    cache : false,
	    async : false,
	    contentType : false,
	    success:function(resp){
	      /*alert(resp);*/
	      $("#area-registrasi-volunteer").hide();
	      $(".area-success").show();
	    }
	  })
	return false;
});
</script>	
@endsection