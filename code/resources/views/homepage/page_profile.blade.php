@extends('layout.template')
@section('title-page', $nama." - PCM")
@section('content')
<div class="container" style="margin-top: 10px;">
	<a href="{{ asset('/') }}" class="btn btn-green">
		<i class="fa fa-chevron-left"></i> Kembali
	</a>
	<h2 style="text-transform: capitalize;">Siapa <?php echo $nama ?>?</h2>
	<hr>
	@foreach($dataProfile as $get)
	<div class="row" style="margin-top: 20px;">
		<div class="col-md-9 col-sm-12 col-xs-12" style="border-right: 1px solid #CDCDCD">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<center>
								<img src="{{ asset('assets/images/autobiografi').'/'.$get->foto }}" alt="" class="img-responsive" style="width: 250px;border: 1px solid #CDCDCD">
							</center>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-md-12">
							<p class="justify"><?php echo $get->autobiografi ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-12 col-xs-12">
			<h4>Berita Lainnya</h4>
			<hr>
			<div class="row">
				<?php
				$getNews = DB::table('tb_berita2')->get();
				foreach ($getNews as $n) {
					?>
				<div class="col-md-12">
					<img src="{{ asset($n->image_link) }}" class="img-responsive">
					<h4 class="read">{{ $n->judul }}</h4>
					<div class="pull-right clearfix">
						<a href="#">Baca selengkapnya <i class="fa fa-chevron-right"></i></a>
					</div>
					<hr>
				</div>
					<?php
				}
				?>
			</div>
		</div>
	</div>
	@endforeach
</div>
@endsection
@section('function')
@endsection