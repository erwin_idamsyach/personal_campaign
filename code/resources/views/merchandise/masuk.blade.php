  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

<?php 


  $cek = $cek = Session::get('username');

  $dataOleh = DB::table('tb_oleh')
              ->select('nama_oleh','id_oleh')
              ->where('akes', $cek)
              ->get();

?>


    <section class="content-header">
      <h1>
       &nbsp;
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Merchandise</li>
      <li class="active">Data Mercahdise Masuk</li>
      </ol> 
    </section>
    <section class="content">

      <div class="se-pre-con"></div>
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Data Mercahdise Masuk</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nama Barang<font color="red">(*)</font></label>
                      <select name="id_oleh" id="id_oleh" class="form-control">
                          <option value="">--- Barang ---</option>
                            @foreach($dataOleh as $tmp)
                              <option value="{{ $tmp->id_oleh }}">{{ $tmp->nama_oleh }}</option>
                            @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tambah Stok <font color="red">(*)</font></label>
                      <input type="number" class="form-control" name="stok" id="stok" placeholder="Stok" required="">
                    </div>
                  </div>
                  
              </div>
              
            </div>
          </div>  
        </div>
        <div class="box-footer clearfix" align="right">
          <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
          <button type="submit" class="btn btn-success" onclick="tambah_stok()" >Simpan</button>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
 @include('include.footer')
 