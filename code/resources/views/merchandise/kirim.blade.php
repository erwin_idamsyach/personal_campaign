  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

<?php 


  $cek = $cek = Session::get('username');

  $dataOleh = DB::table('tb_oleh')
              ->where('akes', $cek)
              ->get();

  $dataRel = DB::table('m_relawan')
              ->where('akses', $cek)
              ->where('diterima', 'YA')
              ->get(); 

  $dataZero = DB::table('tb_oleh')
              ->where('stok', 's')
              ->where('akes', $cek)
              ->get();


    foreach ($dataZero as $kadal ) {

?>


    <section class="content-header">
      <h1>
        Merchandise
        <small>Merchandise Kirim</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Merchandise</li>
      <li class="active">Data Mercahdise Kirim</li>
      </ol> 
    </section>
    <section class="content">

      <div class="se-pre-con"></div>
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Data Mercahdise Kirim</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">

              
                <div class="row" id="tag">
                  <div class="col-md-6">

                    <div class="form-group">

                      <label>Nama Barang<font color="red">(*)</font></label>
                      <select name="id_oleh" id="id_oleh" class="form-control">
                          <option value="">--- Barang ---</option>
                            @foreach($dataOleh as $tmp)
                              
                              <option value="{{ $tmp->id_oleh }}">{{ $tmp->nama_oleh }}</option>

                            @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Jumlah Kirim <font color="red">(*)</font></label>
                      <input type="number" class="form-control" name="kirim" id="kirim" placeholder="Jumlah" required="">

                        
                      <input class="form-control" type="hidden" value="{{ $kadal->id_oleh }}" type="text" id="lol" name="lol"></input>
                    </div>
                  </div>
                  
              </div>

              <div class="row">
                <div class="col-md-6">
                  
                  <div class="form-group">
                      <label>Relawan Penerima<font color="red">(*)</font></label>
                      <select class="form-control" name="id_relawan" id="id_relawan">
                        <option value="">---select---</option>
                        @foreach($dataRel as $asd)
                              <option value="{{ $asd->id_relawan }}">{{ $asd->nama }}</option>
                        @endforeach
                      </select>       
                    </div>

                </div>
                  
                </div>

              <div class="row">
              <div class="col-md-12">
                <div class="row">
                  
                  
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Provinsi <font color="red">(*)</font></label>
                      <select class="form-control" name="provinsi" id="provinsi" onchange="getKota2()" required="">
                        <option value="">---select---</option>
                        <?php
                          $u = DB::table('m_geo_prov_kpu')->orderBy('geo_prov_nama', 'asc')->get();
                          foreach ($u as $y) {
                        ?>
                        <option value="<?php echo $y->geo_prov_id ?>"><?php echo $y->geo_prov_nama ?></option>
                        <?php
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Kota <font color="red">(*)</font></label>
                      <select class="form-control" name="kota" id="kota" onchange="getCamat2()" required="">
                        <option value="">---select---</option>
                      </select>       
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Kecamatan <font color="red">(*)</font></label>
                      <select class="form-control" name="kecamatan" id="kecamatan">
                        <option value="">---select---</option>
                      </select>         
                    </div>
                  </div>        
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Lokasi <font color="red">(*)</font></label>
                      <input type="text" class="form-control" name="lokasi" id="lokasi" placeholder="lokasi" required="">
                    </div>
                  </div>  
                                    
                </div>

                
                <div class="row">
        <div class="box-footer clearfix" align="right">
          <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
          <button type="submit" class="btn btn-success" onclick="kirim_sovenir()" >Kirim</button>
        </div>
        </div>
              
            </div>
          </div>  
        </div>
        

      </div>
    </section>
    <?php
  }
    ?>
    <!-- /.content -->


  </div>
 @include('include.footer')
 