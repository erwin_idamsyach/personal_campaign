<?php
	$users = DB::table('detil_mercendise')
				->join('tb_oleh','detil_mercendise.id_oleh', '=' ,'tb_oleh.id_oleh')
				->join('m_relawan','detil_mercendise.id_relawan', '=', 'm_relawan.id_relawan')
				->join('m_geo_prov','detil_mercendise.provinsiId', '=', 'm_geo_prov.geo_prov_id')
				->join('m_geo_kab','detil_mercendise.kabupatenId', '=', 'm_geo_kab.geo_kab_id')
				->where('detil_mercendise.akses', Session::get('username'))
				->where('detil_mercendise.id_de', $id_de)
				->get();
	foreach ($users as $k) {

?>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<!-- text -->
			<div class="col-md-12">
				<div class="col-md-2">
					<label>Nama_barang</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->nama_oleh ?></label>
				</div>			
				<div class="col-md-2">
					<label>Jumlah Kirim</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->jml_kirim ?></label>
				</div>			
				<div class="col-md-2">
					<label>Nama Relawan</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->nama ?></label>
				</div>
				<div class="col-md-2">
					<label>Tgl Kirim</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->tgl_trans ?></label>
				</div>
				<div class="col-md-2">
					<label>Lokasi Kirim</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->lokasi.",".$k->geo_kab_nama.",".$k->geo_prov_nama ?></label>
				</div>

														
			</div>
			<!-- gambar -->
			<div class="col-md-4">
				<div class="col-md-12">
		          <img src="<?php echo $k->image_link; ?>" style="height: 70%; width: 70%;">
		        </div>	
			</div>
			<?php } ?>
		</div>		
	</div>
</div>