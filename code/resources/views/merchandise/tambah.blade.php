  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <h1>
        Merchandise
        <small>Tambah Data Merchandise</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Merchandise</li>
      <li class="active">Tambah Data Mercahdise</li>
      </ol> 
    </section>
    <section class="content">

      <div class="se-pre-con"></div>
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Tambah Data Mercandise</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nama <font color="red">(*)</font></label>
                      <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Stok <font color="red">(*)</font></label>
                      <input type="number" class="form-control" name="stok" id="stok" placeholder="Stok" required="">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Deskripsi <font color="red">(*)</font></label>
                      <textarea class="form-control" name="description" id="description" placeholder="Deskripsi" required=""></textarea>
                    </div>
                  </div>                  
                </div>
              </div>
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Foto <font color="red">(*)</font></label>
                      <form action="{{ asset('admin/upload_image_oleh') }}" id="frmuploadImg" method="post" target="iframeUploadImg" enctype="multipart/form-data">
                        <input type="file" name="image" id="image" class="form-control drop" data-height="200" onchange="submitImage()">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                      </form>
                      <input class="hidden" type="text" name="gambarout" id="gambarout">
                      <iframe class="hidden" name="iframeUploadImg" id="iframeUploadImg"></iframe>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-12" id="sizepic"></div>
                  </div>                  
                </div>
              </div>


            </div>
          </div>  
        </div>
        <div class="box-footer clearfix" align="right">
          <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
          <button type="submit" class="btn btn-success" onclick="tambah_oleh()" >Simpan</button>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
 @include('include.footer')
 