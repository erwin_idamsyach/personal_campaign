  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Mercandise</li>
      <li class="active"><a href="{{ asset('admin/merchadise') }}">Logistik Mercandise</a></li>
      </ol>     
    </section>

  
    <!-- Main content -->
    <section class="content">

    <?php

      $cek = Session::get('username');
      $caleg = Session::get('idcaleg');
    
      $users = DB::table('detil_mercendise_copy')
                    ->join('tb_oleh', 'tb_oleh.id_oleh', '=', 'detil_mercendise_copy.id_oleh')
                    ->select('tb_oleh.*', 'detil_mercendise_copy.*')
                    ->get();


    ?>
      <div class="se-pre-con"></div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              
              <i class="fa fa-calendar"></i>
              <h3 class="box-title">Daftar Mercandise</h3>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-3 col-sm-4 col-xs-6">
                    <div class="row" style="margin-bottom: 10px;">
                      <div class="input-group date">
                        
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center" style="width: 5px;">No</th>
                  <th class="text-center" style="width: 100px;">nama barang</th>
                  <th class="text-center" style="width: 10px;">Jumlah terima</th>
                  <th class="text-center" style="width: 50px;">tgl</th>
                  <td class="text-center" style="width: 10px">Aksi</td>
                </tr>
                </thead>
                <tbody>
                <?php
                  if(count($users) == 0){
                ?>    
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <?php
                  }else{
                    $start = 0;
                    foreach ($users as $row) {
                    $start++;
                ?>
                <tr>
                  <td class="numeric text-center"><?php echo $start ?></td>
                  <td><?php echo $row->nama_oleh ?></td>
                  <td><?php echo $row->jml_terima ?></td>
                  <td><?php echo $row->tgl_trans; ?></td>
                  <td align="right">          
                    <button class="btn btn-info pull-left" onclick="view_detil1('<?php echo $row->id_de_trim ?>')"><i class="fa fa-eye" data-toggle="tooltip" data-placement="bottom" data-original-title="Detail"></i></button>
                  </td>
                </tr>
                <?php
                    }
                  }                            
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  @include('include.footer')