  @include('include.static-top')
  @include('include.menu')
  <?php
  $asd = DB::table('tb_oleh')
      ->where('id_oleh', $id_oleh)
      ->get();
  foreach ($asd as $row) {                   

  }
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mercendise
        <small>Edit Data Mercendise</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Mercandise</li>
      <li class="active">Edit Data Mercandise</li>
      </ol> 
    </section>
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Form Edit Mercandise</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
            <!-- text -->
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $row->nama_oleh ?>" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                      <label>Stok</label>
                      <input type="number" class="form-control" name="stok" id="stok" placeholder="Stok" value="<?php echo $row->stok ?>" required="">
                    </div>
                  </div>                 
                             
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Keterangan </label>
                      <textarea class="form-control" name="description" id="description" placeholder="Deskripsi" required=""><?php echo $row->keterangan ?></textarea>
                    </div>
                  </div>                   
                </div>
              </div>
              <!-- gambar -->
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Foto</label>
                      <form action="{{ asset('admin/upload_image_oleh') }}" id="frmuploadImg" method="post" target="iframeUploadImg" enctype="multipart/form-data">
                      <input type="file" name="image" id="image" class="form-control drop" data-height="200" onchange="submitImage()" data-default-file="{{ $row->image_link }}">
                      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                      </form>
                      <input class="hidden" type="text" name="gambarout" id="gambarout">
                      <iframe class="hidden" name="iframeUploadImg" id="iframeUploadImg"></iframe>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-12" id="sizepic"></div>
                  </div>
                </div>
              </div>
             
            </div>
          </div>
        </div>
          <div class="box-footer clearfix" align="right">
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="submit" class="btn btn-success" onclick="edit_oleh('<?php echo $row->id_oleh ?>')">Edit</button>
          </div>
        </div>
        
        
      </div>
    </section>
    <!-- /.content -->
  </div>
 @include('include.footer')
 
