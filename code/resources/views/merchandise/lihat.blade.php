<?php
	$tabel = DB::table('tb_oleh')->where('id_oleh', $id_oleh)->get();
	foreach ($tabel as $k) {

?>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<!-- text -->
			<div class="col-md-8">
				<div class="col-md-2">
					<label>Nama</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->nama_oleh ?></label>
				</div>			
				<div class="col-md-2">
					<label>Stok</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->stok ?></label>
				</div>	
				<div class="col-md-2">
					<label>Deskripsi</label>
				</div>
				<div class="col-md-10">
					<label>: <?php echo $k->keterangan ?></label>
				</div>										
			</div>
			<!-- gambar -->
			<div class="col-md-4">
				<div class="col-md-12">
		          <img src="<?php echo $k->image_link; ?>" style="height: 70%; width: 70%;">
		        </div>	
			</div>
			<?php } ?>
		</div>		
	</div>
</div>