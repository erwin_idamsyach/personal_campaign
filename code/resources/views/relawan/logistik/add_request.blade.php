  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb" style="position: relative; right: 20%; margin-top: -36px;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Request Logistik</li>
        <li class="active"><a href="#">Add Request</a></li>
      </ol>     
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px;">
      <a href="{{ asset('relawan/request-logistik') }}" style="color: #3A3A3A; margin-bottom: 10px;">
        <i class="fa fa-arrow-left"></i> Kembali
      </a>&nbsp;&nbsp;
      <button type="button" class="btn btn-green btn-sm" onclick="saveReq()">SIMPAN</button>
    </div>
    <section class="content">
      <div class="box box-primary">
        <div class="box-header">
          <div class="pull-left">
            <h4 class="title">Request Logistik</h4>
          </div>
          <div class="pull-right">
            <button class="btn btn-green btn-xs" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <form id="form-req">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row">
            <div class="col-md-4">
              <div class="row">
                <div class="col-md-12">
                  <b class="ro-bold px-12">Request Detail</b>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3">Deskripsi</label>
                <div class="col-md-9">
                  <textarea name="desc" class="form-control"></textarea>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-12">
                  <b class="ro-bold px-12">Permintaan Logistik</b>
                </div>
              </div>
              <div id="area-clone">
                <div class="form-group row" id="clone-el">
                  <div class="col-md-5">
                    <select name="logistik[]" id="id_logistik" class="form-control input-sm">
                      <option value="">--Pilih--</option>
                      @foreach($dataLogistik as $data)
                      <option value="{{ $data->id }}">{{ $data->nama_barang }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-5">
                    <input type="number" name="stok_req[]" class="form-control input-sm" placeholder="stok diminta">
                  </div>
                  <div class="area-close"></div>
                  <div class="col-md-2" id="rem">
                    <button class="btn btn-green btn-xs" type="button" onclick="appendClone()">
                      <i class="fa fa-plus"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
    </section>
  <!-- /.content -->
</div>
@include('include.footer')
<script type="text/javascript">
var no = 1;
function appendClone(){
  no = parseInt(no);
  no = no+1;
  $("#area-clone").append('<div class="form-group row" id="clone-'+no+'">'+
                  '<div class="col-md-5">'+
                    '<select name="logistik[]" id="id_logistik" class="form-control input-sm">'+
                      '<option value="">--Pilih--</option>'+
                      '@foreach($dataLogistik as $data)'+
                      '<option value="{{ $data->id }}">{{ $data->nama_barang }}</option>'+
                      '@endforeach'+
                    '</select>'+
                  '</div>'+
                  '<div class="col-md-5">'+
                    '<input type="number" name="stok_req[]" class="form-control input-sm" placeholder="stok diminta">'+
                  '</div>'+
                  '<div class="area-close"></div>'+
                  '<div class="col-md-2" id="rem">'+
                    '<button class="btn btn-maroon btn-xs" type="button" onclick="removeAppend('+no+')">'+
                      '<i class="fa fa-times"></i>'+
                    '</button>'+
                  '</div>'+
                '</div>')
}
function removeAppend(key){
  $("#clone-"+key).remove();
}
function saveReq(){
  $("#form-req").trigger('submit')
}
$("#form-req").on('submit', function(){
  var formData = new FormData($(this)[0]);

  $.ajax({
    type : "POST",
    url : "{{ asset('relawan/request-logistik/act_save') }}",
    data : formData,
    async : false,
    success:function(resp){
      /*window.location=("{{ asset('relawan/request-logistik/') }}");*/
    },
    cache: false,
    contentType: false,
    processData: false
  })
  return false;
});
</script>