  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb" style="position: relative; right: 52%; margin-top: -36px;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Logistik</li>
        <li class="active"><a href="#">Stok</a></li>
      </ol>     
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px;">
      <a href="{{asset('relawan/request-logistik/add')}}" class="btn btn-green btn-sm">TAMBAH</a>
    </div>
    <section class="content">
      <div class="box box-primary">
        <div class="box-header">
          <div class="pull-left">
            <h4 class="title">Request List</h4>
          </div>
          <div class="pull-right">
            <button class="btn btn-green btn-xs" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">Request From</th>
                <th class="text-center">Stok Requested</th>
                <th class="text-center">Request Date</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($dataJoined as $data)
              <tr>
                <td class="text-center">{{ $data->id }}</td>
                <td>{{ $data->nama }}</td>
                <td>{{ $data->stok_requested }}</td>
                <td>{{ $data->create_date }}</td>
                <td class="text-right">
                  <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" title="Detail"><i class="fa fa-eye"></i></button>
                  <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-edit"></i></button>
                  <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" title="Approve"><i class="fa fa-check"></i></button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
  <!-- /.content -->
</div>
@include('include.footer')
<script type="text/javascript">
</script>