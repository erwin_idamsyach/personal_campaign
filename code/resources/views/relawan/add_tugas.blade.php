  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb" style="position: relative; right: 34%; margin-top: -36px;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Relawan</li>
        <li class="active"><a href="{{ asset('admin/relawan') }}">Add Penugasan</a></li>
      </ol>     
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px">
      <button class="btn btn-green">SIMPAN</button>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header">
                <div class="pull-left">
                  <h4 class="title">Add Penugasan</h4>
                </div>
                <div class="pull-right">
                  <button class="btn btn-green btn-xs" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group row">
                      <label class="col-md-3">Nama Relawan</label>
                      <div class="col-md-9">
                        <input type="text" name="nama_relawan" class="form-control input-sm" disabled="disabled">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-3">No. Handphone</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control input-sm" disabled="">
                      </div>
                    </div>
                    </div> -->
                    <div class="form-group row">
                      <div class="col-md-3">
                        <label>Provinsi</label>
                      </div>
                      <div class="col-md-9">
                        <select name="a_prov" id="a_prov" class="form-control input-sm">
                          <option value="">--Pilih Provinsi--</option>
                          @foreach($dataProvinsi as $data)
                          <option value="{{ $data->geo_prov_id }}">{{ $data->geo_prov_nama }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-3">
                        <label>Kabupaten</label>
                      </div>
                      <div class="col-md-9">
                        <select name="a_kota" id="a_kota" class="form-control input-sm">
                          <option value="">--Pilih Kota / Kabupaten--</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-3">
                        <label>Kecamatan</label>
                      </div>
                      <div class="col-md-9">
                        <select name="a_kec" id="a_kec" class="form-control input-sm">
                          <option value="">--Pilih Kecamatan--</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-3">
                        <label>Kelurahan</label>
                      </div>
                      <div class="col-md-9">
                        <select name="a_desa" id="a_desa" class="form-control input-sm">
                          <option value="">--Pilih Kelurahan--</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>  
    </section>
  <!-- /.content -->
</div>
@include('include.footer')
<!-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEVojWiqvNgSpxAtagsYYIvu1JSm00lA0&callback=initMap"></script>
<script type="text/javascript">
function initMap() {
    var uluru = {lat: -7.050144, lng: 113.305491};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 9,
      center : uluru,
      draggable : true
    });
    google.maps.event.addListener(map, 'click', function(event) {
       var marker = new google.maps.Marker({
          position: {lat : event.}, 
          map: map
      });
    });
} -->
<script>
  $("#a_prov").on("change", function(){
  var i_prov = $("#a_prov").val();
  $.ajax({
   type : "GET",
   url  : "{{ asset('ajaxGetKabupaten') }}",
   data : "key="+i_prov,
   success:function(resp){
    $("#a_kota").html(resp);

    $("#a_kota").on("change", function(){
     var i_kota = $("#a_kota").val();
     /*getNamaKota(i_kota);*/
     $.ajax({
      type : "GET",
      url  : "{{ asset('ajaxKecamatan') }}",
      data : "key="+i_kota,
      success:function(resp){
       $("#a_kec").html(resp);

       $("#a_kec").on("change", function(){
        var i_kec = $("#a_kec").val();

        $.ajax({
         type : "GET",
         url  : "{{ asset('ajaxKelurahan') }}",
         data : "key="+i_kec,
         success:function(resp){
          $("#a_desa").html(resp);

          $("#a_desa").on('change', function(){
           var id_desa = $("#a_desa").val();
           /*getNamaDesa(id_desa);*/
         });
        }
      })
      });
     }
   })
   });
  }
})
});
</script>