  @include('include.static-top')
  @include('include.menu')
  <?php
  $asd = DB::table('m_relawan')
      ->where('id_relawan',$id_oleh)
      ->get();
  foreach ($asd as $row) {                   

  }
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Relawan
        <small>Edit Data Relawan Mercendise</small>
      </h1>
      <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Relawan</li>
      <li class="active">Edit Data Relawan</li>
      </ol> 
    </section>
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Form Edit relawan</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
            <!-- text -->
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $row->nama ?>" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>No. Hp</label>
                      <input type="number" class="form-control" name="no_hp" id="no_hp" placeholder="Stok" value="<?php echo $row->no_hp ?>" required="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Agama</label>
                        <select class="form-control" name="agama" id="agama">
                          @if($row->agama == "")
                            <option value="">--- Pilih Agama ---</option>
                            @foreach($dataAgama as $tmp)
                              <option value="{{ $tmp->agama_id }}"> {{ ucwords($tmp->agama_label) }} </option>
                            @endforeach
                          @else
                            @foreach($dataAgamaNow as $tmp)
                              <option value="{{ $tmp->agama_id }}"> {{ ucwords($tmp->agama_label) }} </option>
                            @endforeach
                            @foreach($dataAgamaNotNow as $tmp)
                              <option value="{{ $tmp->agama_id }}"> {{ ucwords($tmp->agama_label) }} </option>
                            @endforeach
                          @endif
                          
                        </select>                      
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>alamat</label>
                      <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?php echo $row->alamat ?>" required="">
                    </div>
                  </div>                  
                </div>    
                </div>
              </div>
            </div>
          </div>
        </div>
          <div class="box-footer clearfix" align="right">
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="submit" class="btn btn-success" onclick="edit_relawan('<?php echo $row->id_relawan ?>')">Edit</button>
          </div>
        </div>
        
        
      </div>
    </section>
    <!-- /.content -->
  </div>
 @include('include.footer')
 
