  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Relawan
        <small>Kinerja Relawan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Relawan</li>
        <li class="active">Data Kinerja Relawan</li>
      </ol> 
    </section>
    <section class="content">

      <div class="se-pre-con"></div>
      <div class="box box-info">
        <div class="box-header">
          <i class="fa fa-calendar"></i>
          <h3 class="box-title">Data Kinerja Relawan</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label>Agenda<font color="red">(*)</font></label>
                      <input type="text" class="form-control" name="agenda" id="agenda" placeholder="Agenda" required="">
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="form-group">
                     <label>Relawan Penggerak<font color="red">(*)</font></label>
                     <input type="text" class="form-control" name="nama" id="nama" placeholder="Relawan Penggerak" required="">
                     <div style="margin-top: -34px; margin-left: 395px; ">
                      <button type="submit" class="btn btn-success" onclick="cek()">CARI</button>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Tujuan<font color="red">(*)</font></label>
                    <textarea class="form-control" name="tujuan" id="tujuan" placeholder="tujuan" required=""></textarea>
                  </div>
                </div> 
                <div class="col-md-4">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Foto <font color="red">(*)</font> </label>
                        <form action="{{ asset('admin/upload_image_m') }}" id="frmuploadImg" method="post" target="iframeUploadImg" enctype="multipart/form-data">
                          <input type="file" name="image" id="image" class="form-control drop" data-height="200" >
                          <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
                        </form>
                        <input class="hidden" type="text" name="gambarout" id="gambarout">
                        <iframe class="hidden" name="iframeUploadImg" id="iframeUploadImg" style="height: 1500px;"></iframe>
                        <input type="hidden" name="id_relawan" id="id_relawan">
                      </div>
                    </div>  
                    <div class="form-group">
                      <div class="col-md-12" id="sizepic"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>  
        </div>
        <div class="box-footer clearfix" align="rigt" style="margin-left: 5px;">
          <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
          <button type="submit" class="btn btn-success" onclick="tambah_kin()" >Simpan</button>
        </div>
      </div>
    </section>
    <div class="modal fade" id="modal-volunteer">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" data-dimiss="modal">&times;</button>
            <h4 class="modal-title">Ambil Data Relawan</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="form-group">
                  <label>Cari Relawan</label>
                  <input type="text" name="param" class="form-control" onchange="cariRelawan()">
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-2 text-right"><button class="btn btn-green" style="margin-top: 25px; 
              margin-right: 10px;">CARI</button></div>
            </div>
            <hr>
            <table class="table table-bordered table-striped table-hover">
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Nama</th>
                <th class="text-right">#</th>
              </tr>
              <?php $a = 1; ?>
              @foreach($dataRelawan as $data)
              <tr>
                <td class="text-center">{{ $a++ }}</td>
                <td>{{ $data->nama }}</td>
                <td class="pull-right">
                  <button class="btn btn-green" onclick="pilihRelawan({{ $data->id_relawan }}, '{{ $data->nama }}')">PILIH</button>
                </td>
              </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  @include('include.footer')
  <script type="text/javascript">
  function cek(){
    $("#modal-volunteer").modal('show')
  }
  function pilihRelawan(id, nama){
    $("#nama").val(nama)
    $("#id_relawan").val(id)
    $("#modal-volunteer").modal('hide');
  }
  function tambah_kin(id){    
    var agenda      = $('#agenda').val();
    var nama        = $('#nama').val();
    var tujuan      = tinyMCE.get('tujuan').getContent();
    var image       = $('#image').val();

    if(agenda==""){
      alert("nama Barang Harus Di Pilih");
    }else if (nama==""){
      alert("Relawan Harus dipilih");
    }else{
      $.ajax({
        type : "GET",
        url  : "./tambah_kin",
        data : {
          'agenda'      : agenda,
          'nama'        : nama,
          'tujuan'      : tujuan,
          'image'       : image
        },
        success:function(html){
          window.location.href = "./kinerja_data";
        }
      });
    }
  }
</script>