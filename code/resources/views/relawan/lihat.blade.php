<?php
	$tabel = DB::table('m_relawan')->where('id_relawan', $id_relawan)->get();
	foreach ($tabel as $k) {

?>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<!-- text -->
			<div class="col-md-12">
				<div class="col-md-6">
					<label>Nama</label>
				</div>
				<div class="col-md-6">
					<label>: <?php echo $k->nama ?></label>
				</div>			
				<div class="col-md-6">
					<label>Alamat</label>
				</div>
				<div class="col-md-6">
					<label>: <?php echo $k->alamat ?></label>
				</div>
				
				<div class="col-md-6">
					<label>No. HP</label>
				</div>
				<div class="col-md-6">
					<label>: <?php echo $k->no_hp ?></label>
				</div>
				<div class="col-md-6">
					<label>Agama</label>
				</div>
				<div class="col-md-6">
					<label>: 
					@foreach($dataAgamaNow as $tmp)
                        {{ ucwords($tmp->agama_label) }}
                    @endforeach
                    </label>
				</div>
				<div class="col-md-6">
					<label>Diterima</label>
				</div>
				<div class="col-md-6">
					<label>: <?php

					$ini = $k->diterima;

					if ($ini==null || $ini==""){
						echo "Belum Diterima";
					}
					else{
						echo $k->diterima;
					}

					  ?></label>
				</div>
				

			</div>
			
			<?php } ?>
		</div>		
	</div>
</div>