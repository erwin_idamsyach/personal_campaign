  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb" style="position: relative; right: 37%; margin-top: -36px;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Relawan</li>
        <li class="active"><a href="{{ asset('admin/relawan') }}">Data Relawan</a></li>
      </ol>     
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px">
      <a href="{{ asset('admin/inputrel') }}" class="btn btn-green">TAMBAH</a>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="pull-left">
                <h4 class="title">Data Relawan</h4>
              </div>
              <div class="pull-right">
                <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                  <i class="fa fa-minus"></i> 
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-3 col-sm-4 col-xs-6">
                    <div class="row" style="margin-bottom: 10px;">
                      <div class="input-group date">

                      </div> 
                    </div>
                  </div>
                </div>
              </div>
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 5px;">ID</th>
                    <th class="text-center" style="width: 120px;">First Name</th>
                    <th class="text-center" style="width: 120px;">Last Name</th>
                    <th class="text-center" style="width: 240px;">Address</th>
                    <th class="text-center" style="width: 120px;">Mobile</th>
                    <th class="text-center" style="width: 60px;">Photo</th>
                    <th class="text-center">Reg. Date</th>
                    <td class="text-center" style="width: 220px">Action</td>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(count($users) == 0){
                    ?>    
                    <tr>
                      <td class="text-center"></td>
                      <td class="text-center"></td>
                      <td class="text-center"></td>
                      <td class="text-center"></td>
                      <td class="text-center"></td>
                    </tr>
                    <?php
                  }else{
                    $start = 0;
                    foreach ($users as $row) {
                      $start++;
                      ?>
                      <tr>
                        <td class="numeric text-center"><?php echo $row->id_relawan ?></td>
                        <td class="text-left"><?php echo $row->first_name ?></td>
                        <td class="text-left"><?php echo $row->last_name ?></td>
                        <td class="text-left"><?php echo $row->alamat ?></td>
                        <td class="text-left"><?php echo $row->no_hp; ?></td>
                        <td class="text-center">
                          @if($row->photo == "")
                          <img src="{{ asset('asset/img/foto/nopic.png') }}" style="width: 40px; height: 50px;">
                          @else
                          <img src="{{ asset('assets/images/relawan').'/'.$row->photo }}" class="img-responsive">
                          @endif
                        </td>
                        <td class="text-center">
                          <?php
                          $date = date_create($row->create_date);
                          $date = date_format($date, "d M Y");
                          echo $date;
                          ?>
                        </td>
                        <td align="right">

                          <?php
                          if($row->diterima == "YA"){
                            ?>

                          </a>
                          <?php
                        }else{
                          ?>
                          <a href="./relawan_ref/<?php echo $row->id_relawan ?>">
                            <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" data-original-title="Terima ?"><i class="fa fa-check"></i></button>
                          </a>
                          <?php
                        }
                        ?>  

                        <button class="btn btn-green" onclick="view_relawan('<?php echo $row->id_relawan ?>')"><i class="fa fa-eye" data-toggle="tooltip" data-placement="bottom" data-original-title="Detail"></i></button>
                        <a href="./relawan_edit/<?php echo $row->id_relawan ?>">
                          <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit"><i class="fa fa-edit"></i></button> 
                        </a>
                        <button class="btn btn-green" onclick="delete_relawan('<?php echo $row->id_relawan ?>')" data-toggle="tooltip" data-placement="bottom" data-original-title="Hapus"><i class="fa fa-trash"></i></button>
                        <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" title="Print ID"><i class="fa fa-print"></i></button>
                      </td>
                    </tr>
                    <?php
                  }
                }                            
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
@include('include.footer')
<script type="text/javascript">
function view_relawan(id_relawan){
  $.ajax({
    type : "get",
    url  : "./view_relawan",
    data : "id_relawan="+id_relawan,
    success:function(html){
      $('#myModal').modal('show');
      $('.viewitem').html(html);
    }
  });
}
function delete_relawan(id_relawan){
  if (confirm("Apakah anda yakin ingin menhapus data ini?") == true) {
    $.ajax({
      type : "GET",
      url  : "./delete_relawan",
      data : "id_relawan="+id_relawan,
      success:function(html){
        window.location.reload();
      }
    });        
  } else {

  } 
}
</script>