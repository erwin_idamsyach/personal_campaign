<?php

	$cek = Session::get('username');

	$tabel = DB::table('tb_kinerja_rel')
            	 ->join('m_relawan', 'tb_kinerja_rel.id_relawan', '=', 'm_relawan.id_relawan')
            	 ->select('tb_kinerja_rel.*', 'm_relawan.nama','m_relawan.id_relawan')
            	 ->where('m_relawan.akses', $cek)
            	 ->where('tb_kinerja_rel.akses', $cek)
            	 ->get();


	foreach ($tabel as $k) {

?>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<!-- text -->
			<div class="col-md-12">
				<div class="col-md-6">
					<label>Agenda</label>
				</div>
				<div class="col-md-6">
					<label>: <?php echo $k->judul ?></label>
				</div>			
				<div class="col-md-6">
					<label>Ketua Panitia</label>
				</div>
				<div class="col-md-6">
					<label>: <?php echo $k->nama ?></label>
				</div>
				<div class="col-md-6">
					<label>Alamat</label>
				</div>
				<div class="col-md-6">
					<label>: <?php echo $k->lokasi ?></label>
					</div>
				<div class="col-md-6">
					<label>TGL Mulal</label>
				</div>
				<div class="col-md-6">
					<label>: <?php echo $k->date_start ?></label>
				</div>
				<div class="col-md-6">
					<label>Selesai</label>
				</div>
				<div class="col-md-6">
					<label>: <?php

					$ini = $k->status;

					if ($ini==null || $ini==""){
						echo "Belum Selesai";
					}
					else{
						echo $k->status;
					}

					  ?></label>
				</div>
				

			</div>
			
			<?php } ?>
		</div>		
	</div>
</div>