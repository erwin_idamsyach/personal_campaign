  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb" style="position: relative; right: 20%; margin-top: -36px;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Relawan</li>
        <li class="active"><a href="#">Penugasan Relawan</a></li>
      </ol>     
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="se-pre-con"></div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <div class="pull-left">
                <h4 class="title">Kinerja Relawan</h4>
              </div>
              <div class="pull-right">
                <button class="btn btn-green btn-box-head btn-xs">
                  <i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-3 col-sm-4 col-xs-6">
                    <div class="row" style="margin-bottom: 10px;">
                      <div class="input-group date">

                      </div> 
                    </div>
                  </div>
                </div>
              </div>
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 5px;">ID</th>
                    <th class="text-center" style="width: 60px;">Photo</th>
                    <th class="text-center" style="width: 120px;">First Name</th>
                    <th class="text-center" style="width: 120px;">Last Name</th>
                    <th class="text-center" style="width: 120px;">Mobile</th>
                    <th class="text-center">Area Penugasan</th>
                    <td class="text-center" style="width: 220px">Action</td>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(count($users) == 0){
                    ?>    
                    <tr>
                      <td class="text-center"></td>
                      <td class="text-center"></td>
                      <td class="text-center"></td>
                      <td class
                      ="text-center"></td>
                      <td class="text-center"></td>
                    </tr>
                    <?php
                  }else{
                    $start = 0;
                    foreach ($dataRelawan as $row) {
                      $start++;
                      ?>
                      <tr>
                        <td class="numeric text-center"><?php echo $row->id_relawan ?></td>
                        <td class="text-center">
                          @if($row->photo == "")
                          <img src="{{ asset('asset/img/foto/nopic.png') }}" style="width: 40px; height: 50px;">
                          @else
                          <img src="{{ asset('assets/images/relawan').'/'.$row->photo }}" class="img-responsive">
                          @endif
                        </td>
                        <td class="text-left"><?php echo $row->first_name ?></td>
                        <td class="text-left"><?php echo $row->last_name ?></td>
                        <td class="text-left"><?php echo $row->no_hp; ?></td>
                        <td class="text-left">{{ $row->geo_prov_nama.", ".$row->geo_kab_nama.", ".$row->geo_kec_nama.", ".$row->geo_deskel_nama }}</td>
                        <td align="right">
                        <button class="btn btn-green" onclick="view_relawan('<?php echo $row->id_relawan ?>')"><i class="fa fa-eye" data-toggle="tooltip" data-placement="bottom" data-original-title="Detail"></i></button>
                        <button class="btn btn-green" data-toggle="tooltip" data-placement="bottom" data-original-title="Add Penugasan" onclick="addTugas({{$row->id_relawan}})"><i class="fa fa-map-marker"></i></button> 
                      </td>
                    </tr>
                    <?php
                  }
                }                            
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="modal fade" id="modal-add-tugas">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="height: 46px; background-color: #870b04; color: #fff">
          <h4 class="title" style="margin-top: -5px; font-size: 14px;">Set Penugasan</h4>
          <div class="pull-right" style="margin-top: -30px">
            <button class="btn btn-green btn-sm" data-dismiss="modal">CANCEL</button>
            <button class="btn btn-green btn-sm" onclick="savePenugasan()">SAVE</button>
          </div>
        </div>
        <form id="form-penugasan">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <b class="ro-bold px-12">Detail Relawan</b>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3">Nama Relawan</label>
            <div class="col-md-9">
              <input type="text" id="nama_relawan" name="nama_relawan" class="form-control input-sm" disabled="disabled">
              <input type="hidden" id="id_relawan" name="id_relawan" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3">No. Handphone</label>
            <div class="col-md-9">
              <input type="text" id="no_hp" class="form-control input-sm" disabled="">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <hr>
            </div>
          </div>
          <!-- <div class="form-group row">
            <label class="col-md-12">Area Penugasan</label>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div id="map" style="width: 100%; height: 350px;"></div>
            </div>
          </div> -->
          <div class="row">
            <div class="col-md-12">
              <b class="ro-bold px-12">Detail Tugas</b>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3">
              <label>Role</label>
            </div>
            <div class="col-md-9">
              <label class="radio-inline">
                <input type="radio" name="role" value="KOORDINATOR" onchange="checkval()"> Koordinator
              </label>
              <label class="radio-inline">
                <input type="radio" name="role" value="ANGGOTA" onchange="checkval()"> Anggota
              </label>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3">
              <label>Koordinator</label>
            </div>
            <div class="col-md-9">
              <select name="koor_leader" id="koor-space" class="form-control input-sm" disabled="">
                <option value="">--Pilih--</option>
                @foreach($dataKoord as $koor)
                <option value="{{ $koor->id_relawan }}">{{ $koor->nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <hr>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <b class="ro-bold px-12">Area Penugasan</b>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3">
              <label>Provinsi</label>
            </div>
            <div class="col-md-9">
              <select name="a_prov" id="a_prov" class="form-control input-sm">
                <option value="">--Pilih Provinsi--</option>
                @foreach($dataProvinsi as $data)
                <option value="{{ $data->geo_prov_id }}">{{ $data->geo_prov_nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3">
              <label>Kabupaten</label>
            </div>
            <div class="col-md-9">
              <select name="a_kota" id="a_kota" class="form-control input-sm">
                <option value="">--Pilih Kota / Kabupaten--</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3">
              <label>Kecamatan</label>
            </div>
            <div class="col-md-9">
              <select name="a_kec" id="a_kec" class="form-control input-sm">
                <option value="">--Pilih Kecamatan--</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3">
              <label>Kelurahan</label>
            </div>
            <div class="col-md-9">
              <select name="a_desa" id="a_desa" class="form-control input-sm">
                <option value="">--Pilih Kelurahan--</option>
              </select>
            </div>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>

  <!-- /.content -->
</div>
@include('include.footer')
<script type="text/javascript">
function delete_kinerja(id_kinerja){
  if (confirm("Apakah anda yakin ingin menghapus data ini?") == true) {
    $.ajax({
      type : "GET",
      url  : "./delete_kinerja",
      data : "id_kinerja"+id_kinerja,
      success:function(html){
        window.location.reload();
      }
    });        
  } else {
  } 
}
function addTugas(key){
  $.ajax({
    type : "GET",
    url : "{{ asset('admin/relawan/add_target/') }}"+"/"+key,
    dataType : "json",
    success:function(resp){
      $("#modal-add-tugas").modal('show');
      $("#nama_relawan").val(resp['nama']);
      $("#no_hp").val(resp['no_hp']);
      $("#id_relawan").val(resp['id_relawan']);
    }
  });
}
$("#a_prov").on("change", function(){
  var i_prov = $("#a_prov").val();
  $.ajax({
   type : "GET",
   url  : "{{ asset('ajaxGetKabupaten') }}",
   data : "key="+i_prov,
   success:function(resp){
    $("#a_kota").html(resp);

    $("#a_kota").on("change", function(){
     var i_kota = $("#a_kota").val();
     /*getNamaKota(i_kota);*/
     $.ajax({
      type : "GET",
      url  : "{{ asset('ajaxKecamatan') }}",
      data : "key="+i_kota,
      success:function(resp){
       $("#a_kec").html(resp);

       $("#a_kec").on("change", function(){
        var i_kec = $("#a_kec").val();

        $.ajax({
         type : "GET",
         url  : "{{ asset('ajaxKelurahan') }}",
         data : "key="+i_kec,
         success:function(resp){
          $("#a_desa").html(resp);

          $("#a_desa").on('change', function(){
           var id_desa = $("#a_desa").val();
           /*getNamaDesa(id_desa);*/
         });
        }
      })
      });
     }
   })
   });
  }
})
});
$("#form-penugasan").on('submit', function(){
  var formData = new FormData($(this)[0]);

  $.ajax({
    type : "POST",
    url  : "{{ asset('admin/relawan/penugasan/add') }}",
    data : formData,
    processData : false,
    cache : false,
    async : false,
    contentType : false,
    success:function(){
      location.reload();
    }
  });
  return false;
});
function savePenugasan(){
  $("#form-penugasan").trigger('submit');
}
function checkval(){
  var cek = $("[name='role']:checked").val();
  /*alert(cek)*/
  if(cek == "KOORDINATOR"){
    $("#koor-space").attr('disabled', true);
  }else{
    $("#koor-space").removeAttr('disabled');
  }
}
</script>