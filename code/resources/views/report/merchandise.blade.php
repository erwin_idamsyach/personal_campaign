@include('include.static-top')
@include('include.menu')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      &nbsp;
    </h1>
    <ol class="breadcrumb hidden-sm hidden-xs" style="position: relative; right: 20%; margin-top: -36px;">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#"><i class="fa fa-book"></i> Report</a></li>
    <li><a href="#"><i></i> Merchandise</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="box box-default">
          <div class="box-header">
            <div class="pull-left">
              <h4 class="title">Statistik Pengeluaran</h4>
            </div>
            <div class="pull-right">
              <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <canvas id="chart-pengeluaran"></canvas>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="box box-default">
          <div class="box-header">
            <div class="pull-left">
              <h4 class="title">Statistik Pemasukan</h4>
            </div>
            <div class="pull-right">
              <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <canvas id="chart-pemasukan"></canvas>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box box-default">
          <div class="box-header">
            <div class="pull-left">
              <h4 class="title">Stok Pakaian</h4>
            </div>
            <div class="pull-right">
              <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <h1><b><center>{{isset($stock[1])?$stock[1]:"0"}}</center></b></h1>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box box-default">
          <div class="box-header">
            <div class="pull-left">
              <h4 class="title">Stok Makanan</h4>
            </div>
            <div class="pull-right">
              <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <h1><b><center>{{isset($stock[2])?$stock[2]:"0"}}</center></b></h1>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box box-default">
          <div class="box-header">
            <div class="pull-left">
              <h4 class="title">Stok Aksesoris</h4>
            </div>
            <div class="pull-right">
              <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <h1><b><center>{{isset($stock[3])?$stock[3]:"0"}}</center></b></h1>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@include('include.footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js"></script>
<script>
        var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var config = {
            type: 'line',
            data: {
                labels: MONTHS,
                datasets: [{
                    label: "Pengeluaran",
                    backgroundColor: "#00FF00",
                    borderColor: "#00FF00",
                    data: [
                        {{isset($chart1["01"])?$chart1["01"]:0}},
                        {{isset($chart1["02"])?$chart1["02"]:0}},
                        {{isset($chart1["03"])?$chart1["03"]:0}},
                        {{isset($chart1["04"])?$chart1["04"]:0}},
                        {{isset($chart1["05"])?$chart1["05"]:0}},
                        {{isset($chart1["06"])?$chart1["06"]:0}},
                        {{isset($chart1["07"])?$chart1["07"]:0}},
                        {{isset($chart1["08"])?$chart1["08"]:0}},
                        {{isset($chart1["09"])?$chart1["09"]:0}},
                        {{isset($chart1["10"])?$chart1["10"]:0}},
                        {{isset($chart1["11"])?$chart1["11"]:0}},
                        {{isset($chart1["12"])?$chart1["12"]:0}}
                    ],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:false,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Bulan'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Jumlah Relawan'
                        }
                    }]
                }
            }
        };
        var config2 = {
            type: 'line',
            data: {
                labels: MONTHS,
                datasets: [{
                    label: "Pemasukan",
                    backgroundColor: "#00FF00",
                    borderColor: "#00FF00",
                    data: [
                        {{isset($chart2["01"])?$chart2["01"]:0}},
                        {{isset($chart2["02"])?$chart2["02"]:0}},
                        {{isset($chart2["03"])?$chart2["03"]:0}},
                        {{isset($chart2["04"])?$chart2["04"]:0}},
                        {{isset($chart2["05"])?$chart2["05"]:0}},
                        {{isset($chart2["06"])?$chart2["06"]:0}},
                        {{isset($chart2["07"])?$chart2["07"]:0}},
                        {{isset($chart2["08"])?$chart2["08"]:0}},
                        {{isset($chart2["09"])?$chart2["09"]:0}},
                        {{isset($chart2["10"])?$chart2["10"]:0}},
                        {{isset($chart2["11"])?$chart2["11"]:0}},
                        {{isset($chart2["12"])?$chart2["12"]:0}}
                    ],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:false,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Bulan'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Jumlah Relawan'
                        }
                    }]
                }
            }
        };
        window.onload = function() {
            var ctx = document.getElementById("chart-pengeluaran").getContext("2d");
            window.myLine = new Chart(ctx, config);
            var ctx = document.getElementById("chart-pemasukan").getContext("2d");
            window.myLine = new Chart(ctx, config2);
        };
</script>
