@include('include.static-top')
@include('include.menu')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      &nbsp;
    </h1>
    <ol class="breadcrumb hidden-sm hidden-xs" style="position: relative; right: 20%; margin-top: -36px;">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#"><i class="fa fa-book"></i> Report</a></li>
    <li><a href="#"><i></i> Relawan</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-9">
        <div class="box box-default">
          <div class="box-header">
            <div class="pull-left">
              <h4 class="title">Statistik Pendaftaran Relawan</h4>
            </div>
            <div class="pull-right">
              <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <canvas id="chart-relawan"></canvas>
          </div>
        </div>
      </div
    </div>
    <div class="col-md-3">
      <div class="box box-default">
        <div class="box-header">
          <div class="pull-left">
            <h4 class="title">Total Relawan</h4>
          </div>
          <div class="pull-right">
            <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <h1><b><center>{{$totalRelawan}}</center></b></h1>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="box box-default">
        <div class="box-header">
          <div class="pull-left">
            <h4 class="title">Relawan Aktif</h4>
          </div>
          <div class="pull-right">
            <button class="btn btn-green btn-box-head btn-xs" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <h1><b><center>{{$totalRelawanDiterima}}</center></b></h1>
        </div>
      </div>
    </div>
  </section>
</div>
@include('include.footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js"></script>
<script>
        var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var config = {
            type: 'line',
            data: {
                labels: MONTHS,
                datasets: [{
                    label: "Relawan",
                    backgroundColor: "#00FF00",
                    borderColor: "#00FF00",
                    data: [
                        {{isset($chart["01"])?$chart["01"]:0}},
                        {{isset($chart["02"])?$chart["02"]:0}},
                        {{isset($chart["03"])?$chart["03"]:0}},
                        {{isset($chart["04"])?$chart["04"]:0}},
                        {{isset($chart["05"])?$chart["05"]:0}},
                        {{isset($chart["06"])?$chart["06"]:0}},
                        {{isset($chart["07"])?$chart["07"]:0}},
                        {{isset($chart["08"])?$chart["08"]:0}},
                        {{isset($chart["09"])?$chart["09"]:0}},
                        {{isset($chart["10"])?$chart["10"]:0}},
                        {{isset($chart["11"])?$chart["11"]:0}},
                        {{isset($chart["12"])?$chart["12"]:0}}
                    ],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:false,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Bulan'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Jumlah Relawan'
                        }
                    }]
                }
            }
        };
        window.onload = function() {
            var ctx = document.getElementById("chart-relawan").getContext("2d");
            window.myLine = new Chart(ctx, config);
        };
</script>
