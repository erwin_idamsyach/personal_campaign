<div class="container" id="daftar">
			<h1 class="well res-scr-2">Registrasi Relawan</h1>
			<div class="col-lg-12 well">
				<div class="row">
					<form>
						<div class="col-sm-12">
							<div class="form-group">
								<label>Nama</label>
								<input type="text" placeholder="Masukkan nama" class="form-control" name="nama" id="nama">
							</div>
							<div class="row">
								<div class="col-sm-6 form-group">
									<label>No. HandPhone</label>
									<input type="number" placeholder="Masukkan No. HP" class="form-control" name="no_hp" id="no_hp">
								</div>
								<div class="col-sm-6 form-group">
									<label>Agama</label>
										<select name="agama" id="agama" class="form-control">
				                        	<option value="">--- Agama ---</option>
				                            @foreach($dagama as $yui)
				                              <option value="{{ $yui->agama_id }}">{{ $yui->agama_label }}</option>
				                            @endforeach
				                      	</select>									
								</div>
							</div>					
							<div class="form-group">
								<label>Alamat</label>
								<textarea placeholder="Masukkan Alamat" rows="3" class="form-control" name="alamat" id="alamat"></textarea>
							</div>	
							<!-- <div class="form-group">
								<label class="control-label col-sm-3">Jenis Kelamin</label>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-4">
											<label class="radio-inline">
												<input type="radio" value="Female" name="jk" id="jk">Perempuan
											</label>
										</div>
										<div class="col-sm-4">
											<label class="radio-inline">
												<input type="radio"  value="Male" name="jk" id="jk">Laki-laki
											</label>
										</div>
									</div>
								</div>
							</div> -->
								<input name="_token" type="hidden" value="{!! csrf_token() !!}" />
								<input type="hidden" name="akses" id="akses" value="{{ $akses }}">
								<button type="button" class="btn btn-lg btn-info" onclick="tambah_relawan()">Submit</button>
			</div>
					</form> 
		</div>
			</div>
		</div>