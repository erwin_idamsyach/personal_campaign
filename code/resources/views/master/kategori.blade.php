  @include('include.static-top')
  @include('include.menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;
      </h1>
      <ol class="breadcrumb" style="position: relative; right: 27%; margin-top: -36px;">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Setting</li>
        <li class="active"><a href="#">Master Kategori</a></li>
      </ol>     
    </section>
    <div class="pull-right" style="margin-top: -25px; margin-right: 15px;">
      <a href="#" data-toggle="modal" data-target="#modalAdd" class="btn btn-green btn-sm">TAMBAH</a>
    </div>
    <section class="content">
      <div class="box box-primary">
        <div class="box-header">
          <div class="pull-left">
            <h4 class="title">Master Kategori</h4>
          </div>
          <div class="pull-right">
            <button class="btn btn-green btn-xs" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <div id="area-error"></div>
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th class="text-center" width="50px">ID</th>
                <th class="text-center">Kategori</th>
                <th class="text-center" width="200px">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $a = 1; ?>
              @foreach($dataKategori as $data)
              <tr>
                <td class="text-center">{{ $a++ }}</td>
                <td>{{ $data->kategori_name }}</td>
                <td class="text-right">
                  <button class="btn btn-green btn-sm" data-toggle="tooltip" data-placement="bottom" title="Edit" onclick="editKate({{ $data->id }}, '{{ $data->kategori_name }}')">
                    <i class="fa fa-edit"></i>
                  </button>
                  <button class="btn btn-maroon btn-sm" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="deleteMaster({{ $data->id }})">
                    <i class="fa fa-times"></i>
                  </button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <div class="modal fade" id="modalAdd">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="height: 46px; background-color: #870b04; color: #fff">
            <h4 class="title" style="margin-top: -5px; font-size: 14px;">Add Master Kategori</h4>
            <div class="pull-right" style="margin-top: -30px">
              <button class="btn btn-green btn-sm" data-dismiss="modal">CANCEL</button>
              <button class="btn btn-green btn-sm" onclick="saveMaster()">SAVE</button>
            </div>
          </div>
          <div class="modal-body">
            <form id="form-kategori">
              <div class="form-group row">
                <label class="col-md-3">Nama Kategori</label>
                <div class="col-md-9">
                  <input type="text" name="kategori" class="form-control input-sm" required="">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modal-edit">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="height: 46px; background-color: #870b04; color: #fff">
            <h4 class="title" style="margin-top: -5px; font-size: 14px;">Edit Master Kategori</h4>
            <div class="pull-right" style="margin-top: -30px">
              <button class="btn btn-green btn-sm" data-dismiss="modal">CANCEL</button>
              <button class="btn btn-green btn-sm" onclick="editMaster()">EDIT</button>
            </div>
          </div>
          <div class="modal-body">
            <form id="form-kategori">
              <div class="form-group row">
                <label class="col-md-3">Nama Kategori</label>
                <div class="col-md-9">
                  <input type="hidden" name="id">
                  <input type="text" name="kategori_e" class="form-control input-sm" required="">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  <!-- /.content -->
</div>
@include('include.footer')
<script type="text/javascript">
$("#modalAdd").on("show.bs.modal", function(){
  $("#area-error").html('');
});
function saveMaster(){
  $("#form-kategori").trigger('submit')
}
$("#form-kategori").on('submit', function(){
  var kategori = $("input[name='kategori']").val();
  if(kategori != ""){
    $.ajax({
      type : "GET",
      url : "{{ asset('admin/master/kategori/add') }}",
      data : "kategori="+kategori,
      success:function(resp){
        $("input[name='kategori']").val("");
        if(resp == "success"){
          $("#modalAdd").modal('hide');
          location.reload();
        }else{
          $("#modalAdd").modal('hide');
          $("#area-error").html('<div class="alert alert-danger"><i class="fa fa-times"></i> Data yang diinputkan sudah ada</div>')
        }
      }
    })
  }

  return false;
});
function editKate(id, content){
  $("#modal-edit").modal('show');
  $("input[name='kategori_e']").val(content)
  $("input[name='id']").val(id)
}
function editMaster(){
  var id = $("input[name='id']").val();
  var kategori = $("input[name='kategori_e']").val();
  if(kategori != ""){
    $.ajax({
      type : "GET",
      url : "{{ asset('admin/master/kategori/edit') }}",
      data : {
        'id' : id, 'kategori' : kategori
      },
      success:function(resp){
        location.reload();
      }
    })
  }
}
function deleteMaster(id){
  if(confirm('Apakah anda yakin akan menghapus data ini?')){
    $.ajax({
      type : "GET",
      url : "{{ asset('admin/master/kategori/delete') }}",
      data : "id="+id,
      success:function(resp){
        location.reload();
      }
    })
  }else{

  }
}
</script>