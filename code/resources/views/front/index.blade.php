@extends('layout.layout')
@section('title-page', 'Personal Campaign Management')
@section('content')
<?php
foreach($databio as $dc){
	$nama = $dc->nama;
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-sm-12 col-xs-12">
			<div class="panel">
				<div class="panel-body">
					<h3 class="title-content">Siapa <cite class="green">{{ $nama }}</cite> ?</h3>
					<p class="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.</p>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			<div class="panel">
				<div class="panel-body">
					<h3 class="text-center green title-content">Total Relawan Terdaftar</h3>
					<h2 class="text-center green"><b>1.300</b> Orang</h2>
					<center>
						<button class="btn bg-green">DAFTAR MENJADI RELAWAN</button>
					</center>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6">
			
		</div>
	</div>
</div>
@endsection