<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Personal | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets/asset/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/asset/font-awesome-4.7.0/css/font-awesome.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/asset/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{ asset('asset/css/css.css') }}">
  <link rel="shortcut icon" href="{{ asset('asset/img/foto/ipc-new.jpeg') }}">
  
  <style>
  .required{
    border: 1px solid red;
  }
  label{
    font-weight: normal;
  }
  .border-radius {
    border-radius: 3px !important;
  }
  .se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 99;
    background: url('{{asset('assets/images/privilege/spin.gif')}}')  center no-repeat #fff;
  }
  </style>  
</head>
<body class="hold-transition login-page" style="background-color: #870b04">
<div class="se-pre-con"></div>
<div class="login-box">
  <div class="login-logo">
    <!-- <a href="../../index2.html"><b>Admin</b>LTE</a> -->
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" style="box-shadow: 3px 3px 10px 2px grey; padding: 0">
    <div class="head-login">
      <div class="row">
        <div class="col-md-2">
          <div class="vert-mid">
            <center>
              <img src="{{ asset('asset/img/foto/ipc-new.jpeg') }}" width="40px" height="40px;">
            </center>
          </div>
        </div>
        <div class="col-md-10">
          <div class="vert-mid">
            Personal Campaign Management
          </div>
        </div>
      </div>
    </div>
    <!-- <p class="login-box-msg">Masukan Username & Password</p> -->
    <div class="container-fluid">
    <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
      <div class="form-group has-feedback">
        <label>Username</label>
        <input type="text" class="form-control" id="username" name="username" placeholder="Username" >
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label>Password</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-md-12" id="pesan"></div>
      </div>
      <div class="row">
        <div class="col-md-12" style="padding-bottom: 10px;">          
          <button class="btn btn-block btn-flat btn-auth border-radius btn-maroon" onclick="cekLogin()"><i class="fa fa-sign-in"></i> Masuk</button>
        </div>
        <!-- /.col -->
      </div>
    </div>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset('assets/asset/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/asset/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('assets/asset/plugins/iCheck/icheck.min.js') }}"></script>
<script>
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
  });  

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });

  $(document).keypress(function(e) {
    if(e.which == 13) {
        cekLogin();
    }
  });  

  function cekForm(field,response){
    $(field).removeClass('required');
    $(response).html('');
  }  

  function cekLogin(){
    var username = $('#username').val();
    var password = $('#password').val();
    if(username == ""){
      $('#pesan').html('<div class="alert alert-danger"><i class="fa fa-warning"></i> USERNAME KOSONG</div>');
    }else if(password == ""){
      $('#pesan').html('<div class="alert alert-danger"><i class="fa fa-warning"></i> PASSWORD KOSONG</div>');
    }else{
      $.ajax({
      type : "GET",
      url  : "{{ asset('login') }}",
      data : {
        'username'  : username,
        'password'  : password
      },
        success:function(response){
          if(response == "sukses") {
            window.location=("{{ asset('admin/dashboard') }}")
          }else if(response == ""){
            $('#pesan').html('<div class="alert alert-danger"><i class="fa fa-warning"></i> SALAH USERNAME & PASSWORD</div>');
          }
        }
      });
   }
  }

  function ea(){
    alert('gagal');
  }
</script>
<style>
.alert {
    padding: 5px;
}
</style>
</body>
</html>
