<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],
	'google' => [
	    'client_id' => '347660411334-70gl2js0oras6sur8632n3f8g25rsfo7.apps.googleusercontent.com',
	    'client_secret' => 'CCHLsnxzWuaDpVmAiAJ0qZMB-',
	    'redirect' => 'http://localhost/personal_campaign/u/google/callback',
	 ],
	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],

];
