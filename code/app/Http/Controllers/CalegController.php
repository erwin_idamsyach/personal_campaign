<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use DB;
use Input;
use Validator;
use Redirect;

class CalegController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	/*public function __construct()
	{
		$this->middleware('guest');
	}*/

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cek = Session::get('username');
		if($cek != ""){
			Session::forget('menu');
	      	Session::set('menu', 'caleg');

			$users = DB::table('caleg_drh')
				->orderBy('id', 'asc')
	      		->get();

	      	foreach ($users as $key){}

	      	$provinsi = DB::table('m_geo_prov_kpu')
	      		->where('geo_prov_id', $key->tingkat_provinsi )
	      		->get();

	      	foreach ($provinsi as $tmp ) {
	      		$prov = $tmp->geo_prov_nama;
	      	}

	      	$kabupaten = DB::table('m_geo_kab_kpu')
	      		->where('geo_kab_id', $key->tingkat_kabupaten)
	      		->get();
	      	foreach ($kabupaten as $row) {
	      		$kab = $row->geo_kab_nama;
	      	}
	      		
			$responseView = 'caleg/caleg';
			$responseData = array(
				'users' => $users,
				'provinsi' => $prov,
				'kabupaten' => $kab
			);


			return view($responseView,$responseData);
		}else{
			return redirect('/');	
		}
	}

	public function frm_tambah()
	{
		$cek = Session::get('username');
		if($cek != ""){
			$identitas = DB::table('ref_identitas')
	      		->orderBy('identitas', 'asc')
	      		->get();

			$provinsi = DB::table('m_geo_prov_kpu')
	      		->orderBy('geo_prov_nama', 'asc')
	      		->get();

	      	$agama = DB::table('ref_agama')
	      		->orderBy('agama_label', 'asc')
	      		->get();
/*
	      	$provinsi = DB::table('m_geo_kab_kpu')
	      		->orderBy('geo_prov_nama', 'asc')
	      		->get();

	      	$provinsi = DB::table('m_geo_kec_kpu')
	      		->orderBy('geo_kec_nama', 'asc')
	      		->get();

	      	$provinsi = DB::table('m_geo_deskel_kpu')
	      		->orderBy('geo_prov_nama', 'asc')
	      		->get();*/

			$responseView = 'caleg/tambah_caleg';
			$responseData = array(
				'provinsi' => $provinsi,
				'identitas' => $identitas,
				'agama' => $agama
			);


			return view($responseView,$responseData);
		}else{
			return redirect('/');	
		}
	}	

	public function frm_edit()
	{
		$cek = Session::get('username');
		if($cek != ""){
			$id = $request->input('id');
			return view('agenda/edit_agenda', array('id'=>$id));
		}else{
			return redirect('/');	
		}
	}		

	public function update_data(Request $request, $key)
	{
        return view('agenda/edit_agenda', array('id'=>$key));
	}

	public function tambah_data(Request $request)
	{
		$judul = $request->input('judul');
		$date_start = $request->input('date_start');
		$description = $request->input('description');
		$provinsi = $request->input('provinsi');
		$kota = $request->input('kota');
		$kecamatan = $request->input('kecamatan');
		$lokasi = $request->input('lokasi');
		$image_link = $request->input('image_link');
		$lol = url().'/assets/images/agenda/'.$image_link;
		$creat = Session::get('username');

		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$date1 = date('Y-m-d');

		DB::table('tb_agenda')->insert(
            [
                'judul'   			=> $judul,
                'date_start'       	=> $date_start,
                'date_end' 		   	=> $date1,
                'description'  		=> $description,
                'status'	 		=> 'Terjadwal',
                'provinsi'			=> $provinsi,
                'kabupaten'			=> $kota,
                'kecamatan'			=> $kecamatan,
                'lokasi'			=> $lokasi,
                'image_link'		=> $lol,
                'create_by' 		=> $creat,
                'create_date' 		=> $date1,
                'akses'				=> $creat

            ] 
        );
        return view('agenda/tambah_agenda');
					
	}

	public function updates(Request $request)
	{
		$judul = $request->input('judul');
		$date_start = $request->input('date_start');
		$description = $request->input('description');
		$status = $request->input('status');
		$provinsi = $request->input('provinsi');
		$kota = $request->input('kota');
		$kecamatan = $request->input('kecamatan');
		$lokasi = $request->input('lokasi');
		$image_link = $request->input('image_link');
		$lol = url().'/assets/images/agenda/'.$image_link;
		$id = $request->input('id');
		$creat = Session::get('username');

		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$date1 = date('Y-m-d');

		if($image_link==""){
			DB::table('tb_agenda')->where('id', $id)->update(
	                [
		                'judul' 				=> $judul,
		                'date_start' 			=> $date_start,
		                'date_end'  			=> $date1,
		                'description'       	=> $description, 
		                'status'	 			=> 'Terjadwal',
		                'provinsi'				=> $provinsi,
			            'kabupaten'				=> $kota,
			            'kecamatan'				=> $kecamatan,
			            'lokasi'				=> $lokasi,
			            'akses'					=> $creat
	                ] 
	            );
		}else{
			DB::table('tb_agenda')->where('id', $id)->update(
	                [
		                'judul' 				=> $judul,
		                'date_start' 			=> $date_start,
		                'date_end'  			=> $date1,
		                'description'       	=> $description, 
		                'status'	 			=> 'Terjadwal',
		                'provinsi'				=> $provinsi,
			            'kabupaten'				=> $kota,
			            'kecamatan'				=> $kecamatan,
			            'lokasi'				=> $lokasi,
			            'image_link'			=> $lol,
			            'akses'					=> $creat
	                ] 
	            );
		}

	}

	public function ref(Request $request, $key)
	{
		$id = $request->input('id');

		DB::table('tb_agenda')->where('id', $key)->update(
                [
	                'status' 				=> 'Tidakterjadwal'
                ] 
            );
		return redirect()->action('AgendaController@index');
	}	

	public function refw(Request $request, $key)
	{

		DB::table('tb_agenda')->where('id', $key)->update(
                [
	                'status' 				=> 'Terjadwal'
                ] 
            );
		return redirect()->action('AgendaController@index');
	}		

    public function view_item(Request $request)
    {
        $id = Input::get('id');
        return view('agenda/lihat_agenda', array('id'=>$id));
    }	

    public function delete_item(Request $request)
    {
        $id = $request->input('id');
        DB::table('tb_agenda')->where('id', $id)->delete();
    }	    

	public function getKota(Request $request)
	{
		$prov = $request->input('prov');
		$get  = DB::table('ref_kabupaten')
				->where('provinsiId', $prov)
				->get();

		return view('agenda/get_kota', array('get'=>$get));				
	}    

	public function getCamat(Request $request)
	{
		$city = $request->input('city');
		$get = DB::table('ref_kecamatan')
				->where('kabupatenId', $city)
				->get();

		return view('agenda/get_camat', array('get'=>$get));
	}	

	public function tanggal(Request $request)
	{
      	$tanggal 		= $request->input('tanggal');
      	$tempat = Session::get('menu');
		$usere = Session::get('username');
		
		if($tempat == 'agenda'){
			$lol 	= DB::table('tb_agenda')
				->where('date_start', $tanggal)
				->get();	
		}

		return view('menu/agenda_tabel', ['users' => $lol]);

	}	

    public function save_img(Request $request)
    {
      // getting all of the post data
      $file = array('image' => Input::file('image'));
      // setting up rules
      $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
      // doing the validation, passing post data, rules and the messages
      $validator = Validator::make($file, $rules);
      if ($validator->fails()) {
        // send back to the page with the input data and errors
        return Redirect::to('./assets/images/agenda/')->withInput()->withErrors($validator);
      }else {
        // checking file is valid.
        if(Input::hasFile('image')) {
			$file 	= Input::file('image');
			$besar = $file->getSize();
			if($file->getSize() <= 2000000) {
				$destinationPath = './assets/images/agenda'; // upload path
				$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
				$fileName = rand(11111,99999).'.'.$extension; // renameing image
				Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
				// sending back with message
				echo "<script type='text/javascript'>parent.document.getElementById('gambarout').value='".$fileName."';</script>";
				Session::flash('success', 'Upload successfully'); 
				echo "<script type='text/javascript'>parent.document.getElementById('sizepic').innerHTML='".$besar." byte';</script>";
			} else {
				?><script>
					alert("File Anda Terlalu Besar Max 2 Mb");
				</script><?php
			}
		}else{
          // sending back with error message.
          Session::flash('error', 'uploaded file is not valid');
		}		
      }
    }	
}
