<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use DB;
use Input;
use Validator;
use Redirect;

class BeritaController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	/*public function __construct()
	{
		$this->middleware('guest');
	}*/

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cek = Session::get('username');
		if($cek != ""){
			Session::forget('menu');
	      	Session::set('menu', 'news');

			$users = DB::table('tb_berita2')
				->where('akses', Session::get('idcaleg'))
				->orderBy('create_date', 'desc')
	      		->get();
			return view('menu/berita', ['users' => $users]);
		}else{
			return redirect('/');
		}
	}

	public function frm_tambah()
	{
		$cek = Session::get('username');
		if($cek != ""){
			return view('berita/tambah_berita');
		}else{
			return redirect('/');
		}
	}	

	public function frm_edit(Request $request, $key)
	{
		$cek = Session::get('username');
		if($cek != ""){
			return view('berita/edit_berita', array('id_berita'=>$key));
		}else{
			return redirect('/');
		}
	}		

	public function tambah_data(Request $request)
	{
		$judul = $request->input('judul');
		$isi = $request->input('isi');
		$image_link = $request->input('image_link');
		$tgl =  date("Y-m-d H:i:s", strtotime($request->input('create_date')));
		$tt = $request->input('create_date');
		$creat = Session::get('username');
		$link = '/assets/images/berita/'.$image_link;
		$caleg = Session::get('idcaleg');

		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$date1 = date('Y-m-d');	
		$date2 = date('H:i:s');


/*      	$users = DB::table('tb_berita2')
      		->orderBy('id', 'desc')->get();
      	foreach ($users as $row) {}

		if(count($users) > 0){
			$hidi = $row->id+1;
		}else{
      		$hidi = 1;
		}
*/
		if($tt == ""){
			DB::table('tb_berita2')->insert(
	            [
	                'judul' 		=> $judul,
	                'isi'			=> $isi,
	                'image_link'	=> $link,
	                'create_by' 	=> $creat,
	                'create_date' 	=> $date,
	                'update_by'   	=> $creat, 
	                'update_date' 	=> $date,
	                'akses'			=> $caleg

	            ] 
	        );
		}else{
			DB::table('tb_berita2')->insert(
	            [
	                'judul' 				=> $judul,
	                'isi'			       	=> $isi,
	                'image_link'			=> $link,
	                'create_by' 			=> $creat,
	                'create_date' 			=> $tgl,
	                'update_by'   		=> $creat, 
	                'update_date' 		=> $date,
	                'update_by' 		=> $creat,
	                'update_date' 		=> $date,
	                'delete_by' 		=> $creat,
	                'delete_date' 		=> $date,
	                'akses'				=> $caleg

	            ] 
	        );
		}
        return redirect('admin/profil/news');
	}
					

    public function save_img(Request $request)
    {
      // getting all of the post data
      $file = array('image' => Input::file('image'));
      // setting up rules
      $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
      // doing the validation, passing post data, rules and the messages
      $validator = Validator::make($file, $rules);
      if ($validator->fails()) {
        // send back to the page with the input data and errors
        return Redirect::to('./assets/images/berita/')->withInput()->withErrors($validator);
      }else {
        // checking file is valid.
        if(Input::hasFile('image')) {
			$file 	= Input::file('image');
			$besar = $file->getSize();
			if($file->getSize() <= 2000000) {
				$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
				$fileName = rand(11111,99999).'.'.$extension; // renameing image
				$file->move('assets/images/berita', $fileName); // uploading file to given path
				// sending back with message
				echo "<script type='text/javascript'>parent.document.getElementById('gambarout').value='".$fileName."';</script>";
				Session::flash('success', 'Upload successfully'); 
				echo "<script type='text/javascript'>parent.document.getElementById('sizepic').innerHTML='".$besar." byte';</script>";
			} else {
				?><script>
					alert("File Anda Terlalu Besar Max 2 Mb");
				</script><?php
			}
		}else{
          // sending back with error message.
          Session::flash('error', 'uploaded file is not valid');
		}		
      }
    }	

	public function updates(Request $request)
	{
		$id_berita = $request->input('id_berita');
		$judul = $request->input('judul');
		$isi = $request->input('isi');
		$image_link = $request->input('image_link');
		$tgl =  date("Y-m-d H:i:s", strtotime($request->input('tanggal')));
		$link = url().'/assets/images/berita/'.$image_link;

		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$creat = Session::get('username');
		$caleg = Session::get('idcaleg');

		if($image_link == ""){
            DB::table('tb_berita2')->where('id_berita', $id_berita)->update(
                [
					'judul'       	 	=> $judul,
	                'isi' 		   		=> $isi,
	                'create_date' 		=> $tgl,
                    'update_by'   		=> $creat, 
                    'update_date' 		=> $date
                    ] 
            );
            return redirect('admin/profil/news');
		}else{
            DB::table('tb_berita2')->where('id_berita', $id_berita)->update(
                [
					'judul'       	 	=> $judul,
	                'isi' 		   		=> $isi,
	                'image_link'	 	=> $link,
	                'create_date' 		=> $tgl,
                    'update_by'   		=> $creat, 
                    'update_date' 		=> $date
                    ] 
            );
            return redirect('admin/profil/news');
		}	
	}


    public function view_item(Request $request)
    {
        $id_berita = Input::get('id_berita');
        return view('berita/lihat_berita', array('id_berita'=>$id_berita));
    }	

    public function delete_item(Request $request)
    {
        $id_berita = $request->input('id_berita');
        DB::table('tb_berita2')->where('id_berita', $id_berita)->delete();
    }	    
}
