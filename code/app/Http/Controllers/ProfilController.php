 <?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Validator;
use DB;
use Input;
use Session;
use Redirect;
use File;

class ProfilController extends Controller{
	public function getCurrentUser(){
		$getPersonalActFor = DB::table("tb_personal_act_for")->get();
		foreach($getPersonalActFor as $dat){
			$a = $dat->activated_for;
		}
		return $a;
	}
	public function index_personal(){
		Session::forget('menu');
		Session::set('menu', "data-personal");
		$id = $this->getCurrentUser();
		$dataCalon = DB::table('caleg_drh')->where('id', $id)->get();
		$dataPendidikan = DB::table('caleg_pendidikan')->where('caleg_drh_id', $id)->get();
		$dataOrganisasi = DB::table('caleg_organisasi')->where('caleg_drh_id', $id)->get();
		$dataProvinsi	= DB::table('m_geo_prov_kpu')->get();

		$tgl = DB::table('m_tanggal')->get();
		$bln = DB::table('m_bulan')->get();
		$thn = DB::table('m_tahun')->get();

		$tingkat = DB::table('m_tingkat_pendidikan')->get();

		return view('profil.index_personal', array(
			'dataCalon' => $dataCalon,
			'dataPendidikan' => $dataPendidikan,
			'dataOrganisasi' => $dataOrganisasi,
			'dataProvinsi'	 => $dataProvinsi,
			"tanggal"		 => $tgl,
			"bulan"			 => $bln,
			"tahun"			 => $thn,
			"tingkat_pend"	 => $tingkat,
			"id"			 => $id
		));
	}
	public function save_data($id){
		$foto = Input::file('foto');
		if(Input::hasFile('foto')){
			/*$ext = Input::file('filedaftarRiwayatHidup')->getClientOriginalExtension();*/
			$ext = File::extension($foto->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$id.".".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
			$foto->move('asset/img/foto', $new_name); /* MEMINDAH FILE */
			DB::table('caleg_drh')
				->where('id', $id)
				->update([
					'foto' => $new_name
				]);
		}

		$jenis_identitas = Input::get('jenis_identitas');
		$nomor_identitas = Input::get('nomor_identitas');

		$nama_depan = Input::get('nama_depan');
		$nama_tengah = Input::get('nama_tengah');
		$nama_belakang = Input::get('nama_belakang');

		$nama = $nama_depan." ".$nama_tengah." ".$nama_belakang;

		$jenis_kelamin = Input::get('jenkel');

		$tgl_lahir = Input::get('tgl_lahir');
		$bln_lahir = Input::get('bln_lahir');
		$thn_lahir = Input::get('tahun_lahir');
		$tgll = $thn_lahir."-".$bln_lahir."-".$tgl_lahir;

		$tempat_lahir = Input::get('tempat_lahir');
		$status_kawin = Input::get('status_pernikahan');
		$nama_pasangan = Input::get('nama_pasangan');
		$jumlah_anak = Input::get('jumlah_anak');

		$a_prov = Input::get('a_prov');
		$a_kota = Input::get('a_kota');
		$a_kec  = Input::get('a_kec');
		$a_desa = Input::get('a_desa');

		$update = DB::table('caleg_drh')
				->where('id', $id)
				->update([
					'nama_depan' => $nama_depan,
					'nama_tengah' => $nama_tengah,
					'nama_belakang' => $nama_belakang,
					'nama' => $nama,
					'jenis_kelamin' => $jenis_kelamin,
					'tempat_lahir' => $tempat_lahir,
					'tanggal_lahir' => $tgll,
					'status_kawin'  => 	$status_kawin,
					'pasangan' => $nama_pasangan,
					'anak' => $jumlah_anak,
					'jenis_identitas' => $jenis_identitas,
					'nomer_identitas' => $nomor_identitas,
					'provinsi' => $a_prov,
					'kabupaten' => $a_kota,
					'kecamatan' => $a_kec,
					'kelurahan' => $a_desa
				]);
	}

	public function index_frontpage(){
		Session::forget('menu');
		Session::set('menu', 'front-page');

		$dataFrontpage = DB::table('tb_frontpage')->get();

		return view('profil.index_frontpage', array(
			"dataFrontpage" => $dataFrontpage
		));
	}

	public function index_add_frontpage(){
		Session::forget('menu');
		Session::set('menu', 'front-page');

		return view('profil.add_frontpage');
	}

	public function act_about(){
		$_token  = csrf_token();
		$autobio = Input::get('autobio');
		$autobio = substr($autobio, 3);
		$autobio = substr($autobio, 0, -4);
		$autobio = str_replace("<p>", "", $autobio);
		$autobio = str_replace("</p>", "", $autobio);
		if($autobio != ""){
			$id_ins = DB::table('tb_autobiografi')
				->insertGetId([
					'autobiografi' => $autobio,
					'status' => 'NOT_ACTIVE'
				]);
		}

		$f_gambar = Input::file('thumb');

		if(Input::hasFile('thumb')){
			/*$ext = Input::file('filedaftarRiwayatHidup')->getClientOriginalExtension();*/
			$ext = File::extension($f_gambar->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
			$new_name = strtotime(date('Y-m-d H:i:s'))."_.".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
			$f_gambar->move('assets/images/autobiografi', $new_name); /* MEMINDAH FILE */
			
			DB::table('tb_autobiografi')
				->where('id', $id_ins)
				->update([
					"foto" => $new_name
					]);
		}
		return redirect('admin/profil/front-page');
	}

	public function edit_about(){
		/*echo $id;*/
		$getData = DB::table('tb_frontpage')->where('position', "LEFT")->get();
		return view('profil.edit_frontpage', array(
				"dataAbout" => $getData
		));
	}

	public function act_edit(){
		$_token  = csrf_token();
		$autobio = Input::get('autobio');
		$autobio = substr($autobio, 3);
		$autobio = substr($autobio, 0, -4);
		$autobio = str_replace("<p>", "", $autobio);
		$autobio = str_replace("</p>", "", $autobio);
		if($autobio != ""){
			$id_ins = DB::table('tb_frontpage')
				->where('position', "LEFT")
				->update([
					'deskripsi' => $autobio
				]);
		}

		$f_gambar = Input::file('thumb');

		if(Input::hasFile('thumb')){
			/*$ext = Input::file('filedaftarRiwayatHidup')->getClientOriginalExtension();*/
			$ext = File::extension($f_gambar->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
			$new_name = strtotime(date('Y-m-d H:i:s'))."_.".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
			$f_gambar->move('assets/images/autobiografi', $new_name); /* MEMINDAH FILE */
			
			DB::table('tb_frontpage')
				->where('position', "LEFT")
				->update([
					"foto" => $new_name
					]);
		}
		return redirect('admin/profil/front-page');
	}

	public function index_add_bg(){
		return view('profil.index_add_bg');
	}

	public function add_bg(){
		$motto = Input::get('motto');
		$deskripsi = Input::get('deskripsi');
		$gambar = Input::file('thumb');

		$insGI = DB::table('tb_background')
					->insertGetId([
						'motto' => $motto,
						'deskripsi' => $deskripsi,
						'status' => "PASSIVE",
						"create_date" => date('Y-m-d H:i:s')
					]);

		if(Input::hasFile('thumb')){
			$ext = File::extension($gambar->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
			$new_name = strtotime(date('Y-m-d H:i:s'))."_.".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
			$gambar->move('assets/images/background', $new_name); /* MEMINDAH FILE */
			
			DB::table('tb_background')
				->where('id', $insGI)
				->update([
					"background" => $new_name
					]);
		}
		return redirect('admin/profil/front-page');
	}
	public function index_edit_bg($key){
		$getData = DB::table('tb_frontpage')->where('position', "TOP")->get();
		return view('profil.edit_background', array(
				"dataBg" => $getData,
				"id"     => $key
		));
	}
	public function edit_bg(){
		$motto 	   = Input::get('motto');
		$deskripsi = Input::get('deskripsi');
		$alias 	   = Input::get('alias');
		$gambar    = Input::file('thumb');

		$insGI = DB::table('tb_frontpage')
					->where('position', "TOP")
					->update([
						'title' => $motto,
						'subtitle' => $deskripsi,
						'name_alias' => $alias
					]);

		if(Input::hasFile('thumb')){
			$ext = File::extension($gambar->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
			$new_name = strtotime(date('Y-m-d H:i:s'))."_.".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
			$gambar->move('assets/images/background', $new_name); /* MEMINDAH FILE */
			
			DB::table('tb_frontpage')
				->where('position', "TOP")
				->update([
					"foto" => $new_name
					]);
		}
		return redirect('admin/profil/front-page');	
	}

	public function edit_campaign(){
		$getDataCampaign = DB::table('tb_frontpage')->where('position', 'RIGHT')->get();
		return view('profil.edit_campaign', array(
			"dataCampaign" => $getDataCampaign
		));
	}

	public function act_edit_campaign(){
		$title = Input::get('title');
		$deskripsi = Input::get('deskripsi');

		DB::table('tb_frontpage')
			->where('position', 'RIGHT')
			->update([
				'title' => $title,
				'deskripsi' => $deskripsi
			]);
		return redirect('admin/profil/front-page');
	}

	public function edit_disclaimer(){
		$getDisc = DB::table('tb_frontpage')->where('position', 'BOTTOM')->get();
		return view('profil.edit_disclaimer', array(
					"dataDisc" => $getDisc
				));
	}
}
?>