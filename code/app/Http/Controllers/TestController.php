<?php namespace App\Http\Controllers;

use Auth;
use Socialite;
use Request;

class TestController extends Controller{
	public function google(){
	    return Socialite::driver('google')->redirect();
	}

	public function google_callback(){
		$user = Socialite::driver('google')->user();
		echo $user->getName();
		echo $user->getAvatar();
		echo $user->getEmail();
	}
}

 ?>