<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Validator;
use Input;
use DB;
use Redirect;
use Session;
use File;

class LogistikController extends Controller{
	public function view_master(){
		Session::forget('menu');
		Session::set('menu', 'master-log');
		$getLogistik = DB::table('tb_oleh')
					->select('tb_oleh.*', 'm_kategori_logistik.kategori_name')
					->leftJoin('m_kategori_logistik', 'tb_oleh.kategori', '=', 'm_kategori_logistik.id')
					->paginate(10);
		return view('logistik.master', array(
				"dataLogistik" => $getLogistik
		));
	}

	public function view_master_add(){
		$getKategori = DB::table('m_kategori_logistik')->get();
		return view('logistik.add_master', array(
				"dataKategori" => $getKategori
		));
	}

	public function act_add(){
		$ind = 0;
		$nama_barang = Input::get('nama_barang');
		$kategori 	 = Input::get('kategori');
		$stok 		 = Input::get('stok');
		$detail		 = Input::get('detail');
		$stok_detail = Input::get('stok_detail');
		$detail_count = Input::get('detail_count');

		$foto 		 = Input::file('foto');

		$insert = DB::table('tb_oleh')
				->insertGetId([
					'nama_barang' => $nama_barang,
					'kategori' 	  => $kategori
				]);
		echo $insert;
		$stok_real = 0;
		while ($ind < $detail_count) {
			$id_n = DB::table('tb_oleh_detail')
					->insertGetId([
						'id_logistik' => $insert,
						'detail'	  => $detail[$ind],
						'stok'		  => $stok_detail[$ind]
					]);
			DB::table('tb_stok_detail_log')
					->insert([
						'id_logistik' => $insert,
						'id_detail'   => $id_n,
						'stok_tambahan' => $stok_detail[$ind],
						'date_in'     => date('Y-m-d H:i:s')
					]);
			$stok_real = $stok_real + $stok_detail[$ind];
			$ind++;
		}
		DB::table('tb_oleh')->where('id', $insert)->update(['stok' => $stok_real]);
		if(Input::hasFile('foto')){
			$ext = File::extension($foto->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
			$new_name = strtotime(date('Y-m-d H:i:s'))."_logistik.".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
			$foto->move('assets/images/logistik', $new_name); /* MEMINDAH FILE */
			
			DB::table('tb_oleh')
				->where('id', $insert)
				->update([
					"image_link" => $new_name
					]);
		}
		echo $stok_real;
	}

	public function view_detail(){
		$id = Input::get('key');

		$getData = DB::table('tb_oleh')
					->select('tb_oleh.*', 'm_kategori_logistik.kategori_name')
					->leftJoin('m_kategori_logistik', 'tb_oleh.kategori', '=', 'm_kategori_logistik.id')
					->where('tb_oleh.id', $id)
					->get();

		$getDetail = DB::table('tb_oleh_detail')
					->where('id_logistik', $id)
					->get();
		return view('logistik.modal-response', array(
				"dataLogistik" => $getData,
				"dataDetail"   => $getDetail
		));
	}

	public function add_stok(){
		$id = Input::get('key');
		$arr = array();
		$arr['info'] = array();
		$arr['detail'] = array();

		$arp = array();
		$getData = DB::table('tb_oleh')
					->select('tb_oleh.*', 'm_kategori_logistik.kategori_name')
					->leftJoin('m_kategori_logistik', 'tb_oleh.kategori', '=', 'm_kategori_logistik.id')
					->where('tb_oleh.id', $id)
					->get();

		foreach ($getData as $get) {
			array_push($arr['info'], array(
					"nama_barang" => $get->nama_barang,
					"kategori"	  => $get->kategori_name,
					"id_log"	  => $id
				));
		}

		$getStok = DB::table('tb_oleh_detail')
					->where('id_logistik', $id)
					->get();
		$countData = DB::table('tb_oleh_detail')
					->where('id_logistik', $id)
					->count();
		foreach ($getStok as $data) {
			array_push($arr['detail'], array(
					"detail" => $data->detail,
					"stok"   => $data->stok,
					"id_detail" => $data->id
				));
		}
		/*array_push($arr['detail'], $arp);*/
		$arr['dataLength'] = $countData;
		echo json_encode($arr, JSON_PRETTY_PRINT);
		/*return view('logistik.add_stok', array(
				"dataLogistik" => $getData,
				"dataStok" => $getStok
		));*/
	}

	public function act_add_stok(){
		$id_log = Input::get('id_log');
		$id_detail = Input::get('id_detail');
		$stok 	= Input::get('stok');
		$jumlah_data = Input::get('jumlah_data');
		$a = 0;
		$stok_new = 0;
		while ($a < $jumlah_data) {
			DB::update("UPDATE tb_oleh_detail SET stok=stok+".$stok[$a]." WHERE id='".$id_detail[$a]."'");

			DB::table('tb_stok_detail_log')
					->insert([
						'id_logistik' => $id_log,
						'id_detail'   => $id_detail[$a],
						'stok_tambahan' => $stok[$a],
						'date_in'     => date('Y-m-d H:i:s')
					]);

			$stok_new = $stok_new + $stok[$a];
			$a++;
		}
		DB::update("UPDATE tb_oleh SET stok=stok+".$stok_new." WHERE id='".$id_log."'");
		return redirect('admin/logistik/master');
	}

	public function request_list(){
		Session::forget('menu');
		Session::set('menu', 'req-list');

		$getJoined = DB::table('tb_request_list')
					->select('tb_request_list.id', 'tb_request_list.request_desc', 'tb_request_list.create_date', 'tb_request_list.status', 'm_relawan.nama')
					->leftJoin('m_relawan', 'tb_request_list.id_relawan', '=', 'm_relawan.id_relawan')
					->get();
		return view('logistik.view_request', array(
					"dataJoined" => $getJoined
				));
	}

	public function jsonViewDetail(){
		$key = Input::get('key');
		$arr = array();
		$arr['info'] = array();
		$arr['detail'] = array();
		$getMain = DB::table('tb_request_list')
					->select('tb_request_list.id', 'tb_request_list.request_desc', 'tb_request_list.create_date', 'tb_request_list.status', 'm_relawan.nama')
					->leftJoin('m_relawan', 'tb_request_list.id_relawan', '=', 'm_relawan.id_relawan')
					->where('id', $key)
					->get();

		foreach ($getMain as $get) {
			array_push($arr['info'], array(
				"req_by" => $get->nama,
				"req_desc" => $get->request_desc,
				"req_date" => $get->create_date
			));
		}
		$getDetail = DB::table('tb_request_detail')
						->leftJoin('tb_oleh', 'tb_request_detail.id_logistik', '=', 'tb_oleh.id')
						->where('id_request', $key)
						->get();
		foreach ($getDetail as $data) {
			array_push($arr['detail'], array(
				"nama_barang" => $data->nama_barang,
				"stok_requested" => $data->stok_requested
			));
		}
		echo json_encode($arr, JSON_PRETTY_PRINT);
	}

	public function toggle_state(){
		$key = Input::get('key');
		$state = Input::get('state');

		if($state == 1){

		}else{
			DB::table('tb_request_list')
					->where('id', $key)
					->update([
						'status' => "DECLINE"
					]);
		}
	}

	/* FUNCTION FOR VOLUNTEER */
	public function relawan_req(){
		Session::forget('menu');
		Session::set('menu', 'req-log');

		$getJoined = DB::table('tb_request_list')
					->select('tb_request_list.id', 'tb_request_list.request_desc', 'tb_request_list.create_date', 'tb_request_list.status', 'm_relawan.nama')
					->leftJoin('m_relawan', 'tb_request_list.id_relawan', '=', 'm_relawan.id_relawan')
					->where('tb_request_list.id_relawan', Session::get('idcaleg'))
					->get();

		return view('relawan.logistik.request', array(
				"dataJoined" => $getJoined
		));
	}

	public function relawan_add_request(){
		Session::forget('menu');
		Session::set('menu', 'form-req-log');
		$getLogistik = DB::table('tb_oleh')->get();
		return view('relawan.logistik.add_request', array(
				"dataLogistik" => $getLogistik
			));
	}

	public function relawan_act(){
		$desc = Input::get('desc');
		$id_re = Session::get('idcaleg');
		$logistik = Input::get('logistik');
		$stok = Input::get('stok_req');
		$count = count($logistik);
		$ids = DB::table('tb_request_list')
				->insertGetId([
					"id_relawan" => $id_re,
					"request_desc" => $desc,
					"create_date" => date('Y-m-d H:i:s'),
					"status" => "WAITING"
				]);
		$a = 0;
		while ($a < $count) {
			DB::table('tb_request_detail')
				->insert([
					"id_logistik" => $logistik[$a],
					"id_request" => $ids,
					"stok_requested" => $stok[$a]
				]);
			$a++;
		}

	}
}
?>