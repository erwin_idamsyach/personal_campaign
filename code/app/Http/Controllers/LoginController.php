<?php namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Input;
use Session;
class LoginController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	/*public function __construct()
	{
		$this->middleware('guest');
	}*/

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	
	public function index(Request $data)
	{
		$username = $data->input('username');
		$password = $data->input('password');

    // ->join('caleg_drh', 'tb_login.id', '=', 'caleg_drh.id')
    //         ->select('caleg_drh.*', 'tb_login.username', 'tb_login.password', 'tb_login.role')
        
		$lol = DB::table('tb_login')
            ->where('username', $username)
				->where('password', $password)
				->get();
		foreach ($lol as $key) {
			$caleg = DB::table('caleg_drh')
					->where('id', $key->id)
					->get();
			foreach ($caleg as $row) {
			}
		}	
		

		if(count($lol) > 0){
			if($key->role == 2){
				Session::set('username', $key->username);
				// Session::set('username', 'Drs.Hi. Moh. Yasin Payapo,M.Pd');
				Session::set('idcaleg', $row->id);
				Session::set('namacaleg', $row->nama);
				Session::set('role', $key->role);
				Session::forget('akses');
				Session::set('akses', 'user');
			}else if($key->role == 1){	
				Session::set('username', 'Admin');		
				Session::set('namacaleg', 'Admin');	
				Session::set('role', $key->role);
				session::forget('akses');
				session::set('akses', 'admin');
			}else{
				$getUser = DB::table('tb_login')
						->leftJoin('m_relawan', 'tb_login.rel_id', '=', 'm_relawan.id_relawan')
						->where('username', $username)
						->where('password', $password)
						->get();
				foreach ($getUser as $get) {
					Session::set('username', $get->nama);
					Session::set('idcaleg', $get->id_relawan);
					Session::set('namacaleg', $get->nama);
				// Session::set('username', 'Drs.Hi. Moh. Yasin Payapo,M.Pd');
					Session::set('role', $get->role);
					Session::forget('akses');
					Session::set('akses', 'relawan');
				}
			}
			echo "sukses";
		}else{
			return view('login');
		}
	}

	public function ea()
	{
		$username = Session::get('username');
		if(isset($username) != ""){
			session::forget('menu');
			session::set('menu', 'dashboard');

		    $kabupaten = DB::table('m_geo_kab_kpu')
		    	->join('caleg_drh', 'caleg_drh.tingkat_provinsi', '=', 'm_geo_kab_kpu.geo_prov_id')
		    	->where('caleg_drh.id', Session::get('idcaleg'))
		      	->count();

		    $kecamatan = DB::table('m_geo_kec_kpu')
		    	->join('caleg_drh', 'caleg_drh.tingkat_kabupaten', '=', 'm_geo_kec_kpu.geo_kab_id')
		    	->where('caleg_drh.id', Session::get('idcaleg'))
		      	->count();

		    // $penduduk = DB::table('m_penduduk')
		    // 	->join('caleg_drh', 'caleg_drh.tingkat_kabupaten', '=', 'm_geo_kec_kpu.geo_kab_id')
		    // 	->where('caleg_drh.id', Session::get('idcaleg'))
		    //   	->get();
		      	
		    $relawan = DB::table('m_relawan')
		    	->where('akses', Session::get('idcaleg'))
		    	->count();

		    $responseView = 'menu.dashboard';
			$responseData = array(
				'kabupaten' => $kabupaten,
				'kecamatan' => $kecamatan,
				'relawan'	=> $relawan
			);
			// return view($responseView,$responseData);
			if (Session::get('role') != 1){
				return redirect('admin/dashboard');				
			}else{
				return redirect('admin/caleg_data');				
			}
			
		}else{
			return redirect('/');	
		}
	}

	public function logout()
	{
		// Session::forget('username');
		session()->forget('username');
		return redirect('/');
		/*return redirect()->action('LoginController@index');*/
	}	

}
