<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Validator;
use DB;
use Input;
use Session;
use Redirect;
use File;


class PersonalContoller extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//create
		$cek = Session::get('username');
		if($cek != ""){
			Session::forget('menu');
	      	Session::set('menu', 'merchandise');

	      	$cek = Session::get('username');

			$users = DB::table('tb_oleh')
				->where('akes',$cek)
				->orderBy('id_oleh', 'asc')
	      		->get();
			return view('merchandise/index', ['users' => $users]);
		}else{
			return redirect('/');	
		}
	}

	public function frm_edit()
	{
		Session::forget('menu');
	      	Session::set('menu', 'personal');
		$cek = Session::get('username');
				
		if($cek != ""){
			return view('personal/personal');
		}else{
			return redirect('/');	
		}
		
	}

	public function edit_caleg($type, $key){
		/*echo $key;*/
		if($type == "biodata"){
			$getData = DB::table('caleg_drh')
                ->where('id', $key)
                ->get();
            $goto = "Biodata Diri";
            $act = "biodata";
		}else if($type == "pendidikan"){
			$getData = DB::table('caleg_pendidikan')
                ->where('caleg_drh_id', $key)
                ->get();
            $goto = "Riwayat Pendidikan";
            $act = "pendidikan";
		}else{
			$getData = DB::table('caleg_organisasi')
                ->where('caleg_drh_id', $key)
                ->get();
            $goto = "Organisasi";
            $act = "organisasi";
		}
        $getProv = DB::table('m_geo_prov_kpu')->get();
        return view('caleg/edit', array(
        		"dataCaleg" => $getData,
        		"dataProvinsi" => $getProv,
        		"goto" => $goto,
        		"id"   => $key,
        		"act" => $act
        ));

	}

	public function act_edit_caleg($type, $key){
		if($type == "biodata"){
			$namaDepan = @$_GET['namaDepan'];
			$identitas = @$_GET['identitas'];
			$noIdentitas = @$_GET['noIdentitas'];
			$tempatLahir = @$_GET['tempatLahir'];
			$tanggalLahir = @$_GET['tanggalLahir'];
			$alamat = @$_GET['alamat'];
			$abProv = @$_GET['abProv'];
			$abKab = @$_GET['abKab'];
			$abKec = @$_GET['abKec'];
			$abKel = @$_GET['abKel'];
			$jenisKelamin = @$_GET['jenisKelamin'];
			$statusPernikahan = @$_GET['statusPernikahan'];
			$namaPasangan = @$_GET['namaPasangan'];
			$jumlahAnak = @$_GET['jumlahAnak'];
			$foto = @$_GET['foto'];
			$agama = @$_GET['agama'];
			$telp = @$_GET['telp'];
			$hp = @$_GET['hp'];
			$emailBalon = @$_GET['emailBalon'];
			$twitter = @$_GET['twitter'];
			$facebook = @$_GET['facebook'];
			$note = @$_GET['note'];

			$update = DB::table('caleg_drh')
					->where('id', $key)
					->update([
						'nama' => $namaDepan,
						'jenis_identitas' => $identitas,
						'nomer_identitas' => $noIdentitas,
						'tempat_lahir' => $tempatLahir,
						'tanggal_lahir' => date('Y-m-d', strtotime($tanggalLahir)),
						'jenis_kelamin' => $jenisKelamin,
						'agama' => $agama,
						'alamat' => $alamat,
						'provinsi' => $abProv,
						'kabupaten' => $abKab,
						'kecamatan' => $abKec,
						'kelurahan' => $abKel,
						'telephone' => $telp,
						'handphone' => $hp,
						'email' => $emailBalon,
						'twitter' => $twitter,
						'facebook' => $facebook,
						'status_kawin' => $statusPernikahan,
						'pasangan' => $namaPasangan,
						'anak' => $jumlahAnak
					]);
				return redirect('admin/personal_data');
		}else if($type == "pendidikan"){

		}else{

		}
	}

	public function tampil()
	{
		Session::forget('menu');
	      	Session::set('menu', 'tampil');
		$cek = Session::get('username');
				
		if($cek != ""){
			return view('personal/moto');
		}else{
			return redirect('/');	
		}
		
	}


	public function frm_tambah_moto()
	{
		$cek = Session::get('username');
		if($cek != ""){
			return view('personal/tambah_moto');
		}else{
			return redirect('/');
		}
	}	

	public function frm_edit_moto(Request $request, $key)
	{
		$cek = Session::get('username');
		if($cek != ""){
			return view('personal/edit', array('id'=>$key));
		}else{
			return redirect('/');
		}
	}		

	public function tambah_data_moto(Request $request)
	{
		$deskripsi = $request->input('deskripsi');
		$judul = $request->input('judul');
		$creat = Session::get('username');
		$caleg = Session::get('idcaleg');	

		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$date1 = date('Y-m-d');	
		$date2 = date('H:i:s');

		DB::table('tb_moto')->insert(
            [	
            	'judul'					=>$judul,
                'deskripsi'				=> $deskripsi,
 				'akses'					=> $caleg,
 				'status'				=> 'Pasif',
                'create_by' 			=> $creat,
                'create_date' 			=> $date,
                'update_by' 	  		=> $creat, 
                'update_date' 			=> $date,
                'update_by' 			=> $creat,
                'update_date' 			=> $date,
                'delete_by' 			=> $creat,
                'delete_date' 			=> $date

            ] 
        );
	}
					

    

	public function updates_moto(Request $request)
	{
		$id = $request->input('id');
		$deskripsi = $request->input('deskripsi');
		$judul = $request->input('judul');
		$creat = Session::get('username');
		$caleg = Session::get('idcaleg');		

		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$date1 = date('Y-m-d');	
		$date2 = date('H:i:s');

		
            DB::table('tb_moto')->where('id', $id)->update(
                [
                	'judul'				=> $judul,
					'deskripsi'       	=> $deskripsi,
                    'update_by'   		=> $creat, 
                    'update_date' 		=> $date,
                    'akses'				=> $caleg
                    ] 
            );
			
	}


    public function view_item_moto(Request $request)
    {
        $id = Input::get('id');
        return view('personal/lihat_moto', array('id'=>$id));
    }	

    public function delete_item_moto(Request $request)
    {
        $id = $request->input('id');
        DB::table('tb_moto')->where('id', $id)->delete();
    }

	public function ref_moto(Request $request, $key)
	{
		DB::table('tb_moto')->where('id', $key)->update(
                [
	                'status' 				=> 'Pasif'
                ] 
            );
		return redirect()->action('PersonalContoller@tampil');
	}	

	public function refw_moto(Request $request, $key)
	{
		DB::table('tb_moto')->where('id', $key)->update(
                [
	                'status' 				=> 'Aktif'
                ] 
            );

		/*DB::table('tb_moto')->where('id','!=', $key)->update(
                [
	                'status' 				=> 'Pasif'
                ] 
            );*/
		return redirect()->action('PersonalContoller@tampil');
	}    

	public function pendidikan(){
		$cek = Session::get('username');
		if($cek != ""){
			Session::forget('menu');
	      	Session::set('menu', 'pendidikan');

			$pendidikan = DB::table('caleg_pendidikan')
					->where('caleg_drh_id', Session::get('idcaleg'))
					->get();
			$responseView = 'personal/pendidikan';
			$responseData = array(
				'pendidikan' => $pendidikan
			);

			return view($responseView,$responseData);			
		}else{
			return redirect('/');	
		}		
	}

	public function organisasi(){
		$cek = Session::get('username');
		if($cek != ""){
			Session::forget('menu');
	      	Session::set('menu', 'organisasi');

			$organisasi = DB::table('caleg_organisasi')
					->where('caleg_drh_id', Session::get('idcaleg'))
					->get();
			$responseView = 'personal/organisasi';
			$responseData = array(
				'organisasi' => $organisasi
			);

			return view($responseView,$responseData);			
		}else{
			return redirect('/');	
		}		
	}

	public function index_autobiografi(){
		Session::forget('menu');
		Session::set('menu', 'autobigrafi');

		$getPersonalActFor = DB::table("tb_personal_act_for")->get();
		foreach($getPersonalActFor as $dat){
			$id = $dat->activated_for;
		}

		$getAuto = DB::table('tb_autobiografi')->where('id', $id)->get();

		return view('personal.autobiografi', array(
				"dataAuto" => $getAuto,
				"id_user"  => $id
			));
	}

	public function update_autobiografi($key){
		$_token  = csrf_token();
		$autobio = Input::get('autobio');
		$autobio = substr($autobio, 3);
		$autobio = substr($autobio, 0, -4);
		$autobio = str_replace("<p>", "", $autobio);
		$autobio = str_replace("</p>", "", $autobio);
		if($autobio != ""){
			DB::table('tb_autobiografi')
				->where('auto_for', $key)
				->update([
					'autobiografi' => $autobio
				]);
		}

		$f_gambar = Input::file('thumb');

		if(Input::hasFile('thumb')){
			/*$ext = Input::file('filedaftarRiwayatHidup')->getClientOriginalExtension();*/
			$ext = File::extension($f_gambar->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$key.".".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
			$f_gambar->move('assets/images/autobiografi', $new_name); /* MEMINDAH FILE */
			
			DB::table('tb_autobiografi')
				->where('auto_for', $key)
				->update([
					"foto" => $new_name
					]);
		}
		return redirect('admin/autobiografi');
	}
}
