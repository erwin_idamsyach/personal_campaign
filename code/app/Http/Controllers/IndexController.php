<?php namespace App\Http\Controllers;

use DB;
use Session;

class IndexController extends Controller{


	public function index($key = null)
	{


		// $a = Session::get('username');
		// $id = Session::get('username');
		$getPersonalActFor = DB::table("tb_personal_act_for")->get();
		foreach($getPersonalActFor as $dat){
			$a = $dat->activated_for;
		}
		
		$getNewsAlpha = DB::table('tb_berita2')
		->where('status', 'ACTIVE')
		->orderBy('create_date', 'desc')
		->skip(0)->take(3)->get();
		$getNewsBeta  = DB::table('tb_berita2')
		->select('judul', 'image_link')
		->orderBy('create_date', 'desc')
		->skip(4)->take(4)->get();
		$getPopuler   = DB::table('tb_berita2')
		->select('judul',
			'image_link')
		->orderBy('create_date', 'desc')
		->skip(0)->take(3)->get();
		$getAgenda   = DB::table('tb_agenda')
		->select('judul',
			'image_link',
			'create_date',
			'description',
			DB::raw("SUBSTRING(description, 0, 20) as description"),
			DB::raw("DATE_FORMAT(create_date, '%d %M %Y %H:%i') as d_new"))
		->orderBy('create_date', 'desc')
		->where('akses', $a)
		->skip(0)->take(3)->get(0);
		$getNewsaaaa   = DB::table('tb_berita2')
		->select('judul',
			'image_link',
			'create_date',
			'view_count',
			'isi',
			DB::raw("SUBSTRING(tb_berita2.isi, 0, 20) as description"),
			DB::raw("DATE_FORMAT(create_date, '%d %M %Y %H:%i') as d_new"))
		->orderBy('create_date', 'desc')
		->where('akses', $a)
		->skip(0)->take(3)->get(0);
		$getbio   = DB::table('tb_login')
		->join('caleg_drh','caleg_drh.id','=','tb_login.id')
		->where('caleg_drh.id', $a)
		->get();
		$gethadiah   = DB::table('tb_oleh')
		->select('nama_barang',
			'stok',
			'image_link')
		->get();
		$getBg = DB::table('tb_frontpage')->where('position', "TOP")->get();
		$getAuto = DB::table('tb_frontpage')->where('position', 'LEFT')->get();
		$getProv = DB::table('m_geo_prov_kpu')->get();
		$getDataRight = DB::table('tb_frontpage')->where('position', 'RIGHT')->get();
		$tgl = DB::table('m_tanggal')->get();
		$bln = DB::table('m_bulan')->get();
		$thn = DB::table('m_tahun')->get();
		$dataProvinsi = DB::table('m_geo_prov_kpu')
        		->get();
		// foreach ($getbio as $key) {
		// 	$idcaleg = $key->id
		// }
		return view("homepage.index", array(
			"dataBg"		 => $getBg,
			"dataNews"       => $getNewsAlpha,
			"dataNewsTile"   => $getNewsBeta,
			"dataAgenda"     => $getAgenda,
			"dataPopuler"    => $getPopuler,
			"dataIsi"	 	 => $getNewsaaaa,
			"datahadiah"	 => $gethadiah,
			"databio"		 =>	$getbio,
			"menu"           => "event",
			"akses"			 => $a,
			"dataAuto"		 => $getAuto,
			"dataProv"		 => $getProv,
			"dataRight"		 => $getDataRight,
			'dataProvinsi' 	=> $dataProvinsi,
			"tanggal"		 => $tgl,
			"bulan"			 => $bln,
			"tahun"			 => $thn
		));
	}

	public function proto(){
		$getPersonalActFor = DB::table("tb_personal_act_for")->get();
		foreach($getPersonalActFor as $dat){
			$a = $dat->activated_for;
		}
		
		$getNewsAlpha = DB::table('tb_berita2')
		->select('judul', 'image_link')
		->orderBy('create_date', 'desc')
		->skip(0)->take(4)->get();
		$getNewsBeta  = DB::table('tb_berita2')
		->select('judul', 'image_link')
		->orderBy('create_date', 'desc')
		->skip(4)->take(4)->get();
		$getPopuler   = DB::table('tb_berita2')
		->select('judul',
			'image_link')
		->orderBy('create_date', 'desc')
		->skip(0)->take(3)->get();
		$getAgenda   = DB::table('tb_agenda')
		->select('judul',
			'image_link',
			'create_date',
			'description',
			DB::raw("SUBSTRING(description, 0, 20) as description"),
			DB::raw("DATE_FORMAT(create_date, '%d %M %Y %H:%i') as d_new"))
		->orderBy('create_date', 'desc')
		->where('akses', $a)
		->skip(0)->take(3)->get(0);
		$getNewsaaaa   = DB::table('tb_berita2')
		->select('judul',
			'image_link',
			'create_date',
			'view_count',
			'isi',
			DB::raw("SUBSTRING(tb_berita2.isi, 0, 20) as description"),
			DB::raw("DATE_FORMAT(create_date, '%d %M %Y %H:%i') as d_new"))
		->orderBy('create_date', 'desc')
		->where('akses', $a)
		->skip(0)->take(3)->get(0);
		$getbio   = DB::table('tb_login')
		->join('caleg_drh','caleg_drh.id','=','tb_login.id')
		->where('caleg_drh.id', $a)
		->get();
		$gethadiah   = DB::table('tb_oleh')
		->select('nama_oleh',
			'stok',
			'image_link',
			DB::raw("SUBSTRING(0, 20) as description"))
		->where('akes', $a)
		->get();
		$getmoto   = DB::table('tb_moto')
		->select('deskripsi',
			'judul',
			DB::raw("SUBSTRING(0, 15) as description"))
		->where('akses', $a)
		->where('status','aktif')
		->skip(0)->take(1)->get(0);
		$getAuto = DB::table('tb_autobiografi')->where('id', $a)->get();
		$agama = DB::table('ref_agama')
					->orderBy('agama_label', 'asc')
					->get();

		// foreach ($getbio as $key) {
		// 	$idcaleg = $key->id
		// }
		return view("front.index", array(
			"dataNewsSlider" => $getNewsAlpha,
			"dataNewsTile"   => $getNewsBeta,
			"dataAgenda"     => $getAgenda,
			"dataPopuler"    => $getPopuler,
			"dataIsi"	 	 => $getNewsaaaa,
			"datahadiah"	 => $gethadiah,
			"databio"		 =>	$getbio,
			"datamoto"		 => $getmoto,
			"menu"           => "event",
			"dagama"		 => $agama,
			"akses"			 => $a,
			"dataAuto"       => $getAuto
		));	
	}

	public function read($key){
		$ky = $key;
		$judul = str_replace("-", " ", $key);
		$judul = str_replace("_", "/", $judul);
		$judul = addslashes($judul);
		$update = DB::table('tb_berita2')
		->where('judul', $judul)
		->increment('view_count');

		$getNews = DB::table('tb_berita2')
		->select('judul', 'image_link', 'isi', 'view_count', 'create_date')
		->where('judul', $judul)
		->get();

		return view('read.read', array(
			'dataNews' => $getNews,
			'judul'    => $judul,
			'for_link' => $ky,
			"menu"     => "berita"
		));
	}
	public function view_news_list($key){
		$getNewsaaaa   = DB::table('tb_berita2')
		->select('judul',
			'image_link',
			'create_date',
			'view_count',
			'isi',
			DB::raw("SUBSTRING(tb_berita2.isi, 0, 20) as description"),
			DB::raw("DATE_FORMAT(create_date, '%d %M %Y %H:%i') as d_new"))
		->orderBy('view_count', 'desc')
		->skip(0)->take(3)->get(0);
		return view("welcome", array(
			"dataIsi"	 	 => $getNewsaaaa,
			"menu"           => "berita"
		));
		if($key == "terbaru"){
			$getNewsList = DB::table('tb_berita2')
			->select('judul',
				'image_link',
				'view_count',
				DB::raw('SUBSTRING(isi, 1, 180) as description'),
				DB::raw("DATE_FORMAT(create_date, '%d %M %Y %H:%i') as d_new"))
			->orderBy('create_date', 'desc')
			->paginate(5);
			return view('welcome', array(
				"dataNews" => $getNewsList,
				"type"     => "NEW",
				"menu"     => "berita"
			));
		}else{
			$getNewsList = DB::table('tb_berita2')
			->select('judul',
				'image_link',
				'view_count',
				DB::raw('SUBSTRING(isi, 1, 180) as description'),
				DB::raw("DATE_FORMAT(create_date, '%d %M %Y %H:%i') as d_new"))
			->orderBy('view_count', 'desc')
			->paginate(5);
			return view('welcome', array(
				"dataNews" => $getNewsList,
				"type"     => "POPULAR",
				"menu"     => "berita"
			));
		}
	}

	public function view_profile($user){
		$exp  = explode("_", $user);
		$nama = str_replace("-", " ", $exp[0]);
		$id   = $exp[1];
		$getData = DB::table('caleg_drh')
					->leftJoin('tb_autobiografi', 'caleg_drh.id', '=', 'tb_autobiografi.auto_for')
					->select('caleg_drh.*', 'tb_autobiografi.*')
					->where('caleg_drh.id', $id)
					->get();
		$getDataPendidikan = DB::table('caleg_pendidikan')
							->where('caleg_drh_id', $id)
							->get();
		$getDataOrganisasi = DB::table('caleg_organisasi')
							->where('caleg_drh_id', $id)
							->get();
		return view('homepage.page_profile', array(
					"dataProfile" => $getData,
					"dataPendidikan" => $getDataPendidikan,
					"dataOrganisasi" => $getDataOrganisasi,
					"nama" 		  => $nama,
					"tanpa_menu"  => "YES"
				));
	}
	public function viewPendaftaranRelawan(){

	}

	public function read_news($key){
		$arr  = array();
		$data_ot = array();
		$getNews = DB::table('tb_berita2')
					->where('id_berita', $key)
					->get();
		$getDataOther = DB::table('tb_berita2')
					->select('judul', 'image_link', 'id_berita')
					->where('id_berita', '!=', $key)
					->skip(0)->take(3)->get();
		$dataOtherCount = DB::table('tb_berita2')
					->select('judul', 'image_link', 'id_berita')
					->where('id_berita', '!=', $key)
					->skip(0)->take(3)->count();
		foreach ($getDataOther as $get) {
			array_push($data_ot, array(
				"judul" 	 => $get->judul,
				"image_link" => $get->image_link,
				"id"         => $get->id_berita
			));
		}
		foreach ($getNews as $data) {
			$date = date_create($data->create_date);
			$date = date_format($date, "D, d M Y");
			$arr = array(
				"judul" 	 => $data->judul,
				"content" 	 => $data->isi,
				"image_link" => $data->image_link,
				"date"		 => $date,
				"data_other" => $data_ot,
				"data_other_count" => $dataOtherCount
				);
		}
		echo json_encode($arr, JSON_PRETTY_PRINT);
	}
}
?>