<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use File;
use Redirect;
use DB;
use Vsmoraes\Pdf\Pdf;

class PdfController extends Controller{
  private $pdf;
  public function __construct(Pdf $pdf)
  {
      $this->pdf = $pdf;
  }
  public function PrintPernyataan($id){
    $dataCaleg = DB::table('caleg_drh')
        ->where('id','=',$id)
        ->first();
    $kel = DB::table('ref_desa')
        ->where('desaid','=',$dataCaleg->kelurahan)
        ->first();
    $kec = DB::table('m_geo_kec_kpu')
        ->where('geo_kec_id','=',$dataCaleg->kecamatan)
        ->first();
    $kab = DB::table('ref_kabupaten')
        ->where('kabupatenId','=',$dataCaleg->kabupaten)
        ->first();
    $prov = DB::table('ref_provinsi')
        ->where('provinsiId','=',$dataCaleg->provinsi)
        ->first();
    $html = view('pdf.peryataan')
        ->with('dataCaleg',$dataCaleg)
        ->with('kelurahan_nama',$kel->desaNama)
        ->with('kecamatan_nama',$kec->geo_kec_nama)
        ->with('kabupaten_nama',$kab->kabupatenNama)
        ->with('provinsi_nama',$prov->provinsiNama)
        ->render();
    return $this->pdf
            ->load($html)
            ->show();
  }
  public function PrintDRH($id)
  {
    $dataCaleg = DB::table('caleg_drh')
        ->where('id','=',$id)
        ->first();
    $kel = DB::table('ref_desa')
        ->where('desaid','=',$dataCaleg->kelurahan)
        ->first();
    $kec = DB::table('m_geo_kec_kpu')
        ->where('geo_kec_id','=',$dataCaleg->kecamatan)
        ->first();
    $kab = DB::table('ref_kabupaten')
        ->where('kabupatenId','=',$dataCaleg->kabupaten)
        ->first();
    $prov = DB::table('ref_provinsi')
        ->where('provinsiId','=',$dataCaleg->provinsi)
        ->first();
    $pendidikan = DB::table('caleg_pendidikan')
        ->where('caleg_drh_id','=',$dataCaleg->id)
        ->get();
    $organisasi = DB::table('caleg_organisasi')
        ->where('caleg_drh_id','=',$dataCaleg->id)
        ->get();
    $html = view('pdf.drh')
        ->with('dataCaleg',$dataCaleg)
        ->with('kelurahan_nama',$kel->desaNama)
        ->with('kecamatan_nama',$kec->geo_kec_nama)
        ->with('kabupaten_nama',$kab->kabupatenNama)
        ->with('provinsi_nama',$prov->provinsiNama)
        ->with('pendidikan',$pendidikan)
        ->with('organisasi',$organisasi)
        ->render();
    return $this->pdf
            ->load($html)
            ->show();
  }
}
