<?php namespace App\Http\Controllers;

use DB;
use Input;
use File;
use Request;
use Session;

class MasterController extends Controller{
	public function master_kategori(){
		Session::forget('menu');
		Session::set('menu', 'master-kategori');
		$getKategori = DB::table('m_kategori_logistik')->paginate(10);
		return view('master.kategori', array(
			"dataKategori" => $getKategori
		));
	}
	public function add_kategori(){
		$kategori = Input::get('kategori');

		$count = DB::table('m_kategori_logistik')
					->where('kategori_name', $kategori)
					->count();
		if($count == 1){
			echo "fail";
		}else{
			DB::table('m_kategori_logistik')->insert(['kategori_name' => $kategori]);
			echo "success";
		}
	}

	public function edit_kategori(){
		$id = Input::get('id');
		$kategori = Input::get('kategori');

		$count = DB::table('m_kategori_logistik')
					->where('kategori_name', $kategori)
					->count();
		if($count == 1){
			echo "fail";
		}else{
			DB::table('m_kategori_logistik')->where('id', $id)->update(['kategori_name' => $kategori]);
			echo "success";
		}
	}

	public function delete_kategori(){
		$id = Input::get('id');
		DB::table('m_kategori_logistik')->where('id', $id)->delete();
	}
}
?>