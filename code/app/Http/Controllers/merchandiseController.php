<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use DB;
use Illuminate\Http\Request;
use Input;
use Session;
use Redirect;

class merchandiseController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//create
		$cek = Session::get('username');
		if($cek != ""){
			Session::forget('menu');
	      	Session::set('menu', 'merchandise');

	      	$cek = Session::get('username');

			$users = DB::table('tb_oleh')
				->where('akes', Session::get('idcaleg'))
				->orderBy('id_oleh', 'asc')
	      		->get();
			return view('merchandise/index', ['users' => $users]);
		}else{
			return redirect('/');	
		}
	}

	public function frm_tambah()
	{
		$cek = Session::get('username');
		if($cek != ""){
			return view('merchandise/tambah');
		}else{
			return redirect('/');	
		}
	}

	public function frm_kirim()
	{
		$cek = Session::get('username');
		if($cek != ""){
			return view('merchandise/kirim');
		}else{
			return redirect('/');	
		}
	}

	public function frm_edit(Request $request, $key)
	{
		$cek = Session::get('username');
		if($cek != ""){
			$id = $request->input('id_oleh');
			return view('merchandise/edit', array('id_oleh'=>$key));
		}else{
			return redirect('/');	
		}
	}			


	public function view_item(Request $request)
    {
        $id = Input::get('id_oleh');
        return view('merchandise/lihat', array('id_oleh'=>$id));
    }	

    public function view_item1(Request $request)
    {
        $id_de = Input::get('id_de');
        return view('merchandise/lihat_det', array('id_de'=>$id_de));
    }

    public function view_item2(Request $request)
    {
        $id_de_trim = Input::get('id_de_trim');
        return view('merchandise/lihat_det_trim', array('id_de_trim'=>$id_de_trim));
    }

    public function delete_item(Request $request)
    {
        $id_oleh = $request->input('id_oleh');
        DB::table('tb_oleh')->where('id_oleh', $id_oleh)->delete();
    }	    		

	public function update_data(Request $request, $key)
	{
        return view('agenda/edit_agenda', array('id'=>$key));
	}

	public function tambah_oleh(Request $request)
	{


		$cek = Session::get('idcaleg');
		$nama = $request->input('nama');
		$stok = $request->input('stok');
		$description = $request->input('description');
		$image_link = $request->input('image_link');
		$lol = url().'/assets/images/agenda/'.$image_link;

		DB::table('tb_oleh')->insert(
            [
                'nama_oleh'   	    => $nama,
                'stok'       	    => $stok,
                'keterangan'  		=> $description,
                'image_link'		=> $lol,
                'akes'				=> $cek,       

            ] 
        );

					
	}

	public function updates(Request $request)
	{
		$nama = $request->input('nama');
		$stok = $request->input('stok');
		$description = $request->input('description');
		$image_link = $request->input('image_link');
		$lol = url().'/assets/images/agenda/'.$image_link;
		$id_oleh = $request->input('id_oleh');
		$creat = Session::get('username');


		if($image_link==""){
			DB::table('tb_oleh')->where('id_oleh', $id_oleh)->update(
	                [
		                'nama_oleh' 	    => $nama,
		                'stok' 				=> $stok,
		                'keterangan'       	=> $description
		        
	                ] 
	            );
		}else{
			DB::table('tb_oleh')->where('id_oleh', $id_oleh)->update(
	                [
		                'nama_oleh' 		=> $nama,
		                'stok' 				=> $stok,
		                'keterangan'       	=> $description,
			            'image_link'		=> $lol
	                ] 
	            );
		}
	}


	public function save_img(Request $request)
    {
      // getting all of the post data
      $file = array('image' => Input::file('image'));
      // setting up rules
      $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
      // doing the validation, passing post data, rules and the messages
      $validator = Validator::make($file, $rules);
      if ($validator->fails()) {
        // send back to the page with the input data and errors
        return Redirect::to('./assets/images/agenda/')->withInput()->withErrors($validator);
      }else {
        // checking file is valid.
        if(Input::hasFile('image')) {
			$file 	= Input::file('image');
			$besar = $file->getSize();
			if($file->getSize() <= 2000000) {
				$destinationPath = './assets/images/agenda'; // upload path
				$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
				$fileName = rand(11111,99999).'.'.$extension; // renameing image
				Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
				// sending back with message
				echo "<script type='text/javascript'>parent.document.getElementById('gambarout').value='".$fileName."';</script>";
				Session::flash('success', 'Upload successfully'); 
				echo "<script type='text/javascript'>parent.document.getElementById('sizepic').innerHTML='".$besar." byte';</script>";
			} else {
				?><script>
					alert("File Anda Terlalu Besar Max 2 Mb");
				</script><?php
			}
		}else{
          // sending back with error message.
          Session::flash('error', 'uploaded file is not valid');
		}		
      }
    }


    public function frm_masuk(){
    		
    	$cek = Session::get('username');
		if($cek != ""){
			return view('merchandise/masuk');
		}else{
			return redirect('/');	
		}


	}


    public function jump_log2(){
    		
    	$cek = Session::get('username');
		if($cek != ""){
			return view('merchandise/log_ma');
		}else{
			return redirect('/');	
		}
    }

    public function jump_log(){
    	$cek = Session::get('username');
		if($cek != ""){
			$users = DB::table('detil_mercendise')
				->join('tb_oleh','detil_mercendise.id_oleh', '=' ,'tb_oleh.id_oleh')
				->join('m_relawan','detil_mercendise.id_relawan', '=', 'm_relawan.id_relawan')
				->join('m_geo_prov_kpu','detil_mercendise.provinsiId', '=', 'm_geo_prov_kpu.geo_prov_id')
				->join('m_geo_kab_kpu','detil_mercendise.kabupatenId', '=', 'm_geo_kab_kpu.geo_kab_id')
				->where('detil_mercendise.akses', Session::get('idcaleg'))
				->get();
			return view('merchandise/log', ['users' => $users]);
		}else{
			return redirect('/');	
		}
    }

	public function tambah_stok(Request $request){


		$cek = Session::get('username');
		$nama = $request->input('nama');
		$stok = $request->input('stok');
		$date = date('Y-m-d H:i:s');


		


		$tanggalene 	= DB::table('detil_mercendise_copy')
            				->join('tb_oleh', 'tb_oleh.id_oleh', '=', 'detil_mercendise_copy.id_oleh')
            				->select('tb_oleh.*', 'detil_mercendise_copy.*')
            				->get();		

		foreach ($tanggalene as $key) {
			$tanggal = $key->tgl_trans;
			$stok2   = $key->stok;
			$tok 	 = $key->jml_terima;
			
		}


		$akhir = $stok + $stok2;			
		$akhir2 = $tok + $stok;

		DB::table('tb_oleh')->where('id_oleh', $nama)->where('akes', $cek)->update(
            [
                
                'stok'       	    => $akhir,
            	   

            ] 
        );

		if ($tanggal == $date) {

			DB::table('tb_oleh')->where('id_oleh', $nama)->where('akses', $cek)->where('tgl_trans', $tanggal)->update(
            [
                
                'jml_terima'       	    => $akhir,
            	   

            ] 
        );

		}else{

		DB::table('detil_mercendise_copy')
			->insert(
            [
                
                'id_oleh'       	    => $nama,
                'akses'					=> $cek,
                'tgl_trans'				=> $date,
                'jml_terima'			=> $stok,
            ] 
        );
		}			
	}	

	public function barange(Request $request){
		 $id_oleh = $request->input('id_oleh');


         $dataZero = DB::table('tb_oleh')
              ->where('id_oleh', $id_oleh)
              ->where('akes',  Session::get('username'))
              ->get();
         
         foreach ($dataZero	as $key) {
         	$ea = $key->stok;
         	$idbar = $key->id_oleh;
         }
         
         return view('merchandise/get_barang', array('idbar'=>$ea));
	}

	public function kirim_brg(Request $request){

		$cek = Session::get('username');
		$nama = $request->input('nama');
		$kirim = $request->input('kirim');
		$relawan = $request->input('relawan');
		$provinsi = $request->input('provinsi');
		$kota = $request->input('kota');
		$kecamatan = $request->input('kecamatan');
		$lokasi = $request->input('lokasi');
		$date = date('Y-m-d H:i:s');

		$stokValue = DB::table('tb_oleh')
					->select('stok')
					->where('id_oleh', $nama)
					->where('akes', $cek)					
					->get();

		$users = DB::table('detil_mercendise')
				->join('tb_oleh','detil_mercendise.id_oleh', '=' ,'tb_oleh.id_oleh')
				->join('m_relawan','detil_mercendise.id_relawan', '=', 'm_relawan.id_relawan')
				->join('m_geo_prov','detil_mercendise.provinsiId', '=', 'm_geo_prov.geo_prov_id')
				->join('m_geo_kab','detil_mercendise.kabupatenId', '=', 'm_geo_kab.geo_kab_id')
				->where('detil_mercendise.akses',$cek)
				->get();

	
		foreach ($users as $yos) {
			
			$currentdate = $yos->tgl_trans;

		}

		foreach ($stokValue as $tmp) {
			$stok2 = $tmp->stok;
		}			
		if ($stok2 == 0) {

			return view('merchandise/log', ['users' => $users])->with('errorMessageDuration', 'Stok Habis');
		
		}elseif ($stok2 < $kirim) {
			
			return view('merchandise/log', ['users' => $users])->with('errorMessageDuration', 'Stok Melebihi Batas');

		}else{

		$akhir = $stok2-$kirim;

		DB::table('detil_mercendise')->insert(
            [
                
                'id_oleh'       	    => $nama,
                'jml_kirim'				=> $kirim,
                'id_relawan'			=> $relawan,
                'provinsiId'			=> $provinsi,
                'kabupatenId'			=> $kota,
                'kecamatanId'			=> $kecamatan,
                'lokasi'				=> $lokasi,
            	'tgl_trans'				=> $date,
            	'akses'					=> $cek,   

            ] 
        );
    

		DB::table('tb_oleh')->where('id_oleh', $nama)->where('akes', $cek)->update(
            [
                
                'stok'       	    => $akhir,
            	   

            ] 
        );
    
}
					
	}	

}
