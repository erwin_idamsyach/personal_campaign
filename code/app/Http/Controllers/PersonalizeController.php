<?php namespace App\Http\Controllers;
use DB;
use Input;
use Session;
use File;

class PersonalizeController extends Controller{

	public function index_bg(){
		Session::forget('menu');
		Session::set('menu', 'personal_bg');

		$getPersonalActFor = DB::table("tb_personal_act_for")->get();
		foreach($getPersonalActFor as $dat){
			$id = $dat->activated_for;
		}

		$getBackground = DB::table('tb_background')->paginate(5);
		$bgActive      = DB::table('tb_background')->where('status', 'ACTIVE')->get();
		$bgCount      = DB::table('tb_background')->where('status', 'ACTIVE')->count();

		$getBackground->setPath(url().'/admin/personalize/background');

		return view('personalize.background', array(
				"data_count"			 => $bgCount,
				"data_background_active" => $bgActive,
				"dataBackground" 		 => $getBackground,
				"id_user" 		 		 => $id
			));
	}

	public function update_bg($id){
		/*echo asset('assets/images/background');*/
		$f_gambar = Input::file('gambar');
		$date = date("Y-m-d H:i:s");
		if(Input::hasFile('gambar')){
			/*$ext = Input::file('filedaftarRiwayatHidup')->getClientOriginalExtension();*/
			$ext = File::extension($f_gambar->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
			$new_name = strtotime(date('Y-m-d H:i:s'))."_".$id.".".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
			$f_gambar->move('assets/images/background', $new_name); /* MEMINDAH FILE */
			DB::table('tb_background')
				->insert([
					"background" => $new_name,
					"status"     => "PASSIVE",
					"create_date"=> $date
					]);
		}
		return redirect('admin/personalize/background');
	}

	public function toggle_activation(){
		$id = Input::get('id');
		$state = Input::get('stat');
		if($state == 0){
			DB::table('tb_background')->where('id', $id)->update(["status" => "PASSIVE"]);
		}else{
			DB::table('tb_background')->where('status', 'ACTIVE')->update(['status' => "PASSIVE"]);
			DB::table('tb_background')->where('id', $id)->update(['status' => "ACTIVE"]);
		}
	}

	public function delete_bg(){
		$id = Input::get('id');
		DB::table('tb_background')->where('id', $id)->delete();
	}

	public function index_motto(){
		Session::forget('menu');
		Session::set('menu', 'personal_motto');

		$getPersonalActFor = DB::table("tb_personal_act_for")->get();
		foreach($getPersonalActFor as $dat){
			$id = $dat->activated_for;
		}

		$getMoto = DB::table('tb_moto')->where('akses', $id)->get();
		return view('personalize.motto', array(
					"dataMotto" => $getMoto,
					"id_user"   => $id
				));
	}

	public function update_motto($id){
		/*echo asset('assets/images/background');*/
		$judul = Input::get('judul');
		if($judul != ""){
			$update = DB::table('tb_moto')->where('akses', $id)->update(["judul" => $judul]);
		}

		$desk = Input::get('desk');
		if($desk != ""){
			$update = DB::table('tb_moto')->where('akses', $id)->update(["deskripsi" => $desk]);
		}
		return redirect('admin/personalize/motto');
	}
}

?>