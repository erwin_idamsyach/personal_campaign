<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use DB;
use Input;
use Validator;
use Redirect;
use Cornford\Googlmapper\Mapper;

class DashboardController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	/*public function __construct()
	{
		$this->middleware('guest');
	}*/

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cek = Session::get('username');
		Session::forget('menu');
      	Session::set('menu', 'dashboard');

      	$caleg = DB::table('caleg_drh')
	      	->join('m_geo_prov_kpu', 'm_geo_prov_kpu.geo_prov_id', '=', 'caleg_drh.tingkat_provinsi')
	      	->join('m_geo_kab_kpu', 'm_geo_kab_kpu.geo_kab_id', '=', 'caleg_drh.tingkat_kabupaten')
	      	->where('id', Session::get('idcaleg'))
	      	->count();

	    $kabupaten = DB::table('m_geo_kab_kpu')
	    			->where('geo_kab_id',50331)
	    			->where('geo_kab_id', 50532)
	    			->where('geo_kab_id', 50735)
	    			->where('geo_kab_id', 51095)
	    			->get();

	    /*$responseView = 'menu.dashboard';
		$responseData = ;*/
		return view("dashboard.index");
	}

	public function index_dash(){
		if(Session::get('username') == ""){
			return redirect('login');
		}else{
			Session::set('menu', 'dashboard');
			$getRelawanAktif = DB::table('m_relawan')
						->where('diterima', 'YA')
						->where('akses', session('idcaleg'))
						->count();
			
			$getRelawanPasif = DB::table('m_relawan')
						->where('diterima', 'BELUM')
						->where('akses', session('idcaleg'))
						->count();

			$getLogistik = DB::table('tb_oleh')
						->where('akes', session('idcaleg'))
						->get();
			$kabupaten = DB::table('m_geo_kab_kpu')
	    			->where('geo_kab_id','50031')
	    			->orWhere('geo_kab_id', '50331')
	    			->orWhere('geo_kab_id', '50532')
	    			->orWhere('geo_kab_id', '50735')
	    			->get();
	    	$kecamatan = DB::table('m_geo_kec_kpu')
	    			->where('geo_kab_id','50031')
	    			->orWhere('geo_kab_id', '50331')
	    			->orWhere('geo_kab_id', '50532')
	    			->orWhere('geo_kab_id', '50735')
	    			->get();
	    	$dapil = DB::table('dapil_dpr_ri')->get();
			return view('dashboard.index', array(
						"kabupaten" => $kabupaten,
						"kecamatan" => $kecamatan,
						"dataDapil" => $dapil
						));
		}
	}

	public function viewDashboardWoy($jenis,$type){
		$data = [];
		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$date1 = date('Y-m-d');


		$dataMahasiswa = DB::table('tb_alumni')
					->count();
		$dataMLulus = DB::table('tb_alumni')
					->where('tahun_tamat', '!=', 0)
					->count();

		if($type == 'user'){
			$return = 'dashboard.grafik';

			$dataGrafik = [['Mahasiswa',@$dataMahasiswa],['Lulus',@$dataMLulus]];
		}

				$dataSeries = ",series: [{
					name: '".@$dataGrafik[0][0]."',
					color: '#000000',
					data: [".@$dataGrafik[0][1]."]
				}, {
					name: '".@$dataGrafik[2][0]."',
					color: '#f39c12',
					data: [".@$dataGrafik[2][1]."]
				}]";		


		$data['dataSeries'] = $dataSeries;
		
		$data['type'] = $type;
		$data['jenis'] = $jenis;
		return view($return,$data);	
	}

	public function viewDashboardGrafik(){
		$dates = "''";
		$datakursiall = "";
		$datakursialls = "";
		$datakursiallbelum = "";
		$totalall = "";
		$data_area = "";
		$data_series = ", series: [";
		$label = ",
		dataLabels: {
			enabled: true,
			rotation: -90,
			color: '#000000',
			align: 'right',
			format: '{point.y:f}', // one decimal
			y: -20, // 10 pixels down from the top
			style: {
				fontSize: '10px'
			}
		}";

				$day = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
				$dayInd = ['Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
				$week = date("W");	

				for($a = 0; $a <count($day); $a++){
					$month_name = date("D", strtotime("Sunday +{$a} days"));
					$dataanggota = DB::table('tb_alumni')
						->whereMonth('create_date', '=',$a)
							->get();
					$dates = $dates.',"'.$month_name.'"';
				}


					$dates = substr($dates, 3);
					
					$jenis = [['Android', '#3f8700'],['Web','#00a65a']];
					for($b=0; $b < count($jenis); $b++){
						$data_series = $data_series."{
							name: '".$jenis[$b][0]."',
							color: '".$jenis[$b][1]."',
							data: [";
						
						if($jenis[$b][0] == 'Android'){
								$dataAll = [];
								for($a = 0; $a<count($day); $a++){
									$dataKab = DB::table('tb_alumni')
											->where('create_by','!=', Session::get('username'))
											->whereRaw('DAYNAME(create_date) = "'.$day[$a].'" AND WEEK(create_date) = WEEK(now())')->groupBy('no_id_alumni')
												->count();
									array_push($dataAll,$dataKab);
								}
								$jumlah_kursi = join($dataAll,',');

								$datakursiall = $jumlah_kursi;
								$data_series = $data_series.$datakursiall."]}, ";		
						} else if($jenis[$b][0] == 'Web'){
								$dataAll = [];
								for($a = 0; $a<count($day); $a++){
									$dataKab = DB::table('tb_alumni')
											->where('create_by', Session::get('username'))
											->whereRaw('DAYNAME(create_date) = "'.$day[$a].'" AND WEEK(create_date) = WEEK(now())')->groupBy('no_id_alumni')
												->count();
									array_push($dataAll,$dataKab);
								}
								$jumlah_kursi = join($dataAll,',');

								$datakursiall = $jumlah_kursi;	
								$data_series = $data_series.$datakursiall."]} ";	
						} 
					}
				 
		return view('dashboard/grafik', array(
			'dates' => $dates,			
			'data_series' => $data_series."]"
		));
	}

	public function viewDashboardGrafik2(){
		$dates = "''";
		$datakursiall = "";
		$datakursialls = "";
		$datakursiallbelum = "";
		$totalall = "";
		$data_area = "";
		$data_series = ", series: [";
		$label = ",
		dataLabels: {
			enabled: true,
			rotation: -90,
			color: '#000000',
			align: 'right',
			format: '{point.y:f}', // one decimal
			y: -20, // 10 pixels down from the top
			style: {
				fontSize: '10px'
			}
		}";
		
				$day = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
				$dayInd = ['Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
				$week = date("W");	

				for($a = 0; $a <count($day); $a++){
					$month_name = date("D", strtotime("Sunday +{$a} days"));
					$dates = $dates.',"'.$month_name.'"';
				}

					$dates = substr($dates, 3);
					
					$jenis = [['Alumni', '#3f8700'],['Mahasiswa','#00a65a']];
					for($b=0; $b < count($jenis); $b++){
						$data_series = $data_series."{
							name: '".$jenis[$b][0]."',
							color: '".$jenis[$b][1]."',
							data: [";
						
						if($jenis[$b][0] == 'Alumni'){
								$dataAll = [];
								for($a = 0; $a<count($day); $a++){
									$dataKab = DB::table('tb_alumni')
											->where('status_user', 'Alumni')
											->whereRaw('DAYNAME(create_date) = "'.$day[$a].'" AND WEEK(create_date) = WEEK(now())')->groupBy('no_id_alumni')
												->count();
									array_push($dataAll,$dataKab);
								}
								$jumlah_kursi = join($dataAll,',');

								$datakursiall = $jumlah_kursi;
								$data_series = $data_series.$datakursiall."]}, ";		
						} else if($jenis[$b][0] == 'Mahasiswa'){
								$dataAll = [];
								for($a = 0; $a<count($day); $a++){
									$dataKab = DB::table('tb_alumni')
											->where('status_user', 'Mahasiswa')
											->whereRaw('DAYNAME(create_date) = "'.$day[$a].'" AND WEEK(create_date) = WEEK(now())')->groupBy('no_id_alumni')
												->count();
									array_push($dataAll,$dataKab);
								}
								$jumlah_kursi = join($dataAll,',');

								$datakursiall = $jumlah_kursi;
								$data_series = $data_series.$datakursiall."]} ";	
						} 
					}
				 
		return view('dashboard/grafik2', array(
			'dates' => $dates,			
			'data_series' => $data_series."]"
		));
	}	

	public function viewDashboardGrafik3(){
		$dates = "''";
		$datakursiall = "";
		$datakursialls = "";
		$datakursiallbelum = "";
		$totalall = "";
		$data_area = "";
		$data_series = ", series: [";
		$label = ",
		dataLabels: {
			enabled: true,
			rotation: -90,
			color: '#000000',
			align: 'right',
			format: '{point.y:f}', // one decimal
			y: -20, // 10 pixels down from the top
			style: {
				fontSize: '10px'
			}
		}";
				$day = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
				$dayInd = ['Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
				$week = date("W");
		
				for($a = 0; $a <count($day); $a++){
					// $month_name = date("F", mktime(0, 0, 0, $a, 10));
					$month_name = date("D", strtotime("Sunday +{$a} days"));
					$dates = $dates.',"'.$month_name.'"';
				}


					$dates = substr($dates, 3);
					
					$jenis = [['Submit', '#3f8700'],['Approve','#00a65a']];
					for($b=0; $b < count($jenis); $b++){
						$data_series = $data_series."{
							name: '".$jenis[$b][0]."',
							color: '".$jenis[$b][1]."',
							data: [";
						
						if($jenis[$b][0] == 'S
							ubmit'){
								$dataAll = [];
								for($a = 0; $a<count($day); $a++){
									$dataKab = DB::table('tb_bisnis')
											->whereRaw('DAYNAME(create_date) = "'.$day[$a].'" AND WEEK(create_date) = WEEK(now())')->groupBy('no_id_alumni')
												->count();
									array_push($dataAll,$dataKab);
								}
								$jumlah_kursi = join($dataAll,',');

								$datakursiall = $jumlah_kursi;
								$data_series = $data_series.$datakursiall."]}, ";		
						} else if($jenis[$b][0] == 'Approve'){
								$dataAll = [];
								for($a = 0; $a<count($day); $a++){
									$dataKab = DB::table('tb_bisnis')
											->where('approve', 'Okey')
											->whereRaw('DAYNAME(create_date) = "'.$day[$a].'" AND WEEK(create_date) = WEEK(now())')->groupBy('no_id_alumni')
												->count();
									array_push($dataAll,$dataKab);
								}
								$jumlah_kursi = join($dataAll,',');

								$datakursiall = $jumlah_kursi;
								$data_series = $data_series.$datakursiall."]} ";	
						} 
					}
				 
		return view('dashboard/grafik3', array(
			'dates' => $dates,			
			'data_series' => $data_series."]"
		));
	}	

	public function viewDashboardGrafik4(){
		$dates = "''";
		$datakursiall = "";
		$datakursialls = "";
		$datakursiallbelum = "";
		$totalall = "";
		$data_area = "";
		$data_series = ", series: [";
		$label = ",
		dataLabels: {
			enabled: true,
			rotation: -90,
			color: '#000000',
			align: 'right',
			format: '{point.y:f}', // one decimal
			y: -20, // 10 pixels down from the top
			style: {
				fontSize: '10px'
			}
		}";
		
				$day = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
				$dayInd = ['Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
				$week = date("W");

				$dayCount = array('');
/*				$daftar = [];
				for($i=0; $i < count($day); $i++){
					$login[$i] = DB::table('tb_login_alumni')->where('status_login','actived')->whereRaw('DAYNAME(date_login) = "'.$day[$i].'" AND WEEK(date_login) = WEEK(now())')->groupBy('no_id_alumni')->count();
					$daftar[$i] = DB::table('tb_alumni')->whereRaw('DAYNAME(create_date) = "'.$day[$i].'" AND WEEK(create_date) = WEEK(now())')->groupBy('no_id_alumni')->count();
				}	
				$new = array_push($dayCount,$dayInd,$login,$daftar);*/

				for($a = 0; $a <  count($day); $a++){
					$month_name = date("D", strtotime("Sunday +{$a} days"));
					$dates = $dates.',"'.$month_name.'"';
				}


					$dates = substr($dates, 3);
					
					$jenis = [['Login', '#3f8700'],['Daftar','#00a65a']];
					for($b=0; $b < count($jenis); $b++){
						$data_series = $data_series."{
							name: '".$jenis[$b][0]."',
							color: '".$jenis[$b][1]."',
							data: [";
						
						if($jenis[$b][0] == 'Login'){
								$dataAll = [];
								for($a = 0; $a < count($day); $a++){
									$dataKab = DB::table('tb_login_alumni')->where('status_login','actived')->whereRaw('DAYNAME(date_login) = "'.$day[$a].'" AND WEEK(date_login) = WEEK(now())')->groupBy('no_id_alumni')->count();
									array_push($dataAll,$dataKab);
								}
								$jumlah_kursi = join($dataAll,',');

								$datakursiall = $jumlah_kursi;
								$data_series = $data_series.$datakursiall."]}, ";		
						} else if($jenis[$b][0] == 'Daftar'){
								$dataAll = [];
								for($a = 0; $a < count($day); $a++){
									$dataKab = DB::table('tb_alumni')->whereRaw('DAYNAME(create_date) = "'.$day[$a].'" AND WEEK(create_date) = WEEK(now())')->groupBy('no_id_alumni')->count();
									array_push($dataAll,$dataKab);
								}
								$jumlah_kursi = join($dataAll,',');

								$datakursiall = $jumlah_kursi;
								$data_series = $data_series.$datakursiall."]} ";	
						} 
					}
				 
		return view('dashboard/grafik4', array(
			'dates' => $dates,			
			'data_series' => $data_series."]"
		));
	}		

	public function viewGrafikDashboard($jenis,$type){
		$data = [];
		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$date1 = date('Y-m-d');

		$dataMahasiswa = DB::table('tb_alumni')
					->count();
		$dataMLulus = DB::table('tb_alumni')
					->where('tahun_tamat', '!=', 0)
					->count();
		$dataMKerja = DB::table('tb_karir_alumni')
					->where('pekerjaan_sekarang', '!=', '')
					->count();

		$dataUser 	= DB::table('tb_login_alumni')
					->count();

		$dataUAktif	= DB::table('tb_login_alumni')
					->where('status_login', 'actived')
					->count();

		$dataUAktif	= DB::table('tb_login_alumni')
					->where('verifikasi', 'yes')
					->count();

		$dataBisnis	= DB::table('tb_bisnis')
					->count();

		$dataBMember= DB::table('tb_bisnis')
					->where('no_id_alumni', '=!', '')
					->count();

		$dataBProve= DB::table('tb_bisnis')
					->where('approve', 'Okey')
					->count();			

		$dataAgenda		= DB::table('tb_agenda')
						->count();			

		$dataGexpired	= DB::table('tb_agenda')
						->where('date_end' , '>',$date1 )
						->count();							

		$dataGexpiredx	= DB::table('tb_agenda')
						->where('date_end' , '<=',$date1 )
						->count();							


		if($type == 'mahasiswa'){
			$return = 'dashboard.grafiktu';
			$dataGrafik = [['Mahasiswa',@$dataMahasiswa],['Lulus',@$dataMLulus],['Bekerja',@$dataMKerja]];
		} else if($type == 'user'){
			$return = 'dashboard.grafiktu';					
			$dataGrafik = [['User',@$dataUser],['Aktif',@$dataUAktif],['Terverifikasi',@$dataUAktif]];
		} else if($type == 'bisnis'){
			$return = 'dashboard.grafiktu';					
			$dataGrafik = [['Bisnis',@$dataBisnis],['Member',@$dataUAktif],['Approve',@$dataUAktif]];
		} else if($type == 'agenda'){
			$return = 'dashboard.grafiktu';					
			$dataGrafik = [['Agenda',@$dataAgenda],['Expired',@$dataGexpired],['Aktif',@$dataGexpiredx]];
		}

			$dataSeries = ",series: [{
					name: '".@$dataGrafik[0][0]."',
					color: '#000000',
					data: [".@$dataGrafik[0][1]."]
				}, {
					name: '".@$dataGrafik[2][0]."',
					color: '#f39c12',
					data: [".@$dataGrafik[2][1]."]
				}, {
					name: '".@$dataGrafik[1][0]."',
					color: '#ff0000',
					data: [".@$dataGrafik[1][1]."]
				}]";	

		$data['dataSeries'] = $dataSeries;
		
		$data['type'] = $type;
		$data['jenis'] = $jenis;
		return view($return,$data);	
	}
	


}
