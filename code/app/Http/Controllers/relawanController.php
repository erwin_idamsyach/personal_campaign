<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Input;
use Session;
use File;
use Redirect;
use Validator;

class relawanController extends Controller {

	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//create
		$cek = Session::get('username');
		if ($cek != "") {
			Session::forget('menu');
			Session::set('menu', 'relawan');

			$cek = Session::get('username');

			$users = DB::table('m_relawan')->get();
			$tgl = DB::table('m_tanggal')->get();
			$bln = DB::table('m_bulan')->get();
			$thn = DB::table('m_tahun')->get();
			return view('relawan/index', array(
				'users' => $users,
				"tanggal"		 => $tgl,
				"bulan"			 => $bln,
				"tahun"			 => $thn,
			));
		} else {
			return redirect('/');	
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function viewcek(Request $request)
	{
		$namespace = Input::get('nama');
		$dataRelawan = DB::table('m_relawan')
		->where('nama_relawan', $nama)
		->get();
		foreach ($dataRelawan as $lol) {
		}
		$dataAgamaNow = DB::table('ref_agama')
		->orderBy('agama_label', 'asc')
		->where('agama_id', $lol->agama)
		->get();
		$responseView = 'relawan.view';
		$responseData = array(
			'nama' => $id,
			'dataAgamaNow' => $dataAgamaNow
		);
		return view($responseView,$responseData);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function frm_tambah()
	{
		$cek = Session::get('username');
		if($cek != ""){
			return view('merchandise/tambah');
		}else{
			return redirect('/');	
		}
	}

	public function frm_edit(Request $request, $key)
	{
		$cek = Session::get('username');
		if ($cek != "") {
			$dataRelawan = DB::table('m_relawan')
			->where('id_relawan', $key)
			->get();
			foreach ($dataRelawan as $lol) {
			}
			$dataAgama = DB::table('ref_agama')
			->orderBy('agama_label', 'asc')
			->get();
			$dataAgamaNow = DB::table('ref_agama')
			->orderBy('agama_label', 'asc')
			->where('agama_id', $lol->agama)
			->get();
			$dataAgamaNotNow = DB::table('ref_agama')
			->orderBy('agama_label', 'asc')
			->where('agama_id', '!=' , $lol->agama)
			->get();					
			$responseView = 'relawan.edit';
			$responseData = array(
				'id_oleh' 			=> $key,
				'dataAgama' 		=> $dataAgama,
				'dataAgamaNow' 		=> $dataAgamaNow,
				'dataAgamaNotNow' 	=> $dataAgamaNotNow
			);
			return view($responseView,$responseData);
		} else {
			return redirect('/');	
		}
	}			


	public function frm_kin()
	{
		$cek = Session::get('username');
		if ($cek != "") {
			$getVolunteer = DB::table('m_relawan')
			->where('diterima', 'YA')
			->get();
			return view('relawan/tambah_kin', array(
				"dataRelawan" => $getVolunteer
			));
		} else {
			return redirect('/');	
		}
	}			

	public function view_item(Request $request)
	{
		$id = Input::get('id_relawan');
		$dataRelawan = DB::table('m_relawan')
		->where('id_relawan', $id)
		->get();
		foreach ($dataRelawan as $lol) {
		}
		$dataAgamaNow = DB::table('ref_agama')
		->orderBy('agama_label', 'asc')
		->where('agama_id', $lol->agama)
		->get();
		$responseView = 'relawan.lihat';
		$responseData = array(
			'id_relawan' 	=> $id,
			'dataAgamaNow' 	=> $dataAgamaNow
		);
		return view($responseView,$responseData);
	}	

	public function delete_item(Request $request)
	{
		$id_oleh = $request->input('id_relawan');
		DB::table('m_relawan')
		->where('id_relawan', $id_oleh)
		->update('STATUS', '=', 'TIDAK');
	}	    		

	public function tambah_oleh(Request $request)
	{
		$cek = Session::get('username');
		$caleg = Session::get('idcaleg');
		$akses = $request->input('akses');
		$nama = $request->input('nama');
		$nomer = $request->input('nomer');
		$agama = $request->input('agama');
		$alamat = $request->input('alamat');
		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$date1 = date('Y-m-d');

		DB::table('m_relawan')->insert(
			[
				'nama'   	    		=> $nama,
				'no_hp'       	    	=> $nomer,
				'alamat'  				=> $alamat,
				'agama'					=> $agama,
				'create_date'			=> $date,       
				'akses' 				=> $akses,
				'diterima' 				=> "BELUM"
			] 
		);
	}

	public function updates(Request $request)
	{
		$nama = $request->input('nama');
		$alamat = $request->input('alamat');
		$no_hp = $request->input('no_hp');
		$agama = $request->input('agama');
		$id_relawan = $request->input('id_relawan');
		$creat = Session::get('username');
		$caleg = Session::get('idcaleg');

		DB::table('m_relawan')
		->where('id_relawan', $id_relawan)
		->update(
			[
				'nama'		 		=> $nama,
				'alamat' 			=> $alamat,
				'no_hp'       		=> $no_hp,
				'agama'				=> $agama,
				'akses'				=> $caleg
			] 
		);
	}


	public function save_img(Request $request)
	{
		$file = array(
			'image' => Input::file('image')
		);
		$rules = array(
			'image' => 'required',
		);
		$validator = Validator::make($file, $rules);
		if ($validator->fails()) {
			return Redirect::to('./assets/images/relawan/')->withInput()->withErrors($validator);
		} else {
			if (Input::hasFile('image')) {
				$file 	= Input::file('image');
				$besar = $file->getSize();
				if ($file->getSize() <= 2000000) {
					$destinationPath = './assets/images/relawan'; 
					$extension = Input::file('image')->getClientOriginalExtension(); 
					$fileName = rand(11111,99999).'.'.$extension; 
					Input::file('image')->move($destinationPath, $fileName); 
					echo "<script type='text/javascript'>parent.document.getElementById('gambarout').value='".$fileName."';</script>";
					Session::flash('success', 'Upload successfully'); 
					echo "<script type='text/javascript'>parent.document.getElementById('sizepic').innerHTML='".$besar." byte';</script>";
				} else {
					?>
					<script>
					alert("File Anda Terlalu Besar Max 2 Mb");
				</script>
				<?php
			}
		} else {
			Session::flash('error', 'uploaded file is not valid');
		}		
	}
}


public function ref(Request $request, $key)
{
	DB::table('m_relawan')
	->where('id_relawan', $key)
	->update(
		[
			'diterima' 				=> 'YA'
		] 
	);
	return redirect()->action('relawanController@index');
}


public function tampilKin()
{
	$cek = Session::get('username');
	if ($cek != "") {
		Session::forget('menu');
		Session::set('menu', 'kinrelawan');

		$cek = Session::get('username');
		$caleg = Session::get('idcaleg');

<<<<<<< HEAD
		$users = DB::table('tb_kinerja_rel')
		->join('m_relawan', 'm_relawan.id_relawan', '=', 'tb_kinerja_rel.id_relawan')
		->select('tb_kinerja_rel.agenda', 'tb_kinerja_rel.tujuan', 'tb_kinerja_rel.status', 'tb_kinerja_rel.id_kinerja','m_relawan.nama', 'm_relawan.id_relawan')
		->get();
		return view('relawan/kinerja_rel', ['users' => $users]);
	} else {
		return redirect('/');	
=======
			$users = DB::table('m_relawan')
					->where('diterima', 'YA')
					->count();
			$getRelawan = DB::table('m_relawan')
					->select('m_relawan.*','geo_prov_nama', 'geo_kab_nama', 'geo_kec_nama', 'geo_deskel_nama')
					->leftJoin('ref_penugasan', 'm_relawan.id_relawan', '=', 'ref_penugasan.id_relawan')
					->leftJoin('m_geo_prov_kpu', 'ref_penugasan.provinsi', '=', 'm_geo_prov_kpu.geo_prov_id')
					->leftJoin('m_geo_kab_kpu', 'ref_penugasan.kabupaten', '=', 'm_geo_kab_kpu.geo_kab_id')
					->leftJoin('m_geo_kec_kpu', 'ref_penugasan.kecamatan', '=', 'm_geo_kec_kpu.geo_kec_id')
					->leftJoin('m_geo_deskel_kpu', 'ref_penugasan.kelurahan', '=', 'm_geo_deskel_kpu.geo_deskel_id')
					->where('diterima', 'YA')
					->groupBy('ref_penugasan.id_relawan')
					->get();
			$dataProvinsi = DB::table('m_geo_prov_kpu')->get();
			$dataKoord	  = DB::table('ref_penugasan')
							->select('m_relawan.id_relawan', 'm_relawan.nama')
							->join('m_relawan', 'ref_penugasan.id_relawan', '=', 'm_relawan.id_relawan')
							->where('role', "KOORDINATOR")
							->get();
			return view('relawan/kinerja_rel', array(
						"users" => $users,
						"dataRelawan" => $getRelawan,
						"dataProvinsi" => $dataProvinsi,
						"dataKoord"	  => $dataKoord
					));
		} else {
			return redirect('/');	
		}
>>>>>>> a7fca58214d10f862626aba16b19af137a07b1c5
	}
}


public function view_item2(Request $request)
{
	$id_kinerja = Input::get('id_kinerja');
	return view('relawan/lihat_kinerja', array(
		'id_kinerja'=> $id_kinerja
	));
}


public function delete_item2(Request $request)
{
	$id_kinerja = $request->input('id_kinerja');
	DB::table('tb_kinerja_rel')
	->where('id_kinerja', $id_kinerja)
	->delete();
}

public function tambah_kin(Request $request)
{
	$cek = Session::get('username');
	$agenda = $request->input('agenda');
	$nama = $request->input('nama');
	$image = $request->input('image');
	$tujuan = $request->input('tujuan');
	$link = url().'/assets/images/relawan/'.$image;
	$date = date('Y-m-d H:i:s');

	$users = DB::table('tb_kinerja_rel')
	->join('m_relawan', 'tb_kinerja_rel.id_relawan', '=', 'm_relawan.id_relawan')
	->select('tb_kinerja_rel.*', 'm_relawan.nama','m_relawan.id_relawan')
	->where('m_relawan.akses', $cek)
	->where('tb_kinerja_rel.akses', $cek)
	->get();

	DB::table('tb_kinerja_rel')
	->insert(
		[
			'agenda'				=> $agenda,
			'image'					=> $link,
			'id_relawan'			=> $nama,
			'tujuan'				=> $tujuan,
			'akses'					=> $cek
		]
	);
}

public function pendaftaran(){
	$nama 	 = Input::get('nama');
	$no_hp 	 = Input::get('no_hp');
	$email 	 = Input::get('email');
	$alamat  = Input::get('alamat');
	$prov 	 = Input::get('prov');
	$kota 	 = Input::get('kota');
	$kec 	 = Input::get('kec');
	$kel 	 = Input::get('kel');
	$lat 	 = Input::get('lat');
	$long 	 = Input::get('long');

	$cekData = DB::table('m_relawan')
	->where('nama', $nama)
	->where('no_hp', $no_hp)
	->count();
	if($cekData == 1){

	}else{
		$insert = DB::table('m_relawan')
		->insert([
			
		]);
	}
}

public function inputrel(Request $request)
{
	Session::forget('menu');
	Session::set('menu', 'inputrel');
	$dataProvinsi = DB::table('m_geo_prov_kpu')
	->get();
	
	return view('relawan/tambah_relawan_new',array(
		'dataProvinsi' 	=> $dataProvinsi
	));
}

public function tambahrr(Request $request)
{
	$nama_depan = $request->input('nama_depans');
	$nama_tengah = $request->input('nama_tengahs');
	$nama_belakang = $request->input('nama_belakangs');


	$fullname = $nama_depan." ".$nama_tengah." ".$nama_belakang;

	$tgl_lahir = $request->input('tgl_lahirs');
	$bln_lahir = $request->input('bln_lahirs');
	$tahun_lahir = $request->input('tahun_lahirs');

	$fullDate = $tahun_lahir."-".$bln_lahir."-".$tgl_lahir;

	$tempat_lahir = $request->input('tempat_lahirs');
	$email = $request->input('emais');
	$jk = $request->input('jenkels');
	$email = $request->input('emails');

	$provinsi = $request->input('a_provs');
	$kabupaten = $request->input('a_kotas');
	$kecamatan = $request->input('a_kecs');
	$desa = $request->input('a_desas');

	$now = date('Y-m-d H:i:s');

	$inserting = DB::table('m_relawan')
	->insert(
		[

			'nama' => $fullname,
			'email' => $email,
			'tgl_lahir' => $fullDate,
			'create_date' => $now,
			'akses'=> Session::get('idcaleg'),
			'tmp_lahir' =>$tempat_lahir,
			'jk' => $jk,
			'provinsi' => $provinsi,
			'kota' => $kabupaten,
			'kecamatan' => $kecamatan,
			'kelurahan' =>$desa,
			'email' => $email, 
			'provinsi' => $provinsi,
			'kota' => $kabupaten,
			'kecamatan' => $kecamatan,
			'kelurahan' => $desa,
		]
	);


	if ($inserting) {
		echo "<script>alert('Data Relawan di tambahkan');</script>";
	}
	else{

<<<<<<< HEAD
	echo "<script>alert('There are no fields to generate a report');</script>";
=======
		public function inputrel(Request $request)
		{
			Session::forget('menu');
			Session::set('menu', 'inputrel');
        	$dataProvinsi = DB::table('m_geo_prov_kpu')
        		->get();
        	$tgl = DB::table('m_tanggal')->get();
			$bln = DB::table('m_bulan')->get();
			$thn = DB::table('m_tahun')->get();
				return view('relawan/tambah_relawan_new',array(
					'dataProvinsi' 	=> $dataProvinsi,
					"tanggal"		 => $tgl,
					"bulan"			 => $bln,
					"tahun"			 => $thn,
				));
		}

		public function saveRelawan(){
			$foto 		= Input::file('foto');
			$n_depan    = Input::get('nama_depan');
			$n_tengah   = Input::get('nama_tengah');
			$n_belakang = Input::get('nama_belakang');

			$nama_f 	= $n_depan." ".$n_tengah." ".$n_belakang;

			$tgl_lahir  = Input::get('tgl_lahir');
			$bln_lahir  = Input::get('bln_lahir');
			$thn_lahir  = Input::get('thn_lahir');
			$tmp_lahir	= Input::get('tempat_lahir');
			$jenis_kelamin = Input::get('jenkel');
			$lahir  = $thn_lahir."-".$bln_lahir."-".$tgl_lahir;
			$a_prov = Input::get('a_prov');
			$a_kota = Input::get('a_kota');
			$a_kec  = Input::get('a_kec');
			$a_desa = Input::get('a_desa');

			$rt = Input::get('RT');
			$rw = Input::get('RW');
			$alamat = Input::get('alamat');
			$kodepos = Input::get('kodepos');

			$handphone = Input::get('handphone');
			$telephone = Input::get('telephone');
			$fax	   = Input::get('fax');
			$email 	   = Input::get('email');

			$facebook  = Input::get('facebook');
			$twitter   = Input::get('twitter');
			$instagram = Input::get('instagram');

			$insert = DB::table('m_relawan')
					->insertGetId([
						'nama' => $nama_f,
						'first_name' => $n_depan,
						'mid_name'   => $n_tengah,
						'last_name'  => $n_belakang,
						'no_hp'		 => $handphone,
						'no_telp'	 => $telephone,
						'fax'		 => $fax,
						'email'		 => $email,
						'alamat'	 => $alamat,
						'provinsi'	 => $a_prov,
						'kota'		 => $a_kota,
						'kecamatan'  => $a_kec,
						'kelurahan'	 => $a_desa,
						'rt'		 => $rt,
						'rw'		 => $rw,
						'kodepos'	 => $kodepos,
						'tempat_lahir' => $tmp_lahir,
						'tanggal_lahir' => $lahir,
						'facebook'	 => $facebook,
						'twitter'	 => $twitter,
						'instagram'	 => $instagram,
						'diterima'	 => "YA"
					]);
			if(Input::hasFile('foto')){
				$ext = File::extension($foto->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
				$new_name = strtotime(date('Y-m-d H:i:s'))."_.".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
				$foto->move('assets/images/relawan', $new_name); /* MEMINDAH FILE */
				
				DB::table('m_relawan')
					->where('id_relawan', $insert)
					->update([
						"photo" => $new_name
						]);
			}
			echo "success";

		}

		public function saveRelawanFront(){
			$foto 		= Input::file('foto');
			$n_depan    = Input::get('nama_depan');
			$n_tengah   = Input::get('nama_tengah');
			$n_belakang = Input::get('nama_belakang');

			$nama_f 	= $n_depan." ".$n_tengah." ".$n_belakang;

			$tgl_lahir  = Input::get('tgl_lahir');
			$bln_lahir  = Input::get('bln_lahir');
			$thn_lahir  = Input::get('thn_lahir');
			$tmp_lahir	= Input::get('tempat_lahir');
			$jenis_kelamin = Input::get('jenkel');
			$lahir  = $thn_lahir."-".$bln_lahir."-".$tgl_lahir;
			$a_prov = Input::get('a_prov');
			$a_kota = Input::get('a_kota');
			$a_kec  = Input::get('a_kec');
			$a_desa = Input::get('a_desa');

			$rt = Input::get('RT');
			$rw = Input::get('RW');
			$alamat = Input::get('alamat');
			$kodepos = Input::get('kodepos');

			$handphone = Input::get('handphone');
			$telephone = Input::get('telephone');
			$fax	   = Input::get('fax');
			$email 	   = Input::get('email');

			$facebook  = Input::get('facebook');
			$twitter   = Input::get('twitter');
			$instagram = Input::get('instagram');

			$insert = DB::table('m_relawan')
					->insertGetId([
						'nama' => $nama_f,
						'first_name' => $n_depan,
						'mid_name'   => $n_tengah,
						'last_name'  => $n_belakang,
						'no_hp'		 => $handphone,
						'no_telp'	 => $telephone,
						'fax'		 => $fax,
						'email'		 => $email,
						'alamat'	 => $alamat,
						'provinsi'	 => $a_prov,
						'kota'		 => $a_kota,
						'kecamatan'  => $a_kec,
						'kelurahan'	 => $a_desa,
						'rt'		 => $rt,
						'rw'		 => $rw,
						'kodepos'	 => $kodepos,
						'tempat_lahir' => $tmp_lahir,
						'tanggal_lahir' => $lahir,
						'facebook'	 => $facebook,
						'twitter'	 => $twitter,
						'instagram'	 => $instagram,
						'diterima'	 => "BELUM"
					]);
			if(Input::hasFile('foto')){
				$ext = File::extension($foto->getClientOriginalName()); /* MENGAMBIL EKSTENSI FILE */
				$new_name = strtotime(date('Y-m-d H:i:s'))."_.".$ext; /* RENAME BERDASARKAN MILITIME dan ID USER */
				$foto->move('assets/images/relawan', $new_name); /* MEMINDAH FILE */
				
				DB::table('m_relawan')
					->where('id_relawan', $insert)
					->update([
						"photo" => $new_name
						]);
			}
			echo "success";

		}

		public function add_penugasan($id){
			$arr  = array();
			$getRelawan = DB::table('m_relawan')
						->select('nama', 'no_hp', 'id_relawan')
						->where('id_relawan', $id)
						->get();
			foreach ($getRelawan as $get) {
				$arr = array(
					"nama" => $get->nama,
					"no_hp" => $get->no_hp,
					"id_relawan" => $get->id_relawan
					);
			}
			echo json_encode($arr);
		}

		public function add_tugas(){
			$id = Input::get('id_relawan');
			$role = Input::get('role');
			$koor = Input::get('koor_leader');
			$a_prov = Input::get('a_prov');
			$a_kota = Input::get('a_kota');
			$a_kec  = Input::get('a_kec');
			$a_desa = Input::get('a_desa');

			$insert = DB::table('ref_penugasan')
					->insertGetId([
						'id_relawan' => $id,
						'provinsi'	 => $a_prov,
						'kabupaten'	 => $a_kota,
						'kecamatan'	 => $a_kec,
						'kelurahan'	 => $a_desa,
						'role'		 => $role,
						'leader'	 => $koor,
						'create_date'=> date('Y-m-d H:i:s')	
					]);
		}
>>>>>>> a7fca58214d10f862626aba16b19af137a07b1c5
	}


}
}
