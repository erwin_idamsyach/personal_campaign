<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Input;
use File;
use Redirect;
use DB;
use Vsmoraes\Pdf\Pdf;

class ReportController extends Controller{
  function statistikRelawan(){
		Session::forget('menu');
		Session::set('menu', "report-relawan");
    $chartData = array();
    $data = DB::table('m_relawan')
      ->select(DB::Raw("DATE_FORMAT(create_date,'%m') bulan , count(*) total"))
      ->groupBy(DB::Raw("DATE_FORMAT(create_date,'%m')"))
      ->get();
    foreach ($data as $tmp) {
      $chartData[$tmp->bulan] = $tmp->total;
    }

    $totalRelawan = DB::table('m_relawan')
      ->select(DB::Raw("count(*) total"))
      ->first();
    $totalRelawanDiterima = DB::table('m_relawan')
      ->select(DB::Raw("count(*) total"))
      ->where("diterima","=","YA")
      ->first();
    return view('report.statistik_relawan', array(
      "chart"=>$chartData,
      "totalRelawan"=>$totalRelawan->total,
      "totalRelawanDiterima"=>$totalRelawanDiterima->total
		));
  }
  function statistikMerchandise(){
		Session::forget('menu');
		Session::set('menu', "report-merchandise");
    $chartData1 = array();
    $data1 = DB::table('detil_mercendise')
      ->select(DB::Raw("DATE_FORMAT(tgl_trans,'%m') bulan , sum(jml_kirim) total"))
      ->groupBy(DB::Raw("DATE_FORMAT(tgl_trans,'%m')"))
      ->get();
    foreach ($data1 as $tmp) {
      $chartData1[$tmp->bulan] = $tmp->total;
    }
    $chartData2 = array();
    $data2 = DB::table('detil_mercendise')
      ->select(DB::Raw("DATE_FORMAT(tgl_trans,'%m') bulan , sum(jml_terima) total"))
      ->groupBy(DB::Raw("DATE_FORMAT(tgl_trans,'%m')"))
      ->get();
    foreach ($data2 as $tmp) {
      $chartData2[$tmp->bulan] = $tmp->total;
    }
    $stock = array();
    $data3 = DB::table('tb_oleh')
      ->select(DB::Raw("kategori , sum(stok) total"))
      ->groupBy("kategori")
      ->get();
    foreach ($data3 as $tmp) {
      $stock[$tmp->kategori] = $tmp->total;
    }
    return view('report.merchandise', array(
      "chart1"=>$chartData1,
      "chart2"=>$chartData2,
      "stock"=>$stock
		));
  }
}
