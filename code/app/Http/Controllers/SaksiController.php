<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use DB;
use Input;
use Validator;
use Redirect;

class SaksiController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	/*public function __construct()
	{
		$this->middleware('guest');
	}*/

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cek = Session::get('username');
		if($cek != ""){
			Session::forget('menu');
	      	Session::set('menu', 'saksi');

	      	$users = DB::table('t_vote')
	      		->get();
			$votetps = DB::table('t_vote')
				->join('ref_tps', 'ref_tps.tpsId' , '=' , 't_vote.id_tps')
				->join('ref_tps', 'ref_tps.tpsId' , '=' , 't_vote.id_tps')
	      		->get();
	      	$paslon = DB::table('t_paslon')
	      		->get();
	      	$calon = DB::table('m_calon')
	      		->where('calon_id', Session::get('calonid'))
	      		->get();
	      	$tps = DB::table('ref_tps')
	      		->limit(10)
	      		->get();
	      	foreach ($tps as $tmptps) {
	      		$namatps = $tmptps->tpsNama;
	      	}
	      	foreach ($calon as $idcalon) {
	      		$namacalon = $idcalon->calon_nama_depan;
	      	}
			$responseView = 'saksi/saksi';
			$responseData = array(
				'users' => $users,
				'paslon' => $paslon,
				'namatps' => $namatps,
				'votetps' => $votetps,
				'namacalon' => $namacalon,
				'baj' => 'sd'
			);

			return view($responseView,$responseData);
		}else{
			return redirect('/');
		}
	}

	public function frm_tambah()
	{
		$cek = Session::get('username');
		if($cek != ""){
			return view('sejarah/tambah_sejarah');
		}else{
			return redirect('/');
		}
	}	

	public function frm_edit(Request $request, $key)
	{
		$cek = Session::get('username');
		if($cek != ""){
			return view('sejarah/edit_sejarah', array('id'=>$key));
		}else{
			return redirect('/');
		}
	}		

	public function tambah_data(Request $request)
	{
		$judul = $request->input('judul');
		$isi = $request->input('isi');
		$image_link = $request->input('image_link');
		$creat = Session::get('username');
		$link = url().'/assets/images/sejarah/'.$image_link;

		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$date1 = date('Y-m-d');	
		$date2 = date('H:i:s');

		if($image_link == ""){
			DB::table('tb_sejarah')->insert(
	            [
	                'judul' 				=> $judul,
	                'isi'			       	=> $isi,
	                'create_by' 			=> $creat,
	                'create_date' 			=> $date,
	                'update_by'   		=> $creat, 
	                'update_date' 		=> $date,
	                'update_by' 		=> $creat,
	                'update_date' 		=> $date,
	                'delete_by' 		=> $creat,
	                'delete_date' 		=> $date

	            ] 
	        );
		}else{
			DB::table('tb_sejarah')->insert(
	            [
	                'judul' 				=> $judul,
	                'isi'			       	=> $isi,
	                'image_link'			=> $link,
	                'create_by' 			=> $creat,
	                'create_date' 			=> $date,
	                'update_by'   		=> $creat, 
	                'update_date' 		=> $date,
	                'update_by' 		=> $creat,
	                'update_date' 		=> $date,
	                'delete_by' 		=> $creat,
	                'delete_date' 		=> $date

	            ] 
	        );
		}
        return view('berita/tambah_berita');
	}
					

    public function save_img(Request $request)
    {
      // getting all of the post data
      $file = array('image' => Input::file('image'));
      // setting up rules
      $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
      // doing the validation, passing post data, rules and the messages
      $validator = Validator::make($file, $rules);
      if ($validator->fails()) {
        // send back to the page with the input data and errors
        return Redirect::to('./assets/images/sejarah/')->withInput()->withErrors($validator);
      }else {
        // checking file is valid.
        if(Input::hasFile('image')) {
			$file 	= Input::file('image');
			$besar = $file->getSize();
			if($file->getSize() <= 2000000) {
				$destinationPath = './assets/images/sejarah'; // upload path
				$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
				$fileName = rand(11111,99999).'.'.$extension; // renameing image
				Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
				// sending back with message
				echo "<script type='text/javascript'>parent.document.getElementById('gambarout').value='".$fileName."';</script>";
				Session::flash('success', 'Upload successfully'); 
				echo "<script type='text/javascript'>parent.document.getElementById('sizepic').innerHTML='".$besar." byte';</script>";
			} else {
				?><script>
					alert("File Anda Terlalu Besar Max 2 Mb");
				</script><?php
			}
		}else{
          // sending back with error message.
          Session::flash('error', 'uploaded file is not valid');
		}		
      }
    }	

	public function updates(Request $request)
	{
		$id = $request->input('id');
		$judul = $request->input('judul');
		$isi = $request->input('isi');
		$image_link = $request->input('image_link');
		$link = url().'/assets/images/sejarah/'.$image_link;

		date_default_timezone_set('Asia/Jakarta');
		$date  = date('Y-m-d H:i:s');
		$creat = Session::get('username');

		if($image_link == ""){
            DB::table('tb_sejarah')->where('id', $id)->update(
                [
					'judul'       	 	=> $judul,
	                'isi' 		   		=> $isi,
                    'update_by'   		=> $creat, 
                    'update_date' 		=> $date
                    ] 
            );
            return redirect()->action('SejarahController@index');
		}else{
            DB::table('tb_sejarah')->where('id', $id)->update(
                [
					'judul'       	 	=> $judul,
	                'isi' 		   		=> $isi,
	                'image_link'	 	=> $link,
                    'update_by'   		=> $creat, 
                    'update_date' 		=> $date
                    ] 
            );
            return redirect()->action('SejarahController@index');
		}	
	}


    public function view_item(Request $request)
    {
        $id = Input::get('id');
        return view('sejarah/lihat_sejarah', array('id'=>$id));
    }	

    public function delete_item(Request $request)
    {
        $id = $request->input('id');
        DB::table('tb_sejarah')->where('id', $id)->delete();
    }	    
}
