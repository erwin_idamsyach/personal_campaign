<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
//Route::get('login', 'LoginController@login');
Route::post('admin/upload_image_oleh', 'merchandiseController@save_img');
Route::get('view_oleh', 'merchandiseController@view_item');

Route::post('admin/tambah_b' , 'BeritaController@tambah_data');
Route::post('admin/tambah_bs', 'BisnisController@tambah_data');

Route::get('admin/dashboard', 'DashboardController@index');
Route::get('dashboard/get/data/grafik','DashboardController@viewDashboardGrafik');
Route::get('dashboard/get/data/grafik2','DashboardController@viewDashboardGrafik2');
Route::get('dashboard/get/data/grafik3','DashboardController@viewDashboardGrafik3');
Route::get('dashboard/get/data/grafik4','DashboardController@viewDashboardGrafik4');
Route::get('dashboard/woy/{jenis}/{type}','DashboardController@viewDashboardWoy');
Route::get('grafik/{jenis}/{type}','DashboardController@viewGrafikDashboard');

Route::post('admin/tambah_a', 'AgendaController@tambah_data');
Route::post('admin/upload_image_a', 'AgendaController@save_img');
Route::get('admin/agenda_data', 'AgendaController@index');
Route::get('admin/agenda_tambah', 'AgendaController@frm_tambah');
Route::get('admin/agenda_edit', 'AgendaController@frm_edit');
Route::get('admin/agenda_edit/{key}', 'AgendaController@view_item');
Route::get('admin/agenda_ref/{key}', 'AgendaController@ref');
Route::get('admin/agenda_refw/{key}', 'AgendaController@refw');
Route::post('admin/view_a', 'AgendaController@view_item');
Route::get('admin/delete_a', 'AgendaController@delete_item');
Route::post('admin/update_a', 'AgendaController@updates');
Route::get('admin/getKota2', 'AgendaController@getKota');
Route::get('admin/getKota', 'AgendaController@getKota');
Route::get('admin/getLurah2', 'AgendaController@getLurah');
Route::get('admin/getCamat', 'AgendaController@getCamat');
Route::get('admin/tanggal', 'AgendaController@tanggal');

Route::post('admin/tambah_b' , 'BeritaController@tambah_data');
Route::post('admin/upload_image_b', 'BeritaController@save_img');
Route::get('admin/berita_data', 'BeritaController@index');
Route::get('admin/berita_tambah', 'BeritaController@frm_tambah');
Route::get('admin/berita_edit/{key}', 'BeritaController@frm_edit');
Route::post('admin/view_b', 'BeritaController@view_item');
Route::post('admin/update_b', 'BeritaController@updates');
Route::get('admin/delete_b', 'BeritaController@delete_item');

Route::get('logout', 'LoginController@logout');
//Route::get('/', 'LoginController@index');
Route::get('admin', 'LoginController@ea');
Route::get('admin/dashboard', 'DashboardController@index_dash');

Route::get('admin/penduduk_data', 'PendudukController@index');

//merchandise
Route::post('admin/upload_image_oleh', 'merchandiseController@save_img');
Route::get('admin/merchandise', 'merchandiseController@index');
Route::get('admin/view_oleh','merchandiseController@view_item');
Route::get('admin/mercandise_tambah', 'merchandiseController@frm_tambah');
Route::get('admin/tambah_oleh', 'merchandiseController@tambah_oleh');
Route::get('admin/mercendise_edit/{key}', 'merchandiseController@frm_edit');
Route::get('admin/update_oleh', 'merchandiseController@updates');
Route::get('admin/deleted_oleh', 'merchandiseController@delete_item');
Route::get('admin/merchadise', 'merchandiseController@index');

//Logistik kirim terima
Route::get('admin/barang_masuk', 'merchandiseController@frm_masuk');
Route::get('admin/tambah_stok', 'merchandiseController@tambah_stok');
Route::get('admin/barang_kirim', 'merchandiseController@frm_kirim');
Route::get('admin/kirim_barang', 'merchandiseController@kirim_brg');
Route::get('admin/log_logistik', 'merchandiseController@jump_log');
Route::get('admin/log_masuk', 'merchandiseController@jump_log2');
Route::get('admin/view_det', 'merchandiseController@view_item1');
Route::get('admin/view_det_trim', 'merchandiseController@view_item2');
Route::get('admin/barange', 'merchandiseController@barange');

//Relawan
Route::post('admin/relawan/save-relawan', 'relawanController@saveRelawan');
Route::post('admin/relawan/save-relawan/front', 'relawanController@saveRelawanFront');
Route::get('admin/relawan_data', 'relawanController@index');
Route::get('admin/view_relawan', 'relawanController@view_item');
Route::get('admin/delete_relawan', 'relawanController@delete_item');
Route::get('admin/relawan_edit/{key}', 'relawanController@frm_edit');
Route::get('admin/update_relawan', 'relawanController@updates');
Route::get('admin/relawan_ref/{key}', 'relawanController@ref');
Route::get('admin/kinerja_data', 'relawanController@tampilKin');
Route::get('admin/view_kinerja', 'relawanController@view_item2');
Route::get('admin/delete_kinerja', 'relawanController@delete_item2');
Route::get('admin/kinerja_tambah', 'relawanController@frm_kin');
Route::get('admin/tambah_kin', 'relawanController@tambah_kin');
Route::get('admin/cek', 'relawanController@viewcek');
Route::get('admin/upload_image_m', 'relawanCoctroller@save_img');
Route::get('admin/inputrel', 'relawanController@inputrel');
Route::get('admin/cek/alamat', 'relawanController@cekalamat');
<<<<<<< HEAD
Route::any('admin/tambah_r', 'relawanController@tambahrr');	
=======
Route::get('admin/tambah_rel', 'relawanController@tambahrr');
Route::get('admin/relawan/add_target/{id}', 'relawanController@add_penugasan');
Route::post('admin/relawan/penugasan/add', 'relawanController@add_tugas');

Route::get('admin/logistik/master', 'LogistikController@view_master');
Route::get('admin/logistik/master/add', 'LogistikController@view_master_add');
Route::post('admin/logistik/master/act_add', 'LogistikController@act_add');
Route::get('admin/logistik/viewDetail', 'LogistikController@view_detail');
Route::get('admin/logistik/add_stock', 'LogistikController@add_stok');
Route::get('admin/logistik/add_stock/action', 'LogistikController@act_add_stok');

Route::get('admin/logistik/request-list', 'LogistikController@request_list');
Route::get('admin/logistik/request-list/get_detail', 'LogistikController@jsonViewDetail');
Route::get('admin/logistik/toggle-state', 'LogistikController@toggle_state');
>>>>>>> a7fca58214d10f862626aba16b19af137a07b1c5

//Personal
Route::get('admin/personal_data', 'PersonalContoller@frm_edit');
Route::get('admin/autobiografi', 'PersonalContoller@index_autobiografi');
Route::post('admin/autobiografi/update/{key}', 'PersonalContoller@update_autobiografi');

Route::get('admin/edit/{type}/{key}', 'PersonalContoller@edit_caleg');
Route::get('act_edit/{type}/{key}', 'PersonalContoller@act_edit_caleg');

/* PERSONALISASI */
Route::get('admin/personalize/background', 'PersonalizeController@index_bg');
Route::post('admin/personalize/background/update/{id}', 'PersonalizeController@update_bg');
Route::get('admin/personalize/background/toggle_activation', 'PersonalizeController@toggle_activation');
Route::get('admin/personalize/background/delete', 'PersonalizeController@delete_bg');

Route::get('admin/personalize/motto', 'PersonalizeController@index_motto');
Route::get('admin/personalize/motto/update/{id}', 'PersonalizeController@update_motto');

Route::get('admin/moto_tampil', 'PersonalContoller@tampil');
Route::get('admin/moto_tambah', 'PersonalContoller@frm_tambah_moto');
Route::get('admin/moto_tok', 'PersonalContoller@tambah_data_moto');
Route::get('admin/delete_moto', 'PersonalContoller@delete_item_moto');
Route::get('admin/moto_refw/{key}', 'PersonalContoller@refw_moto');
Route::get('admin/moto_ref/{key}', 'PersonalContoller@ref_moto');
Route::get('admin/view_moto', 'PersonalContoller@view_item_moto');
Route::get('admin/moto_edit/{key}', 'PersonalContoller@frm_edit_moto');
Route::get('admin/moto_update', 'PersonalContoller@updates_moto');
Route::get('admin/pendidikan_data', 'PersonalContoller@pendidikan');
Route::get('admin/organisasi_data', 'PersonalContoller@organisasi');

Route::get('admin/profil/personal', 'ProfilController@index_personal');
Route::post('save-data-personal/{id}', 'ProfilController@save_data');

Route::get('admin/profil/front-page', 'ProfilController@index_frontpage');
Route::get('admin/profil/front-page/add', 'ProfilController@index_add_frontpage');
Route::post('admin/profil/front-page/add_act', 'ProfilController@act_about');
Route::get('admin/profil/front-page/edit', 'ProfilController@edit_about');
Route::post('admin/profil/front-page/edit_act', 'ProfilController@act_edit');

Route::get('admin/profil/front-page/bg/add', 'ProfilController@index_add_bg');
Route::post('admin/profil/front-page/bg/add_bg', 'ProfilController@add_bg');
Route::get('admin/profil/front-page/bg/edit/{key}', 'ProfilController@index_edit_bg');
Route::post('admin/profil/front-page/bg/edit_bg', 'ProfilController@edit_bg');

Route::get('admin/profil/front-page/edit_campaign', 'ProfilController@edit_campaign');
Route::post('admin/profil/front-page/edit_campaign/edit_act', 'ProfilController@act_edit_campaign');

Route::get('admin/profil/front-page/edit_disclaimer', 'ProfilController@edit_disclaimer');
Route::get('admin/profil/front-page/edit_disclaimer/ect', 'ProfilController@act_edit_disclaimer');

Route::get('admin/profil/news', 'BeritaController@index');
Route::get('admin/profil/news/add', 'BeritaController@frm_tambah');
//Caleg for admin
Route::get('admin/caleg_data', 'CalegController@index');
Route::get('admin/caleg_tambah', 'CalegController@frm_tambah');

//paralax or profil web
Route::get('registrasi-relawan', 'relawanController@pendaftaran');

Route::get('/', 'IndexController@index');
Route::get('prototype', 'IndexController@proto');
Route::get('login', 'LoginController@index');
Route::get('read/{key}', 'IndexController@read');
Route::get('berita/{key}', 'IndexController@view_news_list');
Route::get('tambah_relawan', 'relawanController@tambah_oleh');
Route::get('news/read/{key}', 'IndexController@read_news');

Route::get('profil/{user}', 'IndexController@view_profile');
Route::get('pendaftaran-relawan', 'IndexController@viewPendaftaranrelawan');
/* SOCIAL API */
Route::get('u/google', 'TestController@google');
Route::get('u/google/callback', 'TestController@google_callback');
/* AJAX */
Route::get('ajaxGetKabupaten', 'AjaxController@getKab');
Route::get('ajaxKecamatan', 'AjaxController@getKecamatan');
Route::get('ajaxKelurahan', 'AjaxController@getKelurahan');
Route::get('ajaxGetNamaDesa', 'AjaxController@getNamaDesa');
Route::get('ajaxGetNamaKota', 'AjaxController@getNamaKota');
Route::get('ajaxSaveDukungan/{id}', 'AjaxController@saveDukungan');

Route::get('ajax/toggle-status', 'AjaxController@toggleStatusFrontpage');
Route::get('ajax/toggle-state-berita', 'AjaxController@toggleStateBerita');
Route::get('ajax/toggle-status/bg', 'AjaxController@toggleStatusBackground');

Route::get('ajax/delete-data', 'AjaxController@deleteAbout');
Route::get('ajax/delete-data/bg', 'AjaxController@deleteBackground');

/* MASTER by Erwin */
Route::get('admin/master/kategori', 'MasterController@master_kategori');
Route::get('admin/master/kategori/add', 'MasterController@add_kategori');
Route::get('admin/master/kategori/edit', 'MasterController@edit_kategori');
Route::get('admin/master/kategori/delete', 'MasterController@delete_kategori');

// Cretated By Tri
Route::get('pdf/drh/{id}', 'PdfController@PrintDRH');
Route::get('pdf/pernyataan/{id}', 'PdfController@PrintPernyataan');
Route::get('report/relawan', 'ReportController@statistikRelawan');
Route::get('report/merchandise', 'ReportController@statistikMerchandise');

/* RELAWAN by Erwin */
Route::get('relawan/request-logistik', 'LogistikController@relawan_req');
Route::get('relawan/request-logistik/add', 'LogistikController@relawan_add_request');
Route::post('relawan/request-logistik/act_save', 'LogistikController@relawan_act');